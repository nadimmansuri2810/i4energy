<?php
ini_set("memory_limit",-1);
error_reporting(E_ALL);
ini_set('display_errors', 1);

defined('PAGE_LIMIT') || define ('PAGE_LIMIT', 10);
defined('DEFAULT_TIMEZONE') || define ('DEFAULT_TIMEZONE', 'GMT');
defined('DEFAULT_DATEFORMAT') || define ('DEFAULT_DATEFORMAT', 'dS M, Y h:i a');
defined('ADMIN_USER') || define ('ADMIN_USER', 'admin');
// Define path to application directory
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

//Define application root
defined('APPLICATION_ROOT')
|| define('APPLICATION_ROOT', realpath(dirname(__FILE__) . '/..'));

// Define application environment
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
 
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
realpath(APPLICATION_PATH . '/../library'),
get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// router information for standard and custom routes
require_once 'Zend/Controller/Front.php';
require_once 'Zend/Controller/Router/Route.php';
require_once 'Zend/Rest/Route.php';
$ctrl = Zend_Controller_Front::getInstance();
$router = $ctrl->getRouter();

// Common
$router->addRoute('home', new Zend_Controller_Router_Route('/home',array('module'=>'default', 'controller'=>'index', 'action'=>'home')));
$router->addRoute('login', new Zend_Controller_Router_Route('/login',array('module'=>'default', 'controller'=>'index', 'action'=>'login')));
$router->addRoute('logout', new Zend_Controller_Router_Route('/logout',array('module'=>'default', 'controller'=>'index', 'action'=>'logout')));
$router->addRoute('loadmoresites', new Zend_Controller_Router_Route('/loadmoresites',array('module'=>'default', 'controller'=>'index', 'action'=>'loadmoresites')));
$router->addRoute('resetpassword', new Zend_Controller_Router_Route('/resetpassword',array('module'=>'default', 'controller'=>'index', 'action'=>'resetpassword')));
$router->addRoute('home_search', new Zend_Controller_Router_Route('/home/search',array('module'=>'default', 'controller'=>'index', 'action'=>'search')));
$router->addRoute('home_search_select', new Zend_Controller_Router_Route('/home/searchSelect',array('module'=>'default', 'controller'=>'index', 'action'=>'searchdata')));
$router->addRoute('disk_space', new Zend_Controller_Router_Route('/diskspace',array('module'=>'default', 'controller'=>'index', 'action'=>'diskspace')));

// Sites
$router->addRoute('sites', new Zend_Controller_Router_Route('/sites',array('module'=>'default', 'controller'=>'sites', 'action'=>'index')));
$router->addRoute('sites_view', new Zend_Controller_Router_Route('/sites/:id',array('module'=>'default', 'controller'=>'sites', 'action'=>'view')));
$router->addRoute('sites_add', new Zend_Controller_Router_Route('/sites/add',array('module'=>'default', 'controller'=>'sites', 'action'=>'add')));
$router->addRoute('sites_edit', new Zend_Controller_Router_Route('/sites/add/:id',array('module'=>'default', 'controller'=>'sites', 'action'=>'add')));
$router->addRoute('sites_delete', new Zend_Controller_Router_Route('/sites/delete/:id',array('module'=>'default', 'controller'=>'sites', 'action'=>'delete')));
$router->addRoute('sites_usermapper', new Zend_Controller_Router_Route('/sites/usermapper',array('module'=>'default', 'controller'=>'sites', 'action'=>'usermapper')));
$router->addRoute('sites_addusers', new Zend_Controller_Router_Route('/sites/addusers',array('module'=>'default', 'controller'=>'sites', 'action'=>'addusers')));
$router->addRoute('sites_allocatedusers', new Zend_Controller_Router_Route('/sites/allocatedusers',array('module'=>'default', 'controller'=>'sites', 'action'=>'allocatedusers')));
$router->addRoute('sites_allocatedmeters', new Zend_Controller_Router_Route('/sites/allocatedmeters',array('module'=>'default', 'controller'=>'sites', 'action'=>'allocatedmeters')));


// Meters
$router->addRoute('meters', new Zend_Controller_Router_Route('/meters',array('module'=>'default', 'controller'=>'meters', 'action'=>'index')));
$router->addRoute('meters_view', new Zend_Controller_Router_Route('/meters/:site_id/:id',array('module'=>'default', 'controller'=>'meters', 'action'=>'view')));
$router->addRoute('meters_add', new Zend_Controller_Router_Route('/meters/add/:site_id',array('module'=>'default', 'controller'=>'meters', 'action'=>'add')));
$router->addRoute('meters_edit', new Zend_Controller_Router_Route('/meters/add/:site_id/:id',array('module'=>'default', 'controller'=>'meters', 'action'=>'add')));
$router->addRoute('meters_delete', new Zend_Controller_Router_Route('/meters/delete/:id',array('module'=>'default', 'controller'=>'meters', 'action'=>'delete')));
$router->addRoute('meters_reading', new Zend_Controller_Router_Route('/meters/readings/:site_id/:id',array('module'=>'default', 'controller'=>'meters', 'action'=>'readings')));
$router->addRoute('meters_usermapper', new Zend_Controller_Router_Route('/meters/usermapper',array('module'=>'default', 'controller'=>'meters', 'action'=>'usermapper')));
$router->addRoute('meters_allocatedusers', new Zend_Controller_Router_Route('/meters/allocatedusers',array('module'=>'default', 'controller'=>'meters', 'action'=>'allocatedusers')));
$router->addRoute('meters_addmeterusers', new Zend_Controller_Router_Route('/meters/addmeterusers',array('module'=>'default', 'controller'=>'meters', 'action'=>'addmeterusers')));
$router->addRoute('meters_home_reading', new Zend_Controller_Router_Route('/meters/latest-readings',array('module'=>'default', 'controller'=>'meters', 'action'=>'homereadings')));
$router->addRoute('meters_gethourlychartdatabyday', new Zend_Controller_Router_Route('/meters/gethourlychartdatabyday',array('module'=>'default', 'controller'=>'meters', 'action'=>'gethourlychartdatabyday')));
$router->addRoute('meters_gethourlychartdata', new Zend_Controller_Router_Route('/meters/gethourlychartdata',array('module'=>'default', 'controller'=>'meters', 'action'=>'gethourlychartdata')));
$router->addRoute('meters_getmetercsvdata', new Zend_Controller_Router_Route('/meters/getmetercsvdata',array('module'=>'default', 'controller'=>'meters', 'action'=>'getmetercsvdata')));
$router->addRoute('meters_getmetermonthlydatabyday', new Zend_Controller_Router_Route('/meters/getmetermonthlydatabyday',array('module'=>'default', 'controller'=>'meters', 'action'=>'getmetermonthlydatabyday')));
$router->addRoute('meters_hourrun_reading', new Zend_Controller_Router_Route('/meters/hourrun-readings/:site_id/:id',array('module'=>'default', 'controller'=>'meters', 'action'=>'hourrunreadings')));
$router->addRoute('meters_zoomin', new Zend_Controller_Router_Route('/meters-zoomin/:site_id/:id',array('module'=>'default', 'controller'=>'meters', 'action'=>'zoominboiler')));
$router->addRoute('meters_edit_mac', new Zend_Controller_Router_Route('/meters/edit-mac',array('module'=>'default', 'controller'=>'meters', 'action'=>'editmac')));

//Users
$router->addRoute('users', new Zend_Controller_Router_Route('/users',array('module'=>'default', 'controller'=>'users', 'action'=>'index')));
$router->addRoute('users_view', new Zend_Controller_Router_Route('/users/:user_id/:id',array('module'=>'default', 'controller'=>'users', 'action'=>'view')));
$router->addRoute('users_add', new Zend_Controller_Router_Route('/users/add/:user_id',array('module'=>'default', 'controller'=>'users', 'action'=>'add')));
$router->addRoute('users_delete', new Zend_Controller_Router_Route('/users/delete/:user_id',array('module'=>'default', 'controller'=>'users', 'action'=>'delete')));
$router->addRoute('myprofile', new Zend_Controller_Router_Route('/myprofile',array('module'=>'default', 'controller'=>'users', 'action'=>'myprofile')));
$router->addRoute('users_setnewpassword', new Zend_Controller_Router_Route('/users/setnewpassword',array('module'=>'default', 'controller'=>'users', 'action'=>'setnewpassword')));
$router->addRoute('users_setpassword', new Zend_Controller_Router_Route('/users/setpassword',array('module'=>'default', 'controller'=>'users', 'action'=>'setpassword')));



//Customers
$router->addRoute('customers', new Zend_Controller_Router_Route('/customers',array('module'=>'default', 'controller'=>'customers', 'action'=>'index')));
$router->addRoute('customers_view', new Zend_Controller_Router_Route('/customers/:customer_id/:id',array('module'=>'default', 'controller'=>'customers', 'action'=>'view')));
$router->addRoute('customers_add', new Zend_Controller_Router_Route('/customers/add/:customer_id',array('module'=>'default', 'controller'=>'customers', 'action'=>'add')));
$router->addRoute('customers_delete', new Zend_Controller_Router_Route('/customers/delete/:customer_id',array('module'=>'default', 'controller'=>'customers', 'action'=>'delete')));


//Priviledge  
$router->addRoute('priviledge', new Zend_Controller_Router_Route('/priviledge',array('module'=>'default', 'controller'=>'priviledge', 'action'=>'index')));
$router->addRoute('priviledge_add', new Zend_Controller_Router_Route('/priviledge/add/:id',array('module'=>'default', 'controller'=>'priviledge', 'action'=>'add')));
$router->addRoute('priviledge_delete', new Zend_Controller_Router_Route('/priviledge/delete/:id',array('module'=>'default', 'controller'=>'priviledge', 'action'=>'delete')));

$router->addRoute('priviledge_allocatedpriviledge', new Zend_Controller_Router_Route('/priviledge/allocatedpriviledge',array('module'=>'default', 'controller'=>'priviledge', 'action'=>'allocatedpriviledge')));
$router->addRoute('priviledge_addrole', new Zend_Controller_Router_Route('/priviledge/addrolepriviledge',array('module'=>'default', 'controller'=>'priviledge', 'action'=>'addrolepriviledge')));
$router->addRoute('priviledge_rolemapper', new Zend_Controller_Router_Route('/priviledge/rolemapper',array('module'=>'default', 'controller'=>'priviledge', 'action'=>'rolemapper')));

$router->addRoute('accessdenied', new Zend_Controller_Router_Route('/accessdenied',array('module'=>'default', 'controller'=>'index', 'action'=>'accessdenied')));
$router->addRoute('pinger_raw_data', new Zend_Controller_Router_Route('/pinger-data',array('module'=>'default', 'controller'=>'rawdata', 'action'=>'index')));
$router->addRoute('pinger_raw_data_delete', new Zend_Controller_Router_Route('/pinger-data/delete',array('module'=>'default', 'controller'=>'rawdata', 'action'=>'delete')));

//Metersdisplay
$router->addRoute('metersdisplay', new Zend_Controller_Router_Route('/metersdisplay',array('module'=>'default', 'controller'=>'metersdisplay', 'action'=>'index')));
$router->addRoute('getmeters', new Zend_Controller_Router_Route('/metersdisplay/getmeters',array('module'=>'default', 'controller'=>'metersdisplay', 'action'=>'getmeters')));
$router->addRoute('metersdisplay_add', new Zend_Controller_Router_Route('/metersdisplay/add',array('module'=>'default', 'controller'=>'metersdisplay', 'action'=>'add')));
$router->addRoute('metersdisplay_add', new Zend_Controller_Router_Route('/metersdisplay/add/:id',array('module'=>'default', 'controller'=>'metersdisplay', 'action'=>'add')));

//Meterlinkdisplay
$router->addRoute('display', new Zend_Controller_Router_Route('/display',array('module'=>'default', 'controller'=>'meterlinkdisplay', 'action'=>'index')));
$router->addRoute('displayurl', new Zend_Controller_Router_Route('/display/:url',array('module'=>'default', 'controller'=>'meterlinkdisplay', 'action'=>'index')));


// Create application, bootstrap, and run
$application = new Zend_Application(
		APPLICATION_ENV,
		APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
->run();

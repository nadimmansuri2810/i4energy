<?php
define ('DB_HOST', 'localhost');
define ('DB_USER', 'root');
define ('DB_PASSWORD', 'Savitriya@020');
define ('DB_NAME', 'i4energy');
define ('TABLE_RAW_DATA', 'meter_rawdata');
define ('TABLE_PROCESSED_DATA', 'meter_data');
define ('TABLE_METER', 'meters');


$starttime = time();
echo "\nProcessing script started at: " . @date ('Y-m-d H:i:s');
try
{
	$db_conn = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASSWORD);
}
catch (Exception $e)
{
	die ("Cannot connect to database: " . $e->getMessage());
}

$query = "SELECT DISTINCT macid,moduleid,slotno FROM " . TABLE_RAW_DATA . " WHERE status='NA'";
$stmt = $db_conn->prepare($query);
$stmt->execute();

$rawdata_qry = "SELECT * FROM " . TABLE_RAW_DATA . " WHERE status='NA' AND macid=:macid AND moduleid=:moduleid AND slotno=:slotno ORDER BY rec_date ASC";
$rawdata_stmt = $db_conn->prepare($rawdata_qry);

while ($meter_row = $stmt->fetch (PDO::FETCH_ASSOC))
{
	echo $meter_row['macid'] . ", " . $meter_row['moduleid'] . ", " . $meter_row['slotno'] . "\n";
	
	$module = (int) $meter_row['moduleid'];
	$slot = (int) $meter_row['slotno'];
	
	$rawdata_stmt->execute ( array (
		':macid' => $meter_row['macid'],
		':moduleid' => $meter_row['moduleid'],
		':slotno' => $meter_row['slotno']
	));
	
	$old_data = array();
	while ($raw_data = $rawdata_stmt->fetch (PDO::FETCH_ASSOC))
	{
		$function_name = "process_mod{$module}_slot{$slot}";
		$fallback_function_name = "process_mod{$module}_slot_";
		if (function_exists ($function_name))
		{
			$function_name ($db_conn, $cols, $last_ping_data);
		}
		else if (function_exists ($fallback_function_name))
		{
			$fallback_function_name ($db_conn, $cols, $last_ping_data);
		}
		else
		{
			echo "\nfunction: {$function_name}";
			$module_not_found = true;
		}
		
		$old_data = $raw_data;
	}
	
}

echo "\nProcessing script ended at: " . @date ('Y-m-d H:i:s');
echo "\nTotal processing time: " . (time()-$starttime) . " seconds\n";

// Module / Slot processing functions
function process_mod6_slot3 (&$db_conn, &$cols, &$last_ping_data)
{
	$status = strtolower ($cols['status']);
	if ($status != 'na')
		return;

	$data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
	$data_stmt = $db_conn->prepare($data_qry);

	$macid = $cols['macid'];
	$date = $cols['rec_date']; //getFormattedDate ($cols[1]);

	$module = (int) $cols['moduleid'];
	$slot = (int) $cols['slotno'];

	$ch1 = $cols['ch1'];
	$ch2 = $cols['ch2'];
	$ch3 = $cols['ch3'];
	$ch4 = $cols['ch4'];
	$ch5 = $cols['ch5'];
	$ch6 = $cols['ch6'];
	$ch7 = $cols['ch7'];
	$ch8 = $cols['ch8'];
	$ch9 = $cols['ch9'];
	$ch10 = $cols['ch10'];
	$ch11 = $cols['ch11'];
	$ch12 = $cols['ch12'];
	$ch13 = $cols['ch13'];

	/*echo "\nOLD Data:\n";
	print_r($last_ping_data);
	echo "\nNEW Data:\n";
	print_r($cols);*/
	
	// Channel 1
	$channel = 1;
	$data1 = isOn ($ch1);
	$data = (float) preg_replace ('/p$/i', '', $ch1);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch1');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 2
	$channel = 2;
	$data1 = isOn ($ch2);
	$data = (float) preg_replace ('/p$/i', '', $ch2);
	//echo "\nDATA: $data";
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch2');
	//echo ", DIFF: $data\n";
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 3
	$channel = 3;
	$data1 = isOn ($ch3);
	$data = (float) preg_replace ('/p$/i', '', $ch3);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch3');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 4
	$channel = 4;
	$data1 = isOn ($ch4);
	$data = (float) preg_replace ('/p$/i', '', $ch4);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch4');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 5
	$channel = 5;
	$data1 = isOn ($ch5);
	$data = (float) preg_replace ('/p$/i', '', $ch5);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch5');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 6
	$channel = 6;
	$data1 = isOn ($ch6);
	$data = (float) preg_replace ('/p$/i', '', $ch6);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch6');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 7
	$channel = 7;
	$data1 = isOn ($ch7);
	$data = (float) preg_replace ('/p$/i', '', $ch7);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch7');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 8
	$channel = 8;
	$data1 = isOn ($ch8);
	$data = (float) preg_replace ('/p$/i', '', $ch8);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch8');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 9
	$channel = 9;
	$data1 = isOn ($ch9);
	$data = (float) preg_replace ('/p$/i', '', $ch9);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch9');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

		// Channel 10
	$channel = 10;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch10);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 11
	$channel = 11;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch11);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
		
	// Channel 12
	$channel = 12;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch12);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 13
	$channel = 13;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch13);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
}

function process_mod6_slot35 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod6_slot3 ($db_conn, $cols, $last_ping_data);
}

function process_mod6_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod6_slot3 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot33 (&$db_conn, &$cols, &$last_ping_data)
{
	$status = strtolower ($cols['status']);
	if ($status != 'na')
		return;

	$data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
	$data_stmt = $db_conn->prepare($data_qry);

	$macid = $cols['macid'];
	$date = $cols['rec_date']; //getFormattedDate ($cols[1]);

	$module = (int) $cols['moduleid'];
	$slot = (int) $cols['slotno'];

	$ch1 = $cols['ch1'];
	$ch2 = $cols['ch2'];
	$ch3 = $cols['ch3'];
	$ch4 = $cols['ch4'];
	$ch5 = $cols['ch5'];

	// Channel 1
	$channel = 1;
	$data1 = isOn ($ch1);
	$data = (float) preg_replace ('/p$/i', '', $ch1);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch1');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 2
	$channel = 2;
	$data1 = isOn ($ch2);
	$data = (float) preg_replace ('/p$/i', '', $ch2);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch2');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 3
	$channel = 3;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch3);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 4
	$channel = 4;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch4);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 5
	$channel = 5;
	$data1 = 0;
	$data = preg_replace ('/V/i', '.', $ch5);
	if (strtolower($data) == 'v.')
		$data = 0;
	
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
}

function process_mod97_slot34 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot36 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot37 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot48 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot50 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot52 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot53 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot54 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot65 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod99_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod98_slot0 (&$db_conn, &$cols, &$last_ping_data)
{
	$status = strtolower ($cols[4]);
	if ($status != 'na')
		return;

	$data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
	$data_stmt = $db_conn->prepare($data_qry);

	$macid = $cols[0];
	$date = getFormattedDate ($cols[1]);

	$module = (int) $cols[2];
	$slot = (int) $cols[3];

	$ch1 = $cols[5];
	$ch2 = $cols[6];
	$ch3 = $cols[7];
	$ch4 = $cols[8];
	$ch5 = $cols[9];
	$ch6 = $cols[10];
	$ch7 = $cols[11];
	$ch8 = $cols[12];
	$ch9 = $cols[13];
	$ch10 = $cols[14];
	$ch11 = $cols[15];
	$ch12 = $cols[16];
	$ch13 = $cols[17];
	
	// Channel 1
	$channel = 1;
	$data1 = isOn ($ch1);
	$data = (float) preg_replace ('/p$/i', '', $ch1);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch1');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 2
	$channel = 2;
	$data1 = isOn ($ch2);
	$data = (float) preg_replace ('/p$/i', '', $ch2);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch2');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 3
	$channel = 3;
	$data1 = isOn ($ch3);
	$data = (float) preg_replace ('/p$/i', '', $ch3);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch3');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 4
	$channel = 4;
	$data1 = isOn ($ch4);
	$data = (float) preg_replace ('/p$/i', '', $ch4);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch4');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 5
	$channel = 5;
	$data1 = isOn ($ch5);
	$data = (float) preg_replace ('/p$/i', '', $ch5);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch5');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 6
	$channel = 6;
	$data1 = isOn ($ch6);
	$data = (float) preg_replace ('/p$/i', '', $ch6);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch6');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 7
	$channel = 7;
	$data1 = isOn ($ch7);
	$data = (float) preg_replace ('/p$/i', '', $ch7);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch7');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 8
	$channel = 8;
	$data1 = isOn ($ch8);
	$data = (float) preg_replace ('/p$/i', '', $ch8);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch8');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 9
	$channel = 9;
	$data1 = isOn ($ch9);
	$data = (float) preg_replace ('/p$/i', '', $ch9);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch9');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 10
	$channel = 10;
	$data1 = isOn ($ch10);
	$data = (float) preg_replace ('/p$/i', '', $ch10);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch10');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 11
	$channel = 11;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch11);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 12
	$channel = 12;
	$data1 = 0;
	$data = (float) preg_replace ('/D$/i', '', $ch12);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch12');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 13
	$channel = 13;
	$data1 = 0;
	$data = (float) preg_replace ('/D$/i', '', $ch13);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch13');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
}

function process_mod98_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod98_slot0($db_conn, $cols, $last_ping_data);
}

function isOn ($channel_data)
{
	return preg_match ('/P$/', $channel_data);
}

function getFormattedDate ($raw_date)
{
	global $global_date;

	$raw_date = trim ($raw_date);
	if (strlen ($raw_date) == 12 && preg_match ('/^[0-9]+$/', $raw_date))
	{
		$csv_date = '20' . $raw_date;
		$date = substr ($csv_date, 0, 4) . '-' . substr ($csv_date, 4, 2) . '-' . substr ($csv_date, 6, 2) . ' ' . substr ($csv_date, 8, 2) . ':' . substr ($csv_date, 10, 2) . ':' . substr ($csv_date, 12, 2);
	}
	else
	{
		$date = $global_date;
	}

	return $date;
}

function findPulsesDiffrence ($data, &$last_ping_data, $channel_column)
{
	$channel_column = trim($channel_column);
	if (!preg_match ('/^ch/i', $channel_column))
	{
		return $data;
	}
	
	if ($data == '' || $data == 0)
		return $data;
	
	if (is_array ($last_ping_data) && sizeof ($last_ping_data) > 0 && isset($last_ping_data[$channel_column]) && $last_ping_data[$channel_column] != '')
	{
		$pattern = array ('/p$/i', '/D$/i');
		$replace = array ('', '');
		
		$old_data = (float) preg_replace ($pattern, $replace, $last_ping_data[$channel_column]);
		if (strtolower($old_data) == 'n')
			$old_data = 0;
		
		return $data - $old_data;
	}
	
	return $data;
}



<?php
define ('DB_HOST', 'localhost');
//define ('DB_HOST', 'dev.i4energy.com');
define ('DB_USER', 'root');
define ('DB_PASSWORD', 'Savitriya@020');
//define ('DB_PASSWORD', 'root');
define ('DB_NAME', 'i4energy');
define ('TABLE_RAW_DATA', 'meter_rawdata');
define ('TABLE_PROCESSED_DATA', 'meter_data');
define ('TABLE_METER', 'meters');

define ('ROOT_DIR', dirname (__FILE__));
define ('CSV_ROOT', '/home/energy');
//define ('CSV_ROOT', ROOT_DIR . '/energy');
define ('PROCESSING_ROOT', ROOT_DIR . '/processing');
define ('SUCCESS_ROOT', ROOT_DIR . '/processed_success');
define ('FAIL_ROOT', ROOT_DIR . '/processed_fail');
define ('ARCHIVE_ROOT', ROOT_DIR . '/archive');

$starttime = time();
$last24h = $starttime - 86400;
$next1h = $starttime + 3600;

echo "\nProcessing script started at: " . @date ('Y-m-d H:i:s');
try
{
	$db_conn = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASSWORD);
}
catch (Exception $e)
{
	die ("Cannot connect to database: " . $e->getMessage());
}

if (!is_dir (PROCESSING_ROOT)) @mkdir (PROCESSING_ROOT);
if (!is_dir (SUCCESS_ROOT)) @mkdir (SUCCESS_ROOT);
if (!is_dir (FAIL_ROOT)) @mkdir (FAIL_ROOT);
if (!is_dir (ARCHIVE_ROOT)) @mkdir (ARCHIVE_ROOT);

// Read new CSVs
echo "\nCollecting CSV files to process...";
$files_input = scandir (CSV_ROOT, 0);

// Amit 26 July 2016 start sort files
$files = array();
foreach ($files_input as $index =>$filename) {
  if (preg_match ('/\.csv$/i', $filename)){
    $file = array(
      'filename' => $filename,
      'filedatetime' => split("\.", split("\_",$filename)[1])[0]
    );
    $files[] = $file;
  }
}
usort($files, function($file1, $file2) {
  return $file1['filedatetime'] - $file2['filedatetime'];
});

foreach ( $files as $file )
{
  if (!preg_match ('/\.csv$/i', $file["filename"]))
    continue;

  // move file to processing folder
  if (rename (CSV_ROOT . '/' . $file["filename"], PROCESSING_ROOT . '/' . $file["filename"]) === false)
    echo "\nERROR: Cannot move file " . PROCESSING_ROOT . "/{$file["filename"]}";
}
unset ($files_input);

// Amit 26 July 2016 end

//foreach ( $files_input as $file )
//{
//	if (!preg_match ('/\.csv$/i', $file))
//		continue;
//
//	// move file to processing folder
//	if (rename (CSV_ROOT . '/' . $file, PROCESSING_ROOT . '/' . $file) === false)
//		echo "\nERROR: Cannot move file " . PROCESSING_ROOT . "/{$file}";
//}
//unset ($files_input);

$query = "REPLACE INTO " . TABLE_RAW_DATA . " VALUES (:macid,:date,:rawdate,:module,:slot,:status,:ch1,:ch2,:ch3,:ch4,:ch5,:ch6,:ch7,:ch8,:ch9,:ch10,:ch11,:ch12,:ch13,:ch14,:ch15,:ch16,:filename)";
$rawdata_stmt = $db_conn->prepare($query);

$last_ping_qry = "SELECT * from " . TABLE_RAW_DATA . " WHERE macid=:macid AND moduleid=:moduleid AND slotno=:slotno AND status='NA' AND rec_date<:last_ping_date ORDER BY rec_date DESC LIMIT 1";
$last_ping_stmt = $db_conn->prepare ($last_ping_qry);

echo "\nProcessing CSV files...";
// Proccess CSV files
$i=0;	
$files_processing = scandir (PROCESSING_ROOT, 0);
foreach( $files_processing as $file )
{
	if (!preg_match ('/\.csv$/i', $file))
		continue;
		
	$tmp_name = preg_replace ('/\.csv$/i', '', $file);
	$global_macid = substr ($tmp_name, 0, strpos ($tmp_name, '_'));
	$global_date = substr ($tmp_name, strpos ($tmp_name, '_')+1);
	if (strlen ($global_date) > 12)
		$global_date = substr ($global_date, -12);
	$global_date = '20' . $global_date;
	$global_date = substr ($global_date, 0, 4) . '-' . substr ($global_date, 4, 2) . '-' . substr ($global_date, 6, 2) . ' ' . substr ($global_date, 8, 2) . ':' . substr ($global_date, 10, 2) . ':' . substr ($global_date, 12, 2);

	//echo $global_macid . " => " . $global_date . "\n";exit;
	
	$fh = fopen (PROCESSING_ROOT . '/' . $file, 'r');
	if (false === $fh)
	{
		if (rename (PROCESSING_ROOT . '/' . $file, FAIL_ROOT . '/' . $file) === false)
			echo "\nERROR: Cannot move file to fail directory: " . FAIL_ROOT . "/{$file}";
			
		continue;
	}
	
	$module_not_found = false;
	while ($cols = fgetcsv ($fh))
	{
		if (sizeof ($cols) < 21)
		{
			// Something wrong with this CSV, manual inspection required
			if (rename (PROCESSING_ROOT . '/' . $file, FAIL_ROOT . '/' . $file) === false)
				echo "\nERROR: Cannot move file to fail directory: " . FAIL_ROOT . "/{$file}";
				
			break;
		}
		
		$macid = $cols[0];
		$date = getFormattedDate ($cols[1]);
		
		$module = (int) $cols[2];
		$slot = (int) $cols[3];
		$status = $cols[4];
		
		$ch1 = $cols[5];
		$ch2 = $cols[6];
		$ch3 = $cols[7];
		$ch4 = $cols[8];
		$ch5 = $cols[9];
		$ch6 = $cols[10];
		$ch7 = $cols[11];
		$ch8 = $cols[12];
		$ch9 = $cols[13];
		$ch10 = $cols[14];
		$ch11 = $cols[15];
		$ch12 = $cols[16];
		$ch13 = $cols[17];
		$ch14 = $cols[18];
		$ch15 = $cols[19];
		$ch16 = $cols[20];

		// Insert Raw data
		$rawdata_stmt->execute( array (
			':macid' => $macid
			,':date' => $date
			,':rawdate' => $cols[1]
			,':module' => $module
			,':slot' => $slot
			,':status' => $status
			,':ch1' => $ch1
			,':ch2' => $ch2
			,':ch3' => $ch3
			,':ch4' => $ch4
			,':ch5' => $ch5
			,':ch6' => $ch6
			,':ch7' => $ch7
			,':ch8' => $ch8
			,':ch9' => $ch9
			,':ch10' => $ch10
			,':ch11' => $ch11
			,':ch12' => $ch12
			,':ch13' => $ch13
			,':ch14' => $ch14
			,':ch15' => $ch15
			,':ch16' => $ch16
			,':filename' => $file
		) );
		
		// if status is NA, then only process data further
		if (strtolower ($status) != 'na')
			continue;
			
		// Get last ping
		$last_ping_stmt->execute ( array (
			':macid' => $macid,
			':moduleid' => $module,
			':slotno' => $slot,
			':last_ping_date' => $date
		));
		$last_ping_data = $last_ping_stmt->fetch (PDO::FETCH_ASSOC);
		
		// Process data
		$function_name = "process_mod{$module}_slot{$slot}";
		$fallback_function_name = "process_mod{$module}_slot_";

		if (function_exists ($function_name))
		{
			$function_name ($db_conn, $cols, $last_ping_data);
		}
		else if (function_exists ($fallback_function_name))
		{
			$fallback_function_name ($db_conn, $cols, $last_ping_data);
		}
		else
		{
			echo "\nfunction: {$function_name}";
			$module_not_found = true;
		}
	}
	
	fclose ($fh);
	
	if ($module_not_found)
	{
		// We do not have any function to process this module and slot, so put it in failed files
		// so that we can look in and create a processing function
		if (rename (PROCESSING_ROOT . '/' . $file, FAIL_ROOT . '/' . $file) === false)
			echo "\nERROR: Cannot move file to success directory: " . FAIL_ROOT . "/{$file}";
	}
	else
	{
		// All good; move this file to successfully processed
		if (rename (PROCESSING_ROOT . '/' . $file, SUCCESS_ROOT . '/' . $file) === false)
			echo "\nERROR: Cannot move file to success directory: " . SUCCESS_ROOT . "/{$file}";
	}
		
	/*$i++;
	if ($i>20)
		break;*/
}
unset ($files_processing);

echo "\nUpdating meter's latest status...";
$query = "SELECT id,mac,moduleno,slotno,channelno from " . TABLE_METER;
$stmt = $db_conn->prepare($query);
$stmt->execute();
while ($meter_row = $stmt->fetch (PDO::FETCH_ASSOC))
{
	// find processed data for this meter
	$stmt_data = $db_conn->prepare("SELECT data,rec_date FROM " . TABLE_PROCESSED_DATA . " WHERE macid=:macid AND moduleid=:moduleid AND slotno=:slotno AND channelno=:channelno ORDER BY rec_date DESC LIMIT 1");
	$stmt_data->execute ( array (
		':macid' => $meter_row['mac']
		,':moduleid' => $meter_row['moduleno']
		,':slotno' => $meter_row['slotno']
		,':channelno' => $meter_row['channelno']
	));

	$meter_data_row = $stmt_data->fetch (PDO::FETCH_ASSOC);
	if (isset ($meter_data_row['rec_date']) && $meter_data_row['rec_date'] != '')
	{
		$stmt_update = $db_conn->prepare("UPDATE " . TABLE_METER . " SET latestreading=:data, latestreadingtime=:rec_date WHERE id=:meter_id");
		$stmt_update->execute (array (
			':data' => $meter_data_row['data']
			,':rec_date' => $meter_data_row['rec_date']
			,':meter_id' => $meter_row['id']
		));
	}
}

echo "\nProcessing script ended at: " . @date ('Y-m-d H:i:s');
echo "\nTotal processing time: " . (time()-$starttime) . " seconds\n";

// Module / Slot processing functions
function process_mod6_slot3 (&$db_conn, &$cols, &$last_ping_data)
{
	$status = strtolower ($cols[4]);
	if ($status != 'na')
		return;
		
	$data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
	$data_stmt = $db_conn->prepare($data_qry);

	$macid = $cols[0];
	$date = getFormattedDate ($cols[1]);

	$module = (int) $cols[2];
	$slot = (int) $cols[3];
	
	$ch1 = $cols[5];
	$ch2 = $cols[6];
	$ch3 = $cols[7];
	$ch4 = $cols[8];
	$ch5 = $cols[9];
	$ch6 = $cols[10];
	$ch7 = $cols[11];
	$ch8 = $cols[12];
	$ch9 = $cols[13];
	$ch10 = $cols[14];
	$ch11 = $cols[15];
	$ch12 = $cols[16];
	$ch13 = $cols[17];
	$ch14 = $cols[18];
	$ch15 = $cols[19];
	
	// Channel 1
	$channel = 1;
	$data1 = isOn ($ch1);
	$data = (float) preg_replace ('/p$/i', '', $ch1);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch1');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 2
	$channel = 2;
	$data1 = isOn ($ch2);
	$data = (float) preg_replace ('/p$/i', '', $ch2);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch2');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 3
	$channel = 3;
	$data1 = isOn ($ch3);
	$data = (float) preg_replace ('/p$/i', '', $ch3);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch3');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 4
	$channel = 4;
	$data1 = isOn ($ch4);
	$data = (float) preg_replace ('/p$/i', '', $ch4);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch4');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 5
	$channel = 5;
	$data1 = isOn ($ch5);
	$data = (float) preg_replace ('/p$/i', '', $ch5);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch5');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 6
	$channel = 6;
	$data1 = isOn ($ch6);
	$data = (float) preg_replace ('/p$/i', '', $ch6);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch6');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 7
	$channel = 7;
	$data1 = isOn ($ch7);
	$data = (float) preg_replace ('/p$/i', '', $ch7);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch7');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 8
	$channel = 8;
	$data1 = isOn ($ch8);
	$data = (float) preg_replace ('/p$/i', '', $ch8);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch8');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 9
	$channel = 9;
	$data1 = isOn ($ch9);
	$data = (float) preg_replace ('/p$/i', '', $ch9);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch9');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 10
	$channel = 10;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch10);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 11
	$channel = 11;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch11);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
		
	// Channel 12
	$channel = 12;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch12);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// Channel 13
	$channel = 13;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch13);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));	
	
	// Channel 14
	$channel = 14;
	$data1 = 0;
	$data = 0;
	if ($ch14 != 'N' && $ch14 != 'NA')
	{
		$data = preg_replace ('/V/i', '.', $ch14);
		if (strtolower($data) == 'v.')
			$data = 0;
	}
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 15
	$channel = 15;
	$data1 = 0;
	$data = 0;
	if ($ch15 != 'N' && $ch15 != 'NA')
	{
		$data = preg_replace ('/A/i', '.', $ch15);
		if (strtolower($data) == 'a.')
			$data = 0;
	}
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
}

function process_mod6_slot35 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod6_slot3 ($db_conn, $cols, $last_ping_data);
}

function process_mod6_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod6_slot3 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot33 (&$db_conn, &$cols, &$last_ping_data)
{
	$status = strtolower ($cols[4]);
	if ($status != 'na')
		return;
		
	$data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
	$data_stmt = $db_conn->prepare($data_qry);

	$macid = $cols[0];
	$date = getFormattedDate ($cols[1]);

	$module = (int) $cols[2];
	$slot = (int) $cols[3];
	
	$ch1 = $cols[5];
	$ch2 = $cols[6];
	$ch3 = $cols[7];
	$ch4 = $cols[8];
	$ch5 = $cols[9];
	
	// Channel 1
	$channel = 1;
	$data1 = isOn ($ch1);
	$data = (float) preg_replace ('/p$/i', '', $ch1);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch1');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 2
	$channel = 2;
	$data1 = isOn ($ch2);
	$data = (float) preg_replace ('/p$/i', '', $ch2);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch2');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 3
	$channel = 3;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch3);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 4
	$channel = 4;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch4);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 5
	$channel = 5;
	$data1 = 0;
	$data = preg_replace ('/V/i', '.', $ch5);
	if (strtolower($data) == 'v.')
		$data = 0;
	
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
}

function process_mod97_slot34 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot36 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot37 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot48 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot50 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot52 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot53 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot54 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot65 (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod97_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod99_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
}

function process_mod98_slot0 (&$db_conn, &$cols, &$last_ping_data)
{
	$status = strtolower ($cols[4]);
	if ($status != 'na')
		return;
	
	$data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
	$data_stmt = $db_conn->prepare($data_qry);
	
	$macid = $cols[0];
	$date = getFormattedDate ($cols[1]);
	
	$module = (int) $cols[2];
	$slot = (int) $cols[3];
	
	$ch1 = $cols[5];
	$ch2 = $cols[6];
	$ch3 = $cols[7];
	$ch4 = $cols[8];
	$ch5 = $cols[9];
	$ch6 = $cols[10];
	$ch7 = $cols[11];
	$ch8 = $cols[12];
	$ch9 = $cols[13];
	$ch10 = $cols[14];
	$ch11 = $cols[15];
	$ch12 = $cols[16];
	$ch13 = $cols[17];
	
	// Channel 1
	$channel = 1;
	$data1 = isOn ($ch1);
	$data = (float) preg_replace ('/p$/i', '', $ch1);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch1');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 2
	$channel = 2;
	$data1 = isOn ($ch2);
	$data = (float) preg_replace ('/p$/i', '', $ch2);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch2');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 3
	$channel = 3;
	$data1 = isOn ($ch3);
	$data = (float) preg_replace ('/p$/i', '', $ch3);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch3');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 4
	$channel = 4;
	$data1 = isOn ($ch4);
	$data = (float) preg_replace ('/p$/i', '', $ch4);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch4');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 5
	$channel = 5;
	$data1 = isOn ($ch5);
	$data = (float) preg_replace ('/p$/i', '', $ch5);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch5');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 6
	$channel = 6;
	$data1 = isOn ($ch6);
	$data = (float) preg_replace ('/p$/i', '', $ch6);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch6');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 7
	$channel = 7;
	$data1 = isOn ($ch7);
	$data = (float) preg_replace ('/p$/i', '', $ch7);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch7');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 8
	$channel = 8;
	$data1 = isOn ($ch8);
	$data = (float) preg_replace ('/p$/i', '', $ch8);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch8');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 9
	$channel = 9;
	$data1 = isOn ($ch9);
	$data = (float) preg_replace ('/p$/i', '', $ch9);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch9');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 10
	$channel = 10;
	$data1 = isOn ($ch10);
	$data = (float) preg_replace ('/p$/i', '', $ch10);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch10');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 11
	$channel = 11;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch11);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 12
	$channel = 12;
	$data1 = 0;
	$data = (float) preg_replace ('/D$/i', '', $ch12);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch12');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 13
	$channel = 13;
	$data1 = 0;
	$data = (float) preg_replace ('/D$/i', '', $ch13);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch13');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
}

function process_mod98_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	process_mod98_slot0($db_conn, $cols, $last_ping_data);
}

function process_mod7_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	$status = strtolower ($cols[4]);
	if ($status != 'na')
		return;
	
	$data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
	$data_stmt = $db_conn->prepare($data_qry);
	
	$macid = $cols[0];
	$date = getFormattedDate ($cols[1]);
	
	$module = (int) $cols[2];
	$slot = (int) $cols[3];
	
	$ch1 = $cols[5];
	$ch2 = $cols[6];
	$ch3 = $cols[7];
	$ch4 = $cols[8];
	$ch5 = $cols[9];
	$ch6 = $cols[10];
	$ch7 = $cols[11];
	$ch8 = $cols[12];
	$ch9 = $cols[13];
	
	// Channel 1
	$channel = 1;
	$data1 = !isOn ($ch1);
	$data = (float) preg_replace ('/p$/i', '', $ch1);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch1');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 2
	$channel = 2;
	$data1 = !isOn ($ch2);
	$data = (float) preg_replace ('/p$/i', '', $ch2);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch2');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 3
	$channel = 3;
	$data1 = !isOn ($ch3);
	$data = (float) preg_replace ('/p$/i', '', $ch3);
	$data = findPulsesDiffrence ($data, $last_ping_data, 'ch3');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 4
	$channel = 4;
	$data1 = 0;
	$data = (float) preg_replace ('/h$/i', '', $ch4);
	//$data = findPulsesDiffrence ($data, $last_ping_data, 'ch4');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 5
	$channel = 5;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch5);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	
	// Channel 6
	$channel = 6;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch6);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

	// We have two configuration of Module 7. Old configuration has Ch7 - Voltage and Ch8 and Ch9 has nothing useful
	// In new configuration, we have Ch7 as Temprature, Ch8 and Ch9 are Voltage
	// Identify channel 7, 8 and 9 data and based on that process it according to new or old configuration
	if (preg_match ('/\d+K\d+/i', $ch7)) {
		// Channel 7 - Temprature
		$channel = 7;
		$data1 = 0;
		$data = preg_replace ('/K/i', '.', $ch7);
		if (strtolower($data) == 'n.')
			$data = 0;
		else
			$data = $data-273.15;
		$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	}
	else {
		// Channel 7 - Voltage
		$channel = 7;
		$data1 = 0;
		$data = preg_replace ('/V/i', '.', $ch7);
		if (strtolower($data) == 'v.')
			$data = 0;
		$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	}
	
	if (preg_match ('/\d+V\d+/i', $ch8)) {
		// Channel 8 - Voltage
		$channel = 8;
		$data1 = 0;
		$data = preg_replace ('/V/i', '.', $ch8);
		if (strtolower($data) == 'v.')
			$data = 0;
		$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	}
	
	if (preg_match ('/\d+V\d+/i', $ch9)) {
		// Channel 9 - Voltage
		$channel = 9;
		$data1 = 0;
		$data = preg_replace ('/V/i', '.', $ch9);
		if (strtolower($data) == 'v.')
			$data = 0;
		$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	}
}

function process_mod8_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
	$status = strtolower ($cols[4]);
	if ($status != 'na')
		return;
	
	$data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
	$data_stmt = $db_conn->prepare($data_qry);
	
	$macid = $cols[0];
	$date = getFormattedDate ($cols[1]);
	
	$module = (int) $cols[2];
	$slot = (int) $cols[3];
	
	$ch1 = $cols[5];
	$ch2 = $cols[6];
	
	// Channel 1
	$channel = 1;
	$data1 = 0;
	$data = (float) preg_replace ('/h$/i', '', $ch1);
	//$data = findPulsesDiffrence ($data, $last_ping_data, 'ch4');
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
	
	// Channel 2
	$channel = 2;
	$data1 = 0;
	$data = preg_replace ('/K/i', '.', $ch2);
	if (strtolower($data) == 'n.')
		$data = 0;
	else
		$data = $data-273.15;
	$data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
}


// Amit July 18 2016 Start

function process_mod9_slot_ (&$db_conn, &$cols, &$last_ping_data)
{
  $status = strtolower ($cols[4]);

  if ($status != 'na')
    return;

  $data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";

  $data_stmt = $db_conn->prepare($data_qry);

  $macid = $cols[0];
  $date = getFormattedDate ($cols[1]);

  $module = (int) $cols[2];
  $slot = (int) $cols[3];

  $ch1 = $cols[5];
  $ch2 = $cols[6];
  $ch3 = $cols[7];
  $ch4 = $cols[8];
  $ch5 = $cols[9];
  $ch6 = $cols[10];
  $ch7 = $cols[11];
  $ch8 = $cols[12];
  $ch9 = $cols[13];
  $ch10 = $cols[14];
  $ch11 = $cols[15];
  $ch12 = $cols[16];
  $ch13 = $cols[17];
  $ch14 = $cols[18];
  $ch15 = $cols[19];
  $ch16 = $cols[20];

  // Channel 1
  $channel = 1;
  $data1 = !isOn ($ch1);
  $data = (float) preg_replace ('/p$/i', '', $ch1);
  $data = findPulsesDiffrence ($data, $last_ping_data, 'ch1');
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  // Channel 2
  $channel = 2;
  $data1 = isOn ($ch2);
  $data = (float) preg_replace ('/p$/i', '', $ch2);
  $data = findPulsesDiffrence ($data, $last_ping_data, 'ch2');
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  // Channel 3
  $channel = 3;
  $data1 = isOn ($ch3);
  $data = (float) preg_replace ('/p$/i', '', $ch3);
  $data = findPulsesDiffrence ($data, $last_ping_data, 'ch3');
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  // Channel 4
  $channel = 4;
  $data1 = isOn ($ch4);
  $data = (float) preg_replace ('/p$/i', '', $ch4);
  $data = findPulsesDiffrence ($data, $last_ping_data, 'ch4');
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  // Channel 5
  $channel = 5;
  $data1 = isOn ($ch5);
  $data = (float) preg_replace ('/p$/i', '', $ch5);
  $data = findPulsesDiffrence ($data, $last_ping_data, 'ch5');
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  // Channel 6
  $channel = 6;
  $data1 = isOn ($ch6);
  $data = (float) preg_replace ('/p$/i', '', $ch6);
  $data = findPulsesDiffrence ($data, $last_ping_data, 'ch6');
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  // Channel 7
  $channel = 7;
  $data1 = mod_9_isOn ($ch7);
  $data = (float) preg_replace ('/t/i', '.', $ch7);
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


  // Channel 8
  $channel = 8;
  $data1 = mod_9_isOn ($ch8);
  $data = (float) preg_replace ('/t/i', '.', $ch8);
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


  // Channel 9
  $channel = 9;
  $data1 = mod_9_isOn ($ch9);
  $data = (float) preg_replace ('/t/i', '.', $ch9);
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  // Channel 10 - Temprature
  $channel = 10;
  $data1 = 0;
  $data = preg_replace ('/K/i', '.', $ch10);
  if (strtolower($data) == 'n.')
    $data = 0;
  else
    $data = $data-273.15;
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


  // Channel 11 - Temprature
  $channel = 11;
  $data1 = 0;
  $data = preg_replace ('/K/i', '.', $ch11);
  if (strtolower($data) == 'n.')
    $data = 0;
  else
    $data = $data-273.15;
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


  // Channel 12 - Temprature
  $channel = 12;
  $data1 = 0;
  $data = preg_replace ('/K/i', '.', $ch12);
  if (strtolower($data) == 'n.')
    $data = 0;
  else
    $data = $data-273.15;
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  // Channel 13 - Temprature
  $channel = 13;
  $data1 = 0;
  $data = preg_replace ('/K/i', '.', $ch13);
  if (strtolower($data) == 'n.')
    $data = 0;
  else
    $data = $data-273.15;
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  // Channel 14 - Voltage
  $channel = 14;
  $data1 = 0;
  $data = (float) preg_replace ('/Z/i', '.', $ch14);
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


  if (preg_match ('/\d+V\d+/i', $ch15)) {
    // Channel 15 - Voltage
    $channel = 15;
    $data1 = 0;
    $data = preg_replace ('/V/i', '.', $ch15);
    if (strtolower($data) == 'v.')
      $data = 0;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
  }

  // Channel 16
  $channel = 16;
  $data1 = 0;
  $data = (float) preg_replace ('/h$/i', '', $ch16);
  $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

}
// Amit July 18 2016 End


function isOn ($channel_data)
{
	return preg_match ('/P$/', $channel_data);
}

function getFormattedDate ($raw_date)
{
	global $global_date, $last24h, $next1h;
	
	$raw_date = trim ($raw_date);
	if (strlen ($raw_date) == 12 && preg_match ('/^[0-9]+$/', $raw_date))
	{
		$csv_date = '20' . $raw_date;
		$date = substr ($csv_date, 0, 4) . '-' . substr ($csv_date, 4, 2) . '-' . substr ($csv_date, 6, 2) . ' ' . substr ($csv_date, 8, 2) . ':' . substr ($csv_date, 10, 2) . ':' . substr ($csv_date, 12, 2);
		
		$dateStamp = @strtotime ($date);
		if ($dateStamp > $next1h || $dateStamp < $last24h)
			$date = $global_date;
	}
	else
	{
		$date = $global_date;
	}
	
	return $date;
}

function findPulsesDiffrence ($data, &$last_ping_data, $channel_column)
{
	$channel_column = trim($channel_column);
	if (!preg_match ('/^ch/i', $channel_column))
	{
		return $data;
	}
	
	if ($data == '' || $data == 0)
		return $data;
	
	if (is_array ($last_ping_data) && sizeof ($last_ping_data) > 0 && isset($last_ping_data[$channel_column]) && $last_ping_data[$channel_column] != '')
	{
		$pattern = array ('/p$/i', '/D$/i');
		$replace = array ('', '');
		
		$old_data = (float) preg_replace ($pattern, $replace, $last_ping_data[$channel_column]);
		if (strtolower($old_data) == 'n')
			$old_data = 0;
	
		$diff_value = $data - $old_data;
		if ($diff_value < 0) {
			return $data;
		}
		return $diff_value;	
	}
	
	return $data;
}

// Amit 19 July 2016 start
function mod_9_isOn ($channel_data)
{
  return preg_match ('/T/', $channel_data);
}
// Amit 19 July 2016 end


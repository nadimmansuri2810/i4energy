<?php
define ('DB_HOST', 'localhost');
define ('DB_USER', 'root');
define ('DB_PASSWORD', 'Savitriya@020');
define ('DB_NAME', 'i4energy');
define ('TABLE_RAW_DATA', 'meter_rawdata');
define ('TABLE_PROCESSED_DATA', 'meter_data');
define ('TABLE_METER', 'meters');

$starttime = time();
echo "\nProcessing script started at: " . @date ('Y-m-d H:i:s');
try
{
	$db_conn = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASSWORD);
}
catch (Exception $e)
{
	die ("Cannot connect to database: " . $e->getMessage());
}

$raw_update = "UPDATE " . TABLE_RAW_DATA . " SET rec_date=:new_date WHERE rec_date=:old_date";
$row_update_stmt = $db_conn->prepare($raw_update);

$data_update = "UPDATE " . TABLE_PROCESSED_DATA . " SET rec_date=:new_date WHERE rec_date=:old_date";
$data_update_stmt = $db_conn->prepare($data_update);


$future_date = "SELECT * FROM " . TABLE_RAW_DATA . " WHERE rec_date>'" . date ('Y-m-d H:i:s') . "'";
echo $future_date . "\n";
$future_date_stmt = $db_conn->prepare($future_date);
$future_date_stmt->execute();
$rows = $future_date_stmt->fetchAll (PDO::FETCH_ASSOC);
foreach ($rows as $future_date_row)
{
	$filename = $future_date_row['filename'];
	$filename = preg_replace ('/[^0-9]/', '', $filename);
	
	if (strlen ($filename) > 12)
		$filename = substr ($filename, -12);
	$filename = '20' . $filename;
	
	$global_date = substr ($filename, 0, 4) . '-' . substr ($filename, 4, 2) . '-' . substr ($filename, 6, 2) . ' ' . substr ($filename, 8, 2) . ':' . substr ($filename, 10, 2) . ':' . substr ($filename, 12, 2);

	echo "UPDATE " . TABLE_RAW_DATA . " SET rec_date='{$global_date}' WHERE rec_date='{$future_date_row['rec_date']}'\n";
	echo "UPDATE " . TABLE_PROCESSED_DATA . " SET rec_date='{$global_date}' WHERE rec_date='{$future_date_row['rec_date']}'\n";
//	mysql_query ("UPDATE " . TABLE_RAW_DATA . " SET rec_date='{$global_date}' WHERE rec_date='{$future_date_row['rec_date']}'");	
//	mysql_query ("UPDATE " . TABLE_PROCESSED_DATA . " SET rec_date='{$global_date}' WHERE rec_date='{$future_date_row['rec_date']}'");	
	$row_update_stmt->execute (array (':new_date' => $global_date, ':old_date' => $future_date_row['rec_date']));
	$data_update_stmt->execute (array (':new_date' => $global_date, ':old_date' => $future_date_row['rec_date']));
}
unset ($rows);

$past_date = "SELECT * FROM " . TABLE_RAW_DATA . " WHERE rec_date='0000-00-00 00:00:00'";
echo $past_date;exit;
$past_date_stmt = $db_conn->prepare($past_date);
$past_date_stmt->execute();
while ($past_date_row = $past_date_stmt->fetch (PDO::FETCH_ASSOC))
{
	$filename = $past_date_row['filename'];
	$filename = preg_replace ('/[^0-9]/', '', $filename);
	
	if (strlen ($filename) > 12)
		$filename = substr ($filename, -12);
	$filename = '20' . $filename;
	
	$global_date = substr ($filename, 0, 4) . '-' . substr ($filename, 4, 2) . '-' . substr ($filename, 6, 2) . ' ' . substr ($filename, 8, 2) . ':' . substr ($filename, 10, 2) . ':' . substr ($filename, 12, 2);
	
	$row_update_stmt->execute (array (':new_date' => $global_date, ':old_date' => $past_date_row['rec_date']));
	$data_update_stmt->execute (array (':new_date' => $global_date, ':old_date' => $past_date_row['rec_date']));
}

echo "\nProcessing script ended at: " . @date ('Y-m-d H:i:s');
echo "\nTotal processing time: " . (time()-$starttime) . " seconds\n";



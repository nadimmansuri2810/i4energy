<?php
require_once dirname(__FILE__) . '/zendload.php';
$meter_obj=new I4energy_Model_Meters();
$meterlist=$meter_obj->listMeters(array(),'id','ASC');

$layout=Zend_Layout::getMvcInstance();
$layout->disableLayout();

if(isset($meterlist->totalrecords) && $meterlist->totalrecords>0){
	foreach ($meterlist->rows as $meter){
		
		$meter_id=$meter['id'];
		$meter_pingerate=$meter['pingrate'];
		
				
		if ($meter_pingerate == 0 || $meter_pingerate == '' || $meter_pingerate = null){
			$meter_pingerate=1*60*60;
		}

		$last_minute=gmdate('Y-m-d H:i:s',time()-$meter_pingerate);
// 		$last_minute='2014-11-01 06:54:00';
		
		$meter_alertObj = new I4energy_Model_MeterAlerts();
		$meter_alertObj->init ($meter_id);

		$common_where = "macid='{$meter['mac']}' AND moduleid={$meter['moduleno']} AND slotno={$meter['slotno']} AND channelno={$meter['channelno']} ";


		$alert_min_limit = $meter_alertObj->getAlertLimitMin();
		$alert_min_limit_time = $meter_alertObj->getAlertTimeLimitMin();
		
		$alert_max_limit = $meter_alertObj->getAlertLimitMax();
		$alert_max_limit_time = $meter_alertObj->getAlertTimeLimitMax();
		
		
		if ((isset ($alert_min_limit) && $alert_min_limit != null && $alert_min_limit != '') ||(isset ($alert_max_limit) && $alert_max_limit != null && $alert_max_limit != ''))
		{	
				$meterObj=new I4energy_Model_Meters();
				$meterObj->init($meter['id']);
				require_once dirname(__FILE__) . '/../application/controllers/MetersController.php';
				$raw_reading = MetersController::getMeterNowUsage($meterObj);

				$meter_attrs = new I4energy_Model_MeterAttributes();
				$meter_attrs->init ($meterObj->getId());

				$convert_unit_attrs = array();
				$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();

				$latest_reading = I4energy_Model_Meters::convertToUnit ($raw_reading, $meterObj->getType(), $meterObj->getUnit(), $meterObj->getConversationFactor(), $convert_unit_attrs);
					
				if(isset ($alert_min_limit) && $alert_min_limit != null && $alert_min_limit != '')
				{
						$alerttype='min';
						
						if ($alert_min_limit > $latest_reading)
						{
							$alertdatetime=@gmdate('Y-m-d H:i:s');
							$minalertfilter=array();
							$minalertfilter[0]['field'] = 'meter_id';
							$minalertfilter[0]['operation'] = '=';
							$minalertfilter[0]['value'] = $meter_id;
								
							$minalertfilter[1]['field'] = 'alert_type';
							$minalertfilter[1]['operation'] = '=';
							$minalertfilter[1]['value'] = 'min';
								
							$minalertfilter[2]['field'] = 'alert_time';
							$minalertfilter[2]['operation'] = 'gte';
							$minalertfilter[2]['value'] = @gmdate ('Y-m-d H:i:s', @strtotime ('-720 minutes'));
								
							$min_meter_pending_alert=new I4energy_Model_MeterPendingAlert();
							$minalertlist=$min_meter_pending_alert->listMeterPendingAlert($minalertfilter);
								
	//	NO ALERT SET IN LAST 12 HOURS
								if ($minalertlist->totalrecords == 0)
								{
									$meter_pending_alert=new I4energy_Model_MeterPendingAlert();
									$meter_pending_alert->setAlert_datetime($alertdatetime);
									$meter_pending_alert->setAlert_status(0);
									//$meter_pending_alert->setAlert_notify_datetime();
									$meter_pending_alert->setMeter_id($meter_id);
									$meter_pending_alert->setLast_reading_data($meter['latestreading']);
									$meter_pending_alert->setLast_reading_datetime($meter['latestreadingtime']);
									$meter_pending_alert->setAlert_type($alerttype);
										
									$meter_pending_alert->save(true);
								}
							}
					}
				
				
					if (isset ($alert_max_limit) && $alert_max_limit != null && $alert_max_limit != '')
					{
						$alerttype='max';
						if ($alert_max_limit < $latest_reading)
						{
							$alertdatetime=gmdate('Y-m-d H:i:s');
							$maxalertfilter=array();
							$maxalertfilter[0]['field'] = 'meter_id';
							$maxalertfilter[0]['operation'] = '=';
							$maxalertfilter[0]['value'] = $meter_id;
								
							$maxalertfilter[1]['field'] = 'alert_type';
							$maxalertfilter[1]['operation'] = '=';
							$maxalertfilter[1]['value'] = 'max';
								
							$maxalertfilter[2]['field'] = 'alert_time';
							$maxalertfilter[2]['operation'] = 'gte';
							$maxalertfilter[2]['value'] = @gmdate ('Y-m-d H:i:s', @strtotime ('-720 minutes'));
								
								
							$maxalert_meter_pending_alert=new I4energy_Model_MeterPendingAlert();
							$maxalertlist=$maxalert_meter_pending_alert->listMeterPendingAlert($maxalertfilter);
		//  NO ALERT SET IN LAST 12 HOURS
								if ( $maxalertlist->totalrecords == 0)
								{
									$meter_pending_alert=new I4energy_Model_MeterPendingAlert();
									$meter_pending_alert->setAlert_datetime($alertdatetime);
									$meter_pending_alert->setAlert_status(0);
									//$meter_pending_alert->setAlert_notify_datetime();
									$meter_pending_alert->setMeter_id($meter_id);
									$meter_pending_alert->setLast_reading_data($meter['latestreading']);
									$meter_pending_alert->setLast_reading_datetime($meter['latestreadingtime']);
									$meter_pending_alert->setAlert_type($alerttype);
								
									$meter_pending_alert->save(true);
								}		
							}
						}
							
		}
		
		
		$alert_ping_limit = $meter_alertObj->getAlertLimitPing();
		if ( !(isset ($alert_ping_limit) && $alert_ping_limit != null && $alert_ping_limit != '') )
			$alert_ping_limit = 60;
		
		if (isset ($alert_ping_limit) && $alert_ping_limit != null && $alert_ping_limit != '')
		{
				
			$alerttype='noping';
			$meter_data = new I4energy_Model_MeterData();
			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('macid'=>'macid','rec_date' => 'rec_date','moduleid'=>'moduleid','slotno'=>'slotno','channelno'=>'channelno','data' => 'data' ));
			$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime ('-' . $alert_ping_limit . ' minutes')) );
			$select->where($common_where);
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

			$alert = false;
			if (count($rows) > 0)
			{
				$alert=true;
				$alerttype='noping';
			}
				
			if ($alert )
			{
				$alertdatetime=gmdate('Y-m-d H:i:s');
		
				$nopingalertfilter=array();
				$nopingalertfilter[0]['field'] = 'meter_id';
				$nopingalertfilter[0]['operation'] = '=';
				$nopingalertfilter[0]['value'] = $meter_id;
					
				$nopingalertfilter[1]['field'] = 'alert_type';
				$nopingalertfilter[1]['operation'] = '=';
				$nopingalertfilter[1]['value'] = 'noping';
					
				$nopingalertfilter[2]['field'] = 'alert_time';
				$nopinglertfilter[2]['operation'] = 'gte';
				$nopingalertfilter[2]['value'] = @gmdate ('Y-m-d H:i:s', @strtotime ('-720 minutes'));
					
					
				$noping_meter_pending_alert=new I4energy_Model_MeterPendingAlert();
				$nopingalertlist=$noping_meter_pending_alert->listMeterPendingAlert($nopingalertfilter);
//						NO ALERT SET IN LAST 12 HOURS
				if ($nopingalertlist->totalrecords == 0)
				{	
					$meter_pending_alert=new I4energy_Model_MeterPendingAlert();
					$meter_pending_alert->setAlert_datetime($alertdatetime);
					$meter_pending_alert->setAlert_status(0);
					//$meter_pending_alert->setAlert_notify_datetime();
					$meter_pending_alert->setMeter_id($meter_id);
					$meter_pending_alert->setLast_reading_data($meter['latestreading']);
					$meter_pending_alert->setLast_reading_datetime($meter['latestreadingtime']);
					$meter_pending_alert->setAlert_type($alerttype);
			
					$meter_pending_alert->save(true);
				}
			}
		}
		
		
		
		$low_battery_limit = $meter_alertObj->getLowPingerBattery();
		$low_battery_channelno = $meter_alertObj->getLowPingerBatteryChannelNo();
		$alert_low_battery_time = 60;
		
		$common_battery_where = "macid='{$meter['mac']}' AND moduleid={$meter['moduleno']} AND slotno={$meter['slotno']} AND channelno={$low_battery_channelno} ";
		
		if (isset ($low_battery_limit) && $low_battery_limit != null && $low_battery_limit != '' && isset($low_battery_channelno) && $low_battery_channelno !='' && $low_battery_channelno != null)
		{
				if ( !(isset ($alert_low_battery_time) && $alert_low_battery_time != null && $alert_low_battery_time != '') )
					$alert_low_battery_time = 60;
					
				$alerttype='lowbattery';
				$meter_data = new I4energy_Model_MeterData();
				$select = $meter_data->getMapper()->getSelect();
				$select->from ('meter_data', array ('macid'=>'macid','rec_date' => 'rec_date','moduleid'=>'moduleid','slotno'=>'slotno','channelno'=>'channelno','data' => 'data' ));
				$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime ('-' . $alert_low_battery_time . ' minutes')) );
				$select->where($common_battery_where);
				$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
				$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
			
				$alert = true;
				foreach ($rows as $row)
				{
					if ($low_battery_limit < $row['data'])
						$alert = false;
				}
				
				if($alert)
				{
					$alertdatetime=gmdate('Y-m-d H:i:s');
					$lowbatteryalertfilter=array();
					$lowbatteryalertfilter[0]['field'] = 'meter_id';
					$lowbatteryalertfilter[0]['operation'] = '=';
					$lowbatteryalertfilter[0]['value'] = $meter_id;
						
					$lowbatteryalertfilter[1]['field'] = 'alert_type';
					$lowbatteryalertfilter[1]['operation'] = '=';
					$lowbatteryalertfilter[1]['value'] = 'lowbattery';
						
					$lowbatteryalertfilter[2]['field'] = 'alert_time';
					$lowbatterylertfilter[2]['operation'] = 'gte';
					$lowbatteryalertfilter[2]['value'] = @gmdate ('Y-m-d H:i:s', @strtotime ('-720 minutes'));
						
						
					$lowbattery_meter_pending_alert=new I4energy_Model_MeterPendingAlert();
					$lowbatteryalertlist=$lowbattery_meter_pending_alert->listMeterPendingAlert($lowbatteryalertfilter);
	// 				NO ALERT SET IN LAST 12 HOURS
					if ($lowbatteryalertlist->totalrecords == 0)
					{
						$meter_pending_alert=new I4energy_Model_MeterPendingAlert();
						$meter_pending_alert->setAlert_datetime($alertdatetime);
						$meter_pending_alert->setAlert_status(0);
						//$meter_pending_alert->setAlert_notify_datetime();
						$meter_pending_alert->setMeter_id($meter_id);
						$meter_pending_alert->setLast_reading_data($meter['latestreading']);
						$meter_pending_alert->setLast_reading_datetime($meter['latestreadingtime']);
						$meter_pending_alert->setAlert_type($alerttype);
				
						$meter_pending_alert->save(true);
					}
				}
		}
		
	}
}

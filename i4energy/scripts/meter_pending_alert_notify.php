<?php
require_once dirname(__FILE__) . '/zendload.php';


		$filter=array();
		$filter[0]['field'] = 'alert_status';
		$filter[0]['operation'] = '=';
		$filter[0]['value'] = 0;
		
		$meter_pending_alertobj=new I4energy_Model_MeterPendingAlert();
		$pending_alerts=$meter_pending_alertobj->listMeterPendingAlert($filter);

		
// 		print_r($pendingnotify);exit;
		if (isset($pending_alerts->totalrecords) && $pending_alerts->totalrecords > 0)
		{
			
			foreach ($pending_alerts->rows as $rows)
			{
				$pending_alert_obj24=new I4energy_Model_MeterPendingAlert();
				$filter24=array();
				$filter24[0]['field'] = 'meter_id';
				$filter24[0]['operation'] = '=';
				$filter24[0]['value'] = $rows['meter_id'];
				
				$filter24[1]['field'] = 'alert_time';
				$filter24[1]['operation'] = '>=';
				$filter24[1]['value'] = @gmdate ('Y-m-d H:i:s', @strtotime ('- 24 hours'));
				
				$filter24[2]['field'] = 'alert_type';
				$filter24[2]['operation'] = '=';
				$filter24[2]['value'] = $rows['alert_type'];
				
				$pendingalert24=$pending_alert_obj24->listMeterPendingAlert($filter24,'alert_notify_datetime','ASC');
				$status24_array=array();
				if (isset($pendingalert24) && isset($pendingalert24->totalrecords) && $pendingalert24->totalrecords > 0 ){
					
					$status24_value_array=array();
					foreach ($pendingalert24->rows as $rows24)
					{
						if (!in_array($rows24['alert_status'], $status24_array) && $rows24['alert_status'] != 2)
						{
							array_push($status24_array, $rows24['alert_status']);	
						}
						$status24_recordIndex= array_search($rows24['alert_status'], $status24_array);
						
						$status24_value_array[$status24_recordIndex]['id']=$rows24['id'];
						$status24_value_array[$status24_recordIndex]['alert_status']=$rows24['alert_status'];
						$status24_value_array[$status24_recordIndex]['alert_notify_datetime']=$rows24['alert_notify_datetime'];
						$status24_value_array[$status24_recordIndex]['meter_id']=$rows24['meter_id'];
					}
					
					
				}	
				
				$alert_status=0;
				if(isset($status24_array) && in_array(1, $status24_array) || in_array(6, $status24_array))
				{
					if(in_array(4, $status24_array)  || in_array(6, $status24_array))
					{
// after 1 hr
						if(in_array(5, $status24_array) || in_array(6, $status24_array))
						{
						
							if(in_array(6, $status24_array) )
							{
							
							}else
							 {
								$alert_status = 6;
							}
						}else
						 {
								$record_index= array_search(1, $status24_array);
								if(isset($record_index) && isset($status24_value_array[$record_index]['id']) && $status24_value_array[$record_index]['id']>0)
								{
									$dateDiff=time()-strtotime($status24_value_array[$record_index]['alert_notify_datetime'])/3600;
									if ($dateDiff >= 6){
										$alert_status = 5;
									}
										
								}
						}
						
					}else {
						$record_index= array_search(1, $status24_array);
						if(isset($record_index) && isset($status24_value_array[$record_index]['id']) && $status24_value_array[$record_index]['id']>0)
						{
							$dateDiff=time()-strtotime($status24_value_array[$record_index]['alert_notify_datetime'])/3600;
							if ($dateDiff >= 1){
								$alert_status = 4;
							}
															
						}
						
					}
				}else {
					$alert_status = 1;
				}
				
				
				$meter_pending_alert_id=$rows['id'];
				
				$alert_type = $rows['alert_type'];
				$meter_id   = $rows['meter_id'];
				$meter_data = $rows['last_reading_data'];
				
				$meter = new I4energy_Model_Meters();
				$meter->init ($meter_id);
				
				if (!($meter->getId() > 0))
					continue; 
				
				$meter_alertObj = new I4energy_Model_MeterAlerts();
				$meter_alertObj->init ($meter_id);
				
				
				if ($alert_type == 'min')
				{
					$meter_limit=$meter_alertObj->getAlertLimitMin();//(float) I4energy_Model_Meters::convertToUnit (, $meter->getType(), $meter->getUnit(), $meter->getConversationFactor());
					$meter_time_limit = $meter_alertObj->getAlertTimeLimitMin();
				}elseif ($alert_type == 'max')
				{
					$meter_limit=$meter_alertObj->getAlertLimitMax();//(float) I4energy_Model_Meters::convertToUnit ($meter_alertObj->getAlertLimitMax(), $meter->getType(), $meter->getUnit(), $meter->getConversationFactor());
					$meter_time_limit = $meter_alertObj->getAlertTimeLimitMax();
				}elseif ($alert_type == 'noping'){
					$meter_time_limit = $meter_alertObj->getAlertLimitPing();
				}elseif ($alert_type == 'lowbattery'){
					$meter_limit = $meter_alertObj->getLowPingerBattery();
					$meter_time_limit = '60';
				}
				
				
				$subject = "Alert: Meter ".$meter->getName()." crossed ".$alert_type ." limit";
				$content='';
				if ($alert_type == 'min' || $alert_type == 'max')
				{
					$content = '<br/> <h3 style="color: red;"> Alert : Meter '.$meter->getName().' crossed '. $alert_type.' limit of '.$meter_limit.' '.$meter->getUnit().' for more than '.$meter_time_limit.' minutes </h3>';
				}elseif ($alert_type == 'noping')
				{
					$content = '<br/> <h3 style="color: red;"> Alert : Meter '.$meter->getName().' crossed '. $alert_type.' limit  for more than '.$meter_time_limit.' minutes </h3>';
				}elseif ($alert_type == 'lowbattery'){
					$content = '<br/> <h3 style="color: red;"> Alert : Meter '.$meter->getName().' crossed low pinger battery limit of '.$meter_limit.' for more than '.$meter_time_limit.' minutes </h3>';
				}

					$site_obj =new I4energy_Model_Sites();
					$site_obj->init($meter->getSiteId());

					if (!($site_obj->getId() > 0))
						continue;
					
					$content .= '<br/> Site Name : '.$site_obj->getName();
					$content .= '<br/> Meter Name: '.$meter->getName();
					$content .= '<br/> Mac ID    : '.$meter->getMac();
					$content .= '<br/> Slot No   : '.$meter->getSlotNo();
					$content .= '<br/> Channel No: '.$meter->getChannelNo();
					
					$content .= '<br/><br/><br/> Last Reading Date : <b>'.$meter->getLatestReadingTime().' </b>';
					$content .= '<br/> Last Reading : <b>'.$meter->getLatestReading() .' '.$meter->getUnit() .'</b>';
					$meter_link="<a href='http://www.i4energy.co.uk/meters/site-".$meter->getSiteId()."/".$meter->getId()."' >Click Here</a>";
					$content .= '<br/> More Details: '.$meter_link;
					$content .= '<br/> Contact Email: <a href="mailto:support@i4energy.co.uk">support@i4energy.co.uk</a>';

					$content .= '<br/><br/><br/> Regards : ';
					$content .= '<br/> i4energy automatic alert';
						
			
				
					$toArray=array();
					if ($meter_alertObj->getEmail1() !='' && $meter_alertObj->getEmail1() !=null)
					{
							array_push($toArray, $meter_alertObj->getEmail1());
					}
					if($meter_alertObj->getEmail2() != '' && $meter_alertObj->getEmail2() != null)
					{
							array_push($toArray, $meter_alertObj->getEmail2());
					}		
					if ($meter_alertObj->getEmail3() != '' && $meter_alertObj->getEmail3() != null)
					{
						array_push($toArray, $meter_alertObj->getEmail3());
					}
					
					if (sizeof ($toArray) > 0 && $alert_status > 0)
					{
						$alert_type =2;
						I4energy_Model_Utils_SendMail::sendmailzend(implode(',',$toArray), $subject, $content);
					}

					
					$alert_notify_datetime=gmdate('Y-m-d H:i:s');
					
					$meter_alert_pendingObj = new I4energy_Model_MeterPendingAlert();
					$meter_alert_pendingObj->init($meter_pending_alert_id);
					
					$meter_alert_pendingObj->setId($meter_pending_alert_id);
					$meter_alert_pendingObj->setAlert_status($alert_status);
					$meter_alert_pendingObj->setAlert_notify_datetime($alert_notify_datetime);
					$meter_alert_pendingObj->save(true);
			}
		}
							
				// Subject - Alert: Meter "Meter Name" crossed min limit
				// Content -
				// <RED>Alert: Meter "Meter Name" crossed min limit of 12 oC for more than getAlertTimeLimitMin() minutes<RED>
				//
				// Site Name:
				// Meter Name: 
				// MAC ID:
				// Slot No:
				// Channel No:
				//
				// Last Reading Date: <BOLD>
				// Last Reading with Unit: <BOLD>
				//
				// More Details: Link to Meter View Page
				//
				// Regards,
				// i4energy
				
		
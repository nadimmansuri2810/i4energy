<?php
define ('ROOT_DIR', dirname (__FILE__));
define ('CSV_ROOT', ROOT_DIR . '/energy');
define ('PROCESSING_ROOT', ROOT_DIR . '/processing');
define ('SUCCESS_ROOT', ROOT_DIR . '/processed_success');
define ('FAIL_ROOT', ROOT_DIR . '/processed_fail');
define ('ARCHIVE_ROOT', ROOT_DIR . '/archive');

// Archive processed files
echo "\nArchiving CSV files...";
$dh = opendir (SUCCESS_ROOT);
if ($dh === false)
	die ("Cannot open directory " . SUCCESS_ROOT . "!");

$zip_filename = 'i4energy_' . @date('YmdHis') . '.zip';
$zip = new ZipArchive;
if ($zip->open(ARCHIVE_ROOT . "/" . $zip_filename, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) === FALSE)
	die ("Cannot create Archive file: " . ARCHIVE_ROOT . "/" . $zip_filename);

while( false !== ($file = readdir($dh)) )
{
	if (!preg_match ('/\.csv$/i', $file))
		continue;

	// Move success to archive
	if (rename (SUCCESS_ROOT . '/' . $file, ARCHIVE_ROOT . '/' . $file))
		$zip->addFile (ARCHIVE_ROOT . '/' . $file, $file);
	else
		echo "\nCannot move to archive: " . SUCCESS_ROOT . '/' . $file;
}

$zip->close();
closedir( $dh );


echo "\nDeleting processed CSV files...";
$dh = opendir (ARCHIVE_ROOT);
if ($dh === false)
	die ("Cannot open directory " . ARCHIVE_ROOT . "!");

while( false !== ($file = readdir($dh)) )
{
	if (!preg_match ('/\.csv$/i', $file))
		continue;

	// Delete processed files
	unlink (ARCHIVE_ROOT . '/' . $file);
}
closedir( $dh );

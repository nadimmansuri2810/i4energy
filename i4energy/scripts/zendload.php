<?php
ini_set("memory_limit",-1);
error_reporting(E_ALL);
ini_set('display_errors', 1);

defined('PAGE_LIMIT') || define ('PAGE_LIMIT', 10);
defined('DEFAULT_TIMEZONE') || define ('DEFAULT_TIMEZONE', 'GMT');
defined('DEFAULT_DATEFORMAT') || define ('DEFAULT_DATEFORMAT', 'dS M, Y h:i a');
defined('ADMIN_USER') || define ('ADMIN_USER', 'admin');
date_default_timezone_set (DEFAULT_TIMEZONE);
// Define path to application directory
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

//Define application root
defined('APPLICATION_ROOT')
|| define('APPLICATION_ROOT', realpath(dirname(__FILE__) . '/..'));

// Define application environment
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
 
// Ensure library/ is on include_path
set_include_path(
	APPLICATION_ROOT . '/library' . PATH_SEPARATOR
	.APPLICATION_ROOT . '/application' . PATH_SEPARATOR
	.APPLICATION_ROOT . '/application/models' . PATH_SEPARATOR
	.APPLICATION_ROOT . '/application/models/Utils' . PATH_SEPARATOR
	. get_include_path()
);

require_once 'Zend/Loader/Autoloader.php';

$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setDefaultAutoloader(create_function(
		'$class', "include str_replace('_','/',\$class) . '.php';"));

$application = new Zend_Application(
	APPLICATION_ENV,
		array(
			'config' => array(
				APPLICATION_PATH . '/configs/application.ini',
		)
	)
);
$application->bootstrap();

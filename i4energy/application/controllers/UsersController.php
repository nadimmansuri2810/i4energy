<?php

class UsersController extends Zend_Controller_Action
{
	public function init() 
	{
		
		$this->i4energy_session = new Zend_Session_Namespace('I4energySession');
		if (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == ''))
		{
			$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);;
			exit;
		}
	}

	public function indexAction() 
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'user_view', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$users_obj = new I4energy_Model_Users();
		
		$start = 0;
		if (isset ($_POST['start']))
			$_POST['start'] = (int) $_POST['start'];
		
		$limit = PAGE_LIMIT;
		if (isset ($_POST['limit']))
			$limit = (int) $_POST['limit'];
		
		
		$response = $users_obj->listUsers (array(), 'name', 'asc', $start, $limit);
		
		$this->view->totalrecords = $response->totalrecords;
		$this->view->rows = array();
		if ($response->returnrecords > 0)
		{
			foreach ($response->rows as $row)
			{
				$user = new I4energy_Model_Users();
				$user->setId($row['id']);
				$user->setRole_id($row['role_id']);
				$user->setName($row['name']);
				$user->setEmail($row['email']);
				$user->setMobile($row['mobile']);
				$user->setStatus($row['status']);
				$user->setToken($row['token']);
				$user->setTimeZone($row['timezone']);
				$user->setDateFormat($row['dateformat']);
				
				array_push ($this->view->rows, $user);
				unset ($user);
			}
		}
	}
	
	function viewAction ()
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'user_view', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$user_id = (int) $this->_request->getParam ('id');
		if ($user_id > 0)
		{
			$user = new I4energy_Model_Users();
			$user->init ($user_id);
			
			$user_id = $user->getId();
			if (isset ($user_id) && $user_id != '')
			{
				$this->view->user = $user;
			}
			else
			{
				throw new Zend_Controller_Action_Exception('User not found', 404);
			}
		}
		else
		{
			throw new Zend_Controller_Action_Exception('User not found', 404);
		}
	}
	
	function addAction()
	{
		$user_id = (int) $this->_request->getParam ('user_id');
		
		if (isset($user_id) && $user_id > 0)
		{
			if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'user_edit', $this->i4energy_session->user_priviledge))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
		}
		else
		{
			if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'user_add', $this->i4energy_session->user_priviledge))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
		}
		
		$roles=new I4energy_Model_RoleMapper();
		$roles=$roles->listRole();
		if(count($roles->rows)>0){
			$this->view->roles = $roles->rows;
		}
		$response =array();
		if($this->_request->isPost())
		{
			
			$filter=array();
			$user_mapper_obj=new I4energy_Model_UsersMapper();
			
			$user_name = $this->_request->getParam ('user_name');
			$user_email = $this->_request->getParam ('user_email');
			$user_mobile = $this->_request->getParam ('user_mobile');
			$user_password = $this->_request->getParam ('password');
			$conf_password = $this->_request->getParam('confpassword');
			$user_role = $this->_request->getParam ('user_role');
			$user_status = $this->_request->getParam ('user_status');
			$user_timezone= $this->_request->getParam ('user_timezone');

			$user_dateformat = $this->_request->getParam ('user_dateformate');
			$user_timeformate = $this->_request->getParam('user_timeformate');
			$user_dateformat = "$user_dateformat $user_timeformate";
			$customer_id=$this->_request->getParam('customer');
			if($customer_id == ""){
				$customer_id=0;
			}	
			
			$user_theme = $this->_request->getParam('user_theme');
			
			if( $user_email == ""){
					$response['data']['user_email'] ='null';
				}else{
					$response['data']['user_email'] ='valid';
				}
				
			
			if(isset($user_email) && $user_email != ""){
				$filterUser=array();
				$filterUser[0]['field'] = 'email';
				$filterUser[0]['operation'] = 'eq';
				$filterUser[0]['value'] = $user_email;
				if(isset($user_id) && $user_id>0){
					$filterUser[1]['field'] = 'id';
					$filterUser[1]['operation'] = 'ne';
					$filterUser[1]['value'] = $user_id;
				}
				$duplicateUser=$user_mapper_obj->listUsers($filterUser);
				if($duplicateUser->totalrecords > 0){
					$response['data']['user_email'] ='duplicate';
				}
			}

				
			if($user_name == ""){
					$response['data']['user_name'] ='null';
				}else{
					$response['data']['user_name'] ='valid';
				}

		if($user_id==""){
			
				
			if($user_password == ""){
					$response['data']['user_password'] ='null';
				}else{
					$response['data']['user_password'] ='valid';
				}
			

			if($conf_password == ""){
					$response['data']['confpassword'] ='null';
				}else{
					$response['data']['confpassword'] ='valid';
				}

			if($user_password == $conf_password){
				$response['data']['confpassword'] ='valid';
			}else{
				$response['data']['confpassword'] ='passwordnotmatch';
			}	
		}
				if($user_role == ""){
					$response['data']['user_role'] ='null';
				}else{
					$response['data']['user_role'] ='valid';
				}

				if( $user_status == ""){
					$response['data']['user_status'] ='null';
				}else{
					$response['data']['user_status'] ='valid';
				}
			
			if(isset($response['data']) && (in_array("null", $response['data']) || in_array("passwordnotmatch", $response['data']) || in_array("duplicate", $response['data']) || in_array("invalid", $response['data']))){
				$response['returnvalue']="invalid";
			}
			else{
				$response=array();
			}
				
			if(count($response)==0){
				if ( $user_id ==""){
				$usertable = new I4energy_Model_UsersMapper();
				
				$stmt = new Zend_Db_Statement_Pdo($usertable->getDbTable()->getDefaultAdapter(), "SELECT PASSWORD('{$conf_password}') AS new_password");
				$stmt->execute();
				$password_res = $stmt->fetch(Zend_Db::FETCH_ASSOC);
				
				$new_password=$password_res['new_password'];
			}
			$user = new I4energy_Model_Users();

			if (isset($user_id) && $user_id > 0)
				$user->setId($user_id);
			
			$user->setRole_id($user_role);
			$user->setCustomer_id($customer_id);
			$user->setName($user_name);
			$user->setEmail($user_email);
			if ( $user_id ==""){
			$user->setPassword($new_password);
			}
			$user->setMobile($user_mobile);
			$user->setStatus($user_status);
			$user->setTimeZone($user_timezone);
			$user->setDateFormat($user_dateformat);
			$user->setTheme($user_theme);
			
			$saveduser_id = $user->save (true);
			
			if ($saveduser_id > 0)
			{
				
				$this->view->success = true;
			}
			else
			{
				$this->view->success = false;
			}
			
			}
			else{
			$this->view->response = $response;
			}
		}
		if (isset($user_id) && $user_id > 0)
		{
			$user = new I4energy_Model_Users();
			$user->init ($user_id);
			$this->view->user = $user;
			
		}
		
		$customer_obj =new I4energy_Model_Customers();
		$customers=$customer_obj->listCustomers();
		$this->view->customers = $customers;
		
		$user_obj =new I4energy_Model_Users();
		$user=$user_obj->listUsers();
		$allocatedcustomer=array();
		foreach ($user->rows as $row){
			if($row['customer_id'] != 0){
			array_push($allocatedcustomer, $row['customer_id']);
			}
		}
		$this->view->allocatedcustomer=$allocatedcustomer;
	}
	
	function deleteAction()
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'user_delete', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$user_id = (int) $this->_request->getParam ('user_id');
		if($this->_request->isPost())
		{
			$user = new I4energy_Model_Users();
			$user->init ($user_id);
			$user->delete ();
		}
		
		exit;
	}
	
	function myprofileAction(){
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'update_self_profile', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$user_id=$this->i4energy_session->user_id;
		
		$response =array();
		if($this->_request->isPost())
		{
				
			$filter=array();
			$user_mapper_obj=new I4energy_Model_UsersMapper();
				
			$user_name = $this->_request->getParam ('user_name');
			$user_email = $this->_request->getParam ('user_email');
			$user_mobile = $this->_request->getParam ('user_mobile');
			$user_timezone= $this->_request->getParam ('user_timezone');
		
			$user_dateformat = $this->_request->getParam ('user_dateformate');
			$user_timeformate = $this->_request->getParam('user_timeformate');
			$user_dateformat = "$user_dateformat $user_timeformate";
				
			$user_theme = $this->_request->getParam('user_theme');
				
			if( $user_email == ""){
				$response['data']['user_email'] ='null';
			}else{
				$response['data']['user_email'] ='valid';
			}
		
				
			if(isset($user_email) && $user_email != ""){
				$filterUser=array();
				$filterUser[0]['field'] = 'email';
				$filterUser[0]['operation'] = 'eq';
				$filterUser[0]['value'] = $user_email;
				if(isset($user_id) && $user_id>0){
					$filterUser[1]['field'] = 'id';
					$filterUser[1]['operation'] = 'ne';
					$filterUser[1]['value'] = $user_id;
				}
				$duplicateUser=$user_mapper_obj->listUsers($filterUser);
				if($duplicateUser->totalrecords > 0){
					$response['data']['user_email'] ='duplicate';
				}
			}
		
		
			if($user_name == ""){
				$response['data']['user_name'] ='null';
			}else{
				$response['data']['user_name'] ='valid';
			}
		
			if(isset($response['data']) && (in_array("null", $response['data']) || in_array("passwordnotmatch", $response['data']) || in_array("duplicate", $response['data']) || in_array("invalid", $response['data']))){
				$response['returnvalue']="invalid";
			}
			else{
				$response=array();
			}
		
			if(count($response)==0){
				$user = new I4energy_Model_Users();
		
				if (isset($user_id) && $user_id > 0)
					$user->init($user_id);
					
				$user->setName($user_name);
				$user->setEmail($user_email);
				$user->setMobile($user_mobile);
				$user->setTimeZone($user_timezone);
				$user->setDateFormat($user_dateformat);
				$user->setTheme($user_theme);
					
				$user_id = $user->save (true);
					
				if ($user_id > 0)
				{
		
					$this->view->success = true;
					$this->i4energy_session->user_obj = $user;
				}
				else
				{
					$this->view->success = false;
				}
					
			}
			else{
				$this->view->response = $response;
			}
		}
		if (isset($user_id) && $user_id > 0)
		{
			$user = new I4energy_Model_Users();
			$user->init ($user_id);
			$this->view->user = $user;
				
		}
	}
	
	function setnewpasswordAction(){
		
			$user_id=$this->i4energy_session->user_id;
			$oldpassword = $this->_request->getParam ('oldpassword');
			$password = $this->_request->getParam ('password');
			$confpassword = $this->_request->getParam ('confpassword');
			
			$user_obj=new I4energy_Model_Users();

			$select = $user_obj->getMapper()->getSelect();
			$select->from("users");
			$select->where("`id`=?", $user_id);
			$select->where("`password`=password(?)", $oldpassword);
			$stmt = $user_obj->getMapper()->getDbTable()->getAdapter()->query ($select);
			$user_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
	
			$response=array();
			if (isset ($user_row) && $user_row['id'] > 0)
			{
				$stmt_passwd = new Zend_Db_Statement_Pdo($user_obj->getMapper()->getDbTable()->getDefaultAdapter(), "SELECT PASSWORD('{$confpassword}') AS new_password");
				$stmt_passwd->execute();
				$password_res = $stmt_passwd->fetch(Zend_Db::FETCH_ASSOC);
				
				$new_password = $password_res['new_password'];
				
				$user_obj->init($user_row);
				$user_obj->setPassword($new_password);
				$user_id=$user_obj->save(true);	
				if ($user_id > 0)
				{
					$response['data'] ='success';
				}
				else
				{
					$response['data'] ='fail';
				}
			}else {
				$response['data'] ='fail';
			}
			echo json_encode($response);
			exit;		
	}

	
	function setpasswordAction(){
		
		if (I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'user_add', $this->i4energy_session->user_priviledge) && $this->i4energy_session->user_role == ADMIN_USER) 
		{
			
				$user_id = $this->_request->getParam ('user_id');
				$password = $this->_request->getParam ('password');
				$confpassword = $this->_request->getParam ('confpassword');
					
				$user_obj=new I4energy_Model_Users();
					$response=array();
				
					$stmt_passwd = new Zend_Db_Statement_Pdo($user_obj->getMapper()->getDbTable()->getDefaultAdapter(), "SELECT PASSWORD('{$confpassword}') AS new_password");
					$stmt_passwd->execute();
					$password_res = $stmt_passwd->fetch(Zend_Db::FETCH_ASSOC);
			
					$new_password = $password_res['new_password'];
			
					$user_obj->init($user_id);
					$user_obj->setPassword($new_password);
					$user_id=$user_obj->save(true);
					if ($user_id > 0)
					{
						$response['data'] ='success';
					}
					else
					{
						$response['data'] ='fail';
					}
				
				echo json_encode($response);
				exit;
		}else {
			$this->_redirect ('/accessdenied');
			exit;
		}
	
	
	}

}

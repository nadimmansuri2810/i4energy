<?php

class CustomersController extends Zend_Controller_Action
{
	public function init() 
	{
		
		$this->i4energy_session = new Zend_Session_Namespace('I4energySession');
		if (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == ''))
		{
			$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);
			exit;
		}

		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'customer_view', $this->i4energy_session->user_priviledge) || $this->i4energy_session->user_role != ADMIN_USER)
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
	}

	public function indexAction() 
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'customer_view', $this->i4energy_session->user_priviledge) || $this->i4energy_session->user_role != ADMIN_USER)
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$customer_obj = new I4energy_Model_Customers();
		
		$start = 0;
		if (isset ($_POST['start']))
			$_POST['start'] = (int) $_POST['start'];
		
		$limit = PAGE_LIMIT;
		if (isset ($_POST['limit']))
			$limit = (int) $_POST['limit'];
		
		
		$response = $customer_obj->listCustomers (array(), 'name', 'asc', $start, $limit);
		
		$this->view->totalrecords = $response->totalrecords;
		$this->view->rows = array();
		if ($response->returnrecords > 0)
		{
			foreach ($response->rows as $row)
			{
				$customers = new I4energy_Model_Customers();
				$customers->setId($row['id']);
				$customers->setName($row['name']);
				$customers->setLogo($row['logo']);
				$customers->setLogotype($row['logotype']);
				
				array_push ($this->view->rows, $customers);
				unset ($user);
			}
		}
	}
	
	function viewAction ()
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'customer_view', $this->i4energy_session->user_priviledge) || $this->i4energy_session->user_role != ADMIN_USER)
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
	}
	
	function addAction()
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'customer_view', $this->i4energy_session->user_priviledge) || $this->i4energy_session->user_role != ADMIN_USER)
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$user_obj=new I4energy_Model_Users();
		$userlist=	$user_obj->listUsers(array(), '', '', '', '');
		$this->view->users = $userlist;

		$customer_id=$this->_request->getParam('customer_id');
		
		if($this->getRequest()->isPost()){
			
			$allowedExts = array("gif", "jpeg", "jpg", "png");
			$logo_name= 		$_FILES["customer_logo"]["name"];
			$extension = explode(".", $logo_name);
			if ((($_FILES["customer_logo"]["type"] == "image/gif")
					|| ($_FILES["customer_logo"]["type"] == "image/jpeg")
					|| ($_FILES["customer_logo"]["type"] == "image/jpg")
					|| ($_FILES["customer_logo"]["type"] == "image/png"))
					&& ($_FILES["customer_logo"]["size"] < 4194304)
					&& in_array($extension[1], $allowedExts)){
			$customer_name=$this->_request->getParam('customer_name');
				
			if(isset($_FILES["customer_logo"])){
			$logo= 		$_FILES["customer_logo"]["tmp_name"];
			$logo_type= $_FILES["customer_logo"]["type"];
			
			$logo_size= $_FILES["customer_logo"]["size"];
			
			$logo_content=file_get_contents($logo);
			}
			
			$customer_obj=new I4energy_Model_Customers();
			if(isset($customer_id) && $customer_id >0){
				$customer_obj->setId($customer_id);
			}
			
			$customer_obj->setName($customer_name);
			$customer_obj->setLogo($logo_content);
			$customer_obj->setLogotype($logo_type);
				
			$customer_id=$customer_obj->save(true);
		}else{
			$this->view->success=false;
			$this->view->issue="invalidDFileFormate";
			
		}
			
		}
		
	if(isset($customer_id) && $customer_id>0){
		$customer=new I4energy_Model_Customers();
		$customer->init ($customer_id);
		$this->view->customer = $customer;
	}	
		
	}
	
	
	function deleteAction()
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'customer_delete', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}

		if($this->_request->isPost())
		{
			$customer_id = (int) $this->_request->getParam('customer_id');
		
			$customer_obj = new I4energy_Model_Customers();
			$customer_obj->init ($customer_id);
			$customer_obj->delete ();
			
		}
		exit;
	}
	
}

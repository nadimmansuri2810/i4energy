<?php

class MetersController extends Zend_Controller_Action
{
	public function init() 
	{
		$this->i4energy_session = new Zend_Session_Namespace('I4energySession');
		if ($this->i4energy_session->isforward != 'true')
		{
			if (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == ''))
			{
				$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);;
				exit;
			}
			
			if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'meter_view', $this->i4energy_session->user_priviledge))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
		}
	}

	function viewAction ()
	{
		$meter_id = (int) $this->_request->getParam ('id');
		$site_info = $this->_request->getParam ('site_id');
		$site_id = 0;
		if (strpos ($site_info, '-') !== false)
		{
			$site_id = (int) substr($site_info, strpos ($site_info, '-')+1);
		}
		
		if ($site_id == 0)
		{
			throw new Zend_Controller_Action_Exception('Site not found', 404);
		}
		
		// Check whether site is assigned
		if ($this->i4energy_session->isforward != 'true')
		{
			if (!($this->i4energy_session->user_role == ADMIN_USER || in_array ($site_id, $this->i4energy_session->user_assignsites)))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
		}
				
		if ($meter_id > 0)
		{
			$meter = new I4energy_Model_Meters();
			$meter->init ($meter_id);
			
			if ($meter->getId() > 0)
			{
				$meter->setUpdatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($meter->getUpdatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$meter->setCreatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($meter->getCreatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$this->view->meter = $meter;
			}
			else
			{
				throw new Zend_Controller_Action_Exception('Meter not found', 404);
			}
		}
		else
		{
			throw new Zend_Controller_Action_Exception('Meter not found', 404);
		}
		
		if ($site_id > 0)
		{
			$site = new I4energy_Model_Sites();
			$site->init ($site_id);
				
			if ($site->getId() > 0)
			{
				$site->setUpdatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($site->getUpdatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$site->setCreatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($site->getCreatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$this->view->site = $site;
			}
			else
			{
				throw new Zend_Controller_Action_Exception('Site not found', 404);
			}
		}
		else
		{
			throw new Zend_Controller_Action_Exception('Site not found', 404);
		}
		
		// Get meter data
		if ($meter_id > 0)
		{
			$meter_alters = new I4energy_Model_MeterAlerts();
			$meter_alters->init ($meter_id);
			$this->view->meter_alert = $meter_alters;
			
			$meter_attrs = new I4energy_Model_MeterAttributes();
			$meter_attrs->init ($meter_id);
			$this->view->meter_attrs = $meter_attrs;

			if ($meter->getType() == 'hourrun')
			{
				$meter_data_result = self::getHourRunMeterData ($meter, $meter_attrs->getInstallDate(), $meter_attrs);

				$convert_unit_attrs = array();
				$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();

				$this->view->today_run = $meter_data_result['today_run'];
				$this->view->last24h_run = $meter_data_result['last24h_run'];
				$this->view->today_close = $meter_data_result['today_close'];
				$this->view->total_run = $meter_data_result['total_run'];
				$this->view->pulse1 = $meter_data_result['pulse1'];
				$this->view->pulse2 = $meter_data_result['pulse2'];
				$this->view->pulse3 = $meter_data_result['pulse3'];
				$this->view->pulse1_status = $meter_data_result['pulse1_status'];
				$this->view->pulse2_status = $meter_data_result['pulse2_status'];
				$this->view->pulse3_status = $meter_data_result['pulse3_status'];
				$this->view->webdial_reading = (trim($meter_data_result['webdial_reading']) == '')?0:$meter_data_result['webdial_reading'];
				$this->view->current_status = $meter_data_result['current_status'];
				$this->view->hourly_chart_data = $meter_data_result['hourly_chart_data'];

				$this->render('hourrun-view');
			}
			else {
				// Get meter data
				$meter_data_result = self::getMeterData($meter, $meter_attrs->getInstallDate());

				$this->view->current_status = (isset($meter_data_result['current_status'])) ? trim($meter_data_result['current_status']) : '';

				$convert_unit_attrs = array();
				$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();

				// Now
				if ($meter_data_result['now'] == '')
					$this->view->now_usage = 'NA';
				else
					$this->view->now_usage = I4energy_Model_Meters::convertToUnit($meter_data_result['now'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);

				// Today usage
				$this->view->today_usage = I4energy_Model_Meters::convertToUnit($meter_data_result['today'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);

				$carbon_savings = $meter->getCarbonSavings();
				if (isset ($carbon_savings) && $carbon_savings != '' && $carbon_savings != 0) {
					$this->view->today_carbonsavings = I4energy_Model_Meters::calculateCarbonSavings($this->view->today_usage, $meter->getType(), $carbon_savings, $meter_attrs->getCarbonVolumeCorrectionFactor(), $meter_attrs->getCarbonCalorificValue(), $meter_attrs->getCarbonKwhConversionFactor()) . ' kg';
				} else {
					$this->view->today_carbonsavings = $this->view->today_usage . '/' . I4energy_Model_Meters::getDisplayUnit($meter->getUnit());
				}

				// Last 24H usage
				$this->view->last24h_usage = I4energy_Model_Meters::convertToUnit($meter_data_result['last24h'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);

				// total usage
				$this->view->total_usage = I4energy_Model_Meters::convertToUnit($meter_data_result['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);

				$this->view->display_unit = I4energy_Model_Meters::getDisplayUnit($meter->getUnit());

				// Today's usage chart data (hourly)
				$this->view->hourly_chart_data = (isset($meter_data_result['hourly_chart_data'])) ? $meter_data_result['hourly_chart_data'] : null;

				// Last 31 days meter data
				$this->view->daily_chart_data = (isset($meter_data_result['daily_chart_data'])) ? $meter_data_result['daily_chart_data'] : null;

				// Last 12 months meter data
				$this->view->monthly_chart_data = (isset($meter_data_result['monthly_chart_data'])) ? $meter_data_result['monthly_chart_data'] : null;

				// Boiler Chart
				$this->view->boiler_chart_data = (isset($meter_data_result['boiler_chart_data'])) ? $meter_data_result['boiler_chart_data'] : null;

				// Boiler other infor
				$this->view->boilder_other_info = (isset($meter_data_result['boilder_other_info'])) ? $meter_data_result['boilder_other_info'] : null;
			}
		}
		$this->view->mac = $meter->getMac();
	}
	
	function readingsAction()
	{
		$meter_id = (int) $this->_request->getParam ('id');
		$hours = (int) $this->_request->getParam ('time');
		if ($hours <= 0)
			$hours = 1;
		 
		$response = array ('now' => '', 'today' => '', 'last24h' => '', 'total' => '', 'hourly_chart_data' => array(), 'daily_chart_data' => array());
		if ($meter_id > 0)
		{
			$meter = new I4energy_Model_Meters();
			$meter->init ($meter_id);
				
			if ($meter->getId() > 0)
			{
				$meter_attrs = new I4energy_Model_MeterAttributes();
				$meter_attrs->init ($meter_id);
				
				$convert_unit_attrs = array();
				$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();
				
				$response = self::getMeterData ($meter, $meter_attrs->getInstallDate());
				$response['now'] = trim ($response['now']);
				$response['today'] = (trim ($response['today']) == '')?0:trim ($response['today']);
				$response['last24h'] = (trim ($response['last24h']) == '')?0:trim ($response['last24h']);
				$response['total'] = (trim ($response['total']) == '')?0:trim ($response['total']);
				
				$response['now'] = I4energy_Model_Meters::convertToUnit ($response['now'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
				$response['today'] = I4energy_Model_Meters::convertToUnit ($response['today'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
				$response['last24h'] = I4energy_Model_Meters::convertToUnit ($response['last24h'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
				$response['total'] = I4energy_Model_Meters::convertToUnit ($response['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
				
				$response['current_status'] = (isset($response['current_status']))?trim($response['current_status']):'';
			}
			else
			{
				$response['now'] = 'NA';
			}
		}
		
		echo json_encode ($response);
		exit;
	}

	function hourrunreadingsAction () {
		$meter_id = (int) $this->_request->getParam ('id');
		$hours = (int) $this->_request->getParam ('time');
		if ($hours <= 0)
			$hours = 1;

		$response = array ('now' => '', 'today' => '', 'last24h' => '', 'total' => '', 'hourly_chart_data' => array(), 'daily_chart_data' => array());
		if ($meter_id > 0)
		{
			$meter = new I4energy_Model_Meters();
			$meter->init ($meter_id);

			if ($meter->getId() > 0)
			{
				$meter_attrs = new I4energy_Model_MeterAttributes();
				$meter_attrs->init ($meter_id);

				$convert_unit_attrs = array();
				$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();

				$response = self::getHourRunMeterData ($meter, $meter_attrs->getInstallDate(), $meter_attrs);

				$response['today_run'] = (trim ($response['today_run']) == '')?'0':$response['today_run'];
				$response['today_run'] = I4energy_Model_Utils_DateUtils::secondsToHHMM ($response['today_run']);

				$response['last24h_run'] = (trim ($response['last24h_run']) == '')?'0':$response['last24h_run'];
				$response['last24h_run'] = I4energy_Model_Utils_DateUtils::secondsToHHMM ($response['last24h_run']);

				$response['today_close'] = (trim ($response['today_close']) == '')?'0':$response['today_close'];
				$response['today_close'] = I4energy_Model_Utils_DateUtils::secondsToHHMM ($response['today_close']);

				$response['total_run'] = (trim ($response['total_run']) == '')?'0':$response['total_run'];
				$response['total_run'] = I4energy_Model_Utils_DateUtils::secondsToHHMM ($response['total_run']);

				$response['pulse1'] = (trim ($response['pulse1']) == '')?'0':$response['pulse1'];
				$response['pulse2'] = (trim ($response['pulse2']) == '')?'0':$response['pulse2'];
				$response['pulse3'] = (trim ($response['pulse3']) == '')?'0':$response['pulse3'];
				$response['pulse1_status'] = (trim ($response['pulse1_status']) == '')?'':$response['pulse1_status'];
				$response['pulse2_status'] = (trim ($response['pulse2_status']) == '')?'':$response['pulse2_status'];
				$response['pulse3_status'] = (trim ($response['pulse3_status']) == '')?'':$response['pulse3_status'];
				$response['webdial_reading'] = (trim ($response['webdial_reading']) == '')?'0':$response['webdial_reading'];
				$response['current_status'] = (trim ($response['current_status']) == '')?'':$response['current_status'];
				$response['hourly_chart_data'] = $response['hourly_chart_data'];
			}
		}

		echo json_encode ($response);
		exit;
	}
	
	public static function getMeterData (I4energy_Model_Meters &$meter, $install_date = '')
	{
		// Get meter data
		$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";
		
		if ($install_date != '' && @strtotime ($install_date) > 1356998400)
			$common_where .= " AND rec_date >= '{$install_date}'";
			
		$meter_attrs = new I4energy_Model_MeterAttributes();
		$meter_attrs->init ($meter->getId());
		
		$convert_unit_attrs = array();
		$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();
		
		$meter_data = new I4energy_Model_MeterData();
		
		// Consider Offset
		$field_to_query = 'meter_data.data';
		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == '' || $offset == null)
			$offset = '';
		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';
			
		$meter_data_result = array ('now' => '', 'today' => '', 'last24h' => '', 'total' => '', 'hourly_chart_data' => array(), 'daily_chart_data' => array(), 'monthly_chart_data' => array());
		// Now
		
		$meter_data_result['now'] = self::getMeterNowUsage($meter);
				
		if ($meter->getType() == 'number' || $meter->getType() == 'hourrun')
		{
			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('current_status' => 'meter_data.data1'));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-65 minutes')));
			$select->order ('rec_date DESC');
			$select->limit (1,0);
			//echo $select->__toString();exit;
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);
				
			$meter_data_result['current_status'] = trim($rows['current_status']);
		}
		
		// Today usage
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('total' => 'SUM(' . $field_to_query . ')'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', I4energy_Model_Utils_DateUtils::getFormattedDate(@gmdate(), 'Y-m-d 00:00:00', $meter->getTimezone()));
		//echo '<!-- AMIT2: ' . $select->__toString() . ' -->';
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);

		$meter_data_result['today'] = $rows['total'];
		
		// Last 24H usage
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('total' => 'SUM(' . $field_to_query . ')'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', I4energy_Model_Utils_DateUtils::getFormattedDate(@gmdate ('Y-m-d H:i:s', @strtotime('-24 hour')), 'Y-m-d H:i:s', $meter->getTimezone()));
		//echo $select->__toString();exit;
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);

		$meter_data_result['last24h'] = $rows['total'];
		
		// total usage
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('total' => 'SUM(' . $field_to_query . ')'));
		$select->where ($common_where);
		//echo $select->__toString();
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);

		$meter_data_result['total'] = $rows['total'];
		
		$defaultTimeZone = @date_default_timezone_get();
		@date_default_timezone_set($meter->getTimezone());
		$timezone_diff = @date('P');
		@date_default_timezone_set($defaultTimeZone);
		
		if ($meter->getType() == 'boiler')
		{
			$start_date = I4energy_Model_Utils_DateUtils::getFormattedDate(@gmdate(), 'Y-m-d 00:00:00', $meter->getTimezone());
			$boiler_info = self::getBoilerData($meter, $meter_data, $start_date);
			$meter_data_result['boiler_chart_data'] = $boiler_info['boiler_chart_data'];
			$meter_data_result['boilder_other_info'] = $boiler_info['boilder_other_info'];
		}
		else
		{
			$group_function = 'AVG';
			if ($meter->getType() == 'number' || $meter->getType() == 'hourrun' || ($meter->getUnit() == 'kwh' && $meter->getType() == 'power') || $meter->getType() == 'gas' || $meter->getType() == 'heat' || $meter->getType() == 'amps' || $meter->getType() == 'water')
				$group_function = 'SUM';
			
			// Today's usage chart data (hourly)
			$hourly_chart_data = array();
			for ($i=0; $i<24; $i++)
				$hourly_chart_data[str_pad ($i, 2, '0', STR_PAD_LEFT)] = 0;
			
			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('hour_of' => 'EXTRACT(HOUR FROM CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\'))', 'total' => $group_function . '(' . $field_to_query . ')'));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', I4energy_Model_Utils_DateUtils::getFormattedDate(@gmdate(), 'Y-m-d 00:00:00', $meter->getTimezone()));
			$select->group ('hour_of');
			//echo '<!-- AMIT3: ' . $select->__toString() . ' -->';
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
			foreach ($rows as $row)
				$hourly_chart_data[str_pad ($row['hour_of'], 2, '0', STR_PAD_LEFT)] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
	
			$meter_data_result['hourly_chart_data'] = $hourly_chart_data;
			
			// Last 31 days meter data
			$daily_chart_data = array();
			$date_before_31days = @gmdate ('Y-m-d', @strtotime ('-31 days'));
			$dtCnt = 1;
			while ($dtCnt <= 31)
			{
				$daily_chart_data[@gmdate ('Y-m-d', @strtotime ("+{$dtCnt} day", @strtotime ($date_before_31days)))] = 0;
				$dtCnt++;
			}
		
			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('date_of' => 'DATE(rec_date)', 'total' => $group_function . '(' . $field_to_query . ')'));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', $date_before_31days);
			$select->group ('date_of');
			//echo '<!-- AMIT4: ' . $select->__toString() . ' -->';
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
			foreach ($rows as $row)
				$daily_chart_data[$row['date_of']] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
			
			ksort ($daily_chart_data);
			
			$meter_data_result['daily_chart_data'] = $daily_chart_data;
			
			// Last 12 Month Data
			/*
			$monthly_chart_data = array();
			$date_before_12month = @gmdate("Y-m-d", strtotime( gmdate( 'Y-m-02' )." -11 months"));
			$monthCnt=-11;
			while ($monthCnt <= 0)
			{
				$yearMonth = @gmdate("M Y", strtotime( gmdate( 'Y-M-02' )." $monthCnt months"));
				$monthCnt++;
				$monthly_chart_data[$yearMonth]=0;
			}
			
			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('year_of' => 'EXTRACT(YEAR FROM (rec_date))','month_of' => 'EXTRACT(MONTH FROM (rec_date))', 'total' => $group_function . '(' . $field_to_query . ')'));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', $date_before_12month);
			$select->group ('month_of');
			//echo '<!-- AMIT4: ' . $select->__toString() . ' -->';
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
			
			$monthstring = array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'Apr','5'=>'May','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
			foreach ($rows as $row) {
				$monthStr = $monthstring[$row['month_of']];
				$monthly_chart_data[$monthStr . " " . $row['year_of']] = (float)I4energy_Model_Meters::convertToUnit($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
			}

			$meter_data_result['monthly_chart_data'] = $monthly_chart_data;
			*/
			$meter_data_result['monthly_chart_data'] = self::getMonthlyChartData ($meter, @gmdate ('Y'), @gmdate('m'), $install_date, $convert_unit_attrs);
		}
		
		return $meter_data_result;
	}

	public function monthlychartdataAction () {
		$meter_id = $this->_request->getParam ('meter_id');
		$year = $this->_request->getParam ('year');
		$month = $this->_request->getParam ('month');

		if ($meter_id > 0) {
			$meter = new I4energy_Model_Meters();
			$meter->init($meter_id);

			if ($meter->getId() > 0) {
				$meter_attrs = new I4energy_Model_MeterAttributes();
				$meter_attrs->init($meter_id);

				$convert_unit_attrs = array();
				$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();

				$data = self::getMonthlyChartData($meter, $year, $month, $meter_attrs->getInstallDate(), $convert_unit_attrs);
				echo json_encode($data);exit;
			}
		}

		echo '{}';exit;
	}

	public static function getMonthlyChartData (I4energy_Model_Meters &$meter, $year, $month, $install_date = '', $convert_unit_attrs = array()) {

		$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";

		if ($install_date != '' && @strtotime ($install_date) > 1356998400)
			$common_where .= " AND rec_date >= '{$install_date}'";

		$meter_data = new I4energy_Model_MeterData();

		$field_to_query = 'meter_data.data';
		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == '' || $offset == null)
			$offset = '';
		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';

		$group_function = 'AVG';
		if ($meter->getType() == 'number' || $meter->getType() == 'hourrun' || ($meter->getUnit() == 'kwh' && $meter->getType() == 'power') || $meter->getType() == 'gas' || $meter->getType() == 'heat' || $meter->getType() == 'amps' || $meter->getType() == 'water')
			$group_function = 'SUM';

		$monthly_chart_data = array();
		$date_before_12month = @gmdate("Y-m-d", strtotime( gmdate( $year . '-' . $month . '-02' )." -11 months"));
		$current_date = @gmdate ($year . '-' . $month . '-31 23:59:59');
		$monthCnt=-11;
		while ($monthCnt <= 0)
		{
			$yearMonth = @gmdate("M Y", strtotime( gmdate( $year . '-' . $month . '-02' )." $monthCnt months"));
			$monthCnt++;
			$monthly_chart_data[$yearMonth]=0;
		}

		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('year_of' => 'EXTRACT(YEAR FROM (rec_date))','month_of' => 'EXTRACT(MONTH FROM (rec_date))', 'total' => $group_function . '(' . $field_to_query . ')'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', $date_before_12month);
		$select->where ('rec_date <= ?', $current_date);
		$select->group ('month_of');
		//echo '<!-- AMIT4: ' . $select->__toString() . ' -->';
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

		$monthstring = array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'Apr','5'=>'May','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
		foreach ($rows as $row) {
			$monthStr = $monthstring[$row['month_of']];
			$monthly_chart_data[$monthStr . " " . $row['year_of']] = (float)I4energy_Model_Meters::convertToUnit($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}
		unset ($rows);
		unset ($stmt);

		return $monthly_chart_data;
	}

	public static function getMeterNowUsage (I4energy_Model_Meters &$meter)
	{

		// echo "<br> Meter:::::::<br>";
		// print_r ($meter);
		//exit;

		$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";
		
		$meter_data = new I4energy_Model_MeterData();

		// Consider Offset
		$field_to_query = 'meter_data.data';
		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == '' || $offset == null)
			$offset = '';
		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';
		

		if ($meter->getUnit() == 'c' || $meter->getUnit() == 'ma' || $meter->getUnit() == 'v' || $meter->getUnit() == 'rh')
		{

			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('total' => $field_to_query));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-65 minutes')));
			$select->order ('rec_date DESC');
			$select->limit (1,0);
		
			//echo $select->__toString();exit;
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);
				
			return $rows['total'];
		}
		else if ($meter->getUnit() == 'onoff' || $meter->getUnit() == 'hrun' || $meter->getUnit() == 'hopen' || ($meter->getUnit() == 'kwh' && $meter->getType() == 'power') || $meter->getType() == 'gas' || $meter->getType() == 'heat' || $meter->getType() == 'amps' || $meter->getType() == 'water')
		{

			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('total' => 'SUM(' . $field_to_query . ')'));
			$select->where ($common_where);

			//Uncomment when you are done with code
			//$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-1 hour')));

			//I have added please when done
			$select->where ('rec_date >= ?', '2019-04-08 03:30:32');
			//echo '<!-- AMIT1: ' . $select->__toString() . ' -->';
			//echo "\n";
			//exit;
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);

			//I have added so remove when done
			//$select->limit (1,0);

			return $rows['total'];
		}
		else
		{

			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('total' => 'AVG(' . $field_to_query . ')'));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-1 hour')));
			//echo $select->__toString();
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);
				

			return $rows['total'];
		}
		
		return null;
	}
	
	public static function getBoilerData (&$meter, &$meter_data, $start_date, $end_date = null)
	{
		$field_to_query = 'meter_data.data';
		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == '' || $offset == null)
			$offset = '';
		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';
		
		$meter_attrs = new I4energy_Model_MeterAttributes();
		$meter_attrs->init ($meter->getId());
		
		$convert_unit_attrs = array();
		$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();
		
		$defaultTimeZone = @date_default_timezone_get();
		@date_default_timezone_set($meter->getTimezone());
		$timezone_diff = @date('P');
		@date_default_timezone_set($defaultTimeZone);
		
		// For boiler, show line chart using 4 channels
		// Ch10 - Outside temp
		// Ch12 - Heating flow
		// Ch11 - Heating return
		// Ch13 - Hot Water temp
		$boiler_chart = array('outside_temp' => array(), 'return_temp' => array(), 'heating_flow' => array(), 'hot_water' => array(), 'demand' => array());
		for ($i=0; $i<24; $i++)
		{
			$boiler_chart['outside_temp'][str_pad ($i, 2, '0', STR_PAD_LEFT)] = NULL;
			$boiler_chart['return_temp'][str_pad ($i, 2, '0', STR_PAD_LEFT)] = NULL;
			$boiler_chart['heating_flow'][str_pad ($i, 2, '0', STR_PAD_LEFT)] = NULL;
			$boiler_chart['hot_water'][str_pad ($i, 2, '0', STR_PAD_LEFT)] = NULL;
			$boiler_chart['demand'][str_pad ($i, 2, '0', STR_PAD_LEFT)] = NULL;
		}
			
		//$date_to_qry = I4energy_Model_Utils_DateUtils::getFormattedDate($start_date, 'Y-m-d 00:00:00', $meter->getTimezone());
		$all_null = true;
					
		// Outside
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'EXTRACT(HOUR FROM CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\'))', 'total' => 'AVG(' . $field_to_query . ')'));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=10');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->group ('hour_of');
		
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$all_null = false;
			$boiler_chart['outside_temp'][str_pad ($row['hour_of'], 2, '0', STR_PAD_LEFT)] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}
						
		// Heating Flow
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'EXTRACT(HOUR FROM CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\'))', 'total' => 'AVG(' . $field_to_query . ')'));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=12');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->group ('hour_of');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$all_null = false;
			$boiler_chart['heating_flow'][str_pad ($row['hour_of'], 2, '0', STR_PAD_LEFT)] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}
								
		// Heating Return
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'EXTRACT(HOUR FROM CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\'))', 'total' => 'AVG(' . $field_to_query . ')'));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=11');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->group ('hour_of');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$all_null = false;
			$boiler_chart['return_temp'][str_pad ($row['hour_of'], 2, '0', STR_PAD_LEFT)] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}
			
		// Hot Water
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'EXTRACT(HOUR FROM CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\'))', 'total' => 'AVG(' . $field_to_query . ')'));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=13');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->group ('hour_of');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$all_null = false;
			$boiler_chart['hot_water'][str_pad ($row['hour_of'], 2, '0', STR_PAD_LEFT)] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}
										
		// Demand
		// Boiler Demand data1 = 1 means OFF and data1 = 0 means ON
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'EXTRACT(HOUR FROM CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\'))', 'total' => 'SUM(meter_data.data1)', 'cnt' => 'COUNT(meter_data.data1)'));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=7');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->group ('hour_of');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$all_null = false;
			
			// Demand is not in temprature, it is just on and off
			// So if in perticular hour, if on count is more than off count, set current demand to 2 so
			// demand line will be visible on chart
			$current_demand = 0;
			if ($row['cnt']-$row['total'] > 0)
				$current_demand = 2;
			$boiler_chart['demand'][str_pad ($row['hour_of'], 2, '0', STR_PAD_LEFT)] = $current_demand; //(float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}
										
		$final_boiler_chart = array('outside_temp' => array(), 'return_temp' => array(), 'heating_flow' => array(), 'hot_water' => array(), 'demand' => array());
		foreach ($boiler_chart as $chart_type => $data)
		{
			foreach ($data as $time => $value)
			{
				if ($all_null)
					$value = 0;
				$final_boiler_chart[$chart_type][] = array ('time' => $time, 'data' => $value);
			}
		}
		unset ($boiler_chart);
		$meter_data_result['boiler_chart_data'] = $final_boiler_chart;
										
		// Other boiler info
		// Channel 9 - Boiler
		// Channel 7 - Demand
		// Channel 8 - Request
		// Channel 5 - Automatic
		// Channel 6 - 2 Houts Boost
		// Channel 4 - Aux timer

		// If start date is today
		if (@gmdate('Y-m-d') == @date('Y-m-d', @strtotime($start_date)))
			$last_30mins = @gmdate ('Y-m-d H:i:s', @strtotime('-65 minutes'));
		else
			$last_30mins = @date ('Y-m-d H:i:s', @strtotime($start_date));
		
		$boiler_other_info = array();
		
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_rawdata');
		$select->setIntegrityCheck (false);
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('rec_date >= ?', $last_30mins);
		$select->where ('status >= ?', 'NA');
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->order ('rec_date DESC');
		$select->limit (1,0);
		//echo $select->__toString();exit;
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);
		
		$boiler_other_info['boiler_boilerstatus'] = 'NA';
		$boiler_other_info['boiler_boilerdata'] = 'NA';
		$boiler_other_info['boiler_demandstatus'] = 'NA';
		$boiler_other_info['boiler_demanddata'] = 'NA';
		$boiler_other_info['boiler_requeststatus'] = 'NA';
		$boiler_other_info['boiler_requestdata'] = 'NA';
		$boiler_other_info['boiler_automaticstatus'] = 'NA';
		$boiler_other_info['boiler_automaticdata'] = 'NA';
		$boiler_other_info['boiler_booststatus'] = 'NA';
		$boiler_other_info['boiler_boostdata'] = 'NA';

		if (isset ($rows['macid']) && $rows['macid'] != '')
		{
			// Channel 9 - Boiler
			if (preg_match ('/P$/', $rows['ch9']))
				$boiler_other_info['boiler_boilerstatus'] = '1';
			else
				$boiler_other_info['boiler_boilerstatus'] = '0';
			$boiler_other_info['boiler_boilerdata'] = (float) preg_replace ('/p$/i', '', $rows['ch9']);
			
			// Channel 7 - Demand
			if (preg_match ('/P$/', $rows['ch7']))
				$boiler_other_info['boiler_demandstatus'] = '0';
			else
				$boiler_other_info['boiler_demandstatus'] = '1';
			$boiler_other_info['boiler_demanddata'] = (float) preg_replace ('/p$/i', '', $rows['ch7']);
			
			// Channel 8 - Request
			if (preg_match ('/P$/', $rows['ch8']))
				$boiler_other_info['boiler_requeststatus'] = '1';
			else
				$boiler_other_info['boiler_requeststatus'] = '0';
			$boiler_other_info['boiler_requestdata'] = (float) preg_replace ('/p$/i', '', $rows['ch8']);
			
			// Channel 5- Automatic
			if (preg_match ('/P$/', $rows['ch5']))
				$boiler_other_info['boiler_automaticstatus'] = '1';
			else
				$boiler_other_info['boiler_automaticstatus'] = '0';
			$boiler_other_info['boiler_automaticdata'] = (float) preg_replace ('/p$/i', '', $rows['ch5']);
			
			// Channel 6 - 2 Hours Boost
			if (preg_match ('/P$/', $rows['ch6']))
				$boiler_other_info['boiler_booststatus'] = '1';
			else
				$boiler_other_info['boiler_booststatus'] = '0';
			$boiler_other_info['boiler_boostdata'] = (float) preg_replace ('/p$/i', '', $rows['ch6']);
		}
		
		$meter_data_result['boilder_other_info'] = $boiler_other_info;
		
		return $meter_data_result;
	}

	public function zoominboilerAction () {
		$meter_id = (int) $this->_request->getParam ('id');
		$site_info = $this->_request->getParam ('site_id');
		$site_id = 0;
		if (strpos ($site_info, '-') !== false)
		{
			$site_id = (int) substr($site_info, strpos ($site_info, '-')+1);
		}

		if ($site_id == 0)
		{
			throw new Zend_Controller_Action_Exception('Site not found', 404);
		}

		// Check whether site is assigned
		if ($this->i4energy_session->isforward != 'true')
		{
			if (!($this->i4energy_session->user_role == ADMIN_USER || in_array ($site_id, $this->i4energy_session->user_assignsites)))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
		}

		if ($meter_id > 0)
		{
			$meter = new I4energy_Model_Meters();
			$meter->init ($meter_id);

			if ($meter->getId() > 0)
			{
				$meter->setUpdatedDate(
					I4energy_Model_Utils_DateUtils::getFormattedDate ($meter->getUpdatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$meter->setCreatedDate(
					I4energy_Model_Utils_DateUtils::getFormattedDate ($meter->getCreatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$this->view->meter = $meter;
			}
			else
			{
				throw new Zend_Controller_Action_Exception('Meter not found', 404);
			}
		}
		else
		{
			throw new Zend_Controller_Action_Exception('Meter not found', 404);
		}

		if ($site_id > 0)
		{
			$site = new I4energy_Model_Sites();
			$site->init ($site_id);

			if ($site->getId() > 0)
			{
				$site->setUpdatedDate(
					I4energy_Model_Utils_DateUtils::getFormattedDate ($site->getUpdatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$site->setCreatedDate(
					I4energy_Model_Utils_DateUtils::getFormattedDate ($site->getCreatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$this->view->site = $site;
			}
			else
			{
				throw new Zend_Controller_Action_Exception('Site not found', 404);
			}
		}
		else
		{
			throw new Zend_Controller_Action_Exception('Site not found', 404);
		}

		// Get meter data
		if ($meter_id > 0) {
			$this->view->display_unit = I4energy_Model_Meters::getDisplayUnit($meter->getUnit());

			$meter_alters = new I4energy_Model_MeterAlerts();
			$meter_alters->init($meter_id);
			$this->view->meter_alert = $meter_alters;

			$meter_attrs = new I4energy_Model_MeterAttributes();
			$meter_attrs->init($meter_id);
			$this->view->meter_attrs = $meter_attrs;

			$meter_data = new I4energy_Model_MeterData();

			$start_date = $this->_request->getParam ('start_date');
			if ($start_date != '')
				$start_date = @date('Y-m-d', @strtotime($start_date));
			else
				$start_date = @date ('Y-m-d');

			$this->view->start_date = $start_date;
			$this->view->boiler_chart_data = self::getBoilerZoomInChartData($meter, $meter_data, $start_date . ' 00:00:00', $start_date . ' 23:59:59');
		}
	}

	public static function getBoilerZoomInChartData (&$meter, &$meter_data, $start_date, $end_date = null)
	{
		$field_to_query = 'meter_data.data';
		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == '' || $offset == null)
			$offset = '';
		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';

		$meter_attrs = new I4energy_Model_MeterAttributes();
		$meter_attrs->init ($meter->getId());

		$convert_unit_attrs = array();
		$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();

		$defaultTimeZone = @date_default_timezone_get();
		@date_default_timezone_set($meter->getTimezone());
		$timezone_diff = @date('P');
		@date_default_timezone_set($defaultTimeZone);

		// For boiler, show line chart using 4 channels
		// Ch10 - Outside temp
		// Ch12 - Heating flow
		// Ch11 - Heating return
		// Ch13 - Hot Water temp
		$boiler_chart = array('outside_temp' => array(), 'return_temp' => array(), 'heating_flow' => array(), 'hot_water' => array(), 'demand' => array());
		$all_null = true;
		for ($i=0; $i<1440; $i++)
		{
			$formatedTime = I4energy_Model_Utils_DateUtils::secondsToHHMM ($i*60);
			$hourly_chart_data[$formatedTime] = -1;
			$boiler_chart['outside_temp'][$formatedTime] = NULL;
			$boiler_chart['return_temp'][$formatedTime] = NULL;
			$boiler_chart['heating_flow'][$formatedTime] = NULL;
			$boiler_chart['hot_water'][$formatedTime] = NULL;
			$boiler_chart['demand'][$formatedTime] = NULL;
		}

		// Outside
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\')', 'total' => $field_to_query));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=10');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->order ('rec_date ASC');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$all_null = false;
			$boiler_chart['outside_temp'][@date ('H:i', @strtotime($row['hour_of']))] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}

		// Heating Flow
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\')', 'total' => $field_to_query));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=12');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->order ('rec_date ASC');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$all_null = false;
			$boiler_chart['heating_flow'][@date ('H:i', @strtotime($row['hour_of']))] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}

		// Heating Return
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\')', 'total' => $field_to_query));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=11');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->order ('rec_date ASC');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$all_null = false;
			$boiler_chart['return_temp'][@date ('H:i', @strtotime($row['hour_of']))] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}

		// Hot Water
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\')', 'total' => $field_to_query));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=13');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$select->order ('rec_date ASC');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		foreach ($rows as $row)
		{
			$all_null = false;
			$boiler_chart['hot_water'][@date ('H:i', @strtotime($row['hour_of']))] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}

		// Demand
		// Boiler Demand data1 = 1 means OFF and data1 = 0 means ON
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\')', 'total' => 'data1'));
		$select->where ('macid=?', $meter->getMac());
		$select->where ('moduleid=?', $meter->getModuleNo());
		$select->where ('slotno=?', $meter->getSlotNo());
		$select->where ('channelno=7');
		$select->where ('rec_date >= ?', $start_date);
		if ($end_date != null && $end_date != '')
			$select->where ('rec_date <= ?', $end_date);
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		$select->order ('rec_date ASC');
		foreach ($rows as $row)
		{
			$all_null = false;

			// Demand is not in temprature, it is just on and off
			// So if in perticular hour, if on count is more than off count, set current demand to 2 so
			// demand line will be visible on chart
			$current_demand = 0;
			if (1-$row['total'] > 0)
				$current_demand = 2;
			$boiler_chart['demand'][@date ('H:i', @strtotime($row['hour_of']))] = $current_demand; //(float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
		}

		$final_boiler_chart = array('outside_temp' => array(), 'return_temp' => array(), 'heating_flow' => array(), 'hot_water' => array(), 'demand' => array());
		foreach ($boiler_chart as $chart_type => $data) {
			foreach ($data as $time => $value) {
				if ($all_null)
					$value = 0;
				$final_boiler_chart[$chart_type][] = array ('time' => $time, 'data' => $value);
			}
		}
		unset ($boiler_chart);

		return $final_boiler_chart;
	}

	public static function getHourRunMeterData (I4energy_Model_Meters &$meter, $install_date = '', I4energy_Model_MeterAttributes &$meter_attr = null) {
		$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";

		if ($install_date != '' && @strtotime ($install_date) > 1356998400)
			$common_where .= " AND rec_date >= '{$install_date}'";

		//$meter_attrs = new I4energy_Model_MeterAttributes();
		//$meter_attrs->init ($meter->getId());

		$convert_unit_attrs = array();
		$convert_unit_attrs['ref_voltage'] = $meter_attr->getReferenceVoltage();

		$meter_data = new I4energy_Model_MeterData();

		// Consider Offset
		$field_to_query = 'meter_data.data';
		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == '' || $offset == null)
			$offset = '';
		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';

		$meter_data_result = array ('today_run' => '', 'last24h_run' => '', 'today_close' => '', 'total_run' => '', 'pulse1' => '', 'pulse1_status' => '', 'pulse2' => '', 'pulse2_status' => '', 'pulse3' => '', 'pulse3_status' => '', 'current_status' => '', 'webdial_reading' => '', 'hourly_chart_data' => array());

		// current_status to show Running/Off or On/Off under web dial
		$meter_data_result['current_status'] = self::getHourRunNowStatus($meter);
		// current_temparature to show tempratature in web dial
		$meter_data_result['webdial_reading'] = self::getHourRunWebdialReading($meter, $meter_attr);


		$select = $meter_data->getMapper()->getSelect();

		$sql_query = "SELECT rec_date, `data`, data1,statusChanged, macid, moduleid, slotno, channelno
			FROM
			( SELECT (@statusPre <> data1 AND @macidPre=macid AND @moduleidPre=moduleid AND @slotnoPre=slotno AND @channelnoPre=channelno) AS statusChanged
					 , rec_date, `data`, data1, macid, moduleid, slotno, channelno
					 , @statusPre := data1
					 , @macidPre := macid
					 , @moduleidPre := moduleid
					 , @slotnoPre := slotno
					 , @channelnoPre := channelno
				FROM meter_data
				   , (SELECT @statusPre:=NULL, @macidPre:=NULL, @moduleidPre:=NULL, @slotnoPre:=NULL, @channelnoPre:=NULL) AS d
				   WHERE {$common_where}
				ORDER BY rec_date
			  ) AS good
			WHERE statusChanged";

		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($sql_query);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

		$total_on = 0;
		$total_off = 0;
		$last_ping = null;
		$last24H_date = I4energy_Model_Utils_DateUtils::getFormattedDate(@gmdate ('Y-m-d H:i:s', @strtotime('-24 hour')), 'U', $meter->getTimezone());
		$last24H_on = 0;
		$last_status = -1;

		foreach ($rows as $row) {
			if ($last_ping == null) {
				$last_ping = @strtotime($row['rec_date']);
			}

			// meter was ON but its just turned OFF, so this should be considers as on time
			if ($row['data1'] == 0) {
				$total_on += @strtotime ($row['rec_date']) - $last_ping;

				//if ($last_ping >= $last24H_date)
				//	$last24H_on += @strtotime ($row['rec_date']) - $last_ping;
			}

			// meter was OFF but its just turned ON, so this should be considered as off time
			if ($row['data1'] == 1) {
				$total_off += @strtotime ($row['rec_date']) - $last_ping;
			}

			$last_ping = @strtotime($row['rec_date']);
			$last_status = $row['data1'];
		}
		unset ($rows);

		$last_ping_date = '';
		$last24h_reading_qry = "SELECT data1,rec_date from meter_data WHERE {$common_where} AND rec_date>='" . @date('Y-m-d H:i:s', $last24H_date) . "' ORDER BY rec_date";
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($last24h_reading_qry);
		$last24h_reading_rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		$first_reading = -1;

		foreach ($last24h_reading_rows as $row) {
			if ($first_reading == -1 && $row['data1'] == '0') {
				$last24H_on += @strtotime($row['rec_date']) - $last24H_date;
				$first_reading = $row['data1'];
			}
			if ($row['data1'] == '1' && $last_ping_date != '') {
				$last24H_on += @strtotime ($row['rec_date']) - @strtotime ($last_ping_date);
			}

			$last_ping_date = $row['rec_date'];
		}

		$latest_reading_qry = "SELECT data1,rec_date from meter_data WHERE {$common_where} ORDER BY rec_date DESC limit 1";
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($latest_reading_qry);
		$latest_reading_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);

		$diff_from_lastping = (@gmmktime() - @strtotime ($latest_reading_row['rec_date']));
		// If last reading is within 65 minutes
		if ($diff_from_lastping <= 3900 && $last_status > -1) {
			if ($last_status !=  $latest_reading_row['data1']) {
				if ($latest_reading_row['data1'] == '0') {
					//$last24H_on += $diff_from_lastping;
					$total_on += $diff_from_lastping;
				} else {
					$total_off += $diff_from_lastping;
				}
			} else {
				if ($latest_reading_row['data1'] == '1') {
					//$last24H_on += $diff_from_lastping;
					$total_on += $diff_from_lastping;
				} else {
					$total_off += $diff_from_lastping;
				}
			}
		}

		$meter_data_result['today_run'] = $total_on;
		$meter_data_result['last24h_run'] = $last24H_on;
		$meter_data_result['today_close'] = $total_off;
		$meter_data_result['total_run'] = ($total_on + $total_off);

		$pulse_status = self::getHourRunPulseReadings ($meter, $install_date);
		$meter_data_result = array_merge($meter_data_result, $pulse_status);

		$meter_data_result['hourly_chart_data'] = self::getHourRunChartData ($meter, $install_date);

		return $meter_data_result;
	}

	public static function getHourRunNowStatus (I4energy_Model_Meters &$meter) {
		$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";

		$meter_data = new I4energy_Model_MeterData();
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('current_status' => 'meter_data.data1'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-65 minutes')));
		$select->order ('rec_date DESC');
		$select->limit (1,0);
		//echo $select->__toString();exit;
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);

		return $rows['current_status'];
	}

	public static function getHourRunWebdialReading (I4energy_Model_Meters &$meter, I4energy_Model_MeterAttributes &$meter_attr) {
		$webDialChannel = $meter_attr->getHourRunWebDialChannel();
		if ($webDialChannel == null || $webDialChannel == '')
			$webDialChannel= 0;
		$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$webDialChannel}";

		$meter_data = new I4energy_Model_MeterData();

		// Consider Offset
		$field_to_query = 'meter_data.data';
		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == '' || $offset == null)
			$offset = '';
		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';

		$webdial_unit = $meter_attr->getHourRunWebDialUnit();
		if ($webdial_unit == 'c' || $webdial_unit == 'ma' || $webdial_unit == 'v' || $webdial_unit == 'rh')
		{
			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('total' => $field_to_query));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-65 minutes')));
			$select->order ('rec_date DESC');
			$select->limit (1,0);
			//echo $select->__toString();exit;
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);

			return $rows['total'];
		}
		else if ($webdial_unit == 'onoff' || $webdial_unit == 'hrun' || $webdial_unit == 'hopen' || $webdial_unit == 'kwh' || $webdial_unit == 'm3' || $webdial_unit == 'l' || $webdial_unit == 'amps')
		{
			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('total' => 'SUM(' . $field_to_query . ')'));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-1 hour')));
			//echo '<!-- AMIT1: ' . $select->__toString() . ' -->';
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);

			return $rows['total'];
		}
		else
		{
			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('total' => 'AVG(' . $field_to_query . ')'));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-1 hour')));
			//echo $select->__toString();
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);

			return $rows['total'];
		}

		return null;
	}

	public static function getHourRunPulseReadings (I4energy_Model_Meters &$meter, $install_date = '') {
		$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()}";

		if ($install_date != '' && @strtotime ($install_date) > 1356998400)
			$common_where .= " AND rec_date >= '{$install_date}'";

		$meter_data_result = array ('pulse1' => '', 'pulse1_status' => '', 'pulse2' => '', 'pulse2_status' => '', 'pulse3' => '', 'pulse3_status' => '');

		$meter_data = new I4energy_Model_MeterData();
		$select = $meter_data->getMapper()->getSelect();

		// Pulse 1 / Channel 1
		$sql_query = "SELECT rec_date, `data`, data1,statusChanged, macid, moduleid, slotno, channelno
			FROM
			( SELECT (@statusPre <> data1 AND @macidPre=macid AND @moduleidPre=moduleid AND @slotnoPre=slotno AND @channelnoPre=channelno) AS statusChanged
					 , rec_date, `data`, data1, macid, moduleid, slotno, channelno
					 , @statusPre := data1
					 , @macidPre := macid
					 , @moduleidPre := moduleid
					 , @slotnoPre := slotno
					 , @channelnoPre := channelno
				FROM meter_data
				   , (SELECT @statusPre:=NULL, @macidPre:=NULL, @moduleidPre:=NULL, @slotnoPre:=NULL, @channelnoPre:=NULL) AS d
				   WHERE {$common_where} AND channelno=1
				ORDER BY rec_date
			  ) AS good
			WHERE statusChanged";

		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($sql_query);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		$meter_data_result['pulse1'] = sizeof ($rows);
		unset ($rows);

		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('current_status' => 'meter_data.data1'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-65 minutes')));
		$select->where ('channelno=1');
		$select->order ('rec_date DESC');
		$select->limit (1,0);
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);
		$meter_data_result['pulse1_status'] = $rows['current_status'];

		// Pulse 2 / Channel 3
		$sql_query = "SELECT rec_date, `data`, data1,statusChanged, macid, moduleid, slotno, channelno
			FROM
			( SELECT (@statusPre <> data1 AND @macidPre=macid AND @moduleidPre=moduleid AND @slotnoPre=slotno AND @channelnoPre=channelno) AS statusChanged
					 , rec_date, `data`, data1, macid, moduleid, slotno, channelno
					 , @statusPre := data1
					 , @macidPre := macid
					 , @moduleidPre := moduleid
					 , @slotnoPre := slotno
					 , @channelnoPre := channelno
				FROM meter_data
				   , (SELECT @statusPre:=NULL, @macidPre:=NULL, @moduleidPre:=NULL, @slotnoPre:=NULL, @channelnoPre:=NULL) AS d
				   WHERE {$common_where} AND channelno=2
				ORDER BY rec_date
			  ) AS good
			WHERE statusChanged";

		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($sql_query);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		$meter_data_result['pulse2'] = sizeof ($rows);
		unset ($rows);

		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('current_status' => 'meter_data.data1'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-65 minutes')));
		$select->where ('channelno=2');
		$select->order ('rec_date DESC');
		$select->limit (1,0);
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);
		$meter_data_result['pulse2_status'] = $rows['current_status'];

		// Pulse 3 / Channel 3
		$sql_query = "SELECT rec_date, `data`, data1,statusChanged, macid, moduleid, slotno, channelno
			FROM
			( SELECT (@statusPre <> data1 AND @macidPre=macid AND @moduleidPre=moduleid AND @slotnoPre=slotno AND @channelnoPre=channelno) AS statusChanged
					 , rec_date, `data`, data1, macid, moduleid, slotno, channelno
					 , @statusPre := data1
					 , @macidPre := macid
					 , @moduleidPre := moduleid
					 , @slotnoPre := slotno
					 , @channelnoPre := channelno
				FROM meter_data
				   , (SELECT @statusPre:=NULL, @macidPre:=NULL, @moduleidPre:=NULL, @slotnoPre:=NULL, @channelnoPre:=NULL) AS d
				   WHERE {$common_where} AND channelno=3
				ORDER BY rec_date
			  ) AS good
			WHERE statusChanged";

		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($sql_query);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		$meter_data_result['pulse3'] = sizeof ($rows);
		unset ($rows);

		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('current_status' => 'meter_data.data1'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', @gmdate ('Y-m-d H:i:s', @strtotime('-65 minutes')));
		$select->where ('channelno=3');
		$select->order ('rec_date DESC');
		$select->limit (1,0);
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetch(Zend_Db::FETCH_ASSOC);
		$meter_data_result['pulse3_status'] = $rows['current_status'];

		return $meter_data_result;
	}

	public static function getHourRunChartData (I4energy_Model_Meters &$meter, $install_date = '', $start_date = '') {
		$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";

		if ($install_date != '' && @strtotime ($install_date) > 1356998400)
			$common_where .= " AND rec_date >= '{$install_date}'";

		// Today's usage chart data (hourly)
		$hourly_chart_data = array();
		$i = 0;
		while ( $i < 1440 ) // 1 day has 1440 minutes
		{
			$formatedTime = I4energy_Model_Utils_DateUtils::secondsToHHMM ($i*60);
			$hourly_chart_data[$formatedTime] = -1;
			$i++;
		}

		$defaultTimeZone = @date_default_timezone_get();
		@date_default_timezone_set($meter->getTimezone());
		$timezone_diff = @date('P');
		@date_default_timezone_set($defaultTimeZone);

		if ($start_date == '')
			$start_date = @gmdate('Y-m-d');

		$meter_data = new I4energy_Model_MeterData();
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('rec_date', 'data1' ));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', $start_date . ' 00:00:00' );
		$select->where ('rec_date <= ?', $start_date . ' 23:59:59' );
		$select->order ('rec_date ASC');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

		foreach ($rows as $row)
		{
			$hourMinFromDate = @date ('H:i', @strtotime($row['rec_date']));
			$hourly_chart_data[$hourMinFromDate] = $row['data1'];
		}

		// on/off status is conteneous. We receive pings at every 5 or 10 minutes
		// as we are showing status on every minute, we need to assume that status as same between
		// all minites till we receive next ping, but catch is, if we do not receive any pings for
		// 20 minutes, then consider it as no ping and this should be rendered on cart
		$pings = 0;
		$lastStatus = -1;
		foreach ($hourly_chart_data as $key => $value) {
			if ($value > -1) {
				$lastStatus = $value;
				$pings = 0;
			}

			$pings++;
			if ($pings > 20) {
				// We are missing ping from last 20 mins
				// change status to -1 - no ping
				$lastStatus = -1;
			}
			$hourly_chart_data[$key] = $lastStatus;
		}

		$meter_data_result['hourly_chart_data'] = $hourly_chart_data;

		return $hourly_chart_data;
	}

	function boilerdataAction()
	{
		$start_date = $this->_request->getParam ('date');
		$meter_id = $this->_request->getParam ('meter_id');
		$end_date = $start_date . ' 23:59:59';
		$start_date = $start_date . ' 00:00:00';
		
		$meter = new I4energy_Model_Meters();
		$meter->init ($meter_id);
		
		if ($meter->getId() > 0)
		{
			$meter_data = new I4energy_Model_MeterData();
			
			$boiler_data = self::getBoilerData ($meter, $meter_data, $start_date, $end_date);
			echo json_encode ($boiler_data);
			exit;
		}
		
		echo json_encode(array());
		exit;
	}

	function hourrunchartdataAction()
	{
		$start_date = $this->_request->getParam ('date');
		$meter_id = $this->_request->getParam ('meter_id');

		$meter = new I4energy_Model_Meters();
		$meter->init ($meter_id);

		if ($meter->getId() > 0)
		{
			$meter_attrs = new I4energy_Model_MeterAttributes();
			$meter_attrs->init ($meter_id);

			$chart_data = self::getHourRunChartData ($meter, $meter_attrs->getInstallDate(), $start_date);
			echo json_encode (array ('chart_data' => $chart_data));
			exit;
		}

		echo json_encode(array());
		exit;
	}
	
	function gethourlychartdataAction()
	{
		$meter_id = $this->_request->getParam ('meter_id');
		$chart_hour=$this->_request->getParam('chart_hour');
		$today_date=$this->_request->getParam('today_date');

		$endtime=$chart_hour+1;
		
		$meter = new I4energy_Model_Meters();
		$meter->init($meter_id);
		$common_where='';
		if ($meter->getId() > 0)
		{
			$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";
		}
		
		$meter_attrs = new I4energy_Model_MeterAttributes();
		$meter_attrs->init ($meter->getId());
		
		$convert_unit_attrs = array();
		$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();
		
		$start_datetime_gmt=I4energy_Model_Utils_DateUtils::getFormattedDate("$today_date $chart_hour:00:00", "Y-m-d H:i:s",'GMT',$meter->getTimezone());
		$end_datetime_gmt=I4energy_Model_Utils_DateUtils::getFormattedDate("$today_date $endtime:00:00", "Y-m-d H:i:s",'GMT',$meter->getTimezone());	
		
		$defaultTimeZone = @date_default_timezone_get();
		@date_default_timezone_set($meter->getTimezone());
		$timezone_diff = @date('P');
		@date_default_timezone_set($defaultTimeZone);

		$meter_data = new I4energy_Model_MeterData();
		// Today's usage chart data (hourly)
		
		$field_to_query = 'meter_data.data';
		
		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == null)
			$offset = '';
		
		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';
		
		$hourly_chart_data = array();
		for ($i=0; $i<60; $i++)
			$hourly_chart_data[str_pad ($i, 2, '0', STR_PAD_LEFT)] = 0;
			 
		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('minute_of' => 'EXTRACT(minute FROM CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\'))','date_of' => 'DATE(rec_date)','total' => $field_to_query ));
		$select->where ($common_where);
		$select->where ('rec_date >= ?',$start_datetime_gmt );
		$select->where ('rec_date <= ?',$end_datetime_gmt );
		// echo $select;
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		
		//print_r($rows);
		
		foreach ($rows as $row)
			$hourly_chart_data[str_pad ($row['minute_of'], 2, '0', STR_PAD_LEFT)] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
	
		$response=array();
		$chart_date=explode(" ", I4energy_Model_Utils_DateUtils::getFormattedDate($today_date, "Y-m-d 00:00:00",'GMT' ,$meter->getTimezone()));
		$response['date']=$chart_date[0];
		$response['data']=$hourly_chart_data;
		//print_r($hourly_chart_data);exit;
		echo json_encode($response);
		exit;
	
	}
	
	function gethourlychartdatabydayAction()
	{
		$meter_id = $this->_request->getParam ('meter_id');
	    $chart_date=$this->_request->getParam('chart_date');
	    
	    $chart_start_date = $chart_date." 00:00:00";
	    $chart_end_date = $chart_date." 23:59:59";
	    $meter = new I4energy_Model_Meters();
	    $meter->init($meter_id);
	    
	    $common_where='';
	    if ($meter->getId() > 0)
	    {
	    	$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";
	    }

	    $meter_attrs = new I4energy_Model_MeterAttributes();
	    $meter_attrs->init ($meter->getId());
	    
	    $convert_unit_attrs = array();
	    $convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();
	    
	    $defaultTimeZone = @date_default_timezone_get();
	    @date_default_timezone_set($meter->getTimezone());
	    $timezone_diff = @date('P');
	    @date_default_timezone_set($defaultTimeZone);
	    
	    $meter_data = new I4energy_Model_MeterData();
	    
	    $group_function = 'AVG';
	    if ($meter->getType() == 'number' || ($meter->getUnit() == 'kwh' && $meter->getType() == 'power') || $meter->getType() == 'gas' || $meter->getType() == 'heat' || $meter->getType() == 'amps' || $meter->getType() == 'water')
	    	$group_function = 'SUM';
	    
	    $field_to_query = 'meter_data.data';
	    
	    $offset = $meter->getOffset();
	    if ($offset instanceof Zend_Db_Expr || $offset == null)
	    	$offset = '';
	    
	    if ($offset != '')
	    	$field_to_query = 'meter_data.data + (' . $offset . ')';
	    
	    // Today's usage chart data (hourly)
	    $hourly_chart_data = array();
	    for ($i=0; $i<24; $i++)
	    	$hourly_chart_data[str_pad ($i, 2, '0', STR_PAD_LEFT)] = 0;
	    		
	    $select = $meter_data->getMapper()->getSelect();
	    $select->from ('meter_data', array ('hour_of' => 'EXTRACT(HOUR FROM CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\'))', 'total' => $group_function . '(' . $field_to_query . ')'));
	    $select->where ($common_where);
	    $select->where ('rec_date >= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($chart_start_date, 'Y-m-d H:i:s', 'GMT' ,$meter->getTimezone()));
	    $select->where ('rec_date <= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($chart_end_date, 'Y-m-d H:i:s', 'GMT' ,$meter->getTimezone()));
	    	
	    $select->group ('hour_of');
	    $stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
	    $rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
	    
	    foreach ($rows as $row)
	    	$hourly_chart_data[str_pad ($row['hour_of'], 2, '0', STR_PAD_LEFT)] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
	    	
	    $response=array();
	    $chart_date=explode(" ", $chart_date);
	    $response['date']=$chart_date[0];
	    $response['data']=$hourly_chart_data;
	    echo json_encode($response);
		exit;
	
	}
	function getmetermonthlydatabydayAction(){
		$meter_id = $this->_request->getParam ('meter_id');
		$chart_month = $this->_request->getParam('chart_month');

		$monthArr = array ('jan'=>'01', 'feb' => '02', 'mar' => '03', 'apr' => '04', 'may' => '05', 'jun' => '06', 'jul' => '07', 'aug' => '08', 'sep' => '09', 'oct' => '10', 'nov' => '11', 'dec' => '12');
		$yearMonthArr = explode(' ', $chart_month);

		$chart_start_date = $yearMonthArr[1] . '-' . $monthArr[strtolower($yearMonthArr[0])] . "-01";
		$chart_end_date = $yearMonthArr[1] . '-' . $monthArr[strtolower($yearMonthArr[0])] . "-31";

		$meter = new I4energy_Model_Meters();
		$meter->init($meter_id);
		 
		$common_where='';
		if ($meter->getId() > 0)
		{
			$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";
		}
		
		$meter_attrs = new I4energy_Model_MeterAttributes();
		$meter_attrs->init ($meter->getId());
		 
		$convert_unit_attrs = array();
		$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();
		 
		$defaultTimeZone = @date_default_timezone_get();

		@date_default_timezone_set($meter->getTimezone());
		$timezone_diff = @date('P');

    @date_default_timezone_set($defaultTimeZone);

		$meter_data = new I4energy_Model_MeterData();
		 
		$group_function = 'AVG';
		if ($meter->getType() == 'number' || ($meter->getUnit() == 'kwh' && $meter->getType() == 'power') || $meter->getType() == 'gas' || $meter->getType() == 'heat' || $meter->getType() == 'amps' || $meter->getType() == 'water')
			$group_function = 'SUM';
		 
		$field_to_query = 'meter_data.data';
		 
		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == null)
			$offset = '';
		 
		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';

		// Last 31 days meter data
		$daily_chart_data = array();
		$dtCnt = 1;
		$totalDaysInMonth = cal_days_in_month(CAL_GREGORIAN, $monthArr[strtolower($yearMonthArr[0])], $yearMonthArr[1]);

		while ($dtCnt <= $totalDaysInMonth)
		{
			$daily_chart_data[@gmdate ('Y-m-d', @strtotime ("+{$dtCnt} day", @strtotime ($chart_start_date)))] = 0;
			$dtCnt++;
		}

		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('date_of' => 'DATE(rec_date)', 'total' => $group_function . '(' . $field_to_query . ')'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', $chart_start_date);
		$select->where ('rec_date <= ?', $chart_end_date . " 23:59:59");
		$select->group ('date_of');

		//echo '<!-- AMIT4: ' . $select->__toString() . ' -->';
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);

    $rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

    foreach ($rows as $row)
			$daily_chart_data[$row['date_of']] = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
			
		ksort ($daily_chart_data);
		
		$response=array();
		$response['date']=$chart_month;
		$response['data']=$daily_chart_data;

    echo json_encode($response);
		exit;
	}
	
	function homereadingsAction ()
	{

		$meter_ids = $this->_request->getParam ('ids');
		$reading_response = array();
		
		if ($meter_ids != '')
		{
			$meter_ids_arr = explode (',', $meter_ids);
			//$meter_ids_arr = [171];
			
			foreach ($meter_ids_arr as $meter_id)
			{
				$meter = new I4energy_Model_Meters();
				$meter->init ($meter_id);

				//echo "<br>Site id::::<br>";
				//print_r ($meter->getSiteId());

				
				if ($meter->getId() > 0)
				{

					if ($meter->getLatestReadingTime() != '' && $meter->getType() != 'hourrun')
					{

						// echo "Raw reading:::";
						// print_r ($raw_reading);
						// exit;

						// Now
						$raw_reading = self::getMeterNowUsage($meter);
						
						
						$meter_attrs = new I4energy_Model_MeterAttributes();
						$meter_attrs->init ($meter->getId());
						
						$convert_unit_attrs = array();
						$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();
						
						$latest_reading = I4energy_Model_Meters::convertToUnit ($raw_reading, $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
						
						$meter_alert = new I4energy_Model_MeterAlerts();
						$meter_alert->init ($meter->getId());
						
						$css_class = '';
						if ($meter_alert->getStatusGoodMin() != $meter_alert->getStatusGoodMax())
						{
							if ($meter_alert->getStatusGoodMin() != '' && $meter_alert->getStatusGoodMax() != '' && $latest_reading >= $meter_alert->getStatusGoodMin() && $latest_reading <= $meter_alert->getStatusGoodMax())
							{
								$css_class = 'internal-border-green';
							}
							else if ($meter_alert->getStatusWarningMin() != '' && $meter_alert->getStatusWarningMax() != '' && $latest_reading >= $meter_alert->getStatusWarningMin() && $latest_reading <= $meter_alert->getStatusWarningMax())
							{
								$css_class = 'internal-border-yellow';
							}
							else if ($meter_alert->getStatusAlertMin() != '' && $meter_alert->getStatusAlertMax() != '' && $latest_reading >= $meter_alert->getStatusAlertMin() && $latest_reading <= $meter_alert->getStatusAlertMax())
							{
								$css_class = 'internal-border-red';
							}
							else if ($meter_alert->getStatusAlertMax() != '' && $latest_reading > $meter_alert->getStatusAlertMax())
							{
								$css_class = 'internal-border-red';
							}
							else if ($meter_alert->getStatusGoodMin() != '' && $latest_reading < $meter_alert->getStatusGoodMin())
							{
								$css_class = 'internal-border-red';
							}
						}
						else
						{
							$css_class = 'internal-border-bold';
						}
						
						$display_reading = '';
						if ($latest_reading != null && $latest_reading != '')
						{
							$unit_per = ' / H';
							if ($meter->getUnit() == 'amps' || $meter->getUnit() == 'c' || $meter->getUnit() == 'rh' || $meter->getUnit() == 'ma' || $meter->getUnit() == 'v' || ($meter->getType() == 'power' && $meter->getUnit() == 'kwh') || $meter->getType() == 'heat')
								$unit_per = '';
							
							$display_reading = $latest_reading . ' ' . I4energy_Model_Meters::getDisplayUnit ($meter->getUnit()) . $unit_per;
						}
						
						$reading_response[$meter->getId()] = array ('class' => $css_class, 'reading' => $latest_reading, 'display_reading' => $display_reading);
					}
					else if ($meter->getType() == 'hourrun') {
						$now_reading = self::getHourRunNowStatus ($meter);
						$css_class = 'internal-border-yellow';
						$latest_reading = 'No Ping';

						if ($meter->getUnit() == 'hrun')
						{
							$onText = "RUNNING";
							$offText = "STOP";
						}
						else{
							$onText = "OPEN";
							$offText = "CLOSED";
						}
						if ($now_reading == '1') {
							$css_class = 'internal-border-red';
							$latest_reading = $onText;
						}
						else if ($now_reading == '0') {
							$css_class = 'internal-border-green';
							$latest_reading = $offText;
						}

						$reading_response[$meter->getId()] = array ('class' => $css_class, 'reading' => $latest_reading, 'display_reading' => $latest_reading);
					}
				}
			}
		}

		echo json_encode ($reading_response);
		exit;
	}
	
	function addAction()
	{
		$site_info = $this->_request->getParam ('site_id');
		$meter_id = (int) $this->_request->getParam ('id');
		$update = false;
		if($meter_id>0){
			$update=true;
		}
		
		$site_id = 0;
		if (strpos ($site_info, '-') !== false)
		{
			$site_id = (int) substr($site_info, strpos ($site_info, '-')+1);
		}
		
		if ($site_id == 0)
		{
			throw new Zend_Controller_Action_Exception('Site not found', 404);
		}
		
		if (isset($meter_id) && $meter_id > 0)
		{
			if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'meter_edit', $this->i4energy_session->user_priviledge))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
				
			// Check whether site is assigned
			if (!($this->i4energy_session->user_role == ADMIN_USER || in_array ($site_id, $this->i4energy_session->user_assignsites)))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
		}
		else
		{
			if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'meter_add', $this->i4energy_session->user_priviledge))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
		}
		
		if($this->_request->isPost())
		{
			$response=array();
			$name = $this->_request->getParam ('meter_name');
			if($name == ""){
				$response['data']['meter_name'] ='null';
			}else{
				$response['data']['meter_name'] ='valid';
			}
			$users = $this->_request->getParam('users');
			
			$desc = $this->_request->getParam ('meter_desc');
			$location = $this->_request->getParam ('meter_location');
			$mac = $this->_request->getParam ('mac_code');
			if($mac == ""){
				$response['data']['mac_code'] ='null';
			}else{
				$response['data']['mac_code'] ='valid';
			}
			
			$type = $this->_request->getParam ('meter_type');
			if($type == ""){
				$response['data']['meter_type'] ='null';
			}else{
				$response['data']['meter_type'] ='valid';
			}
			
			$moduleno = $this->_request->getParam ('module_no');
			if($moduleno == ""){
				$response['data']['module_no'] ='null';
			}else{
				$response['data']['module_no'] ='valid';
			}
			
			$slotno = $this->_request->getParam ('slot_no');
			if($slotno == ""){
				$response['data']['slot_no'] ='null';
			}else{
				$response['data']['slot_no'] ='valid';
			}
			
			$channelno = $this->_request->getParam ('channel_no');
			if($channelno == ""){
				$response['data']['channel_no'] ='null';
			}else{
				$response['data']['channel_no'] ='valid';
			}
			
			$conversationfactor = $this->_request->getParam ('conversation_factor');
			$unit = $this->_request->getParam ('meter_unit');
			if($unit == ""){
				$response['data']['meter_unit'] ='null';
			}else{
				$response['data']['meter_unit'] ='valid';
			}
			$offset = $this->_request->getParam ('offset_number');
			$cost = $this->_request->getParam ('cost');
			$carbonsavings = $this->_request->getParam ('carbon_savings');
			$timezone = $this->_request->getParam ('meter_timezone');
			$pingrate = $this->_request->getParam ('ping_rate');
			
			
			if(isset($response['data']) && (in_array("null", $response['data'])  )){
				$response['returnvalue']="invalid";
			}
			else{
				$response=array();
			}
			
			if(count($response)==0)
			{
				$meter = new I4energy_Model_Meters();
				
				if (isset($meter_id) && $meter_id > 0)
				{
					$meter->setId($meter_id);
				}
				else
				{
					$meter->setCreatedBy($this->i4energy_session->user_id);
					$meter->setCreatedDate(gmdate ('Y-m-d H:i:s'));
				}
				
				$meter->setSiteId($site_id);
				$meter->setName($name);
				$meter->setDescription($desc);
				$meter->setLocation($location);
				$meter->setMac($mac);
				$meter->setType($type);
				$meter->setModuleNo($moduleno);
				$meter->setSlotNo($slotno);
				$meter->setChannelNo($channelno);
				$meter->setConversationFactor($conversationfactor);
				$meter->setUnit($unit);
				$meter->setOffset($offset);
				$meter->setCost($cost);
				$meter->setCarbonSavings($carbonsavings);
				$meter->setTimezone($timezone);
				$meter->setPingRate($pingrate);
				$meter->setUpdatedBy($this->i4energy_session->user_id);
				$meter->setUpdatedDate(@gmdate ('Y-m-d H:i:s'));
				
				$meter_id = $meter->save(true);
				if (isset ($meter_id) && $meter_id > 0)
				{
					$webminscale = $this->_request->getParam ('min_scale');
					$webmaxscale = $this->_request->getParam ('max_scale');
					$webupdateinterval = $this->_request->getParam ('update_interval');
					$isgcm = $this->_request->getParam ('gcm_device');
					$networkname = $this->_request->getParam ('network_name');
					$phoneno = $this->_request->getParam ('phone_no');
					$installdate = $this->_request->getParam ('install_date');
					$carboncorrectionfactor = $this->_request->getParam ('volume_correction_factor');
					$carboncalorificfactor = $this->_request->getParam ('calorific_value');
					$carbonkwhconversionfactor = $this->_request->getParam ('kwh_conversion_factor');
					$simcardno = $this->_request->getParam ('sim_card_no');
					$referencevoltage = $this->_request->getParam ('ref_voltage');
					$controlLink = $this->_request->getParam ('control_link');
					$hr_webdial_channel = $this->_request->getParam ('hr_channel_no');
					$hr_webdial_unit = $this->_request->getParam ('hr_unit');
					$hr_pulse1_text = $this->_request->getParam ('hr_pulse1_text');
					$hr_pulse2_text = $this->_request->getParam ('hr_pulse2_text');
					$hr_pulse3_text = $this->_request->getParam ('hr_pulse3_text');
					
					$meter_attr = new I4energy_Model_MeterAttributes();
					$meter_attr->setId ($meter_id);
					$meter_attr->setWebMinScale($webminscale);
					$meter_attr->setWebMaxScale($webmaxscale);
					$meter_attr->setWebUpdateInterval($webupdateinterval);
					$meter_attr->setNetworkName($networkname);
					$meter_attr->setIsGcm($isgcm);
					$meter_attr->setPhoneNo($phoneno);
					$meter_attr->setInstallDate($installdate);
					$meter_attr->setCarbonVolumeCorrectionFactor($carboncorrectionfactor);
					$meter_attr->setCarbonCalorificValue($carboncalorificfactor);
					$meter_attr->setCarbonKwhConversionFactor($carbonkwhconversionfactor);
					$meter_attr->setSimCardNo($simcardno);
					$meter_attr->setReferenceVoltage($referencevoltage);
					$meter_attr->setControlLink($controlLink);
					$meter_attr->setHourRunWebDialChannel($hr_webdial_channel);
					$meter_attr->setHourRunWebDialUnit($hr_webdial_unit);
					$meter_attr->setHourRunPulseText1($hr_pulse1_text);
					$meter_attr->setHourRunPulseText2($hr_pulse2_text);
					$meter_attr->setHourRunPulseText3($hr_pulse3_text);
					
					$meter_attr->save(true);
					$meter_alert = new I4energy_Model_MeterAlerts();
					
					$phoneno = $this->_request->getParam ('alert_phone_no');
					$email1 = $this->_request->getParam ('email1');
					$email2 = $this->_request->getParam ('email2');
					$email3 = $this->_request->getParam ('email3');
					$statusgoodmin = $this->_request->getParam ('status_good_min');
					$statusgoodmax = $this->_request->getParam ('status_good_max');
					$statuswarningmin = $this->_request->getParam ('status_warn_min');
					$statuswarningmax = $this->_request->getParam ('status_warn_max');
					$statusalertmin = $this->_request->getParam ('status_alert_min');
					$statusalertmax = $this->_request->getParam ('status_alert_max');
					$alertlimitmin = $this->_request->getParam ('alert_min_limit');
					$alertlimitmax = $this->_request->getParam ('alert_max_limit');
					$alerttimelimitmin = $this->_request->getParam ('alert_min_time');
					$alerttimelimitmax = $this->_request->getParam ('alert_max_time');
					$alertlimitping = $this->_request->getParam ('ping_limit');
					$lowpingerbattery = $this->_request->getParam ('low_battery');
					$lowpingerbatterychannelno = $this->_request->getParam('battery_channel_no');
					
					
					$meter_alert->setId($meter_id);
					$meter_alert->setPhoneNo($phoneno);
					$meter_alert->setEmail1($email1);
					$meter_alert->setEmail2($email2);
					$meter_alert->setEmail3($email3);
					$meter_alert->setStatusGoodMin($statusgoodmin);
					$meter_alert->setStatusGoodMax($statusgoodmax);
					$meter_alert->setStatusWarningMin($statuswarningmin);
					$meter_alert->setStatusWarningMax($statuswarningmax);
					$meter_alert->setStatusAlertMin($statusalertmin);
					$meter_alert->setStatusAlertMax($statusalertmax);
					$meter_alert->setAlertLimitMin($alertlimitmin);
					$meter_alert->setAlertLimitMax($alertlimitmax);
					$meter_alert->setAlertTimeLimitMin($alerttimelimitmin);
					$meter_alert->setAlertTimeLimitMax($alerttimelimitmax);
					$meter_alert->setAlertLimitPing($alertlimitping);
					$meter_alert->setLowPingerBattery($lowpingerbattery);
					$meter_alert->setLowPingerBatteryChannelNo($lowpingerbatterychannelno);
					
					$meter_id=$meter_alert->save(true);

					if($update != 1 && $this->i4energy_session->user_role=='engineer'){
					$meteruser_obj=new I4energy_Model_MeterUser();
					$meteruser_obj->setMeter_id($meter_id);
					$meteruser_obj->setUser_id($this->i4energy_session->user_id);
					$meteruser_obj->save(true);
					}
					
					if ($meter_id > 0)
					{
						$this->view->success = true;
					}
					else
					{
						$this->view->success = false;
					}
				}
			}
			else 
			{
				$this->view->response = $response;
			}
		}
		
		if (isset ($meter_id) && $meter_id > 0)
		{
			$meter = new I4energy_Model_Meters();
			$meter->init ($meter_id);
			$this->view->meter = $meter;
			
			$meter_attr = new I4energy_Model_MeterAttributes();
			$meter_attr->init ($meter_id);
			$this->view->meter_attr = $meter_attr;
			
			$meter_alert = new I4energy_Model_MeterAlerts();
			$meter_alert->init ($meter_id);
			$this->view->meter_alert = $meter_alert;
			
			
			$meter_user_obj =new I4energy_Model_MeterUser();
			$filterUser=array();
			if(isset($meter_id) && $meter_id>0) {
				$filterUser[0]['field'] = 'meter_id';
				$filterUser[0]['operation'] = '=';
				$filterUser[0]['value'] = $meter_id;
				$query=$meter_user_obj->listMeterUser($filterUser);
			}
			
			$allocated_user_array=array();
			if (isset($query->rows))
			{
				foreach ($query->rows as $row){
					array_push($allocated_user_array, $row['user_id']);
				}
			}
			$this->view->allocated_user=$allocated_user_array;
		}
		
		if (isset($site_id) && $site_id > 0)
		{
			$site = new I4energy_Model_Sites();
			$site->init ($site_id);
			
			$this->view->site = $site;
		}
//    $this->layout('layout');

    $user_obj = new I4energy_Model_Users();
		$userlist=	$user_obj->listUsers(array(), '', '', '', '');
		$this->view->users = $userlist;
		
	}
	
	function deleteAction()
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'meter_delete', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$meter_id = (int) $this->_request->getParam ('id');
	
		if($this->_request->isPost())
		{
			$meter = new I4energy_Model_Meters();
			$meter->init ($meter_id);
			$meter->delete ();
		}
		
		exit;
	}
	
	function usermapperAction() {
		$site_obj =new I4energy_Model_Sites();
		
		$sitefilter=$this->i4energy_session->user_sitefilter;
		
		$sitelist=$site_obj->listSites($sitefilter);
		$this->view->sites = $sitelist;
		
		$meter_obj = new I4energy_Model_Meters();
		$user_obj = new I4energy_Model_Users();
		
		
		$meterlist = $meter_obj->listMeters (array(), '', '', '', '');
		$this->view->meters = $meterlist;
		$this->view->totalrecords = $meterlist->totalrecords;

		$userlist=	$user_obj->listUsers(array(), '', '', '', '');
		$this->view->users = $userlist;
	}
	
	function allocatedusersAction() {
		$meter_id = $this->_request->getParam ('meterid');
		
		$meter_user_obj =new I4energy_Model_MeterUser();
		$filter=array();
		$userArray=array();
		if(isset($meter_id) && $meter_id>0) {
			$filterUser[0]['field'] = 'meter_id';
			$filterUser[0]['operation'] = '=';
			$filterUser[0]['value'] = $meter_id;
			$users=$meter_user_obj->listMeterUser($filterUser);
			$userArray=$users->rows;
		}
		$response=array();
		$i=0;
		foreach ($userArray as $user) {
			$response['data'][$i]=$user['user_id'];
			$i++;
				
		}
		echo json_encode($response);
		exit;
		
	}
	
	function addusersAction() {
		$meter_id = $this->_request->getParam ('meterid');
		$user_id = $this->_request->getParam ('userid');
		
		$meteruser_obj =new I4energy_Model_MeterUserMapper();
		$response=array();
		$delete ="";
		if(isset($meter_id) && $meter_id != "") {
			$where = $meteruser_obj->getDbTable()->getAdapter()->quoteInto('meter_id = ?',$meter_id );
			$meteruser_obj->getDbTable()->delete ($where);
		}
		
		if(isset($user_id)) {
			foreach($user_id as $row) {
				$meteruser_obj=new I4energy_Model_MeterUser();
				$meteruser_obj->setMeter_id($meter_id);
				$meteruser_obj->setUser_id($row);
		
				try {
					$lastmeteruser_id = $meteruser_obj->save (true);
					$response['returnvalue']="valid";
					$this->view->addmeterusersuccess = true;
				}
				catch (Exception $e)
				{
					$this->view->addmeterusersuccess = false;
				}
			}
			echo json_encode($response);
			exit;
		}
		
	}
	
	function getmetercsvdataAction()
	{		
//		$layout = Zend_Layout::getMvcInstance();
//		$layout->disableLayout();
		
		$start_date = $this->_request->getParam ('start_date');
		$end_date   = $this->_request->getParam ('end_date');
		$meter_id   = $this->_request->getParam ('meter_id');
		
		$meter = new I4energy_Model_Meters();
		$meter->init($meter_id);
		 
		if ($meter->getType() == 'boiler')
		{
			$defaultTimeZone = @date_default_timezone_get();
			@date_default_timezone_set($meter->getTimezone());
			$timezone_diff = @date('P');
			@date_default_timezone_set($defaultTimeZone);
			
			$meter_data = new I4energy_Model_MeterData();
			
			$select = $meter_data->getMapper()->getSelect();
			$select->setIntegrityCheck (false);
			$select->from ('meter_rawdata');
			$select->where ('macid=?', $meter->getMac());
			$select->where ('moduleid=?', $meter->getModuleNo());
			$select->where ('slotno=?', $meter->getSlotNo());
			$select->where ('status=?', 'NA');
			$select->where ('rec_date >= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($start_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()));
			$select->where ('rec_date <= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($end_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()));
			$select->limit(5000);
			$select->order('rec_date', 'DESC');
			//echo $select->__toString();exit;
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
			
			$file_name = 'MeterNameVal_MAC-MacVal_MODULE-ModuleIdVal_SLOT-_SlotNoVal.csv';
			$meterName=str_replace(' ','-',$meter->getName());
			$file_name=str_replace("MeterNameVal", strtolower($meterName), $file_name);
			$file_name=str_replace("MacVal", $meter->getMac(), $file_name);
			$file_name=str_replace("ModuleIdVal", $meter->getModuleNo(), $file_name);
			$file_name=str_replace("SlotNoVal", $meter->getSlotNo(), $file_name);
			$file_name=str_replace("ChannelNoVal", $meter->getChannelNo(), $file_name);
				
			header("Content-type: application/vnd.ms-excel");
			header("Content-disposition: attachment; filename=$file_name");
				
			$out = fopen('php://output', 'w');
				
			$site_obj = new I4energy_Model_Sites();
			$site_obj->init($meter->getSiteId());
				
			fputcsv($out, array('Site Name', 'Meter Name', 'Module Id', 'Slot No', 'Date (UTC)'
					, 'Boiler', 'Boiler Pulse'
					, 'Demand', 'Demand Pulse'
					, 'Request', 'Request Pulse'
					, 'Automatic', 'Automatic Pulse'
					, '2Hours Boost', '2Hours Boost Pulse'
					, 'AUX Timer', 'AUX Timer Pulse'
					, 'Outside Temp. (Celsius)'
					, 'Flow Temp. (Celsius)'
					, 'Return Temp. (Celsius)'
					, 'Hot Water Temp. (Celsius)'
			));
			
			foreach ($rows as $row) 
			{
				$boiler_boilerstatus = 'NA';
				$boiler_boilerdata = 'NA';
				$boiler_demandstatus = 'NA';
				$boiler_demanddata = 'NA';
				$boiler_requeststatus = 'NA';
				$boiler_requestdata = 'NA';
				$boiler_automaticstatus = 'NA';
				$boiler_automaticdata = 'NA';
				$boiler_booststatus = 'NA';
				$boiler_boostdata = 'NA';
				$boiler_auxstatus = 'NA';
				$boiler_auxdata = 'NA';
				
				// Channel 9 - Boiler
				if (preg_match ('/P$/', $row['ch9']))
					$boiler_boilerstatus = 'On';
				else
					$boiler_boilerstatus = 'Off';
				$boiler_boilerdata = (float) preg_replace ('/p$/i', '', $row['ch9']);
					
				// Channel 7 - Demand
				if (preg_match ('/P$/', $row['ch7']))
					$boiler_demandstatus = 'Off';
				else
					$boiler_demandstatus = 'On';
				$boiler_demanddata = (float) preg_replace ('/p$/i', '', $row['ch7']);
					
				// Channel 8 - Request
				if (preg_match ('/P$/', $row['ch8']))
					$boiler_requeststatus = 'On';
				else
					$boiler_requeststatus = 'Off';
				$boiler_requestdata = (float) preg_replace ('/p$/i', '', $row['ch8']);
					
				// Channel 5- Automatic
				if (preg_match ('/P$/', $row['ch5']))
					$boiler_automaticstatus = 'On';
				else
					$boiler_automaticstatus = 'Off';
				$boiler_automaticdata = (float) preg_replace ('/p$/i', '', $row['ch5']);
					
				// Channel 6 - 2 Hours Boost
				if (preg_match ('/P$/', $row['ch6']))
					$boiler_booststatus = 'On';
				else
					$boiler_booststatus = 'Off';
				$boiler_boostdata = (float) preg_replace ('/p$/i', '', $row['ch6']);
				
				// Channel 4 - AUX Timer
				if (preg_match ('/P$/', $row['ch4']))
					$boiler_auxstatus = 'On';
				else
					$boiler_auxstatus = 'Off';
				$boiler_auxdata = (float) preg_replace ('/p$/i', '', $row['ch4']);
				
				$outside_temp = preg_replace ('/K/i', '.', $row['ch10']);
				if (strtolower($outside_temp) == 'n.')
					$outside_temp = 0;
				else
					$outside_temp = $outside_temp-273.15;
				
				$flow_temp = preg_replace ('/K/i', '.', $row['ch12']);
				if (strtolower($flow_temp) == 'n.')
					$flow_temp = 0;
				else
					$flow_temp = $flow_temp-273.15;
				
				$return_temp = preg_replace ('/K/i', '.', $row['ch11']);
				if (strtolower($return_temp) == 'n.')
					$return_temp = 0;
				else
					$return_temp = $return_temp-273.15;
				
				$hot_water = preg_replace ('/K/i', '.', $row['ch13']);
				if (strtolower($hot_water) == 'n.')
					$hot_water = 0;
				else
					$hot_water = $hot_water-273.15;
				
				fputcsv($out, array( $site_obj->getName(), $meter->getName(), $meter->getModuleNo(), $meter->getSlotNo(), $row['rec_date']
						,$boiler_boilerstatus, $boiler_boilerdata
						,$boiler_demandstatus, $boiler_demanddata
						,$boiler_requeststatus, $boiler_requestdata
						,$boiler_automaticstatus, $boiler_automaticdata
						,$boiler_booststatus, $boiler_boostdata
						,$boiler_auxstatus, $boiler_auxdata
						,$outside_temp
						,$flow_temp
						,$return_temp
						,$hot_water
				));
			}
				
			fclose($out);
			exit;
		}
		else
		{
			$common_where='';
			if ($meter->getId() > 0)
			{
				$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";
			}
			
			$defaultTimeZone = @date_default_timezone_get();
			@date_default_timezone_set($meter->getTimezone());
			$timezone_diff = @date('P');
			@date_default_timezone_set($defaultTimeZone);
		
			$meter_data = new I4energy_Model_MeterData();
			// Today's usage chart data (hourly)
			 
			$select = $meter_data->getMapper()->getSelect();
			$select->from ('meter_data', array ('date' => 'CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\')', 'data' => 'data','' => '','' => '','' => ''));
			$select->where ($common_where);
			$select->where ('rec_date >= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($start_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()));
			$select->where ('rec_date <= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($end_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()));
			$select->limit(5000);
			$select->order('rec_date', 'DESC');
			$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
			$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
	
			$file_name = 'MeterNameVal_MAC-MacVal_MODULE-ModuleIdVal_SLOT-SlotNoVal_CH-_ChannelNoVal.csv';
			$meterName=str_replace(' ','-',$meter->getName());
			$file_name=str_replace("MeterNameVal", strtolower($meterName), $file_name);
			$file_name=str_replace("MacVal", $meter->getMac(), $file_name);
			$file_name=str_replace("ModuleIdVal", $meter->getModuleNo(), $file_name);
			$file_name=str_replace("SlotNoVal", $meter->getSlotNo(), $file_name);
			$file_name=str_replace("ChannelNoVal", $meter->getChannelNo(), $file_name);
			
			// 	$file_name = $meter->getMac() . '-' . $meter->getModuleNo() . '-' . $meter->getSlotNo() . '-' . $meter->getChannelNo() . '.csv';
			header("Content-type: application/vnd.ms-excel");
			header("Content-disposition: attachment; filename=$file_name");
			
			$out = fopen('php://output', 'w');
			
			$site_obj = new I4energy_Model_Sites();
			$site_obj->init($meter->getSiteId());
			$display_unit = I4energy_Model_Meters::getDisplayUnit($meter->getUnit());
			$display_unit = str_replace ('M<sup>3</sup>', 'm3', $display_unit);
			$display_unit = str_replace ('&deg;C', 'C', $display_unit);

			$meter_attrs = new I4energy_Model_MeterAttributes();
			$meter_attrs->init ($meter->getId());

			$convert_unit_attrs = array();
			$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();

			fputcsv($out, array('Site Name','Meter Name','Module Id','Slot No','Channel No','Date (UTC)','Pinger Data', 'Data (' . $display_unit . ')'));
			foreach ($rows as $row) {
				$processed_data = (float) I4energy_Model_Meters::convertToUnit ($row['data'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
				fputcsv($out, array($site_obj->getName(),$meter->getName(),$meter->getModuleNo(),$meter->getSlotNo(),$meter->getChannelNo(),$row['date'],$row['data'],$processed_data));
			}
			
			fclose($out);
			exit;
		}			
	}

	function getmeterhourlycsvdataAction () {
//		$layout = Zend_Layout::getMvcInstance();
//		$layout->disableLayout();

		$start_date = $this->_request->getParam ('hourly_start_date');
		$end_date   = $this->_request->getParam ('hourly_end_date');
		$meter_id   = $this->_request->getParam ('hourly_meter_id');

		$meter = new I4energy_Model_Meters();
		$meter->init($meter_id);

		$common_where='';
		if ($meter->getId() > 0)
		{
			$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";
		}

		$defaultTimeZone = @date_default_timezone_get();
		@date_default_timezone_set($meter->getTimezone());
		$timezone_diff = @date('P');
		@date_default_timezone_set($defaultTimeZone);

		$meter_attrs = new I4energy_Model_MeterAttributes();
		$meter_attrs->init ($meter_id);
		$convert_unit_attrs = array();
		$convert_unit_attrs['ref_voltage'] = $meter_attrs->getReferenceVoltage();

		$meter_data = new I4energy_Model_MeterData();
		$group_function = 'AVG';
		if ($meter->getType() == 'number' || $meter->getType() == 'hourrun' || ($meter->getUnit() == 'kwh' && $meter->getType() == 'power') || $meter->getType() == 'gas' || $meter->getType() == 'heat' || $meter->getType() == 'amps' || $meter->getType() == 'water')
			$group_function = 'SUM';

		$field_to_query = 'meter_data.data';

		$offset = $meter->getOffset();
		if ($offset instanceof Zend_Db_Expr || $offset == null)
			$offset = '';

		if ($offset != '')
			$field_to_query = 'meter_data.data + (' . $offset . ')';

		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('hour_of' => 'DATE_FORMAT(CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\'), \'%Y-%m-%d %H:00\')', 'total' => $group_function . '(' . $field_to_query . ')'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($start_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()));
		$select->where ('rec_date <= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($end_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()));
		$select->group ('hour_of');
		//echo $select->__toString();exit;
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

		$file_name = 'MeterNameVal_MAC-MacVal_MODULE-ModuleIdVal_SLOT-SlotNoVal_CH-_ChannelNoVal.csv';
		$meterName=str_replace(' ','-',$meter->getName());
		$file_name=str_replace("MeterNameVal", strtolower($meterName), $file_name);
		$file_name=str_replace("MacVal", $meter->getMac(), $file_name);
		$file_name=str_replace("ModuleIdVal", $meter->getModuleNo(), $file_name);
		$file_name=str_replace("SlotNoVal", $meter->getSlotNo(), $file_name);
		$file_name=str_replace("ChannelNoVal", $meter->getChannelNo(), $file_name);

		// 	$file_name = $meter->getMac() . '-' . $meter->getModuleNo() . '-' . $meter->getSlotNo() . '-' . $meter->getChannelNo() . '.csv';
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=$file_name");

		$out = fopen('php://output', 'w');

		$site_obj = new I4energy_Model_Sites();
		$site_obj->init($meter->getSiteId());
		$display_unit = I4energy_Model_Meters::getDisplayUnit($meter->getUnit());
		$display_unit = str_replace ('M<sup>3</sup>', 'm3', $display_unit);
		$display_unit = str_replace ('&deg;C', 'C', $display_unit);

		fputcsv($out, array('Site Name','Meter Name','Module Id','Slot No','Channel No','Date (UTC)','Pinger Data','Data (' . $display_unit . ')'));
		foreach ($rows as $row) {
			$processed_data = (float) I4energy_Model_Meters::convertToUnit ($row['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor(), $convert_unit_attrs);
			fputcsv($out, array($site_obj->getName(),$meter->getName(),$meter->getModuleNo(),$meter->getSlotNo(),$meter->getChannelNo(),$row['hour_of'],$row['total'],$processed_data));
		}

		fclose($out);
		exit;
	}

	function downloadhourrundataAction () {
//		$layout = Zend_Layout::getMvcInstance();
//		$layout->disableLayout();

		$start_date = $this->_request->getParam ('start_date');
		$end_date   = $this->_request->getParam ('end_date');
		$meter_id   = $this->_request->getParam ('meter_id');

		$meter = new I4energy_Model_Meters();
		$meter->init($meter_id);

		$common_where='';
		if ($meter->getId() > 0)
			$common_where = "macid='{$meter->getMac()}' AND moduleid={$meter->getModuleNo()} AND slotno={$meter->getSlotNo()} AND channelno={$meter->getChannelNo()}";

		$date_where = '';
		if ($start_date != '')
			$date_where .= ' AND rec_date >= \'' . I4energy_Model_Utils_DateUtils::getFormattedDate($start_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()) . '\'';
		if ($end_date != '')
			$date_where .= ' AND rec_date <= \'' . I4energy_Model_Utils_DateUtils::getFormattedDate($end_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()) . '\'';

		$defaultTimeZone = @date_default_timezone_get();
		@date_default_timezone_set($meter->getTimezone());
		$timezone_diff = @date('P');
		@date_default_timezone_set($defaultTimeZone);

		$meter_data = new I4energy_Model_MeterData();
		/*$sql_query = "SELECT rec_date, `data`, data1, statusChanged
			FROM
			( SELECT (@statusPre <> data1 AND @macidPre=macid AND @moduleidPre=moduleid AND @slotnoPre=slotno AND @channelnoPre=channelno) AS statusChanged
					 , rec_date, `data`, data1, macid, moduleid, slotno, channelno
					 , @statusPre := data1
					 , @macidPre := macid
					 , @moduleidPre := moduleid
					 , @slotnoPre := slotno
					 , @channelnoPre := channelno
				FROM meter_data
				   , (SELECT @statusPre:=NULL, @macidPre:=NULL, @moduleidPre:=NULL, @slotnoPre:=NULL, @channelnoPre:=NULL) AS d
				   WHERE {$common_where} {$date_where}
				ORDER BY rec_date
			  ) AS good
			WHERE statusChanged";
		echo $sql_query;exit;
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($sql_query);*/

		$select = $meter_data->getMapper()->getSelect();
		$select->from ('meter_data', array ('date' => 'CONVERT_TZ(rec_date, \'+00:00\', \'' . $timezone_diff . '\')', 'data' => 'data','' => '','data1' => 'data1'));
		$select->where ($common_where);
		$select->where ('rec_date >= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($start_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()));
		$select->where ('rec_date <= ?', I4energy_Model_Utils_DateUtils::getFormattedDate($end_date, 'Y-m-d H:i:s','GMT' ,$meter->getTimezone()));
		$select->limit(5000);
		$select->order('rec_date', 'DESC');
		$stmt = $meter_data->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

		$file_name = 'MeterNameVal_MAC-MacVal_MODULE-ModuleIdVal_SLOT-SlotNoVal_CH-_ChannelNoVal.csv';
		$meterName=str_replace(' ','-',$meter->getName());
		$file_name=str_replace("MeterNameVal", strtolower($meterName), $file_name);
		$file_name=str_replace("MacVal", $meter->getMac(), $file_name);
		$file_name=str_replace("ModuleIdVal", $meter->getModuleNo(), $file_name);
		$file_name=str_replace("SlotNoVal", $meter->getSlotNo(), $file_name);
		$file_name=str_replace("ChannelNoVal", $meter->getChannelNo(), $file_name);

		// 	$file_name = $meter->getMac() . '-' . $meter->getModuleNo() . '-' . $meter->getSlotNo() . '-' . $meter->getChannelNo() . '.csv';
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=$file_name");

		$out = fopen('php://output', 'w');

		$site_obj = new I4energy_Model_Sites();
		$site_obj->init($meter->getSiteId());

		if ($meter->getUnit() == 'hrun') {
			$onText = 'RUNNING';
			$offText = 'OFF';
		} else {
			$onText = 'OPEN';
			$offText = 'CLOSED';
		}

		fputcsv($out, array('Site Name','Meter Name','Module Id','Slot No','Channel No','Date (UTC)','Status','Pulse Value'));
		foreach ($rows as $row) {
			$status = 'NA';
			if ($row['data1'] == 1)
				$status = $onText;
			else if ($row['data1'] == 0)
				$status = $offText;

			fputcsv($out, array($site_obj->getName(),$meter->getName(),$meter->getModuleNo(),$meter->getSlotNo(),$meter->getChannelNo(),$row['date'],$status,$row['data']));
		}

		fclose($out);
		exit;
	}
	
	function editmacAction() {
		$new_mac = $this->_request->getParam ('new_mac');
		$old_mac = $this->_request->getParam ('old_mac');
	
		$data = array(
			'macid' => $new_mac,
		);
	
		$db_table = new I4energy_Model_DbTable_MeterRawData();
		$where = $db_table->getAdapter()->quoteInto('macid = ?', $old_mac);
		$db_table->update($data, $where);
	
		$db_table = new I4energy_Model_DbTable_MeterData();
		$where = $db_table->getAdapter()->quoteInto('macid = ?', $old_mac);
		$db_table->update($data, $where);

		$db_table = new I4energy_Model_DbTable_Meters();
		$where = $db_table->getAdapter()->quoteInto('mac = ?', $old_mac);
		$data = array(
			'mac' => $new_mac,
			'updatedby' => $this->i4energy_session->user_id,
			'updateddate' => @gmdate ('Y-m-d H:i:s')
		);
		$db_table->update($data, $where);

		echo json_encode(0);
		exit;
	}
}

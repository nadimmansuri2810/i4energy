<?php

class MeterlinkdisplayController extends Zend_Controller_Action
{
	public function init() 
	{
		$this->i4energy_session = new Zend_Session_Namespace('I4energySession');
		
		if (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == ''))
		{
			$this->i4energy_session->user_obj = new stdClass();
			$this->i4energy_session->user_obj->dateformat = DEFAULT_DATEFORMAT;
			$this->i4energy_session->user_obj->timezone = DEFAULT_TIMEZONE;
		}
	}

	function indexAction ()
	{
		$url = $this->_request->getParam('url');
		$meter = new I4energy_Model_Meters();
		$meterlinkObj =new I4energy_Model_MeterDisplayLinks();
		$filter=array();
				$filter[0]['field'] = 'url';
				$filter[0]['operation'] = 'eq';
				$filter[0]['value'] = $url;
		$meterlink = $meterlinkObj->listMeterDisplayLinks($filter);
		
		require_once dirname(__FILE__) . '/MetersController.php';
		
		if (isset($meterlink->totalrecords) && $meterlink->totalrecords >0 && $meterlink->rows[0]['id'] >0)
		{
			
			if (isset($meterlink->rows[0]['loginrequired']) && $meterlink->rows[0]['loginrequired']==1 && (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == '')))
			{
						$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);
						exit;
			}else {
					$totalmeters =0;
					
					if (isset($meterlink->rows[0]['meter1']) && $meterlink->rows[0]['meter1']>0)
					{
						$meter_id1=$meterlink->rows[0]['meter1'];
						$totalmeters=1;
					}
					if (isset($meterlink->rows[0]['meter2']) && $meterlink->rows[0]['meter2']>0)
					{
						$meter_id2=$meterlink->rows[0]['meter2'];
						$totalmeters=2;
					}
					if (isset($meterlink->rows[0]['meter3']) && $meterlink->rows[0]['meter3']>0)
					{
						$meter_id3=$meterlink->rows[0]['meter3'];
						$totalmeters=3;
					}
					if (isset($meterlink->rows[0]['meter4']) && $meterlink->rows[0]['meter4']>0)
					{
						$meter_id4=$meterlink->rows[0]['meter4'];
						$totalmeters=4;
					}
					if (!isset($this->i4energy_session->user_id) && isset($meterlink->rows[0]['loginrequired']) && $meterlink->rows[0]['loginrequired']==0 && $meterlink->rows[0]['customer_id'] >0 && $meterlink->rows[0]['theme'] !=''){
						
						$this->i4energy_session->customer_id = $meterlink->rows[0]['customer_id'];
						$user_obj=new I4energy_Model_Users();
						$user_obj->setTheme($meterlink->rows[0]['theme']);
						$this->i4energy_session->user_obj = $user_obj;
					}
			
					$this->view->totalmeters =$totalmeters;
					$meterdata=array();
					$meterdata['id']=$meter_id1;
					
					$meter->init($meter_id1);
					$meterdata['site_id']='site-'.$meter->getSiteId();
					$this->i4energy_session->isforward = 'true';
					if($totalmeters == 1){
						$this->_forward('view','Meters','application',$meterdata);
					}
					
					$response=array();
					for ($i=1;$i<=$totalmeters;$i++)
					{
						if ($i==1){ $meter_id=$meter_id1;}
						elseif ($i==2){ $meter_id=$meter_id2;}
						elseif ($i==3){ $meter_id=$meter_id3;}
						elseif ($i==4){	$meter_id=$meter_id4;}
						
						if(isset($meter_id) && $meter_id >0)
						{   
							$meter = new I4energy_Model_Meters();
							$meter->init ($meter_id);
							$response[$i]['meter'] =$meter;
// 								print_r($meter);exit;
								$site =new I4energy_Model_Sites();
								$site->init($meter->getSiteId());
								$response[$i]['site'] =$site;
						
								$meter_alters = new I4energy_Model_MeterAlerts();
								$meter_alters->init ($meter_id);
								$response[$i]['meter_alert'] =$meter_alters;
									
								$meter_attrs = new I4energy_Model_MeterAttributes();
								$meter_attrs->init ($meter_id);
								$response[$i]['meter_attrs'] =$meter_attrs;
								
								// Get meter data
								
								$meter_data_result = MetersController::getMeterData ($meter, $meter_attrs->getInstallDate());
									
								$response[$i]['current_status'] = (isset($meter_data_result['current_status']))?trim($meter_data_result['current_status']):'';
									
								// Now
								if ($meter_data_result['now'] == '')
									$response[$i]['now_usage'] = 'NA';
								else
									$response[$i]['now_usage'] = I4energy_Model_Meters::convertToUnit ($meter_data_result['now'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor());
									
								// Today usage
								$response[$i]['today_usage'] = I4energy_Model_Meters::convertToUnit ($meter_data_result['today'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor());
									
								$carbon_savings = $meter->getCarbonSavings();
								if (isset ($carbon_savings) && $carbon_savings != '' && $carbon_savings != 0)
								{
									$response[$i]['today_carbonsavings'] = I4energy_Model_Meters::calculateCarbonSavings ($this->view->today_usage, $meter->getType(), $carbon_savings) . ' kg';
								}
								else
								{
									$response[$i]['today_carbonsavings'] = $this->view->today_usage . '/' . I4energy_Model_Meters::getDisplayUnit($meter->getUnit());
								}
									
								// Last 24H usage
								$response[$i]['last24h_usage'] = I4energy_Model_Meters::convertToUnit ($meter_data_result['last24h'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor());
									
								// total usage
								$response[$i]['total_usage'] = I4energy_Model_Meters::convertToUnit ($meter_data_result['total'], $meter->getType(), $meter->getUnit(), $meter->getConversationFactor());
									
								$response[$i]['display_unit'] = I4energy_Model_Meters::getDisplayUnit ($meter->getUnit());
									
								// Today's usage chart data (hourly)
								$response[$i]['hourly_chart_data'] = (isset($meter_data_result['hourly_chart_data']))?$meter_data_result['hourly_chart_data']:null;
									
								// Last 31 days meter data
								$response[$i]['daily_chart_data'] = (isset($meter_data_result['daily_chart_data']))?$meter_data_result['daily_chart_data']:null;
									
								// Boiler Chart
								$response[$i]['boiler_chart_data'] = (isset($meter_data_result['boiler_chart_data']))?$meter_data_result['boiler_chart_data']:null;
							}
						}
						$this->view->response = $response;
						
				}
			}
			
		}
	}

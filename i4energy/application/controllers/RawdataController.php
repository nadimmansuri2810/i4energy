<?php

// define ('DB_HOST', 'localhost');
// //define ('DB_HOST', 'dev.i4energy.com');
// define ('DB_USER', 'root');
// define ('DB_PASSWORD', 'Savitriya@020');
// //define ('DB_PASSWORD', 'root');
// define ('DB_NAME', 'i4energy');
// //define ('DB_NAME', 'i4energy_20');
// define ('TABLE_RAW_DATA', 'meter_rawdata');
// define ('TABLE_PROCESSED_DATA', 'meter_data');
// define ('TABLE_METER', 'meters');

define ('DB_HOST', '127.0.0.1');
define ('DB_USER', 'root');
define ('DB_PASSWORD', '');
define ('DB_NAME', 'i4energy');
define ('TABLE_RAW_DATA', 'meter_rawdata');
define ('TABLE_PROCESSED_DATA', 'meter_data');
define ('TABLE_METER', 'meters');


class RawdataController extends Zend_Controller_Action
{


	public function init() 
	{
		$this->i4energy_session = new Zend_Session_Namespace('I4energySession');
		if (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == ''))
		{
			$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);;
			exit;
		}
		
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'pinger_data', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}


	}

	public static function convertToCelsius ($data)
	{
		if (preg_match ('/K/', $data))
		{
			$kelvin = preg_replace ('/K/', '.', $data);
			
			if (strtolower($kelvin) == 'n.')
				$celsius = 'NA';
			else
				$celsius = number_format($kelvin-273.15, 1, '.', '') . ' &deg;C';
			
			return $celsius;
		}	
		return $data;
	}
	
	public function indexAction() 
	{
		if($this->_request->isPost())
		{
			$mac_id = $this->_request->getParam ('macid');
			$module_id = $this->_request->getParam ('moduleid');
			$slot_no = $this->_request->getParam ('slotno');
			$limit = (int) $this->_request->getParam ('limit');
			$from_date = $this->_request->getParam ('from_date');
			$to_date = $this->_request->getParam ('to_date');
			
			if ($limit == 0)
				$limit = 50;
			
			$db_table = new I4energy_Model_DbTable_MeterRawData();
			$select = $db_table->select();
			/*
			$select->setIntegrityCheck(false);
			$select->from ( array ('rd' => 'meter_rawdata') );
			$select->joinLeft (array ('m' => 'meters'), 'm.mac=rd.macid AND m.moduleno=rd.moduleid AND m.slotno=rd.slotno', array());
			$select->joinLeft (array ('s' => 'sites'), 's.id=m.siteid', array('name'));
			*/
			if ($mac_id != '')
				$select->where ('macid = ?', $mac_id);
			if ($module_id != '')
				$select->where ('moduleid = ?', $module_id);
			if ($slot_no != '')
				$select->where ('slotno = ?', $slot_no);
			if ($from_date != '')
				$select->where ('rec_date >= ?', $from_date);
			if ($to_date != '')
				$select->where ('rec_date <= ?', $to_date);
			
			$select->order ('rec_date DESC');
			$select->limit ($limit, 0);

			$stmt = $db_table->getAdapter()->query($select);
			$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
			
			if (!(isset ($rows) && is_array($rows)))
				$rows = array();
			
			for ($i=0; $i < sizeof ($rows); $i++)
			{
				$rows[$i]['customer'] = 'UNKNOWN';
				
				$meter = new I4energy_Model_Meters();
				$select = $meter->getMapper()->getSelect();
				$select->setIntegrityCheck(false);
				$select->from (array ('m' => 'meters'), array());
				$select->join (array ('s' => 'sites'), 's.id=m.siteid', array ('name'));
				$select->where ('m.mac=?', $rows[$i]['macid']);
				$select->where ('m.moduleno=?', $rows[$i]['moduleid']);
				$select->where ('m.slotno=?', $rows[$i]['slotno']);
				$select->limit (1, 0);
				
				$customer_stmt = $meter->getMapper()->getDbTable()->getAdapter()->query($select);
				$customer_row = $customer_stmt->fetch(Zend_Db::FETCH_ASSOC);
				
				if ($customer_row['name'] != null)
					$rows[$i]['customer'] = trim($customer_row['name']);
				
				$rows[$i]['ch1'] = self::convertToCelsius($rows[$i]['ch1']);
				$rows[$i]['ch2'] = self::convertToCelsius($rows[$i]['ch2']);
				$rows[$i]['ch3'] = self::convertToCelsius($rows[$i]['ch3']);
				$rows[$i]['ch4'] = self::convertToCelsius($rows[$i]['ch4']);
				$rows[$i]['ch5'] = self::convertToCelsius($rows[$i]['ch5']);
				$rows[$i]['ch6'] = self::convertToCelsius($rows[$i]['ch6']);
				$rows[$i]['ch7'] = self::convertToCelsius($rows[$i]['ch7']);
				$rows[$i]['ch8'] = self::convertToCelsius($rows[$i]['ch8']);
				$rows[$i]['ch9'] = self::convertToCelsius($rows[$i]['ch9']);
				$rows[$i]['ch10'] = self::convertToCelsius($rows[$i]['ch10']);
				$rows[$i]['ch11'] = self::convertToCelsius($rows[$i]['ch11']);
				$rows[$i]['ch12'] = self::convertToCelsius($rows[$i]['ch12']);
				$rows[$i]['ch13'] = self::convertToCelsius($rows[$i]['ch13']);
				$rows[$i]['ch14'] = self::convertToCelsius($rows[$i]['ch14']);
				$rows[$i]['ch15'] = self::convertToCelsius($rows[$i]['ch15']);
				$rows[$i]['ch16'] = self::convertToCelsius($rows[$i]['ch16']);
				
				unset ($meter);
				unset ($select);
				unset ($customer_stmt);
				unset ($customer_row);
			}
			
			echo json_encode($rows);
			exit;
		}
	}
	
	function deleteAction()
	{
		$data = json_decode(stripslashes($_POST['data']));
		$where ="";

		if(count($data) > 0)
		{
			foreach ($data as $value)
			{
				$where .= "(macid = '".$value[0]."' AND moduleid = ".$value[1]." AND slotno = ".$value[2]." AND rec_date = '".$value[3]."')";
				$where .= " or ";
			}
		$where = rtrim($where," or ");

		$db_table = new I4energy_Model_DbTable_MeterRawData();
		$db_table->delete($where);
		}
		exit;
	}


  public function correctindexAction()
  {
    if($this->_request->isPost()) {
      try
      {
        $db_conn = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASSWORD);
      }
      catch (Exception $e)
      {
        die ("Cannot connect to database: " . $e->getMessage());
      }

      $mac_id = $this->_request->getParam('macid');
      $module_id = $this->_request->getParam('moduleid');
      $slot_no = $this->_request->getParam('slotno');
      $from_date = $this->_request->getParam('from_date');
      $to_date = $this->_request->getParam('to_date');

      $meter_raw_data = new I4energy_Model_DbTable_MeterRawData();
      $select = $meter_raw_data->select();
      $select->where('macid = ?', $mac_id);
//      if ($module_id != '')
        $select->where('moduleid = ?', $module_id);
//      if ($slot_no != '')
        $select->where('slotno = ?', $slot_no);
      $select->where ('rec_date >= ?', $from_date);
      $select->where ('rec_date <= ?', $to_date);

      $stmt = $meter_raw_data->getAdapter()->query($select);

      $raw_data_stmt = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

      $flag = 0;

      if(count($raw_data_stmt) > 0){
        if (!(isset ($raw_data_stmt) && is_array($raw_data_stmt)))
          $raw_data_stmt = array();

        for ($i=0; $i < sizeof ($raw_data_stmt); $i++)
        {
          $cols = array();
          $cols[0] = $mac_id;
          $cols[1] = $raw_data_stmt[$i]["rec_date"];
          $cols[2]= $raw_data_stmt[$i]["moduleid"];
          $cols[3]= $raw_data_stmt[$i]["slotno"];
          $cols[4]= $raw_data_stmt[$i]["status"];
          $cols[5]= $raw_data_stmt[$i]["ch1"];
          $cols[6]= $raw_data_stmt[$i]["ch2"];
          $cols[7]= $raw_data_stmt[$i]["ch3"];
          $cols[8]= $raw_data_stmt[$i]["ch4"];
          $cols[9]= $raw_data_stmt[$i]["ch5"];
          $cols[10]= $raw_data_stmt[$i]["ch6"];
          $cols[11]= $raw_data_stmt[$i]["ch7"];
          $cols[12]= $raw_data_stmt[$i]["ch8"];
          $cols[13]= $raw_data_stmt[$i]["ch9"];
          $cols[14]= $raw_data_stmt[$i]["ch10"];
          $cols[15]= $raw_data_stmt[$i]["ch11"];
          $cols[16]= $raw_data_stmt[$i]["ch12"];
          $cols[17]= $raw_data_stmt[$i]["ch13"];
          $cols[18]= $raw_data_stmt[$i]["ch14"];
          $cols[19]= $raw_data_stmt[$i]["ch15"];
          $cols[20]= $raw_data_stmt[$i]["ch16"];

          $last_ping_qry = "SELECT * from " . TABLE_RAW_DATA . " WHERE macid=:macid AND moduleid=:moduleid AND slotno=:slotno AND status='NA' AND rec_date<:last_ping_date ORDER BY rec_date DESC LIMIT 1";
          $last_ping_stmt = $db_conn->prepare ($last_ping_qry);

          // Get last ping
          $last_ping_stmt->execute ( array (
            ':macid' => $mac_id,
            ':moduleid' => $module_id,
            ':slotno' => $slot_no,
            ':last_ping_date' => $raw_data_stmt[$i]["rec_date"]
          ));

          $last_ping_data = $last_ping_stmt->fetch (PDO::FETCH_ASSOC);

          $function_name = "process_mod".$module_id."_slot".$slot_no;
          $fallback_function_name = "process_mod".$module_id."_slot_";

          if (method_exists('I4energy_Model_Utils_ProcessData',$function_name))
          {
            I4energy_Model_Utils_ProcessData::$function_name($db_conn, $cols, $last_ping_data);
            $flag = 1;
          }
          else if (method_exists('I4energy_Model_Utils_ProcessData',$fallback_function_name))
          {
            I4energy_Model_Utils_ProcessData::$fallback_function_name ($db_conn, $cols, $last_ping_data);
            $flag = 1;
          }
          else
          {
            echo "\nfunction: {I4energy_Model_Utils_ProcessData::$function_name}";
            $module_not_found = true;
            $flag = 2;
          }
        }
      }else{
        $flag = 0;
      }
      echo json_encode($flag);
      exit;
    }
  }


}


<?php

class HelpController extends Zend_Controller_Action
{
  public function init()
  {
    $this->i4energy_session = new Zend_Session_Namespace('I4energySession');
    if (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == ''))
    {
      $this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);
      exit;
    }

  }

  function indexAction ()
  {
    $helps_obj = new I4energy_Model_Help();
    $start = 0;
    if (isset ($_POST['start']))
      $_POST['start'] = (int) $_POST['start'];

    $limit = PAGE_LIMIT;
    if (isset ($_POST['limit']))
      $limit = (int) $_POST['limit'];
    $response = $helps_obj->listHelps (array(), 'title', 'asc', $start, $limit);
    $this->view->totalrecords = $response->totalrecords;
    $this->view->rows = array();
    if ($response->returnrecords > 0)
    {
      foreach ($response->rows as $row)
      {
        $help = new I4energy_Model_Help();
        $help->setId($row['id']);
        $help->setTitle($row['title']);
        $help->setUrl($row['url']);
        $help->setCreatedBy($row['createdby']);
        $help->setCreatedDate(
            I4energy_Model_Utils_DateUtils::getFormattedDate ($row['createddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
        );
        $help->setUpdatedBy($row['updatedby']);
        $help->setUpdatedDate(
            I4energy_Model_Utils_DateUtils::getFormattedDate ($row['updateddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
        );
        array_push ($this->view->rows, $help);
        unset ($user);
      }
    }
  }



  public function addAction(){
    $help_id = (int) $this->_request->getParam ('help_id');
    if (isset($help_id) && $help_id > 0)
    {
      if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'help_edit', $this->i4energy_session->user_priviledge))
      {
        $this->_redirect ('/accessdenied');
        exit;
      }
    }
    else
    {
      if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'help_add', $this->i4energy_session->user_priviledge))
      {
        $this->_redirect ('/accessdenied');
        exit;
      }
    }

    $response =array();
    if($this->_request->isPost())
    {
      $filter=array();
      $title = $this->_request->getParam ('title');
      $url = $this->_request->getParam ('url');

// 		site edit
      if (isset($help_id) && $help_id > 0){
        $filter[1]['field']='id';
        $filter[1]['operation']='ne';
        $filter[1]['value']=$help_id;
      }

      if(count($response)==0){

        $help = new I4energy_Model_Help();

        if (isset($help_id) && $help_id > 0)
          $help->setId($help_id);
        else
        {
          $help->setCreatedBy($this->i4energy_session->user_id);
          $help->setCreatedDate(gmdate ('Y-m-d H:i:s'));
        }
        $help->setTitle($title);
        $help->setUrl($url);
        $help->setUpdatedBy($this->i4energy_session->user_id);
        $help->setUpdatedDate(gmdate ('Y-m-d H:i:s'));

        $help_id = $help->save (true);

        if ($help_id > 0)
        {
          $this->view->success = true;
        }
        else
        {

          $this->view->success = false;
        }

      }
      else{

        $this->view->response = $response;
      }
    }

    if (isset($help_id) && $help_id > 0)
    {
      $help = new I4energy_Model_Help();
      $help->init ($help_id);
      $this->view->help = $help;
    }
  }

  function viewAction ()
  {
    $help_id = (int) $this->_request->getParam ('help_id');


    if ($help_id > 0)
    {
      $help = new I4energy_Model_Help();
      $help->init ($help_id);

      $help_id = $help->getId();
      if (isset ($help_id) && $help_id != '')
      {
        $help->setUpdatedDate(
            I4energy_Model_Utils_DateUtils::getFormattedDate ($help->getUpdatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
        );
        $help->setCreatedDate(
            I4energy_Model_Utils_DateUtils::getFormattedDate ($help->getCreatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
        );
        $this->view->help = $help;
      }
      else
      {
        throw new Zend_Controller_Action_Exception('Site not found', 404);
      }
    }
    else
    {
      throw new Zend_Controller_Action_Exception('Site not found', 404);
    }
  }

  function deleteAction()
  {
    $help_id = (int) $this->_request->getParam ('help_id');


    if($this->_request->isPost())
    {
      $help = new I4energy_Model_Help();
      $help->init ($help_id);
      $help->delete ();
    }

    exit;
  }
}

<?php

class IndexController extends Zend_Controller_Action
{
	public function init() 
	{

		$this->i4energy_session = new Zend_Session_Namespace('I4energySession');
		$this->page_limit = 30;
	}

	public function indexAction() 
	{
		if (isset($this->i4energy_session->user_id) && ($this->i4energy_session->user_id != ''))
		{
			$this->_redirect ('/home');
		}
		
		$auth_token = $this->getRequest()->getCookie('token');
		if (isset ($auth_token) && $auth_token != '')
		{
			$users_obj = new I4energy_Model_Users();
			$users_obj = $users_obj->authenticateToken ($auth_token);
			if ($users_obj != null && $users_obj->getId() > 0)
			{
				$this->i4energy_session->user_id = $users_obj->getId();
				$this->i4energy_session->using_token = true;
				
				$timezone = $users_obj->getTimeZone();
				if (!isset ($timezone) || $timezone == '')
					$users_obj->setTimeZone (DEFAULT_TIMEZONE);
				
				$data_format = $users_obj->getDateFormat();
				if (!isset ($data_format) || $data_format == '')
					$users_obj->setDateFormat(DEFAULT_DATEFORMAT);
				
				$this->i4energy_session->user_obj = $users_obj;
				
				$rolePriviledge_mapper_obj =new I4energy_Model_RolePriviledgeMapper();
				$priviledge_mapper_obj=new I4energy_Model_PriviledgeMapper();
				$role_mapper_obj=new I4energy_Model_RoleMapper();
					
				$rolePriviledge_filter=array();
				$rolePriviledge_filter[0]['field']="role_id";
				$rolePriviledge_filter[0]['value']=$users_obj->getRole_id();
				$rolePriviledge_filter[0]['operation']="eq";
				
				$rolePriviledge_list = $rolePriviledge_mapper_obj->listRolePriviledge($rolePriviledge_filter);
				if($rolePriviledge_list->totalrecords >0){
					$priviledge_ids = "";
					foreach ($rolePriviledge_list->rows as $rws){
						if($priviledge_ids ==""){
							$priviledge_ids =$rws['priviledge_id'];
						}else{
							$priviledge_ids .= ",".$rws['priviledge_id'];
						}
					}
				}
				
				$priviledge_filter=array();
				$permitedPriviledge=array();
				if(isset($priviledge_ids) && $priviledge_ids != "" ){
					$priviledge_filter[0]['field']="id";
					$priviledge_filter[0]['value']=$priviledge_ids;
					$priviledge_filter[0]['operation']="in";
					$priviledge_list = $priviledge_mapper_obj->listPriviledge($priviledge_filter);
					foreach ($priviledge_list->rows as $rws){
						if(!in_array($rws['priviledge'], $permitedPriviledge)){
							array_push($permitedPriviledge,$rws['priviledge']);
						}
					}
				
				}
					
				$role_filter=array();
				$role_filter[0]['field']="id";
				$role_filter[0]['value']=$users_obj->getRole_id();
				$role_filter[0]['operation']="eq";
					
				$user_role_query=$role_mapper_obj->listRole($role_filter);
				$this->i4energy_session->user_priviledge = $permitedPriviledge;
				$this->i4energy_session->user_role = strtolower($user_role_query->rows[0]['role']);
				
				// 	assigne site filter: Start
				$assignsitesArray=array();
				if($this->i4energy_session->user_role != ADMIN_USER ){
					$siteuser_obj=new I4energy_Model_SitesUsersMapper();
					$search=array();
					$search[0]['field'] = 'user_id';
					$search[0]['operation'] = '=';
					$search[0]['value'] = $this->i4energy_session->user_id;
					$assigne_sites_query =$siteuser_obj->listSitesUsers($search);
				}
				$assignsites="";
				if(isset($assigne_sites_query) && $assigne_sites_query->totalrecords>0){
					foreach ($assigne_sites_query->rows as $row){
						array_push($assignsitesArray, $row['site_id']);
						if($assignsites ==""){
							$assignsites = $row['site_id'];
						}
						else {
							$assignsites .=" , ".$row['site_id'];
						}
					}
				}
				$user_sitefilter=array();
				if($assignsites !="" && $this->i4energy_session->user_role != ADMIN_USER){
				
					$user_sitefilter[0]['field'] = 'id';
					$user_sitefilter[0]['operation'] = 'in';
					$user_sitefilter[0]['value'] = $assignsites;
				}
				$this->i4energy_session->user_sitefilter = $user_sitefilter;
				$this->i4energy_session->user_assignsites = $assignsitesArray;
				// assigne site filter :End
				
				$back_url = $this->_request->getParam('bk');
				if (isset ($back_url) && $back_url != '')
					$this->_redirect ($back_url);
				else
					$this->_redirect ('/home');
				$this->_redirect ('/home');
			}
		}
		
		$back_url = $this->_request->getParam('bk');
		if (isset ($back_url) && $back_url != '')
			$this->view->bkurl = '?bk=' . $back_url;
		else
			$this->view->bkurl = '';
	}

	public function loginAction ()
	{
		

		if (isset($this->i4energy_session->user_id) && ($this->i4energy_session->user_id != ''))
		{
			$this->_redirect ('/home');
		}
		else if($this->_request->isPost())
		{   

			unset($this->i4energy_session->isforward);
			unset($this->i4energy_session->customer_id);
			unset($this->i4energy_session->user_obj);
			
			$user_name = trim ($_POST['email']);
			$password = trim ($_POST['password']);
			$remember = false;

			if (isset ($_POST['rememberMe']))
				$remember = true;
			
			if ($user_name == '' || $password == '')
			{
				$this->view->error = 'Email and Password are mandatory';
				$this->_forward ('index');
			}
			else if ($user_name != '' && $password != '')
			{

				$users_obj = new I4energy_Model_Users();
				$users_obj = $users_obj->authenticate($user_name, $password);
				
				if ($users_obj == null)
				{
					$this->view->error = 'Invalid email or password';
					$this->_forward ('index');
				}
				else if ($users_obj->getStatus() == 0)
				{
					$this->view->error = 'Your account has been deactivated, please contact administrator';
					$this->_forward ('index');
				}
				else
				{
					$this->i4energy_session->user_id = $users_obj->getId();
					$this->i4energy_session->customer_id = $users_obj->getCustomer_id();
					if ($remember)
					{
						$token = md5(uniqid(mt_rand(), true));
						$users_obj->setToken ($token);
						$users_obj->updateToken ($token);
						setcookie('token', $token, time() + 31536000, '/');
					}
					else
					{
						$users_obj->setToken ('');
						$users_obj->updateToken ('');
						setcookie('token', '', time() - 3600, '/');
					}
					
					$timezone = $users_obj->getTimeZone();
					if (!isset ($timezone) || $timezone == '')
						$users_obj->setTimeZone (DEFAULT_TIMEZONE);
					
					$data_format = $users_obj->getDateFormat();
					if (!isset ($data_format) || $data_format == '')
						$users_obj->setDateFormat(DEFAULT_DATEFORMAT);
					
					$this->i4energy_session->user_obj = $users_obj;
					
					$rolePriviledge_mapper_obj =new I4energy_Model_RolePriviledgeMapper();
					$priviledge_mapper_obj=new I4energy_Model_PriviledgeMapper();
					$role_mapper_obj=new I4energy_Model_RoleMapper();
					
					$rolePriviledge_filter=array();
					$rolePriviledge_filter[0]['field']="role_id";
					$rolePriviledge_filter[0]['value']=$users_obj->getRole_id();
					$rolePriviledge_filter[0]['operation']="eq";
						
					$rolePriviledge_list = $rolePriviledge_mapper_obj->listRolePriviledge($rolePriviledge_filter);
					if($rolePriviledge_list->totalrecords >0){
						$priviledge_ids = "";
						foreach ($rolePriviledge_list->rows as $rws){
							if($priviledge_ids ==""){
								$priviledge_ids =$rws['priviledge_id'];
							}else{
								$priviledge_ids .= ",".$rws['priviledge_id'];
							}
						}
					}
						
					$priviledge_filter=array();
					$permitedPriviledge=array();
					if(isset($priviledge_ids) && $priviledge_ids != "" ){
						$priviledge_filter[0]['field']="id";
						$priviledge_filter[0]['value']=$priviledge_ids;
						$priviledge_filter[0]['operation']="in";
						$priviledge_list = $priviledge_mapper_obj->listPriviledge($priviledge_filter);
						foreach ($priviledge_list->rows as $rws){
						if(!in_array($rws['priviledge'], $permitedPriviledge)){
								array_push($permitedPriviledge,$rws['priviledge']);
							}
						}
						
					}
					
					$role_filter=array();
					$role_filter[0]['field']="id";
					$role_filter[0]['value']=$users_obj->getRole_id();
					$role_filter[0]['operation']="eq";
					
					$user_role_query=$role_mapper_obj->listRole($role_filter);
					$this->i4energy_session->user_priviledge = $permitedPriviledge;
					$this->i4energy_session->user_role = strtolower($user_role_query->rows[0]['role']);

					// 	assigne site filter: Start					
					$assignsitesArray=array();
					if($this->i4energy_session->user_role != ADMIN_USER ){
						$siteuser_obj=new I4energy_Model_SitesUsersMapper();
						$search=array();
						$search[0]['field'] = 'user_id';
						$search[0]['operation'] = '=';
						$search[0]['value'] = $this->i4energy_session->user_id;
						$assigne_sites_query =$siteuser_obj->listSitesUsers($search);
					}
					$assignsites="";
					if(isset($assigne_sites_query) && $assigne_sites_query->totalrecords>0){
						foreach ($assigne_sites_query->rows as $row){
							array_push($assignsitesArray, $row['site_id']);
							if($assignsites ==""){
								$assignsites = $row['site_id'];
							}
							else {
								$assignsites .=" , ".$row['site_id'];
							}
						}
					}
					$user_sitefilter=array();
					if($assignsites !="" && $this->i4energy_session->user_role != ADMIN_USER){

						$user_sitefilter[0]['field'] = 'id';
						$user_sitefilter[0]['operation'] = 'in';
						$user_sitefilter[0]['value'] = $assignsites;
					}
					$this->i4energy_session->user_sitefilter = $user_sitefilter;
					$this->i4energy_session->user_assignsites = $assignsitesArray;
					// assigne site filter :End 
								
					
					// assigne meter filter:Start
					/*$assignmetersArray=array();
					if($this->i4energy_session->user_role != ADMIN_USER ){
						$meteruser_obj=new I4energy_Model_MeterUser();
						$search=array();
						$search[0]['field'] = 'user_id';
						$search[0]['operation'] = '=';
						$search[0]['value'] = $this->i4energy_session->user_id;
						$assigne_meters_query =$meteruser_obj->listMeterUser($search);
					}
					$assignmeters="";
					if(isset($assigne_meters_query) && $assigne_meters_query->totalrecords>0){
						foreach ($assigne_meters_query->rows as $row){
							array_push($assignmetersArray, $row['meter_id']);
							if($assignmeters ==""){
								$assignmeters = $row['meter_id'];
							}
							else {
								$assignmeters .=" , ".$row['meter_id'];
							}
						}
					}
					
					$this->i4energy_session->user_assignmetersarray = $assignmetersArray;
					$this->i4energy_session->user_assignmeters = $assignmeters;		*/
					// 	assigne meter filters: End
						
					$back_url = $this->_request->getParam('bk');
					if (isset ($back_url) && $back_url != '')
						$this->_redirect ($back_url);
					else
						$this->_redirect ('/home');
				}
			}
			else
			{
				$this->view->error = 'Something is wrong! Please contact administrator';
				$this->_forward ('index');
			}
		}
		else
		{

			$this->view->error = 'Something is wrong! Please contact administrator';
			$this->_redirect ('index');
		}
	}
	
	public function logoutAction()
	{
		if ( isset($this->i4energy_session->user_id) && ($this->i4energy_session->user_id != ''))
		{
			$users_obj = new I4energy_Model_Users();
			$users_obj->init ($this->i4energy_session->user_id);
			$users_obj->setToken ('');
			$users_obj->updateToken ('');
			setcookie('token', '', time() - 3600, '/');
		}
				
		Zend_Session::destroy(true, true);
		$this->_redirect('/');
	}
	
	public function homeAction()
	{
		if ( !(isset($this->i4energy_session->user_id) && ($this->i4energy_session->user_id != '')))
		{
			$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);;
		}

		$assignsites = array();
		if($this->i4energy_session->user_role != ADMIN_USER )
		{
			$assignsites = $this->i4energy_session->user_assignsites;
			if (sizeof ($assignsites) == 0)
				array_push($assignsites, 0);
		}

		$page = 1;
		
		$site_obj = new I4energy_Model_Sites();
		$select = $site_obj->getMapper()->getSelect();
		$select->sql_cals_found_rows (true);
		$select->from ('sites', array ('id' => 'id','name' => 'name','code'=>'code','createdby'=>'createdby','createddate'=>'createddate','updatedby'=>'updatedby','updateddate'=>'updateddate',));
		if (sizeof ($assignsites) > 0)
			$select->where ("id IN (" . implode (',', $assignsites) . ")");
		$select->limitPage($page, $this->page_limit);
 		//echo $select->__toString();exit;
		$stmt = $site_obj->getMapper()->getDbTable()->getAdapter()->query($select);
		$response = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		
		$stmt = new Zend_Db_Statement_Pdo($site_obj->getMapper()->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
		$stmt->execute();
		$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
		
		$this->view->page_limit = $this->page_limit;
		$this->view->totalsites = $count['totalrecords'];
		$site_details = array();
		
		if (count($response) > 0)
		{
			foreach ($response as $row)
			{
				$site_id = $row['id'];
				
				$site = new I4energy_Model_Sites();
				$site->setId($row['id']);
				$site->setName($row['name']);
				$site->setCode($row['code']);
				$site->setCreatedBy($row['createdby']);
				$site->setCreatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($row['createddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$site->setUpdatedBy($row['updatedby']);
				$site->setUpdatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($row['updateddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
		
				$site_details[$site_id]['site'] = $site;
				$site_details[$site_id]['meters'] = array();
				
				$search = array();
				$search[0]['field'] = 'siteid';
				$search[0]['operation'] = '=';
				$search[0]['value'] = $site_id;

				$meter_obj = new I4energy_Model_Meters();
				$meters = $meter_obj->listMeters ($search, '', '', null, null);
				
				 if ($meters->returnrecords > 0)
				{
					foreach ($meters->rows as $meter_row)
					{

						$meter = new I4energy_Model_Meters();
						$meter->init ($meter_row['id']);
						$meter->setCreatedDate(
								I4energy_Model_Utils_DateUtils::getFormattedDate ($meter_row['createddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
						);
						$meter->setUpdatedDate(
								I4energy_Model_Utils_DateUtils::getFormattedDate ($meter_row['updateddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
						);

						array_push ($site_details[$site_id]['meters'], $meter);
						unset ($meter);
					}
				 }
				
				unset ($site);
			}
		}
		
		$this->view->sites = $site_details;
	
	}
	
	public function loadmoresitesAction(){
		$htmlstring='';
		$meterarray=array();
		if ( !(isset($this->i4energy_session->user_id) && ($this->i4energy_session->user_id != '')))
		{
			$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);;
		}
		
		$assignsites = array();
		if($this->i4energy_session->user_role != ADMIN_USER )
		{
			$assignsites = $this->i4energy_session->user_assignsites;
			if (sizeof ($assignsites) == 0)
				array_push($assignsites, 0);
		}
		
		$page=$this->_request->getParam('page');
// 		echo $page;exit;
		$limit=$this->_request->getParam('limit');
		
		$site_obj = new I4energy_Model_Sites();
		$select = $site_obj->getMapper()->getSelect();
		$select->sql_cals_found_rows (true);
		$select->from ('sites', array ('id' => 'id','name' => 'name','code'=>'code','createdby'=>'createdby','createddate'=>'createddate','updatedby'=>'updatedby','updateddate'=>'updateddate',));
		if (sizeof ($assignsites) > 0)
			$select->where ("id IN (" . implode (',', $assignsites) . ")");
		$select->limitPage($page, $limit);
		// 		echo $select->__toString();exit;
		$stmt = $site_obj->getMapper()->getDbTable()->getAdapter()->query($select);
		$response = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
		
		$stmt = new Zend_Db_Statement_Pdo($site_obj->getMapper()->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
		$stmt->execute();
		$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
		
		$total_pages = ceil($count['totalrecords'] / $limit);
		
		$this->view->totalsites = count($response);
		$site_details = array();
		
		if (count($response) > 0)
		{
			foreach ($response as $row)
			{
				$site_id = $row['id'];
		
				$site = new I4energy_Model_Sites();
				$site->setId($row['id']);
				$site->setName($row['name']);
				$site->setCode($row['code']);
				$site->setCreatedBy($row['createdby']);
				$site->setCreatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($row['createddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$site->setUpdatedBy($row['updatedby']);
				$site->setUpdatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($row['updateddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
		
		$htmlstring .= '<div class="row white-border" id="site_'.$site_id.'" style="border-bottom: 1px solid #666666;" >
		<div class="col-xs-6 .col-sm-4 col-md-2 only-border margin-bottom22 box1">
		      <p class="text-center text-capitalize">'.$site->getName().'</p>
		      <br/>
		      <p class="text-center">'.$site->getCode().'</p>
		    </div>
		    <div class="col-xs-6 .col-sm-8 col-md-9 only-border pull-right border-none">';

		$search = array();
		$search[0]['field'] = 'siteid';
		$search[0]['operation'] = '=';
		$search[0]['value'] = $site_id;
		$meter_obj = new I4energy_Model_Meters();
		$meters = $meter_obj->listMeters ($search, '', '', null, null);
		
		if ($meters->returnrecords > 0)
		{
			$htmlstring .='<div class="row">';
			foreach ($meters->rows as $meter_row)
			{		
					if (!in_array($meter_row['id'],$meterarray)){
						array_push($meterarray, $meter_row['id']);
					}
					
					$meter = new I4energy_Model_Meters();
					$meter->init ($meter_row['id']);
					$meter->setCreatedDate(
							I4energy_Model_Utils_DateUtils::getFormattedDate ($meter_row['createddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
					);
					$meter->setUpdatedDate(
							I4energy_Model_Utils_DateUtils::getFormattedDate ($meter_row['updateddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
					);
			
				$css_class = 'internal-border';
				$latest_reading = null;

				$meter_type = $meter->getType();
				if ($meter_type == 'hourrun')
					$meter_type = 'hours run';

				$htmlstring .= '<div class="col-xs-6 .col-sm-4 col-md-2">
	          				<p id="meter-id-'.$meter->getId().'" class="text-center text-capitalizen '.$css_class.'">
	          				<a class="link" href="/meters/site-'.$site_id.'/'.$meter->getId().'">'. $meter->getName().'</a> 
				          	<br/>
	            			'.$meter->getMac() . ' / ' . $meter_type.'
				            <br/>	<span id="meter-reading-'.$meter->getId().'"></span>
	    	    			</p>
		        			</div>';
				
			}
				$htmlstring .='</div>';
				
		}else{
			$htmlstring .= '<p class="text-center">No meter associated with this site.</p>';
		}
		
     
		$htmlstring .= '</div>
						</div>';
		
			}
			
		}
		
		$result = array();
		$result['page'] = $page+1;
		$result['totalpages'] = $total_pages;
		$result['meterids'] = $meterarray;
		$result['data'] = $htmlstring;
		echo json_encode($result);
		exit;
	}
	
	public function accessdeniedAction () {}
	
	public function resetpasswordAction(){
		
		if($this->_request->isPost()){
			$email = $this->_request->getParam('email');
			
			$alphabet = "abcde9874562310fghijklmn^^opqrstuwx@#^%&*()_+yzABCDEFGHI78654JKLMNOPQRSTUWXYZ0123456789";
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			$password = implode($pass);
			$usertable = new I4energy_Model_UsersMapper();
			$stmt = new Zend_Db_Statement_Pdo($usertable->getDbTable()->getDefaultAdapter(), "SELECT PASSWORD('{$password}') AS new_password");
			$stmt->execute();
			$password_res = $stmt->fetch(Zend_Db::FETCH_ASSOC);
			
			$new_password=$password_res['new_password'];
			
			$users_obj = new I4energy_Model_Users();
			$search = array();
			$search[0]['field'] = 'email';
			$search[0]['operation'] = '=';
			$search[0]['value'] = $email;

			$user = $users_obj->listUsers($search);
			
			if(isset($user->totalrecords) && $user->totalrecords > 0 && isset($user->rows[0]['id']) && $user->rows[0]['id']>0){
				$userObj=new I4energy_Model_Users();
				$user_id=$user->rows[0]['id'];
				$userObj->init($user_id);
				$user_name=ucfirst($userObj->getName());
				
				$userObj->setPassword($new_password);
				$newuser_id=$userObj->save(true);
				
				$users_auth = new I4energy_Model_Users();
				$users_auth = $users_auth->authenticate($email, $new_password);
				
				if( isset($users_auth) && $users_auth !=null)
				{
					$message = "Hi $user_name,<br/><br/>Please find your new password below<br/><br/>";
					$message .= "Username: " . $email;
					$message .= "<br/>New Password: " . $password;
					$message .= "<br/><br/>Regards,<br/>i4energy<br/>";
					
					I4energy_Model_Utils_SendMail::sendmailzend($email, 'i4energy: Password Reset', $message);
					$this->view->success = true;	
				}else{
					$this->view->success = false;
				}
			}
			else
			{
				$this->view->success = false;
				$this->view->error_msg = "Specified email address not found.";
			}
			
		}
		
	}
	
	public function searchAction()
	{
		$return_arr = array();
		$assignsites = array();
		if($this->i4energy_session->user_role != ADMIN_USER )
		{
			$assignsites = $this->i4energy_session->user_assignsites;
			if (sizeof ($assignsites) == 0)
				array_push($assignsites, 0);
		}
		
 		/*  $db_table = new Zend_Db_Table('sites');
		$select = $db_table->select()->setIntegrityCheck(false);
		$select
		->from(array('s' => 'sites'),array('id'))
				->joinLeft(array('m' => 'meters'),'s.id = m.siteid',
						   array('sname' => 's.name','mname' => 'm.name'));
		
		//$select->where("s.name  LIKE '%".$_GET['term']."%' || m.name LIKE '%".$_GET['term']."%' ");
				$select->where("s.name  LIKE '%".$_GET['term']."%' ");
		//echo $select;
		$select->group(array("sname"));
		$stmt = $db_table->getAdapter()->query($select);
		
		while($row = $stmt->fetch()) {
			$return_arr[] =  "meters :  ".$row['mname'];
			$return_arr[] =  "Sites	: ".$row['sname'];
		} */
		
			$sites_table = new I4energy_Model_DbTable_Sites();
			$select = $sites_table->select();
			$select->where ("sites.name  LIKE '%".$_GET['term']."%' ");
			if (sizeof ($assignsites) > 0)
				$select->where ("id IN (" . implode (',', $assignsites) . ")");
			$stmt = $sites_table->getAdapter()->query($select);
			while($row = $stmt->fetch()) {
				$return_arr[] =  "Sites :  ".$row['name'];
			}
			
			$select = $sites_table->select();
			$select->where ("sites.code LIKE '%".$_GET['term']."%' ");
			if (sizeof ($assignsites) > 0)
				$select->where ("id IN (" . implode (',', $assignsites) . ")");
			$stmt = $sites_table->getAdapter()->query($select);
			while($row = $stmt->fetch()) {
				$return_arr[] =  "Sites Code :  ".$row['code'];
			}
			
			
			$meter_table = new I4energy_Model_DbTable_Meters();
			$select = $meter_table->select()
						->distinct();
			$select->where("meters.name LIKE '%".$_GET['term']."%'");
			if (sizeof ($assignsites) > 0)
				$select->where ("id IN (" . implode (',', $assignsites) . ")");
			$select->group('name');
			$stmt = $meter_table->getAdapter()->query($select);
			while($row = $stmt->fetch()) {
				$return_arr[] =  "Meters :  ".$row['name'];
			}
			
			
			$select = $meter_table->select()
			->distinct();
			$select->where("meters.mac LIKE '%".$_GET['term']."%'");
			$select->group('mac');
			$stmt = $meter_table->getAdapter()->query($select);
			while($row = $stmt->fetch()) {
				$return_arr[] =  "MAC :  ".$row['mac'];
			}
				
			$meter_attr_table = new I4energy_Model_DbTable_MeterAttributes();
			$select = $meter_attr_table->select()
			->distinct();
			$select->where("simcardno LIKE '%".$_GET['term']."%'");
			if (sizeof ($assignsites) > 0)
				$select->where ("id IN (" . implode (',', $assignsites) . ")");
			$select->group('simcardno');
			$stmt = $meter_attr_table->getAdapter()->query($select);
			while($row = $stmt->fetch()) {
				$return_arr[] =  "Sim Card No :  ".$row['simcardno'];
			}
			
			$select = $meter_attr_table->select()
			->distinct();
			$select->where("phoneno LIKE '%".$_GET['term']."%'");
			if (sizeof ($assignsites) > 0)
				$select->where ("id IN (" . implode (',', $assignsites) . ")");
			$select->group('phoneno');
			$stmt = $meter_attr_table->getAdapter()->query($select);
			while($row = $stmt->fetch()) {
				$return_arr[] =  "Phone No :  ".$row['phoneno'];
			} 
			
			/* $customer_table = new I4energy_Model_DbTable_Customers();
			$select = $customer_table->select()
			->distinct();
			$select->where("name LIKE '%".$_GET['term']."%'");
			if (sizeof ($assignsites) > 0)
				$select->where ("id IN (" . implode (',', $assignsites) . ")");
			$select->group('name');
			$stmt = $customer_table->getAdapter()->query($select);
			while($row = $stmt->fetch()) {
				$return_arr[] =  "Customer :  ".$row['name'];
			} */
			//print_r($return_arr);
			echo json_encode($return_arr);

			exit;
	}
	
	
	public function searchdataAction()
	{
		$htmlstring='';
		$meterarray=array();
		$result=array();
		if ( !(isset($this->i4energy_session->user_id) && ($this->i4energy_session->user_id != '')))
		{
			$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);;
		}
	
		$assignsites = array();
		if($this->i4energy_session->user_role != ADMIN_USER )
		{
			$assignsites = $this->i4energy_session->user_assignsites;
			if (sizeof ($assignsites) == 0)
				array_push($assignsites, 0);
		}
	
		$page = 1;
		$res_i = 0;
		
		if(!isset($_GET['table']))
		{
			$site_obj = new I4energy_Model_Sites();
			$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
			$select->from(array('s' => 'sites'),array('sname' => 's.name','siteid' => 's.id','code' => 's.code','createdby'=>'s.createdby','updatedby'=>'s.updatedby','createddate'=>'s.createddate','updateddate'=>'s.updateddate'))
			->joinLeft(array('m' => 'meters'), 'm.siteid = s.id', array('mname' => 'm.name','m.siteid'))
			->joinLeft(array('ma' => 'meter_attr'), 'ma.id = m.id', array());
				
			$select->where("s.name LIKE '%".$_GET['data']."%' ||   code  LIKE '%".$_GET['data']."%' || 
							m.name  LIKE '%".$_GET['data']."%' ||  mac  LIKE '%".$_GET['data']."%' ||
							ma.phoneno  LIKE '%".$_GET['data']."%' || ma.simcardno LIKE '%".$_GET['data']."%' ");
			$select->group('sname'); 
		}
		else 
		{
			if($_GET['table'] == 'Sites' || $_GET['table'] == 'Sites Code' )
			{
				$site_obj = new I4energy_Model_Sites();
				$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
				$select->sql_cals_found_rows (true);
				$select->from(array('s' => 'sites'),
							  array('sname' => 's.name','siteid' => 's.id','code' => 's.code','createdby'=>'s.createdby','updatedby'=>'s.updatedby','createddate'=>'s.createddate','updateddate'=>'s.updateddate'))
						->join(array('m' => 'meters'),'s.id = m.siteid',array('mname' => 'm.name'));
				
				$select->where("s.name  LIKE '%".addslashes($_GET['data'])."%' || code LIKE '%".addslashes($_GET['data'])."%' ");
				$select->group('s.name');
			}	
			if($_GET['table'] == 'Meters' || $_GET['table'] == 'MAC')
			{
				$site_obj = new I4energy_Model_Sites();
				$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
				$select
				->from(array('s' => 'sites'))
				->join(array('m' => 'meters'),'s.id = m.siteid',
						array('sname' => 's.name','mname' => 'm.name','m.siteid'));

				$select->where("m.name  LIKE '%".$_GET['data']."%' || m.mac LIKE '%".$_GET['data']."%' ");
				$select->group('s.name');
			}	
			if($_GET['table'] == 'Phone No' || $_GET['table'] == 'Sim Card No')
			{
				$site_obj = new I4energy_Model_Sites();
				$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
				$select->from(array('s' => 'sites'))
				->join(array('m' => 'meters'), 'm.siteid = s.id', array('sname' => 's.name','mname' => 'm.name','m.siteid'))
				->join(array('ma' => 'meter_attr'), 'ma.id = m.id', array());
			
				$select->where("ma.phoneno  LIKE '%".$_GET['data']."%' || ma.simcardno LIKE '%".$_GET['data']."%' ");
			}
			if($_GET['table'] == 'Customer')
			{
				$site_obj = new I4energy_Model_Sites();
				$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
				$select->from(array('s' => 'sites'))
				->join(array('m' => 'meters'), 'm.siteid = s.id', array('sname' => 's.name','mname' => 'm.name','m.siteid'))
				->join(array('ma' => 'meter_attr'), 'ma.id = m.id', array());
					
				$select->where("ma.phoneno  LIKE '%".$_GET['data']."%' || ma.simcardno LIKE '%".$_GET['data']."%' ");
			}
		}
		//echo $select;
				if (sizeof ($assignsites) > 0)
				{
					$select->where ("s.id IN (" . implode (',', $assignsites) . ")");
				}
				$select->limitPage($page, $this->page_limit);
				$stmt = $site_obj->getMapper()->getDbTable()->getAdapter()->query($select);
		
				$response = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
				$stmt = new Zend_Db_Statement_Pdo($site_obj->getMapper()->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
				
				$stmt->execute();
				$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
				
				$this->view->page_limit = $this->page_limit;
				$this->view->totalsites = $count['totalrecords'];
				$site_details = array();
				//print_r($response);
				//echo count($response);
				if (count($response) > 0)
				{
					foreach ($response as $row)
					{

						$site_id = $row['siteid'];
						
						$site = new I4energy_Model_Sites();
						$site->setId($site_id);
						$site->setName($row['sname']);
						$site->setCode($row['code']);
						$site->setCreatedBy($row['createdby']);
						$site->setCreatedDate(
								I4energy_Model_Utils_DateUtils::getFormattedDate ($row['createddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
						);
						$site->setUpdatedBy($row['updatedby']);
						$site->setUpdatedDate(
								I4energy_Model_Utils_DateUtils::getFormattedDate ($row['updateddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
						);
						
						$htmlstring .= '<div class="row white-border" id="site_'.$site_id.'" style="border-bottom: 1px solid #666666;" >
			<div class="col-xs-6 .col-sm-4 col-md-2 only-border margin-bottom22 box1">
		      <p class="text-center text-capitalize">'.$site->getName().'</p>
		      <br/>
		      <p class="text-center">'.$site->getCode().'</p>
		    </div>
		    <div class="col-xs-6 .col-sm-8 col-md-9 only-border pull-right border-none">';
						
						$search = array();
						$search[0]['field'] = 'siteid';
						$search[0]['operation'] = '=';
						$search[0]['value'] = $site_id;
						
						/* if(isset($_GET['table']) && !($_GET['table'] == 'Sites' || $_GET['table'] == 'Sites Code' ))
						{
							$search[1]['field'] = 'name';
							$search[1]['operation'] = 'cn';
							$search[1]['value'] = $row['mname'];
						} */
						if(isset($_GET['table']) && ($_GET['table'] == 'Meters'))
						{
							$search[1]['field'] = 'name';
							$search[1]['operation'] = 'cn';
							$search[1]['value'] = $_GET['data'];
						}
						if(isset($_GET['table']) && ($_GET['table'] == 'MAC'))
						{
							$search[1]['field'] = 'mac	';
							$search[1]['operation'] = 'cn';
							$search[1]['value'] = $_GET['data'];
						}
						 if(isset($_GET['table']) && ($_GET['table'] == 'Phone No'))
						{
							$search[1]['field'] = 'name';
							$search[1]['operation'] = 'cn';
							$search[1]['value'] = $row['mname'];
						} 
						$meter_obj = new I4energy_Model_Meters();
						$meters = $meter_obj->listMeters ($search, '', '', null, null);
					//	print_r($meters);
						if ($meters->returnrecords > 0)
						{
							$htmlstring .='<div class="row">';
							foreach ($meters->rows as $meter_row)
							{
								if (!in_array($meter_row['id'],$meterarray)){
									array_push($meterarray, $meter_row['id']);
								}
						
								$meter = new I4energy_Model_Meters();
								$meter->init ($meter_row['id']);
								$meter->setCreatedDate(
										I4energy_Model_Utils_DateUtils::getFormattedDate ($meter_row['createddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
								);
								$meter->setUpdatedDate(
										I4energy_Model_Utils_DateUtils::getFormattedDate ($meter_row['updateddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
								);
						
								$css_class = 'internal-border';
								$latest_reading = null;

								$meter_type = $meter->getType();
								if ($meter_type == 'hourrun')
									$meter_type = 'hours run';
								$htmlstring .= '<div class="col-xs-6 .col-sm-4 col-md-2">
	          				<p id="meter-id-'.$meter->getId().'" class="text-center text-capitalizen '.$css_class.'">
	          				<a class="link" href="/meters/site-'.$site_id.'/'.$meter->getId().'">'. $meter->getName().'</a>
				          	<br/>
	            			'.$meter->getMac() . ' / ' . $meter_type.'
				            <br/>	<span id="meter-reading-'.$meter->getId().'"></span>
	    	    			</p>
		        			</div>';
						
							}
							$htmlstring .='</div>';
						
						}else{
							$htmlstring .= '<p class="text-center">No meter associated with this site.</p>';
						}
						
						$htmlstring .= '</div>
						</div>';
						
						$result[$res_i]['data'] = $htmlstring;
						$result[$res_i]['meterids'] = $meterarray;
						$res_i++;
						
						unset ($site);
							
					}
				}
				echo json_encode($result);
			//}

		exit;
		$this->view->sites = $site_details;
	}
	
	public function diskspaceAction()
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini', APPLICATION_ENV);
		$res = $config->resources->multidb->db1->toArray();
		
		$dbConfig = array (
							'host'     => $res["host"],
							'username' => $res["username"],
							'password' => $res["password"],
							'dbname'   => $res["dbname"],
							);
		if (isset ($res["unix_socket"]) && $res["unix_socket"] != '')
			$dbConfig['unix_socket'] = $res['unix_socket'];
		
		$db = Zend_Db::factory($res["adapter"], $dbConfig);
		
		$stmt = $db->query(
				'SELECT table_name AS "Table", round(((data_length + index_length) / 1024 / 1024), 2) "Size in MB" FROM information_schema.TABLES
				 WHERE table_schema = "i4energy" AND table_name IN ("meter_rawdata","meter_data");'
		);
		
		
		$result = $stmt->fetchAll();
		$this->view->tablesize = $result;
		
		$stmt = $db->query(
			'SELECT table_schema "i4energy", round(SUM( data_length + index_length) / 1024 / 1024,2) "db_size_in_mb" 
				FROM information_schema.TABLES WHERE table_schema="i4energy" GROUP BY table_schema ;'
		);
		$result = $stmt->fetchAll();
		$this->view->dbsize = $result;
		
	}
	
}


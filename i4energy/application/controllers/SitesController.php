<?php

class SitesController extends Zend_Controller_Action
{
	public function init() 
	{

		$this->i4energy_session = new Zend_Session_Namespace('I4energySession');
		if (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == ''))
		{
			$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);;
			exit;
		}
		
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'site_view', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
				
	}

	public function indexAction() 
	{



		$site_obj = new I4energy_Model_Sites();
		$siteuser_obj=new I4energy_Model_SitesUsers();
		
		
		$start = 0;
		if (isset ($_POST['start']))
			$_POST['start'] = (int) $_POST['start'];
		
		$limit = PAGE_LIMIT;
		if (isset ($_POST['limit']))
			$limit = (int) $_POST['limit'];
		
		
		if($this->i4energy_session->user_role != ADMIN_USER ){
			$search=array();
			$search[0]['field'] = 'user_id';
			$search[0]['operation'] = '=';
			$search[0]['value'] = $this->i4energy_session->user_id;
			$assigne_sites_query =$siteuser_obj->listSitesUsers($search);
			
			if ($assigne_sites_query->totalrecords == 0)
			{
				$assigne_sites_query->rows = array ( array ('site_id' => 0) );
				$assigne_sites_query->totalrecords = 1;
			}
		}
		
		$assignsites = array();
		if(isset($assigne_sites_query) && $assigne_sites_query->totalrecords>0){
			
			foreach ($assigne_sites_query->rows as $row){
				array_push ($assignsites, $row['site_id']);
			}
		}
		
		$sitefilter=array();
		if(sizeof($assignsites) > 0) {
			$sitefilter[0]['field'] = 'id';
			$sitefilter[0]['operation'] = 'in';
			$sitefilter[0]['value'] = implode (',', $assignsites);
				
		}

		$response = $site_obj->listSites ($sitefilter, '', '', $start, $limit);
		
		$this->view->totalrecords = $response->totalrecords;
		$this->view->rows = array();
		if ($response->returnrecords > 0)
		{
			foreach ($response->rows as $row)
			{
				$site = new I4energy_Model_Sites();
				$site->setId($row['id']);
				$site->setName($row['name']);
				$site->setCode($row['code']);
				$site->setCreatedBy($row['createdby']);
				$site->setCreatedDate( 
						I4energy_Model_Utils_DateUtils::getFormattedDate ($row['createddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$site->setUpdatedBy($row['updatedby']);
				$site->setUpdatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($row['updateddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
					);
				
				array_push ($this->view->rows, $site);
				unset ($site);
			}
		}
	}

	function searchsitesAction () {
		$site_obj = new I4energy_Model_Sites();
		$siteuser_obj=new I4energy_Model_SitesUsers();

		$start = 0;
		if (isset ($_POST['start']))
			$_POST['start'] = (int) $_POST['start'];

		$limit = PAGE_LIMIT;
		if (isset ($_POST['limit']))
			$limit = (int) $_POST['limit'];

		if($this->i4energy_session->user_role != ADMIN_USER ){
			$search=array();
			$search[0]['field'] = 'user_id';
			$search[0]['operation'] = '=';
			$search[0]['value'] = $this->i4energy_session->user_id;
			$assigne_sites_query =$siteuser_obj->listSitesUsers($search);

			if ($assigne_sites_query->totalrecords == 0)
			{
				$assigne_sites_query->rows = array ( array ('site_id' => 0) );
				$assigne_sites_query->totalrecords = 1;
			}
		}

		$assignsites = array();
		if(isset($assigne_sites_query) && $assigne_sites_query->totalrecords>0){

			foreach ($assigne_sites_query->rows as $row){
				array_push ($assignsites, $row['site_id']);
			}
		}

		if(!isset($_GET['table']))
		{
			$site_obj = new I4energy_Model_Sites();
			$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
			$select->from(array('s' => 'sites'),array('sname' => 's.name','siteid' => 's.id','code' => 's.code','createdby'=>'s.createdby','updatedby'=>'s.updatedby','createddate'=>'s.createddate','updateddate'=>'s.updateddate'))
				->joinLeft(array('m' => 'meters'), 'm.siteid = s.id', array('mname' => 'm.name','m.siteid'))
				->joinLeft(array('ma' => 'meter_attr'), 'ma.id = m.id', array());

			$select->where("s.name LIKE '%".$_GET['data']."%' ||   code  LIKE '%".$_GET['data']."%' ||
							m.name  LIKE '%".$_GET['data']."%' ||  mac  LIKE '%".$_GET['data']."%' ||
							ma.phoneno  LIKE '%".$_GET['data']."%' || ma.simcardno LIKE '%".$_GET['data']."%' ");
			$select->group('sname');
		}
		else
		{
			if($_GET['table'] == 'Sites' || $_GET['table'] == 'Sites Code' )
			{
				$site_obj = new I4energy_Model_Sites();
				$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
				$select->sql_cals_found_rows (true);
				$select->from(array('s' => 'sites'),
					array('sname' => 's.name','siteid' => 's.id','code' => 's.code','createdby'=>'s.createdby','updatedby'=>'s.updatedby','createddate'=>'s.createddate','updateddate'=>'s.updateddate'))
					->join(array('m' => 'meters'),'s.id = m.siteid',array('mname' => 'm.name'));

				$select->where("s.name  LIKE '%".addslashes($_GET['data'])."%' || code LIKE '%".addslashes($_GET['data'])."%' ");
				$select->group('s.name');
			}
			if($_GET['table'] == 'Meters' || $_GET['table'] == 'MAC')
			{
				$site_obj = new I4energy_Model_Sites();
				$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
				$select
					->from(array('s' => 'sites'))
					->join(array('m' => 'meters'),'s.id = m.siteid',
						array('sname' => 's.name','mname' => 'm.name','m.siteid'));

				$select->where("m.name  LIKE '%".$_GET['data']."%' || m.mac LIKE '%".$_GET['data']."%' ");
				$select->group('s.name');
			}
			if($_GET['table'] == 'Phone No' || $_GET['table'] == 'Sim Card No')
			{
				$site_obj = new I4energy_Model_Sites();
				$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
				$select->from(array('s' => 'sites'))
					->join(array('m' => 'meters'), 'm.siteid = s.id', array('sname' => 's.name','mname' => 'm.name','m.siteid'))
					->join(array('ma' => 'meter_attr'), 'ma.id = m.id', array());

				$select->where("ma.phoneno  LIKE '%".$_GET['data']."%' || ma.simcardno LIKE '%".$_GET['data']."%' ");
			}
			if($_GET['table'] == 'Customer')
			{
				$site_obj = new I4energy_Model_Sites();
				$select = $site_obj->getMapper()->getSelect()->setIntegrityCheck(false);
				$select->from(array('s' => 'sites'))
					->join(array('m' => 'meters'), 'm.siteid = s.id', array('sname' => 's.name','mname' => 'm.name','m.siteid'))
					->join(array('ma' => 'meter_attr'), 'ma.id = m.id', array());

				$select->where("ma.phoneno  LIKE '%".$_GET['data']."%' || ma.simcardno LIKE '%".$_GET['data']."%' ");
			}
		}

		if (sizeof ($assignsites) > 0)
		{
			$select->where ("s.id IN (" . implode (',', $assignsites) . ")");
		}

		$stmt = $site_obj->getMapper()->getDbTable()->getAdapter()->query($select);
		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

		$html = '';
		foreach ($rows as $row) {
			$html .= '
				<h2 class="h2-head">' . $row['code'] . ': ' . $row['sname'] . '</h2>
				<p><strong class="h2-head">' . $row['updateddate'] . '</strong></p>

				<p>
					<a href="/sites/' . $row['siteid'] . '" class="btn btn-success" style="margin-right: 5px;">
						<i class="fa fa-file-text"></i> View Site
					</a>';
				if (I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'site_edit', $this->i4energy_session->user_priviledge)) {
					$html .= '<a href="/sites/add/' . $row['siteid'] . '" class="btn btn-info" style="margin-right: 5px;">
						<i class="fa fa-pencil-square-o"></i> Edit Site
					</a>';
				}

				if (I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'site_delete', $this->i4energy_session->user_priviledge)) {
					$html .= '<a class="btn btn-danger" data-toggle="modal" data-target="#site_delete_modal" onclick="set_site_id(' . $row['siteid'] . ')">
						<i class="fa fa-times"></i> Delete Site
					</a>';
				}

			$html .= '</p>

				<div id="site_msg_' . $row['siteid'] . '"></div>
				<br/>';
		}

		echo $html;
		exit;
	}

	function viewAction ()
	{
		$site_id = (int) $this->_request->getParam ('id');
		
		// Check whether site is assigned
		if (!($this->i4energy_session->user_role == ADMIN_USER || in_array ($site_id, $this->i4energy_session->user_assignsites)))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$user_assignsites=$this->i4energy_session->user_assignsites;
		
		$siteIndex = array_search($site_id, (array) $user_assignsites);
		if(is_int($siteIndex) !=1 && $this->i4energy_session->user_role !=ADMIN_USER){
			$this->_redirect ('/sites');
			exit;
		}
		if ($site_id > 0)
		{
			$site = new I4energy_Model_Sites();
			$site->init ($site_id);
			
			$site_id = $site->getId();
			if (isset ($site_id) && $site_id != '')
			{
				$site->setUpdatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($site->getUpdatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$site->setCreatedDate(
						I4energy_Model_Utils_DateUtils::getFormattedDate ($site->getCreatedDate(), $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
				);
				$this->view->site = $site;
				
				$meter_obj = new I4energy_Model_Meters();
				
				$start = 0;
				if (isset ($_POST['start']))
					$_POST['start'] = (int) $_POST['start'];
				
				$limit = PAGE_LIMIT;
				if (isset ($_POST['limit']))
					$limit = (int) $_POST['limit'];
				
				$search = array();
				$search[0]['field'] = 'siteid';
				$search[0]['operation'] = '=';
				$search[0]['value'] = $site_id;

// 				assigne meter filter
				$assigne_meters=$this->i4energy_session->user_assignmeters;
				if ($assigne_meters==""){
					$assigne_meters=0;
				}
				if($assigne_meters !="" ){
					$search[1]['field'] = 'id';
					$search[1]['operation'] = 'in';
					$search[1]['value'] = $assigne_meters;
				}
				
				$response = $meter_obj->listMeters ($search, '', '', $start, $limit);
				
				$this->view->totalrecords = $response->totalrecords;
				$this->view->meters = array();
				if ($response->returnrecords > 0)
				{
					foreach ($response->rows as $row)
					{
						$meter = new I4energy_Model_Meters();
						$meter->init ($row['id']);
						$meter->setCreatedDate(
								I4energy_Model_Utils_DateUtils::getFormattedDate ($row['createddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
						);
						$meter->setUpdatedDate(
								I4energy_Model_Utils_DateUtils::getFormattedDate ($row['updateddate'], $this->i4energy_session->user_obj->dateformat, $this->i4energy_session->user_obj->timezone)
						);
						
						array_push ($this->view->meters, $meter);
						unset ($meter);
					}
				}
			}
			else
			{
				throw new Zend_Controller_Action_Exception('Site not found', 404);
			}
		}
		else
		{
			throw new Zend_Controller_Action_Exception('Site not found', 404);
		}
	}
	
	function addAction()
	{
		$site_id = (int) $this->_request->getParam ('id');
		if (isset($site_id) && $site_id > 0)
		{
			if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'site_edit', $this->i4energy_session->user_priviledge))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
				
			// Check whether site is assigned
			if (!($this->i4energy_session->user_role == ADMIN_USER || in_array ($site_id, $this->i4energy_session->user_assignsites)))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
		}
		else
		{
			if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'site_add', $this->i4energy_session->user_priviledge))
			{
				$this->_redirect ('/accessdenied');
				exit;
			}
		}
		
		$response =array();
		if($this->_request->isPost())
		{
			$filter=array();
			$site_name = $this->_request->getParam ('site_name');
				if($site_name == ""){
					$response['data']['site_name'] ='null';
				}else{
					$response['data']['site_name'] ='valid';
				}
			
			$site_code = $this->_request->getParam ('site_code');
				if( $site_code == ""){
					$response['data']['site_code'] ='null';
				}else{
					$response['data']['site_code'] ='valid';
					$filter[0]['field']='code';
					$filter[0]['operation']='eq';
					$filter[0]['value']=$site_code;
				}
// 		site edit		
			if (isset($site_id) && $site_id > 0){
				$filter[1]['field']='id';
				$filter[1]['operation']='ne';
				$filter[1]['value']=$site_id;
			}
			
			$site_mapper = new I4energy_Model_SitesMapper();
			$duplicate_sites=$site_mapper->listSites($filter);
		
			if($duplicate_sites->totalrecords > 0){
				$response['data']['site_code'] ='duplicate';
			}	
			
			if(isset($response['data']) && (in_array("null", $response['data']) || in_array("duplicate", $response['data']) || in_array("invalid", $response['data']))){
				$response['returnvalue']="invalid";
			}
			else{
				$response=array();
			}
				
			if(count($response)==0){
			
			$site = new I4energy_Model_Sites();
			
			if (isset($site_id) && $site_id > 0)
				$site->setId($site_id);
			else
			{
				$site->setCreatedBy($this->i4energy_session->user_id);
				$site->setCreatedDate(gmdate ('Y-m-d H:i:s'));
			}
			$site->setName($site_name);
			$site->setCode($site_code);
			$site->setUpdatedBy($this->i4energy_session->user_id);
			$site->setUpdatedDate(gmdate ('Y-m-d H:i:s'));
			
			$site_id = $site->save (true);
			
			if ($site_id > 0)
			{
				$this->view->success = true;
			}
			else
			{
				$this->view->success = false;
			}
			
			}
			else{
			$this->view->response = $response;
			}
		}
		
		if (isset($site_id) && $site_id > 0)
		{
			$site = new I4energy_Model_Sites();
			$site->init ($site_id);
			
			$this->view->site = $site;
		}
	}
	
	function deleteAction()
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'site_delete', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$site_id = (int) $this->_request->getParam ('id');
	
		// Check whether site is assigned
		if (!($this->i4energy_session->user_role == ADMIN_USER || in_array ($site_id, $this->i4energy_session->user_assignsites)))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		if($this->_request->isPost())
		{
			$site = new I4energy_Model_Sites();
			$site->init ($site_id);
			$site->delete ();
		}
		
		exit;
	}
	
	function usermapperAction()
	{
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'site_mapping_view', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
		$site_obj = new I4energy_Model_Sites();
		$user_obj = new I4energy_Model_Users();
		$sitesusers_obj=new I4energy_Model_SitesUsers();
		
		$sitefilter = array();
		if($this->i4energy_session->user_role != ADMIN_USER )
		{
			$sitefilter = $this->i4energy_session->user_sitefilter;
		}
		$sitelist = $site_obj->listSites ($sitefilter, '', '', '', '');
		$this->view->sites = $sitelist;
		$this->view->totalrecords = $sitelist->totalrecords;
		
		$userlist=	$user_obj->listUsers(array(), '', '', '', '');
		$this->view->users = $userlist;
		
		
					
		$siteArray =array();
		$siteusersArray=array();
		foreach ($sitelist->rows as $site){
			if(!in_array($site['id'],$siteArray)){
				array_push($siteArray, $site['id']);
				$siteRecordIndex=array_search($site['id'],$siteArray);
				$siteusersArray[$siteRecordIndex]['id'] = $site['id'];
				$siteusersArray[$siteRecordIndex]['name'] =ucwords($site['name']);
			}
		}
		
		$sitesuserslist=$sitesusers_obj->listSitesUsers(array(),'','','','');
		
		foreach ($sitesuserslist->rows as $rows){
			$siteRecordIndex=array_search($rows['id'],$siteArray);
		
			if(!isset($siteusersArray[$siteRecordIndex]['users'])){
				$siteusersArray[$siteRecordIndex]['users']=array();
			}
			
			$data=(array)$siteusersArray[$siteRecordIndex]['users'];
			$usersRecordIndex=-1;
// 			for($i=0;$i<count($data);$i++){
// 				if($data[$i]['id']==$rows['user_id']){
// 					$usersRecordIndex=$i;
// 					break;
// 				}
// 			}
			if($usersRecordIndex==-1){
				$usersRecordIndex=count($data);
				if(!in_array($rows['user_id'],$siteusersArray[$siteRecordIndex]['users'])){
					array_push($siteusersArray[$siteRecordIndex]['users'], $rows['user_id']);
				}			
			}
		}
		$this->view->siteuserArray = $siteusersArray;
		
		
		
	}
	
	function addusersAction(){
		$site_id = $this->_request->getParam ('siteid');
		$user_id = $this->_request->getParam ('userid');
		$sites_user_mapper_obj =new I4energy_Model_SitesUsersMapper();
		
		$delete ="";
		if(isset($site_id) && $site_id != ""){
			$where = $sites_user_mapper_obj->getDbTable()->getAdapter()->quoteInto('site_id = ?',$site_id );
			$sites_user_mapper_obj->getDbTable()->delete ($where);
		}
		if(isset($user_id)){
			foreach($user_id as $row){
				$sitesusers_obj=new I4energy_Model_SitesUsers();
				$sitesusers_obj->setUser_id($row);
				$sitesusers_obj->setSite_id($site_id);
				try{
					$lastsite_id = $sitesusers_obj->save (true);
					$response['returnvalue']="valid";
					$this->view->addusersuccess = true;
				}
				catch (Exception $e)
				{
					$this->view->addusersuccess = false;
				}
			}
			echo json_encode($response);
			exit;
		}
	
	}
	
	function allocatedusersAction(){
		$site_id = $this->_request->getParam ('siteid');
		
		$sites_user_mapper_obj =new I4energy_Model_SitesUsersMapper();
		$filter=array();
		$userArray=array();
		if(isset($site_id) && $site_id>0){
			$filterUser[0]['field'] = 'site_id';
			$filterUser[0]['operation'] = '=';
			$filterUser[0]['value'] = $site_id;
			$users=$sites_user_mapper_obj->listSitesUsers($filterUser);
			$userArray=$users->rows;
		}
		$response=array();
		$i=0;
		foreach ($userArray as $user){
				$response['data'][$i]=$user['user_id'];
				$i++;
			
		}
		echo json_encode($response);
		exit;
	}
	
	function allocatedmetersAction(){
		
		$site_id = $this->_request->getParam ('siteid');
		$meter_obj =new I4energy_Model_Meters();
		
		$filter=array();
		if(isset($site_id) && $site_id>0){
			$filter[0]['field'] = 'siteid';
			$filter[0]['operation'] = '=';
			$filter[0]['value'] = $site_id;
			$meters=$meter_obj->listMeters($filter);
		}

		$response=$meters;
		echo json_encode($response);
		exit;
	}
}

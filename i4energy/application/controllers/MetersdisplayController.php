<?php

class MetersdisplayController extends Zend_Controller_Action
{
	public function init() 
	{
		$this->i4energy_session = new Zend_Session_Namespace('I4energySession');
		if (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == ''))
		{
			$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);;
			exit;
		}
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'meter_display_link_view', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		
	}

	public function indexAction(){
		$meterlink_obj = new I4energy_Model_MeterDisplayLinks();
		
		$start = 0;
		if (isset ($_POST['start']))
			$_POST['start'] = (int) $_POST['start'];
		
		$limit = PAGE_LIMIT;
		if (isset ($_POST['limit']))
			$limit = (int) $_POST['limit'];
		
		
		$response = $meterlink_obj->listMeterDisplayLinks();
		
		$this->view->totalrecords = $response->totalrecords;
		$this->view->rows = array();

		if ($response->returnrecords > 0)
		{
			foreach ($response->rows as $row)
			{
				$meterlink = new I4energy_Model_MeterDisplayLinks();
				$meterlink->setId($row['id']);
				$meterlink->setTitle($row['title']);
				$meterlink->setUrl($row['url']);
				$meterlink->setLoginrequired($row['loginrequired']);
				$meterlink->setCustomerId($row['customer_id']);
				$meterlink->setTheme($row['theme']);
				$meterlink->setMeter1($row['meter1']);
				$meterlink->setMeter2($row['meter4']);
				$meterlink->setMeter3($row['meter3']);
				$meterlink->setMeter4($row['meter2']);
				
				array_push ($this->view->rows, $meterlink);
				unset ($meterlink);
			}
		}
	}

	public function addAction(){
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'meter_display_link_edit', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		$customer_obj =new I4energy_Model_Customers();
		$customers=$customer_obj->listCustomers();
		$this->view->customers = $customers;
		
		$site_obj = new I4energy_Model_Sites();
		$user_assignsites=$this->i4energy_session->user_sitefilter;
		$sitelist = $site_obj->listSites ($user_assignsites, '', '', '', '');
		$this->view->sites = $sitelist;
		
		$meterlink_id=$this->_request->getParam ('id');
		$response =array();
		if($this->_request->isPost())
		{
				
			$filter=array();
			$meterlink_obj=new I4energy_Model_MeterDisplayLinks();
				
			$title = $this->_request->getParam ('title');
			$url = $this->_request->getParam ('url');
			$sites = $this->_request->getParam ('site');
			$meters = $this->_request->getParam ('meters');
			$loginrequired = $this->_request->getParam('login_required');
			$customerid = $this->_request->getParam ('customer');
			$theme = $this->_request->getParam ('theme');
			
			$url = preg_replace('/[^a-z0-9_-]/i','-', strtolower($url));
			$url = preg_replace('/-+/i', '-',$url);
			$url = preg_replace('/^-/i', '',$url);
			$url = preg_replace('/-$/i', '',$url);
			
			if(isset($meters[0]) && $meters[0]>0){
				$meter1 = 	$meters[0];
			}
			
			if(isset($meters[1]) && $meters[1]>0){
				$meter2 = 	$meters[1];
			}else {
				$meter2 = new Zend_Db_Expr('NULL');
			}

			if(isset($meters[2]) && $meters[2]>0){
				$meter3 = 	$meters[2];
			}else {
				$meter3 = new Zend_Db_Expr('NULL');
			}
			
			if(isset($meters[3]) && $meters[3]>0){
				$meter4 = 	$meters[3];
			}else {
				$meter4 = new Zend_Db_Expr('NULL');
			}
			if( $title == ""){
				$response['data']['title'] ='null';
			}else{
				$response['data']['title'] ='valid';
			}
		
				
			if(isset($url) && $url != ""){
				$filter=array();
				$filter[0]['field'] = 'url';
				$filter[0]['operation'] = 'eq';
				$filter[0]['value'] = $url;
				
				if(isset($meterlink_id) && $meterlink_id>0){
					$filter[1]['field'] = 'id';
					$filter[1]['operation'] = 'ne';
					$filter[1]['value'] = $meterlink_id;
				}
				
				$duplicateUrl=$meterlink_obj->listMeterDisplayLinks($filter);
				
				if($duplicateUrl->totalrecords > 0){
					$response['data']['url'] ='duplicate';
				}
			}
		
			if(isset($response['data']) && (in_array("null", $response['data']) ||  in_array("duplicate", $response['data']) || in_array("invalid", $response['data']))){
				$response['returnvalue']="invalid";
			}
			else{
				$response=array();
			}
// 			print_r($response);exit;
		if(count($response)==0){
				$meterlink_obj = new I4energy_Model_MeterDisplayLinks();
		
				if (isset($meterlink_id) && $meterlink_id > 0)
				$meterlink_obj->setId($meterlink_id);
				
				$meterlink_obj->setTitle($title);
				$meterlink_obj->setUrl($url);
				$meterlink_obj->setLoginrequired($loginrequired);
				$meterlink_obj->setCustomerId($customerid);
				$meterlink_obj->setTheme($theme);
				$meterlink_obj->setMeter1($meter1);
				$meterlink_obj->setMeter2($meter2);
				$meterlink_obj->setMeter3($meter3);
				$meterlink_obj->setMeter4($meter4);
				
					
				$meterlink_id = $meterlink_obj->save (true);
					
				if ($meterlink_id > 0)
				{
		
					$this->view->success = true;
				}
				else
				{
					$this->view->success = false;
				}
					
			}
			else{
				$this->view->response = $response;
			}
		}

		if(isset($meterlink_id) && $meterlink_id>0){
			$meterlink=new I4energy_Model_MeterDisplayLinks();
			$meterlink->init($meterlink_id);
			$this->view->meterlink = $meterlink;
			$meters='';
			if ( $meterlink->getId()>0){
					if ( $meterlink->getMeter1()>0){
						if ($meters ==''){
							$meters=$meterlink->getMeter1();
						}else {
							$meters .=','.$meterlink->getMeter1();
						}		
					}
					
					if ( $meterlink->getMeter2()>0){
						if ($meters ==''){
							$meters=$meterlink->getMeter2();
						}else {
							$meters .=','.$meterlink->getMeter2();
						}
					}
					
					if ( $meterlink->getMeter3()>0){
						if ($meters ==''){
							$meters=$meterlink->getMeter3();
						}else {
							$meters .=','.$meterlink->getMeter3();
						}
					}
					
					if ( $meterlink->getMeter4()>0){
						if ($meters ==''){
							$meters=$meterlink->getMeter4();
						}else {
							$meters .=','.$meterlink->getMeter4();
						}
					}
			}

			$filterMeter=array();
			$filterMeter[0]['field'] = 'id';
			$filterMeter[0]['operation'] = 'in';
			$filterMeter[0]['value'] = $meters;
			
			$meter_obj=new I4energy_Model_MetersMapper();
			$meter_list=$meter_obj->listMeters($filterMeter);
			$meteridarray=array();
			$siteidarray=array();
			foreach ($meter_list->rows as $row){
				if (!in_array($row['id'], $meteridarray)){
					array_push($meteridarray, $row['id']);
				}
				if (!in_array($row['siteid'], $siteidarray)){
					array_push($siteidarray, $row['siteid']);
				}	
			}
			
			$this->view->siteidarray =$siteidarray;
			$this->view->meteridarray =$meteridarray;			
		}
		
	}
	
	public function getmetersAction(){
		$site_ids = $this->_request->getParam ('siteids');
		$filterid = '';
		foreach ($site_ids as $id){
			if($filterid==''){
				$filterid=$id;
			}else{
				$filterid .=','.$id;
			}
		}
		$meterArray=array();
	if(isset($site_ids) && $site_ids != ''){
			$meter_obj = new I4energy_Model_Meters();
			$filterArray = array();
			$filterArray[0]['field']='siteid';
			$filterArray[0]['operation']='in';
			$filterArray[0]['value']=$filterid;
			
			$meterlist=$meter_obj->listMeters($filterArray);
			$meterArray=$meterlist->rows;
			}
			echo json_encode($meterArray);
			exit;
		}
		
	public function deleteAction ()
	{
		
		if (!I4energy_Model_Utils_CheckAccess::checkaccess($this->i4energy_session->user_role, 'meter_display_link_delete', $this->i4energy_session->user_priviledge))
		{
			$this->_redirect ('/accessdenied');
			exit;
		}
		$id = $this->_request->getParam ('id');
		
		if($this->_request->isPost())
		{
			$displayLink = new I4energy_Model_MeterDisplayLinks();
			$displayLink->init ($id);
			$displayLink->delete ();
		}
		exit;
	}

}

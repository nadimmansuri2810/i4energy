<?php

class PriviledgeController extends Zend_Controller_Action
{
	public function init() 
	{
		$this->i4energy_session = new Zend_Session_Namespace('I4energySession');
		if (!isset($this->i4energy_session->user_id) || ($this->i4energy_session->user_id == ''))
		{
			$this->_redirect ('/?bk=' . $_SERVER['REQUEST_URI']);;
			exit;
		}
	}

	public function indexAction() 
	{
		$priviledge_obj = new I4energy_Model_PriviledgeMapper();
		$priviledgelist=	$priviledge_obj->listPriviledge(array(), '', '', '', '');
		$this->view->priviledge = $priviledgelist;
		$this->view->priviledgerows = $priviledgelist->rows;
	}
	
	public function addAction(){

		$priviledge_id = (int) $this->_request->getParam ('id');
		$response =array();
		if($this->_request->isPost())
		{
			$filter=array();
			$priviledge = $this->_request->getParam ('priviledge');
			if($priviledge == ""){
				$response['data']['priviledge'] ='null';
			}else{
				$response['data']['priviledge'] ='valid';
				$filter[0]['field']='priviledge';
				$filter[0]['operation']='eq';
				$filter[0]['value']=$priviledge;
			}
			
			if(isset($priviledge_id) && $priviledge_id>0){
				$filter[1]['field']='id';
				$filter[1]['operation']='ne';
				$filter[1]['value']=$priviledge_id;				
			}
			$priviledge_mapper = new I4energy_Model_PriviledgeMapper();
			$duplicate_priviledge=$priviledge_mapper->listPriviledge($filter);
			
			
			if($duplicate_priviledge->totalrecords > 0){
				$response['data']['priviledge'] ='duplicate';
			}
				
			if(isset($response['data']) && (in_array("null", $response['data']) || in_array("duplicate", $response['data']) || in_array("invalid", $response['data']))){
				$response['returnvalue']="invalid";
			}
			else{
				$response=array();
			}
		
			if(count($response)==0){
					
				$priviledge_obj = new I4energy_Model_Priviledge();
					
				if (isset($priviledge_id) && $priviledge_id > 0)
					$priviledge_obj->setId($priviledge_id);

				$priviledge_obj->setPriviledge($priviledge);
					
				$lastpriviledge_id = $priviledge_obj->save (true);
					
				if ($lastpriviledge_id > 0)
				{
					$this->view->success = true;
				}
				else
				{
					$this->view->success = false;
				}
					
			}
			else{
				$this->view->response = $response;
			}
		}
		
		if (isset($priviledge_id) && $priviledge_id > 0)
		{
			$priviledge_obj = new I4energy_Model_Priviledge();
			$priviledge_obj->init ($priviledge_id);
				
			$this->view->priviledge = $priviledge_obj;
		}
		
		
	}

	function deleteAction()
	{
		$priviledge_id = (int) $this->_request->getParam ('id');
		if($this->_request->isPost())
		{
			$priviledge = new I4energy_Model_Priviledge();
			$priviledge->init ($priviledge_id);
			
			$priviledge->delete ();
			
			
			
		}
	
		exit;
	}
	
	public function rolemapperAction()
	{
		$role_obj = new I4energy_Model_RoleMapper();
		$priviledge_obj = new I4energy_Model_PriviledgeMapper();
	
	
		$rolelist = $role_obj->listRole (array(), '', '', '', '');
		$this->view->roles = $rolelist;
		$this->view->totalrecords = $rolelist->totalrecords;
	
		$priviledgelist=	$priviledge_obj->listPriviledge(array(), '', '', '', '');
		$this->view->priviledge = $priviledgelist;
	}
	

	
	function addrolepriviledgeAction(){
		$role_id = $this->_request->getParam ('roleid');
		$priviledge_id = $this->_request->getParam ('priviledgeid');

		$role_priviledge_mapper_obj =new I4energy_Model_RolePriviledgeMapper();
		$response=array();
		$delete ="";
		if(isset($role_id) && $role_id != ""){
			$where = $role_priviledge_mapper_obj->getDbTable()->getAdapter()->quoteInto('role_id = ?',$role_id );
			$role_priviledge_mapper_obj->getDbTable()->delete ($where);
		}
		
		if(isset($priviledge_id)){
			foreach($priviledge_id as $row){
				$rolepriviledge_obj=new I4energy_Model_RolePriviledge();
				$rolepriviledge_obj->setRole_id($role_id);
				$rolepriviledge_obj->setPriviledge_id($row);
				
				try{
					$lastprivilege_id = $rolepriviledge_obj->save (true);
					$response['returnvalue']="valid";
					$this->view->addrolepriviledgesuccess = true;
				}
				catch (Exception $e)
				{
					$this->view->addrolepriviledgesuccess = false;
				}
			}
			echo json_encode($response);
			exit;
		}
	
	}
	
	function allocatedpriviledgeAction(){
		$role_id = $this->_request->getParam ('roleid');
	
		$role_priviledge_mapper_obj =new I4energy_Model_RolePriviledgeMapper();
		$filter=array();
		$userArray=array();
		if(isset($role_id) && $role_id>0){
			$filterUser[0]['field'] = 'role_id';
			$filterUser[0]['operation'] = '=';
			$filterUser[0]['value'] = $role_id;
			$priviledges=$role_priviledge_mapper_obj->listRolePriviledge($filterUser);
			$priviledgeArray=$priviledges->rows;
		}
		$response=array();
		$i=0;
		foreach ($priviledgeArray as $priviledge){
			$response['data'][$i]=$priviledge['priviledge_id'];
			$i++;
				
		}
		echo json_encode($response);
		exit;
	}
	
}

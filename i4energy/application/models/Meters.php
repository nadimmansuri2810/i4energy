<?php
class I4energy_Model_Meters {
	protected $id;
	protected $siteid;
    protected $name;
    protected $desc;
    protected $location;
    protected $mac;
    protected $type;
    protected $moduleno;
    protected $slotno;
    protected $channelno;
    protected $conversationfactor;
    protected $unit;
    protected $offset;
    protected $cost;
    protected $carbonsavings;
    protected $timezone;
    protected $pingrate;
    protected $createddate;
    protected $createdby;
    protected $updateddate;
    protected $updatedby;
    protected $latestreading;
    protected $latestreadingtime;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Site property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Meter property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setSiteId($id) {
    	$this->siteid = $id;
    	return $this;
    }
    public function getSiteId() {
    	return $this->siteid;
    }
    
    public function setName ($name) {
    	$this->name = $name;
    	return $this;
    }
    public function getName () {
    	return $this->name;
    }
    
	public function setDescription($desc) {
        $this->desc = $desc;
        return $this;
    }
    public function getDescription() {
        return $this->desc;
    }
    
    public function setLocation ($location) {
    	$this->location = $location;
    	return $this;
    }
    public function getLocation () {
    	return $this->location;
    }
    
    public function setMac ($mac) {
    	$this->mac = $mac;
    	return $this;
    }
    public function getMac () {
    	return $this->mac;
    }
    
    public function setType ($type) {
    	$this->type = $type;
    	return $this;
    }
    public function getType () {
    	return $this->type;
    }
    
    public function setModuleNo ($moduleno) {
    	$this->moduleno = $moduleno;
    	return $this;
    }
    public function getModuleNo () {
    	return $this->moduleno;
    }
    
    public function setSlotNo ($slotno) {
    	$this->slotno = $slotno;
    	return $this;
    }
    public function getSlotNo () {
    	return $this->slotno;
    }
    
    public function setChannelNo ($channelno) {
    	$this->channelno = $channelno;
    	return $this;
    }
    public function getChannelNo () {
    	return $this->channelno;
    }
    
    public function setConversationFactor ($conversationfactor) {
    	$this->conversationfactor = $conversationfactor;
    	return $this;
    }
    public function getConversationFactor () {
    	return $this->conversationfactor;
    }
    
    public function setUnit ($unit) {
    	$this->unit = $unit;
    	return $this;
    }
    public function getUnit () {
    	return $this->unit;
    }
    
    public function setOffset ($offset) {
    	if ($offset == ''){
    		$this->offset = new Zend_Db_Expr('NULL');
    	}else{
    		$this->offset = $offset;
    	}
    	
    	return $this;
    }
    public function getOffset () {
    	return $this->offset;
    }
    
    public function setCost ($cost) {
    	$this->cost = $cost;
    	return $this;
    }
    public function getCost () {
    	return $this->cost;
    }
    
    public function setCarbonSavings ($carbonsavings) {
    	$this->carbonsavings = $carbonsavings;
    	return $this;
    }
    public function getCarbonSavings () {
    	return $this->carbonsavings;
    }
    
    public function setTimezone ($timezone) {
    	$this->timezone = $timezone;
    	return $this;
    }
    public function getTimezone () {
    	return $this->timezone;
    }
    
    public function setPingRate ($pingrate) {
    	$this->pingrate = $pingrate;
    	return $this;
    }
    public function getPingRate () {
    	return $this->pingrate;
    }
    
	public function setCreatedBy($createdBy) {
        $this->createdby = $createdBy;
        return $this;
    }
    public function getCreatedBy() {
        return $this->createdby;
    }
    
    public function setCreatedDate ($createdDate) {
    	$this->createddate = $createdDate;
    	return $this;
    }
    public function getCreatedDate () {
    	return $this->createddate;
    }
    
    public function setUpdatedBy ($updatedBy) {
    	$this->updatedby = $updatedBy;
    	return $this;
    }
    public function getUpdatedBy () {
    	return $this->updatedby;
    }
    
    public function setUpdatedDate ($updatedDate) {
    	$this->updatedDate = $updatedDate;
    	return $this;
    }
    public function getUpdatedDate () {
    	return $this->updatedDate;
    }
    
    public function setLatestReading ($reading) {
    	$this->latestreading = $reading;
    	return $this;
    }
    public function getLatestReading () {
    	return $this->latestreading;
    }
    
    public function setLatestReadingTime ($time) {
    	$this->latestreadingtime = $time;
    	return $this;
    }
    public function getLatestReadingTime () {
    	return $this->latestreadingtime;
    }

    public function setMapper(I4energy_Model_MetersMapper $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_MetersMapper());
        }
        return $this->_mapper;
    }
    
    public function listMeters ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listMeters ($filters, $orderby, $sort, $start, $limit);
    }
    
    public function init ($meter)
    {
    	if (is_numeric($meter))
    	{
    		$select = $this->getMapper()->getSelect();
    		$select->from("meters");
    		$select->where("`id`=?", $meter);
    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    		$meter_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	}
    	else
    	{
    		$meter_row = $meter;
    	}
    	
    	$this->setId($meter_row['id']);
    	$this->setSiteId($meter_row['siteid']);
		$this->setName($meter_row['name']);
		$this->setDescription($meter_row['description']);
		$this->setLocation($meter_row['location']);
		$this->setMac($meter_row['mac']);
		$this->setType($meter_row['type']);
		$this->setModuleNo($meter_row['moduleno']);
		$this->setSlotNo($meter_row['slotno']);
		$this->setChannelNo($meter_row['channelno']);
		$this->setConversationFactor($meter_row['conversationfactor']);
		$this->setUnit($meter_row['unit']);
		$this->setOffset($meter_row['offset']);
		$this->setCost($meter_row['cost']);
		$this->setCarbonSavings($meter_row['carbonsavings']);
		$this->setTimezone($meter_row['timezone']);
		$this->setPingRate($meter_row['pingrate']);
		$this->setCreatedBy($meter_row['createdby']);
		$this->setCreatedDate($meter_row['createddate']);
		$this->setUpdatedBy($meter_row['updatedby']);
		$this->setUpdatedDate($meter_row['updateddate']);
		$this->setLatestReading($meter_row['latestreading']);
		$this->setLatestReadingTime($meter_row['latestreadingtime']);
    }
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);	
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    	
    	$meter_attr = new I4energy_Model_MeterAttributes();
    	$meter_attr->setId ($this->getId());
    	$meter_attr->delete();
    	
    	$meter_alert = new I4energy_Model_MeterAlerts();
    	$meter_alert->setId ($this->getId());
    	$meter_alert->delete();
    }
    
    public static function convertToUnit ($data, $type, $unit, $conversationFactor, $other_data = array())
    {
    	$data = (float) $data;

    	//echo '<!-- Conversion: ' . $data . ", " . $conversationFactor . ", " . ($data*$conversationFactor) . " -->";
    	if (isset ($conversationFactor) && $conversationFactor != 0 && $conversationFactor != '')
    		$data = $data*$conversationFactor;
    	
    	switch ($type)
    	{
    		case 'gas':
    			if ($unit == 'm3')
    			{
    				return number_format($data, 2, '.', '');
    			}
    			else if ($unit == 'kwh')
    			{
    				$kwh = ($data * 1.022640 * 39.3) / 3.6;
    				return number_format($kwh, 2, '.', '');
    			}
    			break;
    		case 'water':
    			if ($unit == 'm3')
    				return number_format($data, 2, '.', '');
    			else if ($unit == 'gl')
    				return number_format($data * 0.264172, 2, '.', '');
    			else if ($unit == 'pints')
					return number_format($data * 2.11338, 2, '.', '');
    			else
    				return number_format($data, 2, '.', '');
    			break;
    		case 'amps':
    			$ref_voltage = 230;
    			if (isset ($other_data['ref_voltage']) && $other_data['ref_voltage'] != '')
    				$ref_voltage = $other_data['ref_voltage'];
    			
    			return number_format(($data / $ref_voltage)*1000, 2, '.', '');
    			break;
    		case 'number':
			case 'hourrun':
    			return $data;
    			break;
    		case 'temp':
    		case 'boiler':
    			return number_format($data, 1, '.', '');
    			break;	
    		default:
    			return number_format($data, 2, '.', '');
    			break;
    	}
    	
    	return number_format ($data, 2, '.', '');
    }
    
    public static function calculateCarbonSavings ($data, $type, $carbonSavings, $volumeCorrectionFactor = 1.022640, $calorificValue = 39.3, $kwhConversionFactor = 3.6)
    {
    	$data = (float) $data;
    	if (isset ($carbonSavings) && $carbonSavings != 0 && $carbonSavings != '')
    	{
    		if ($volumeCorrectionFactor == '')
    			$volumeCorrectionFactor = 1.022640;
    		if ($calorificValue == '')
    			$calorificValue = 39.3;
    		if ($kwhConversionFactor == '')
    			$kwhConversionFactor = 3.6;
    		
	    	switch ($type)
	    	{
	    		case 'gas':
	    			$kwh = ($data * $volumeCorrectionFactor * $calorificValue) / $kwhConversionFactor;
	    			$kgCo2 = number_format ($kwh * $carbonSavings, 2, '.', '');
	    			return $kgCo2;
	    			break;
	    		case 'power':
	    			return number_format($data * $carbonSavings, 2, '.', '');
	    			break;
	    		default:
	    			return number_format($data, 2, '.', '');
	    			break;
	    	}
    	}    	
    	return number_format($data, 2, '.', '');
    }
    
    public static function getDisplayUnit ($unit)
    {
    	switch ($unit)
    	{
    		case 'm3':
    			return 'M<sup>3</sup>';
    			break;
    		case 'c':
    			return '&deg;C';
    			break;
    		case 'l':
    			return 'L';
    			break;
    		case 'gl':
    			return 'Gl';
    			break;
    		case 'amps':
    			return 'Amps';
    			break;
    		case 'pints';
    			return 'Pints';
    			break;
    		case 'kw':
    		case 'kwh':
    			return 'kWh';
    			break;
    		case 'v':
    			return 'V';
    			break;
    		case 'ma':
    			return 'mA';
    			break;
    		case 'onoff':
    			return 'No';
    			break;
			case 'hrun':
			case 'hopen':
				return '';
				break;
			case 'rh':
				return 'RH%';
				break;
    		default:
    			return $unit;
    			break;
    	}
    	
    	return $unit;
    }
}
?>
<?php
class I4energy_Model_SitesUsersMapper {
	protected $sites_users_dbtable;
	protected $__select;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->sites_users_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->sites_users_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_SitesUsers');
        }
        return $this->sites_users_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    		return $this->__select;
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		return $this->__select;
    	}
    }
    
    public function listSitesUsers ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	$select = $this->getSelect();
    	$select->from ('sites_users');
    	
    	if ( is_array ($filters) )
    	{
    		for ($i=0; $i < sizeof ($filters); $i++)
    		{
	    		if (isset($filters[$i]['field']) && $filters[$i]['field'] != '' && isset($filters[$i]['operation']) && $filters[$i]['operation'] != '')
	    			$select->addCondition ($filters[$i]['field'], $filters[$i]['operation'], $filters[$i]['value']);
    		}
   		}
   		
   		if ($sort == '')
   			$sort = 'DESC';
   		
   		if ($orderby == '')
   			$select->order ('id ' . $sort);
   		else
   			$select->order ($orderby . ' ' . $sort);
   		
   		if ($start != null && $limit != null)
   			$select->limit ($limit, $start);
   		
   		$select->sql_cals_found_rows (true);
   		
   		$stmt = $this->getDbTable()->getAdapter()->query($select);
   		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
   		
   		$stmt = new Zend_Db_Statement_Pdo($this->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
   		$stmt->execute();
   		$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
   		
   		$response = new stdClass();
   		$response->totalrecords = $count['totalrecords'];
   		$response->returnrecords = sizeof ($rows);
   		$response->rows = $rows;
   		
   		return $response;
    }
    
    public function save (I4energy_Model_SitesUsers &$sitesUsers, $ignore_unset = false)
    {
    	$data = array (
    		'site_id' => $sitesUsers->getSite_id()
    		,'user_id' => $sitesUsers->getUser_id()
    	);
    	
    	if ($ignore_unset)
    	{
    		if (!isset ($data['id']))
    			unset ($data['id']);
    		if (!isset ($data['site_id']))
    			unset ($data['site_id']);
    		if (!isset ($data['user_id']))
    			unset ($data['user_id']);
    
    	}
    	
    	
    	if ($sitesUsers->getId() == null || $sitesUsers->getId() == '')
    	{
    		$this->getDbTable()->insert($data);
    		$lastid = $this->getDbTable()->getAdapter()->lastInsertId();
    		return $lastid;
    	}
    	else
    	{
    		$this->getDbTable()->update($data, array('id = ?' => $sitesUsers->getId()));
    		return $sitesUsers->getId();
    	}
    }
    
    public function delete (I4energy_Model_SitesUsers &$sitesusers)
    {
    	$where = $this->getDbTable()->getAdapter()->quoteInto('site_id = ?', $sitesusers->getSite_id());
    	$this->getDbTable()->delete ($where);
    }
}

<?php
class I4energy_Model_RoleMapper {
	protected $sites_dbtable;
	protected $__select;
	protected $_mapper;
	
    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->sites_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->sites_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_Role');
        }
        return $this->sites_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    		return $this->__select;
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		return $this->__select;
    	}
    }
    
    public function listRole ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	$select = $this->getSelect();
    	$select->from ('role');
    	
    	if ( is_array ($filters) )
    	{
    		for ($i=0; $i < sizeof ($filters); $i++)
    		{
	    		if (isset($filters[$i]['field']) && $filters[$i]['field'] != '' && isset($filters[$i]['operation']) && $filters[$i]['operation'] != '')
	    			$select->addCondition ($filters[$i]['field'], $filters[$i]['operation'], $filters[$i]['value']);
    		}
   		}
   		if ($sort == '')
   			$sort = 'DESC';
   		
   		if ($orderby == '')
   			$select->order ('id ' . $sort);
   		else
   			$select->order ($orderby . ' ' . $sort);
   		
   		if ($start != null && $limit != null)
   			$select->limit ($limit, $start);
   		
   		$select->sql_cals_found_rows (true);
   		
   		$stmt = $this->getDbTable()->getAdapter()->query($select);
   		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
   		
   		$stmt = new Zend_Db_Statement_Pdo($this->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
   		$stmt->execute();
   		$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
   		
   		$response = new stdClass();
   		$response->totalrecords = $count['totalrecords'];
   		$response->returnrecords = sizeof ($rows);
   		$response->rows = $rows;
   		
   		return $response;
    }
    
    public function init ($id)
    {
    	$select = $this->getSelect();
    	$select->from ('role');
    	
    	$select->addCondition ('role.id', '=', $id);
    	$select->limit (1, 0);
    	$stmt = $this->getDbTable()->getAdapter()->query($select);
    	$row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	
    	if (isset ($row['id']) && $row['id'] > 0)
    	{
    		$role = new I4energy_Model_Role();
    		$role->setId ($row['id']);
    		$role->setRole($row['role']);
    		return $role;		
    	}
    	
    	return null;
    }
    
    public function save (I4energy_Model_Role &$role, $ignore_unset = false)
    {
    	$data = array (
    		'id' => $role->getId()
    		,'role' => $role->getRole()
    	);
    	
    	if ($ignore_unset)
    	{
    		if (!isset ($data['id']))
    			unset ($data['id']);
    		if (!isset ($data['role']))
    			unset ($data['role']);
    	}
    	
    	
    	if ($role->getId() == null || $role->getId() == '')
    	{
    		
    		$this->getDbTable()->insert($data);
    		$lastid = $this->getDbTable()->getAdapter()->lastInsertId();
    		return $lastid;
    	}
    	else
    	{
    		$this->getDbTable()->update($data, array('id = ?' => $role->getId()));
    		return $role->getId();
    	}
    }
    
    public function delete (I4energy_Model_Role &$role)
    {
    	$where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $role->getId());
    	$this->getDbTable()->delete ($where);
    }
}

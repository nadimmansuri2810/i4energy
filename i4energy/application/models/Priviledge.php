<?php
class I4energy_Model_Priviledge {
	protected $id;
    protected $priviledge;
    protected $_mapper;
    
   public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid User property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Users property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    	

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setPriviledge ($priviledge) {
    	$this->priviledge = $priviledge;
    	return $this;
    }
    public function getPriviledge () {
    	return $this->priviledge;
    }
    
    public function setMapper(I4energy_Model_PriviledgeMapper $mapper) {
    	$this->_mapper = $mapper;
    	return $this;
    }
    
    public function getMapper() {
    	if (null === $this->_mapper) {
    		$this->setMapper(new I4energy_Model_PriviledgeMapper());
    	}
    	return $this->_mapper;
    }
    
    public function listPriviledge ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listPriviledge ($filters, $orderby, $sort, $start, $limit);
    }
    
    public function init ($priviledge)
    {
    	if (is_numeric($priviledge))
    	{
    		$select = $this->getMapper()->getSelect();
    		$select->from("priviledge");
    		$select->where("`id`=?", $priviledge);
    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    		$priviledge_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	}
    	else
    	{
    		$priviledge_row = $priviledge;
    	}
    	 
    	$this->setId($priviledge_row['id']);
    	$this->setPriviledge($priviledge_row['priviledge']);
    }
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
}
?>
<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_Users extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'users';
	protected $_primary = 'id';
}
<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_Help extends Savitriya_Db_Table_Abstract {
  protected $_name    = 'helps';
  protected $_primary = 'id';
}
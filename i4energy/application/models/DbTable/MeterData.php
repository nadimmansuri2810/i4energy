<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_MeterData extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'meter_data';
	protected $_primary = array ('macid', 'rec_date', 'moduleid', 'slotno', 'channelno');
}
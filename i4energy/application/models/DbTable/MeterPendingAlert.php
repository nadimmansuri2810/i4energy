<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_MeterPendingAlert extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'meter_pending_alert';
	protected $_primary = 'id';
}
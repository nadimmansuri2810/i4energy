<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_Role extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'role';
	protected $_primary = 'id';
}
<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_Meters extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'meters';
	protected $_primary = 'id';
}
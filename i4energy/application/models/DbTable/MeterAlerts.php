<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_MeterAlerts extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'meter_alerts';
	protected $_primary = 'id';
}
<?php

class Savitriya_Db_Table_Abstract extends Zend_Db_Table_Abstract

{

	public function __construct($config = array())

	{

		parent::__construct($config);

	}

	

	public function select($withFromPart = self::SELECT_WITHOUT_FROM_PART)

	{

		require_once 'Savitriya_Db_Select.php';

		$select = new Savitriya_Db_Select($this);


		// echo "select:::::<br>";
		// exit;

		if ($withFromPart == self::SELECT_WITH_FROM_PART) {

			$select->from($this->info(self::NAME), Zend_Db_Table_Select::SQL_WILDCARD, $this->info(self::SCHEMA));

		}

		return $select;

	}

	

	public function fetchWithCount (Zend_Db_Table_Select $select)

	{

		$stmt = $this->_db->query($select);

		$data = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

		return $data;

	}

	

}

?>
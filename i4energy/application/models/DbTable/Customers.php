<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_Customers extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'customers';
	protected $_primary = 'id';
}
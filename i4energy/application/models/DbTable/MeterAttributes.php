<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_MeterAttributes extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'meter_attr';
	protected $_primary = 'id';
}
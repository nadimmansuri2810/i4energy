<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_SitesUsers extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'sites_users';
	protected $_primary = 'id';
}
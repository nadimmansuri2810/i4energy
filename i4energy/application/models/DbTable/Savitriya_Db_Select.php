<?php 

class Savitriya_Db_Select extends Zend_Db_Table_Select

{

	const SQL_FOUND_ROWS = 'SQL_CALC_FOUND_ROWS';

	const FOUND_ROWS = 'calc_found_rows';

	

	public function __construct(Zend_Db_Table_Abstract $table)

	{


		$tmpPartsArr = self::$_partsInit;

		self::$_partsInit = array();

		self::$_partsInit[self::FOUND_ROWS] = false;

		foreach ($tmpPartsArr as $partKey => $partValue)

		{

			self::$_partsInit[$partKey] = $partValue;

		}

		// echo "Table::::::";
		// print_r($table);
		// exit;


		parent::__construct($table);


	}

	

	public function sql_cals_found_rows ($flag = true)

	{

		$this->_parts[self::FOUND_ROWS] = (bool) $flag;

		return $this;

	}

	

	protected function _renderCalc_found_rows($sql)

	{

		if ($this->_parts[self::FOUND_ROWS]) {

			$sql .= ' ' . self::SQL_FOUND_ROWS;

		}

	

		return $sql;

	}

	

	public function addCondition ($field, $operator, $value)

	{

		if ($operator == 'eq' || $operator == 'is')

			$this->where ($field . "='" . $value . "'");

		else if ($operator == 'ne' || $operator == 'isnot')

			$this->where ($field . "<>'" . $value . "'");

		else if($operator == 'bw' || $operator == 'beginwith')

			$this->where ($field . " like '" . $value . "%'");

		else if ($operator == 'bn' || $operator == 'notbeginwith')

			$this->where ($field . " NOT LIKE '" . $value . "%'");

		else if ($operator == 'ew' || $operator == 'endswith')

			$this->where ($field . " LIKE '%" . $value . "'");

		else if ($operator == 'en' || $operator == 'notendswith')

			$this->where ($field . " NOT LIKE '%" . $value . "'");

		else if ($operator == 'cn' || $operator == 'contains')

			$this->where ($field . " LIKE '%" . $value . "%'");

		else if ($operator == 'nc' || $operator == 'notcontains')

			$this->where ($field . " NOT LIKE '%" . $value . "%'");

		else if ($operator == 'nu')

			$this->where ($field . " IS NULL");

		else if ($operator == 'nn')

			$this->where ($field . " IS NOT NULL");

		else if ($operator == 'in')

			$this->where ($field . " IN ('" . str_replace(",", "','", $value) . "')");

		else if ($operator == 'ni' || $operator == 'notin')

			$this->where ($field . " NOT IN ('" . str_replace(",", "','", $value) . "')");

		else if ($operator == 'gt' || $operator == 'greaterthan')

			$this->where ($field . ">'" . $value . "'");

		else if ($operator == 'gte' || $operator == 'greaterthanandequal')

			$this->where ($field . ">='" . $value . "'");

		else if ($operator == 'lt' || $operator == 'lessthan')

			$this->where ($field . "<'" . $value . "'");

		else if ($operator == 'lte' || $operator == 'lessthanandequal')

			$this->where ($field . "<='" . $value . "'");

		else

			$this->where ($field . "='" . $value . "'");



	}

} 
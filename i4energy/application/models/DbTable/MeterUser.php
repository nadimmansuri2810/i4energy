<?php

require_once 'Savitriya_Db_Table_Abstract.php';

class I4energy_Model_DbTable_MeterUser extends Savitriya_Db_Table_Abstract {
	protected $_name    = 'meter_user';
	protected $_primary = 'id';
}
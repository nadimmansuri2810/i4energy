
<?php

class I4energy_Model_Utils_ProcessData {


// Module / Slot processing functions
  function process_mod6_slot3 (&$db_conn, &$cols, &$last_ping_data)
  {

    $status = strtolower ($cols[4]);
    if ($status != 'na')
      return;

    $data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
    $data_stmt = $db_conn->prepare($data_qry);

    $macid = $cols[0];
//    $date = getFormattedDate ($cols[1]);
    $date = $cols[1];

    $module = (int) $cols[2];
    $slot = (int) $cols[3];

    $ch1 = $cols[5];
    $ch2 = $cols[6];
    $ch3 = $cols[7];
    $ch4 = $cols[8];
    $ch5 = $cols[9];
    $ch6 = $cols[10];
    $ch7 = $cols[11];
    $ch8 = $cols[12];
    $ch9 = $cols[13];
    $ch10 = $cols[14];
    $ch11 = $cols[15];
    $ch12 = $cols[16];
    $ch13 = $cols[17];
    $ch14 = $cols[18];
    $ch15 = $cols[19];

// Channel 1
    $channel = 1;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch1);
    $data = (float) preg_replace ('/p$/i', '', $ch1);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch1');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 2
    $channel = 2;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch2);
    $data = (float) preg_replace ('/p$/i', '', $ch2);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch2');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 3
    $channel = 3;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch3);
    $data = (float) preg_replace ('/p$/i', '', $ch3);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch3');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 4
    $channel = 4;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch4);
    $data = (float) preg_replace ('/p$/i', '', $ch4);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch4');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 5
    $channel = 5;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch5);
    $data = (float) preg_replace ('/p$/i', '', $ch5);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch5');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 6
    $channel = 6;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch6);
    $data = (float) preg_replace ('/p$/i', '', $ch6);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch6');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 7
    $channel = 7;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch7);
    $data = (float) preg_replace ('/p$/i', '', $ch7);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch7');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 8
    $channel = 8;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch8);
    $data = (float) preg_replace ('/p$/i', '', $ch8);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch8');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 9
    $channel = 9;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch9);
    $data = (float) preg_replace ('/p$/i', '', $ch9);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch9');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 10
    $channel = 10;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch10);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 11
    $channel = 11;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch11);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 12
    $channel = 12;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch12);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 13
    $channel = 13;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch13);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 14
    $channel = 14;
    $data1 = 0;
    $data = 0;
    if ($ch14 != 'N' && $ch14 != 'NA')
    {
      $data = preg_replace ('/V/i', '.', $ch14);
      if (strtolower($data) == 'v.')
        $data = 0;
    }
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 15
    $channel = 15;
    $data1 = 0;
    $data = 0;
    if ($ch15 != 'N' && $ch15 != 'NA')
    {
      $data = preg_replace ('/A/i', '.', $ch15);
      if (strtolower($data) == 'a.')
        $data = 0;
    }
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
  }

  function process_mod6_slot35 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod6_slot3 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod6_slot_ (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod6_slot3 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot33 (&$db_conn, &$cols, &$last_ping_data)
  {
    $status = strtolower ($cols[4]);
    if ($status != 'na')
      return;

    $data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
    $data_stmt = $db_conn->prepare($data_qry);

    $macid = $cols[0];
//    $date = getFormattedDate ($cols[1]);
    $date = $cols[1];

    $module = (int) $cols[2];
    $slot = (int) $cols[3];

    $ch1 = $cols[5];
    $ch2 = $cols[6];
    $ch3 = $cols[7];
    $ch4 = $cols[8];
    $ch5 = $cols[9];

// Channel 1
    $channel = 1;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch1);
    $data = (float) preg_replace ('/p$/i', '', $ch1);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch1');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 2
    $channel = 2;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch2);
    $data = (float) preg_replace ('/p$/i', '', $ch2);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch2');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 3
    $channel = 3;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch3);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 4
    $channel = 4;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch4);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 5
    $channel = 5;
    $data1 = 0;
    $data = preg_replace ('/V/i', '.', $ch5);
    if (strtolower($data) == 'v.')
      $data = 0;

    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
  }

  function process_mod97_slot34 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot36 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot37 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot48 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot50 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot52 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot53 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot54 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot65 (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod97_slot_ (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod99_slot_ (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod97_slot33 ($db_conn, $cols, $last_ping_data);
  }

  function process_mod98_slot0 (&$db_conn, &$cols, &$last_ping_data)
  {
    $status = strtolower ($cols[4]);
    if ($status != 'na')
      return;

    $data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
    $data_stmt = $db_conn->prepare($data_qry);

    $macid = $cols[0];
//    $date = getFormattedDate ($cols[1]);
    $date = $cols[1];

    $module = (int) $cols[2];
    $slot = (int) $cols[3];

    $ch1 = $cols[5];
    $ch2 = $cols[6];
    $ch3 = $cols[7];
    $ch4 = $cols[8];
    $ch5 = $cols[9];
    $ch6 = $cols[10];
    $ch7 = $cols[11];
    $ch8 = $cols[12];
    $ch9 = $cols[13];
    $ch10 = $cols[14];
    $ch11 = $cols[15];
    $ch12 = $cols[16];
    $ch13 = $cols[17];

// Channel 1
    $channel = 1;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch1);
    $data = (float) preg_replace ('/p$/i', '', $ch1);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch1');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 2
    $channel = 2;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch2);
    $data = (float) preg_replace ('/p$/i', '', $ch2);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch2');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 3
    $channel = 3;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch3);
    $data = (float) preg_replace ('/p$/i', '', $ch3);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch3');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 4
    $channel = 4;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch4);
    $data = (float) preg_replace ('/p$/i', '', $ch4);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch4');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 5
    $channel = 5;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch5);
    $data = (float) preg_replace ('/p$/i', '', $ch5);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch5');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 6
    $channel = 6;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch6);
    $data = (float) preg_replace ('/p$/i', '', $ch6);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch6');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 7
    $channel = 7;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch7);
    $data = (float) preg_replace ('/p$/i', '', $ch7);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch7');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 8
    $channel = 8;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch8);
    $data = (float) preg_replace ('/p$/i', '', $ch8);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch8');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 9
    $channel = 9;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch9);
    $data = (float) preg_replace ('/p$/i', '', $ch9);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch9');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 10
    $channel = 10;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch10);
    $data = (float) preg_replace ('/p$/i', '', $ch10);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch10');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 11
    $channel = 11;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch11);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 12
    $channel = 12;
    $data1 = 0;
    $data = (float) preg_replace ('/D$/i', '', $ch12);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch12');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 13
    $channel = 13;
    $data1 = 0;
    $data = (float) preg_replace ('/D$/i', '', $ch13);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch13');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
  }

  function process_mod98_slot_ (&$db_conn, &$cols, &$last_ping_data)
  {
    I4energy_Model_Utils_ProcessData::process_mod98_slot0($db_conn, $cols, $last_ping_data);
  }

  function process_mod7_slot_ (&$db_conn, &$cols, &$last_ping_data)
  {
    $status = strtolower ($cols[4]);
    if ($status != 'na')
      return;

    $data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
    $data_stmt = $db_conn->prepare($data_qry);

    $macid = $cols[0];
//    $date = I4energy_Model_Utils_ProcessData::getFormattedDate ($cols[1]);
    $date = $cols[1];

    $module = (int) $cols[2];
    $slot = (int) $cols[3];

    $ch1 = $cols[5];
    $ch2 = $cols[6];
    $ch3 = $cols[7];
    $ch4 = $cols[8];
    $ch5 = $cols[9];
    $ch6 = $cols[10];
    $ch7 = $cols[11];
    $ch8 = $cols[12];
    $ch9 = $cols[13];

// Channel 1
    $channel = 1;
    $data1 = !I4energy_Model_Utils_ProcessData::isOn ($ch1);
    $data = (float) preg_replace ('/p$/i', '', $ch1);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch1');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 2
    $channel = 2;
    $data1 = !I4energy_Model_Utils_ProcessData::isOn ($ch2);
    $data = (float) preg_replace ('/p$/i', '', $ch2);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch2');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 3
    $channel = 3;
    $data1 = !I4energy_Model_Utils_ProcessData::isOn ($ch3);
    $data = (float) preg_replace ('/p$/i', '', $ch3);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch3');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 4
    $channel = 4;
    $data1 = 0;
    $data = (float) preg_replace ('/h$/i', '', $ch4);
//$data = findPulsesDiffrence ($data, $last_ping_data, 'ch4');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 5
    $channel = 5;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch5);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


// Channel 6
    $channel = 6;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch6);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// We have two configuration of Module 7. Old configuration has Ch7 - Voltage and Ch8 and Ch9 has nothing useful
// In new configuration, we have Ch7 as Temprature, Ch8 and Ch9 are Voltage
// Identify channel 7, 8 and 9 data and based on that process it according to new or old configuration
    if (preg_match ('/\d+K\d+/i', $ch7)) {
// Channel 7 - Temprature
      $channel = 7;
      $data1 = 0;
      $data = preg_replace ('/K/i', '.', $ch7);
      if (strtolower($data) == 'n.')
        $data = 0;
      else
        $data = $data-273.15;
      $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
    }
    else {
// Channel 7 - Voltage
      $channel = 7;
      $data1 = 0;
      $data = preg_replace ('/V/i', '.', $ch7);
      if (strtolower($data) == 'v.')
        $data = 0;
      $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
    }

    if (preg_match ('/\d+V\d+/i', $ch8)) {
// Channel 8 - Voltage
      $channel = 8;
      $data1 = 0;
      $data = preg_replace ('/V/i', '.', $ch8);
      if (strtolower($data) == 'v.')
        $data = 0;
      $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
    }

    if (preg_match ('/\d+V\d+/i', $ch9)) {
// Channel 9 - Voltage
      $channel = 9;
      $data1 = 0;
      $data = preg_replace ('/V/i', '.', $ch9);
      if (strtolower($data) == 'v.')
        $data = 0;
      $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
    }
  }

  function process_mod8_slot_ (&$db_conn, &$cols, &$last_ping_data)
  {
    $status = strtolower ($cols[4]);
    if ($status != 'na')
      return;

    $data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";
    $data_stmt = $db_conn->prepare($data_qry);

    $macid = $cols[0];
//    $date = getFormattedDate ($cols[1]);
    $date = $cols[1];

    $module = (int) $cols[2];
    $slot = (int) $cols[3];

    $ch1 = $cols[5];
    $ch2 = $cols[6];

// Channel 1
    $channel = 1;
    $data1 = 0;
    $data = (float) preg_replace ('/h$/i', '', $ch1);
//$data = findPulsesDiffrence ($data, $last_ping_data, 'ch4');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 2
    $channel = 2;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch2);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
  }


// Amit July 18 2016 Start

  function process_mod9_slot_ (&$db_conn, &$cols, &$last_ping_data)
  {
    $status = strtolower ($cols[4]);

    if ($status != 'na')
      return;

    $data_qry = "REPLACE INTO " . TABLE_PROCESSED_DATA . " VALUES (:macid,:date,:module,:slot,:channel,:data,:data1)";

    $data_stmt = $db_conn->prepare($data_qry);

    $macid = $cols[0];
    $date = $cols[1];

    $module = (int) $cols[2];
    $slot = (int) $cols[3];

    $ch1 = $cols[5];
    $ch2 = $cols[6];
    $ch3 = $cols[7];
    $ch4 = $cols[8];
    $ch5 = $cols[9];
    $ch6 = $cols[10];
    $ch7 = $cols[11];
    $ch8 = $cols[12];
    $ch9 = $cols[13];
    $ch10 = $cols[14];
    $ch11 = $cols[15];
    $ch12 = $cols[16];
    $ch13 = $cols[17];
    $ch14 = $cols[18];
    $ch15 = $cols[19];
    $ch16 = $cols[20];

// Channel 1
    $channel = 1;
    $data1 = !I4energy_Model_Utils_ProcessData::isOn ($ch1);
    $data = (float) preg_replace ('/p$/i', '', $ch1);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch1');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

//    $meter_data_obj = new I4energy_Model_MeterData();
//    $meter_data_obj->setMac($macid);
//    $meter_data_obj->setDate($date);
//    $meter_data_obj->setModuleNo($module);
//    $meter_data_obj->setSlotNo($slot);
//    $meter_data_obj->setChannelNo($channel);
//    $meter_data_obj->setData($data);
//    $meter_data_obj->setData1($data1);
//    $meter_data = $meter_data_obj->save(true);

// Channel 2
    $channel = 2;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch2);
    $data = (float) preg_replace ('/p$/i', '', $ch2);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch2');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 3
    $channel = 3;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch3);
    $data = (float) preg_replace ('/p$/i', '', $ch3);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch3');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 4
    $channel = 4;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch4);
    $data = (float) preg_replace ('/p$/i', '', $ch4);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch4');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 5
    $channel = 5;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch5);
    $data = (float) preg_replace ('/p$/i', '', $ch5);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch5');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 6
    $channel = 6;
    $data1 = I4energy_Model_Utils_ProcessData::isOn ($ch6);
    $data = (float) preg_replace ('/p$/i', '', $ch6);
    $data = I4energy_Model_Utils_ProcessData::findPulsesDiffrence ($data, $last_ping_data, 'ch6');
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 7
    $channel = 7;
    $data1 = I4energy_Model_Utils_ProcessData::mod_9_isOn ($ch7);
    $data = (float) preg_replace ('/t/i', '.', $ch7);
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


// Channel 8
    $channel = 8;
    $data1 = I4energy_Model_Utils_ProcessData::mod_9_isOn ($ch8);
    $data = (float) preg_replace ('/t/i', '.', $ch8);
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


// Channel 9
    $channel = 9;
    $data1 = I4energy_Model_Utils_ProcessData::mod_9_isOn ($ch9);
    $data = (float) preg_replace ('/t/i', '.', $ch9);
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 10 - Temprature
    $channel = 10;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch10);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


// Channel 11 - Temprature
    $channel = 11;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch11);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


// Channel 12 - Temprature
    $channel = 12;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch12);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 13 - Temprature
    $channel = 13;
    $data1 = 0;
    $data = preg_replace ('/K/i', '.', $ch13);
    if (strtolower($data) == 'n.')
      $data = 0;
    else
      $data = $data-273.15;
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

// Channel 14 - Voltage
    $channel = 14;
    $data1 = 0;
    $data = (float) preg_replace ('/Z/i', '.', $ch14);
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));


    if (preg_match ('/\d+V\d+/i', $ch15)) {
// Channel 15 - Voltage
      $channel = 15;
      $data1 = 0;
      $data = preg_replace ('/V/i', '.', $ch15);
      if (strtolower($data) == 'v.')
        $data = 0;
      $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));
    }

// Channel 16
    $channel = 16;
    $data1 = 0;
    $data = (float) preg_replace ('/h$/i', '', $ch16);
    $data_stmt->execute (array (':macid' => $macid, ':date' => $date, ':module' => $module, ':slot' => $slot, ':channel' => $channel, ':data' => $data, ':data1' => $data1));

  }
// Amit July 18 2016 End


  function isOn ($channel_data)
  {
    return preg_match ('/P$/', $channel_data);
  }

  function getFormattedDate ($raw_date)
  {
    global $global_date, $last24h, $next1h;

    $raw_date = trim ($raw_date);
    if (strlen ($raw_date) == 12 && preg_match ('/^[0-9]+$/', $raw_date))
    {
      $csv_date = '20' . $raw_date;
      $date = substr ($csv_date, 0, 4) . '-' . substr ($csv_date, 4, 2) . '-' . substr ($csv_date, 6, 2) . ' ' . substr ($csv_date, 8, 2) . ':' . substr ($csv_date, 10, 2) . ':' . substr ($csv_date, 12, 2);

      $dateStamp = @strtotime ($date);
      if ($dateStamp > $next1h || $dateStamp < $last24h)
        $date = $global_date;
    }
    else
    {
      $date = $global_date;
    }
    return $date;
  }

  function findPulsesDiffrence ($data, &$last_ping_data, $channel_column)
  {
    $channel_column = trim($channel_column);
    if (!preg_match ('/^ch/i', $channel_column))
    {
      return $data;
    }

    if ($data == '' || $data == 0)
      return $data;

    if (is_array ($last_ping_data) && sizeof ($last_ping_data) > 0 && isset($last_ping_data[$channel_column]) && $last_ping_data[$channel_column] != '')
    {
      $pattern = array ('/p$/i', '/D$/i');
      $replace = array ('', '');

      $old_data = (float) preg_replace ($pattern, $replace, $last_ping_data[$channel_column]);
      if (strtolower($old_data) == 'n')
        $old_data = 0;

      $diff_value = $data - $old_data;
      if ($diff_value < 0) {
        return $data;
      }
      return $diff_value;
    }

    return $data;
  }

// Amit 19 July 2016 start
  function mod_9_isOn ($channel_data)
  {
    return preg_match ('/T/', $channel_data);
  }
// Amit 19 July 2016 end

}


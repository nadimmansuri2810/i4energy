<?php
class Websitebuilder_Model_Utils_CombineFiles
{
	public static function getCombineUrl ($filesArr, $type = "js")
	{
		$cacheDir = '/min';
		
		if (isset($_SERVER['SUBDOMAIN_DOCUMENT_ROOT']) && $_SERVER['SUBDOMAIN_DOCUMENT_ROOT'] != '')
			$documentRoot = $_SERVER['SUBDOMAIN_DOCUMENT_ROOT'];
		else
			$documentRoot = $_SERVER['DOCUMENT_ROOT'];
		$domain = $_SERVER['HTTP_HOST'];
		$lastAccessTime = 0;
		
		$totalFiles = sizeof ($filesArr);
		$fileNames = '';
		foreach ($filesArr as $file)
		{
			if (!file_exists($documentRoot . '/' . $file))
			{
				$totalFiles--;
				continue;
			}
			
			$mTime = filemtime ($documentRoot . '/' . $file);
			
			if ($mTime > $lastAccessTime)
				$lastAccessTime = $mTime;
			
			$fileNames .= basename($documentRoot . '/' . $file);
		}
		
		$combinedFileName = $totalFiles . '-' . $lastAccessTime . md5($domain.$fileNames) . '.' . $type;
		
		// Check in cache
		if (file_exists ($documentRoot . $cacheDir . '/' . $combinedFileName))
		{
			return $cacheDir . '/' . $combinedFileName;
		}
		
		foreach ($filesArr as $file)
		{
			if (!file_exists($documentRoot . '/' . $file))
			{
				file_put_contents($documentRoot . $cacheDir . '/' . $combinedFileName, "\n/* ERROR File: $file */\n", FILE_APPEND);
				continue;
			}
				
			file_put_contents($documentRoot . $cacheDir . '/' . $combinedFileName, "\n/* File: $file */\n" . file_get_contents($documentRoot . '/' . $file), FILE_APPEND);
		}
		
		return $cacheDir . '/' . $combinedFileName;
	}
}
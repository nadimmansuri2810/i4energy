<?php 
class I4energy_Model_Utils_DateUtils
{
	public static function getFormattedDate ($date, $date_format, $timezone = 'GMT', $src_timezone = 'GMT')
	{
		if ($timezone == null || $timezone == '')
			$timezone = DEFAULT_TIMEZONE;
		if ($src_timezone == null || $src_timezone == '')
			$src_timezone = DEFAULT_TIMEZONE;
		if ($date_format == null || $date_format == '')
			$date_format = DEFAULT_DATEFORMAT;
		
		$new_date = I4energy_Model_Utils_DateUtils::convertTimezone ($date, $timezone, $src_timezone);
		
		return $new_date->format ($date_format);
	}
	
	public static function convertTimezone ($date, $dst_timezone, $src_timezone = 'GMT')
	{
		if ($dst_timezone == null || $dst_timezone == '')
			$dst_timezone = DEFAULT_TIMEZONE;
		if ($src_timezone == null || $src_timezone == '')
			$src_timezone = DEFAULT_TIMEZONE;
		
		$new_date = new DateTime($date, new DateTimeZone($src_timezone) );
		$new_date->setTimeZone (new DateTimeZone($dst_timezone));
		
		return $new_date;
	}

	public static function secondsToHHMM ($seconds) {
		return sprintf("%'02s:%'02s", intval($seconds/60/60), abs(intval(($seconds%3600) / 60)));
	}
}

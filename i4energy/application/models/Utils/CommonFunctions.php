<?php
class I4energy_Model_Utils_CommonFunctions {

	public static function getDateTimeFormateList()
	{
		$dateTimeFormateArray =array();
		$dateTimeFormateArray['0']['exp']='Y-m-d H:i:s';
		$dateTimeFormateArray['0']['val']=@date('Y-m-d H:i:s', 1416511247);
		
		$dateTimeFormateArray['1']['exp']='Y-m-d h:i a';
		$dateTimeFormateArray['1']['val']=@date('Y-m-d h:i a', 1416511247);
		
		$dateTimeFormateArray['2']['exp']='m/d/Y H:i:s';
		$dateTimeFormateArray['2']['val']=@date('m/d/Y H:i:s', 1416511247);
		
		$dateTimeFormateArray['3']['exp']='m/d/y H:i:s';
		$dateTimeFormateArray['3']['val']=@date('m/d/y H:i:s', 1416511247);
		
		$dateTimeFormateArray['4']['exp']='d/m/Y H:i:s';
		$dateTimeFormateArray['4']['val']=@date('d/m/Y H:i:s', 1416511247);
		
		$dateTimeFormateArray['5']['exp']='d/m/y H:i:s';
		$dateTimeFormateArray['5']['val']=@date('d/m/y H:i:s', 1416511247);
		
		$dateTimeFormateArray['6']['exp']='d M, y';
		$dateTimeFormateArray['6']['val']=@date('d M, y', 1416511247);
		
		$dateTimeFormateArray['7']['exp']='M d, y';
		$dateTimeFormateArray['7']['val']=@date('M d, y', 1416511247);
		
		$dateTimeFormateArray['8']['exp']='M d, Y';
		$dateTimeFormateArray['8']['val']=@date('M d, Y', 1416511247);
		
		return $dateTimeFormateArray;
	}
	
	public static function getDateFormateList(){
		$dateFormateArray =array();
		$dateFormateArray['0']['exp']='Y-m-d';
		$dateFormateArray['0']['val']=@date('Y-m-d', 1416511247);
		
		$dateFormateArray['1']['exp']='y-m-d';
		$dateFormateArray['1']['val']=@date('y-m-d', 1416511247);
		
		$dateFormateArray['2']['exp']='m/d/Y';
		$dateFormateArray['2']['val']=@date('m/d/Y', 1416511247);
		
		$dateFormateArray['3']['exp']='m/d/y';
		$dateFormateArray['3']['val']=@date('m/d/y', 1416511247);
		
		$dateFormateArray['4']['exp']='d/m/Y';
		$dateFormateArray['4']['val']=@date('d/m/Y', 1416511247);
		
		$dateFormateArray['5']['exp']='d/m/y';
		$dateFormateArray['5']['val']=@date('d/m/y', 1416511247);
		
		$dateFormateArray['6']['exp']='d M, Y';
		$dateFormateArray['6']['val']=@date('d M, Y', 1416511247);
		
		$dateFormateArray['7']['exp']='dS M, Y';
		$dateFormateArray['7']['val']=@date('dS M, Y', 1416511247);

		$dateFormateArray['8']['exp']='D d M, Y';
		$dateFormateArray['8']['val']=@date('D d M, Y', 1416511247);
		
		$dateFormateArray['9']['exp']='D dS M, Y';
		$dateFormateArray['9']['val']=@date('D dS M, Y', 1416511247);
		
		$dateFormateArray['10']['exp']='d M, y';
		$dateFormateArray['10']['val']=@date('d M, y', 1416511247);
		
		$dateFormateArray['11']['exp']='dS M, y';
		$dateFormateArray['11']['val']=@date('dS M, y', 1416511247);
		
		$dateFormateArray['12']['exp']='D d M, y';
		$dateFormateArray['12']['val']=@date('D d M, y', 1416511247);
		
		$dateFormateArray['13']['exp']='D dS M, y';
		$dateFormateArray['13']['val']=@date('D dS M, y', 1416511247);
		
		$dateFormateArray['14']['exp']='M d, y';
		$dateFormateArray['14']['val']=@date('M d, y', 1416511247);
		
		$dateFormateArray['15']['exp']='M dS, y';
		$dateFormateArray['15']['val']=@date('M dS, y', 1416511247);
		
		$dateFormateArray['16']['exp']='M d, Y';
		$dateFormateArray['16']['val']=@date('M d, Y', 1416511247);
		
		$dateFormateArray['17']['exp']='M dS, Y';
		$dateFormateArray['17']['val']=@date('M dS, Y', 1416511247);
		
		return $dateFormateArray;
	}
	
	public static function getTimeFormateList(){
		$timeFormateArray =array();
		$timeFormateArray['0']['exp']='H:i:s';
		$timeFormateArray['0']['val']=@date('H:i:s', 1416511247);
		
		$timeFormateArray['1']['exp']=' h:i:s a';
		$timeFormateArray['1']['val']=@date('h:i:s a', 1416511247);
		
		$timeFormateArray['2']['exp']='H:i';
		$timeFormateArray['2']['val']=@date('H:i', 1416511247);
		
		$timeFormateArray['3']['exp']='h:i a';
		$timeFormateArray['3']['val']=@date('h:i a', 1416511247);
		
	
		return $timeFormateArray;
	}
	
	public static function data_uri($file, $mime)
	{
		$base64   = base64_encode($file);
		return ('data:' . $mime . ';base64,' . $base64);
	}
	
	public static function resizeimage ($imageContent, $newwidth, $newheight, $outExtension = 'png')
	{
		if ($imageContent == null || $imageContent == '')
		{
			return '/assets/images/i4energy_logo.jpg';
		}
		
		ini_set('gd.jpeg_ignore_warning', 1);

		if (!is_dir(APPLICATION_PATH . '/tmp_upload/'))
			mkdir(APPLICATION_PATH . '/tmp_upload/', 0764);
		
		$tmpFile = tempnam(APPLICATION_PATH . '/tmp_upload/', 'resize_');
		$tmpFh = fopen ($tmpFile, 'w');
		fwrite ($tmpFh, $imageContent);
		fclose ($tmpFh);
		
		$imageInfo = getimagesize ($tmpFile);

		$width = $imageInfo[0];
		$height = $imageInfo[1];
		
		$imageMimeType = $imageInfo['mime'];
		if($imageMimeType == "image/jpeg" || $imageMimeType == "image/jpg")
		{
			$src = @imagecreatefromjpeg($tmpFile);
		}
		else if($imageMimeType == "image/png")
		{
			$src = imagecreatefrompng($tmpFile);
		}
		else
		{
			$src = @imagecreatefromgif($tmpFile);
		}
	
		$newheight = ($height/$width) * $newwidth;
		$tmp = imagecreatetruecolor($newwidth, $newheight);
		if($imageMimeType == "image/png")
		{
			imagealphablending($tmp, false);
			imagesavealpha($tmp, true);
		}
		 
		@imagecopyresampled ($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		 
		$dstTmpName = tempnam(APPLICATION_PATH . '/tmp_upload/', 'final_');
	
		if ($outExtension == 'jpg' || $outExtension == 'jpeg' || $outExtension == 'image/jpeg')
		{
			imagejpeg($tmp, $dstTmpName, 100);
			$return['mime'] = 'image/jpeg';
			$return['size'] = filesize($dstTmpName);
			$return['content'] = file_get_contents($dstTmpName);
		}
		else if ($outExtension == 'gif' || $outExtension == 'image/gif')
		{
			imagegif($tmp, $dstTmpName);
			$return['mime'] = 'image/gif';
			$return['size'] = filesize($dstTmpName);
			$return['content'] = file_get_contents($dstTmpName);
		}
		else
		{
			imagepng($tmp, $dstTmpName, 9);
			$return['mime'] = 'image/png';
			$return['size'] = filesize($dstTmpName);
			$return['content'] = file_get_contents($dstTmpName);
		}
	
	
		@imagedestroy ($src);
	
		@imagedestroy ($tmp);
		@unlink ($tmpFile);
		@unlink ($dstTmpName);
		
		return ('data:' . $return['mime'] . ';base64,' . base64_encode($return['content']));
// 		return $return;
	}
}

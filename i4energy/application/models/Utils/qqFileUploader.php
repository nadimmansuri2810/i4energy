<?php

/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
    	$input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        
        if ($realSize != $this->getSize()){            
            return false;
        }
        
        $target = fopen($path, "w");        
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        
        return true;
    }
    function getName() {
        return $_GET['qqfile'];
    }
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];            
        } else {
            throw new Exception('Getting content length is not supported.');
        }      
    }   
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {  
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) { 
    	if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
        return true;
    }
    function getName() {
        return $_FILES['qqfile']['name'];
    }
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
}

class Maintenancemgr_Model_Utils_qqFileUploader {
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;
    
	function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){        
        $allowedExtensions = array_map("strtolower", $allowedExtensions);
            
        $this->allowedExtensions = $allowedExtensions;        
        $this->sizeLimit = $sizeLimit;
        
        $this->checkServerSettings();       

        if (isset($_GET['qqfile'])) {
        	$this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
        	$this->file = new qqUploadedFileForm();
        } else {
            $this->file = false; 
        }
    }
    
	private function checkServerSettings(){        
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));                
        
        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");    
        }        
    }
    
	private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }    
    
    function handleUpload($uploadDirectory, $replaceOldFile = FALSE){
    	$authUser_NameSpace = new Zend_Session_Namespace('Maintenancemgr_Auth');
    	if (!is_writable($uploadDirectory)){
            return array('error' => "Server error. Upload directory isn't writable.");
        }
        
        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }
        
        $size = $this->file->getSize();
        
        if ($size == 0) {
            return array('error' => 'File is empty');
        }
        
        if ($size > $this->sizeLimit) {
            return array('error' => 'File is too large');
        }
        $pathinfo = pathinfo($this->file->getName());
        $filename = $pathinfo['filename'];
        $ext = $pathinfo['extension'];
        $modulename ="";
    	$moduleid = "";
    	$vendor_image_type = 0;
    	if(isset($_GET['module_name'])){
    		$modulename =$_GET['module_name'];
    	}
    	
        if(isset($_GET['module_id'])){
        	$moduleid =$_GET['module_id'];
        }
        if(isset($_GET['vendor_type'])){
        	$vendor_image_type = $_GET['vendor_type'];
        }
        if($modulename == "hrm_vendor"){
        	if(isset($moduleid) && $moduleid > 0){
	        	$fileutil = new Maintenancemgr_Model_Utils_File();
	        	$fileutil->DeleteFileByVendorIdFieldId($moduleid, $vendor_image_type);
        	}
        }
        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }
        
        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
                $filename .= rand(10, 99);
            }
        }
        
        $file_name = "";
        $file_name = $filename . '.' . $ext;
        $filenamearr = array();
        
        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){
        	
        	$content = file_get_contents("php://input");
        	list($imagewidth, $imageheight, $type, $attr) = getimagesize("php://input");
		
	        $fileType = $this->getfiletype($ext);
			if($moduleid>0){  
				$data["filepath"] = $uploadDirectory . $filename . '.' . $ext;  
				$data["filename"] = $filename.".".$ext; 	
				$data["content"] = $content;
				$data["filetype"] = $fileType;
				$data["size"] = $size;
				$data["modulename"] = $modulename;
				$data["moduleid"] = $moduleid;
				$data["createdby"] = $authUser_NameSpace->user_id;
				$data["updatedby"] = $authUser_NameSpace->user_id;
				$data["vendor_image_type"] = $vendor_image_type;
				try {
					$res = $this->insertintodb($data);
					if($res["returnvalue"]=="valid") {
						return array('success'=>true,'lastid'=>$res["fileid"],'vendor_type'=>$vendor_image_type);
					} else {
						return array('error'=> 'Could not save uploaded file.'.'The upload was cancelled, or server error encountered');
					}
				} catch(Exception $e) {
					return array('error'=> 'Could not save uploaded file.'.'The upload was cancelled, or server error encountered');
				}
	        } else {
	        		return array('success'=>true,'filelocation'=>$uploadDirectory . $filename . '.' . $ext,'vendor_type'=>$vendor_image_type);
	        }
	        exit;
        }
    }
    
    function insertintodb($data = array()) {
    	
    	$file = new Maintenancemgr_Model_DbTable_File();
        $file_model = new Maintenancemgr_Model_File();
    	$file_model->setFileName($data["filename"]);
		$file_model->setFileContent($data["content"]);
		$file_model->setFileType($data["filetype"]);
		$file_model->setFileSize($data["size"]);
		$file_model->setModuleName($data["modulename"]);
		$file_model->setModuleID($data["moduleid"]);
		$file_model->setIsSignature('0');
		$file_model->setCreatedDate(date('Y-m-d H:i:s'));
		$file_model->setUpdatedDate(date('Y-m-d H:i:s'));
		$file_model->setCreatedBy($data["createdby"]);
		$file_model->setUpdatedBy($data["updatedby"]);
		if(isset($data["vendor_image_type"]) && $data["vendor_image_type"] > 0)
			$file_model->setVendorImageType($data["vendor_image_type"]);
		else 
			$file_model->setVendorImageType(0);
		$file_model->setFlag(1);
		$res = $file_model->save();
		if($res["returnvalue"]=="valid") {
			unlink($data["filepath"]);
		}
		return $res;
    }
    
    function getfiletype($ext) {
    	$fileType ="";
		if($ext=="jpg" || $ext=="jpeg"){
			$fileType = "image/jpeg";
		}
        if($ext=="png"){
			$fileType = "image/png";
		}
        if($ext=="bmp"){
			$fileType = "image/bmp";
		}
        if($ext=="doc"){
			$fileType = "application/doc";
		}
        if($ext=="docx"){
			$fileType = "application/docx";
		}
        if($ext=="xls"){
			$fileType = "application/xls";
		}
        if($ext=="xlsx"){
			$fileType = "application/xlsx";
		}
        if($ext=="ppt"){
			$fileType = "application/ppt";
		}
        if($ext=="pptx"){
			$fileType = "application/pptx";
		}
        if($ext=="pdf"){
			$fileType = "application/pdf";
		}
        if($ext=="gif"){
			$fileType = "image/gif";
		}
		return $fileType;
    }
}
<?php



/**

 * Handle file uploads via XMLHttpRequest

 */

class Websitebuilder_Model_Utils_FileUploaderXhr {

    /**

     * Save the file to the specified path

     * @return boolean TRUE on success

     */

    function save($path) {    

        $input = fopen("php://input", "r");

        $temp = tmpfile();

        $realSize = stream_copy_to_stream($input, $temp);

        fclose($input);

        

        if ($realSize != $this->getSize()){            

            return false;

        }



        $target = fopen($path, "w");        

        fseek($temp, 0, SEEK_SET);

        stream_copy_to_stream($temp, $target);

        fclose($target);

        

        return true;

    }

    

    function saveInDatabase ($refTypeId, $refId, $imageId)

    {

    	$input = fopen("php://input", "r");

    	$contents = stream_get_contents($input);

    	fclose($input);



    	$this->fileMime = $_SERVER['CONTENT_TYPE'];

    	

    	$image = new Websitebuilder_Model_ImageMaster();

    	if ($imageId != '' && $imageId != '0')

    		$image->setId($imageId);

    	else 

    		$image->setId(null);

    		

    	$sequenceNo = $image->getMapper()->getNextSequence($refTypeId, $refId);

    	$image->setSequence($sequenceNo);

    	$image->setIsPrimary(1);

    	$image->setMimeType($_SERVER['CONTENT_TYPE']);

    	$image->setSize($this->getSize());

    	$image->setReferenceType($refTypeId);

    	$image->setReferenceId($refId);

    	$image->setContent($contents);



	   	$imageId = $image->getMapper()->save ($image);

    	return $imageId;

    }

    

    function getName() {

        return $_GET['qqfile'];

    }

    function getSize() {

        if (isset($_SERVER["CONTENT_LENGTH"])){

            return (int)$_SERVER["CONTENT_LENGTH"];            

        } else {

            throw new Exception('Getting content length is not supported.');

        }      

    }

    function getMime()

    {

    	return $_SERVER['CONTENT_TYPE'];

    } 

}



/**

 * Handle file uploads via regular form post (uses the $_FILES array)

 */

class Websitebuilder_Model_Utils_FileUploaderForm {  

    /**

     * Save the file to the specified path

     * @return boolean TRUE on success

     */

    function save($path) {

        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){

            return false;

        }

        return true;

    }

    

    function saveInDatabase ($refTypeId, $refId, $imageId)

    {

    	$input = fopen($_FILES['qqfile']['tmp_name'], "r");

    	$contents = stream_get_contents($input);

    	fclose($input);



    	$this->fileMime = $_FILES['qqfile']['type'];

    	

    	$image = new Websitebuilder_Model_ImageMaster();

    	if ($imageId != '' && $imageId != '0')

    		$image->setId($imageId);

    	else

    		$image->setId(null);

    

    	$sequenceNo = $image->getMapper()->getNextSequence($refTypeId, $refId);

    	$image->setSequence($sequenceNo);

    	$image->setIsPrimary(0);

    	$image->setMimeType($_FILES['qqfile']['type']);

    	$image->setSize($this->getSize());

    	$image->setReferenceType($refTypeId);

    	$image->setReferenceId($refId);

    	$image->setContent($contents);

    	 

    	$imageId = $image->getMapper()->save ($image);

    	return $imageId;

    }

        

    function getName() {

        return $_FILES['qqfile']['name'];

    }

    function getSize() {

        return $_FILES['qqfile']['size'];

    }

    function getMime()

    {

    	return $_FILES['qqfile']['type'];

    }

}



class Websitebuilder_Model_Utils_FileUploader {

    private $allowedExtensions = array();

    private $sizeLimit = 10485760;

    private $file;



    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){        

        $allowedExtensions = array_map("strtolower", $allowedExtensions);

            

        $this->allowedExtensions = $allowedExtensions;        

        $this->sizeLimit = $sizeLimit;

        

        $this->checkServerSettings();       



        if (isset($_GET['qqfile'])) {

            $this->file = new Websitebuilder_Model_Utils_FileUploaderXhr();

        } elseif (isset($_FILES['qqfile'])) {

            $this->file = new Websitebuilder_Model_Utils_FileUploaderForm();

        } else {

            $this->file = false; 

        }

    }

    

    private function checkServerSettings(){        

        $postSize = $this->toBytes(ini_get('post_max_size'));

        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));        

        

        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){

            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             

            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");    

        }        

    }

    

    private function toBytes($str){

        $val = trim($str);

        $last = strtolower($str[strlen($str)-1]);

        switch($last) {

            case 'g': $val *= 1024;

            case 'm': $val *= 1024;

            case 'k': $val *= 1024;        

        }

        return $val;

    }

    

    /**

     * Returns array('success'=>true) or array('error'=>'error message')

     */

    function handleUpload($uploadDirectory, $replaceOldFile = FALSE){

        if (!is_writable($uploadDirectory)){

            return array('error' => "Server error. Upload directory isn't writable.");

        }

        

        if (!$this->file){

            return array('error' => 'No files were uploaded.');

        }

        

        $size = $this->file->getSize();

        

        if ($size == 0) {

            return array('error' => 'File is empty');

        }

        

        if ($size > $this->sizeLimit) {

            return array('error' => 'File is too large');

        }

        

        $pathinfo = pathinfo($this->file->getName());

        $filename = $pathinfo['filename'];

        //$filename = md5(uniqid());

        $ext = $pathinfo['extension'];



        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){

            $these = implode(', ', $this->allowedExtensions);

            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');

        }

        

        if(!$replaceOldFile){

            /// don't overwrite previous files that were uploaded

            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {

                $filename .= rand(10, 999);

            }

        }

        

        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){

            return array('success'=>true,'message' => '','filepath' => $filename . '.' . $ext,'mime' => $this->file->getMime());

        } else {

            return array('success'=>false,'message'=> 'Could not save uploaded file.' .

                'The upload was cancelled, or server error encountered');

        }

        

    }



    function uploadInDatabase ($refTypeId, $refId, $imageId)

    {

    	if (!$this->file){

    		return array('error' => 'No files were uploaded.');

    	}

    

    	$size = $this->file->getSize();

    

    	if ($size == 0) {

    		return array('error' => 'File is empty');

    	}

    

    	if ($size > $this->sizeLimit) {

    		return array('error' => 'File is too large');

    	}

    

    	$pathinfo = pathinfo($this->file->getName());

    	$filename = $pathinfo['filename'];

    	//$filename = md5(uniqid());

    	$ext = $pathinfo['extension'];

    

    	if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){

    		$these = implode(', ', $this->allowedExtensions);

    		return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');

    	}

    

    	$imageId = $this->file->saveInDatabase($refTypeId, $refId, $imageId);

    	if ($imageId){

    		return array('success'=>true, 'imageid'=>$imageId);

    	} else {

    		return array('error'=> 'Could not save uploaded file.' .

    				'The upload was cancelled, or server error encountered');

    	}

    

    }

    

}


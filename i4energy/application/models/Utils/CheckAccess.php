<?php
class I4energy_Model_Utils_CheckAccess {

	public static function checkaccess($role, $priviledge, &$permitedPrivilageArray)
	{
		//echo strtolower($role) . " = " . $priviledge;exit;
		if (strtolower($role) == ADMIN_USER)
			return true;
		
		if (isset($permitedPrivilageArray) && count($permitedPrivilageArray)>0)
			return in_array(trim($priviledge), $permitedPrivilageArray);
		else 
			return false;
	}
}

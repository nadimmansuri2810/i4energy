<?php
class I4energy_Model_SitesUsers {
	protected $id;
	protected $site_id;
	protected $user_id;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Site property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Site property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setUser_id ($user_id) {
    	$this->user_id = $user_id;
    	return $this;
    }
    public function getUser_id () {
    	return $this->user_id;
    }
    
	public function setSite_id($site_id) {
        $this->site_id = $site_id;
        return $this;
    }
    public function getSite_id() {
        return $this->site_id;
    }
    
    public function setMapper(I4energy_Model_SitesUsersMapper $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_SitesUsersMapper());
        }
        return $this->_mapper;
    }
    
    public function listSitesUsers ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listSitesUsers ($filters, $orderby, $sort, $start, $limit);
    }
    
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);	
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
}
?>
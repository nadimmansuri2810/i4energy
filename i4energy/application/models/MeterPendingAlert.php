<?php
class I4energy_Model_MeterPendingAlert {
	protected $id;
	protected $alert_time;
    protected $alert_status;
    protected $alert_notify_datetime;
    protected $meter_id;
    protected $last_reading_data;
    protected $alert_type;
    protected $last_reading_datetime;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid meter pending alert property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid meter pending alert property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setAlert_datetime($alertdatetime) {
    	$this->alert_time = $alertdatetime;
    	return $this;
    }
    public function getAlert_datetime() {
    	return $this->alert_time;
    }
    
    public function setAlert_status ($alertstatus) {
    	$this->alert_status = $alertstatus;
    	return $this;
    }
    public function getAlert_status () {
    	return $this->alert_status;
    }
    
	public function setAlert_notify_datetime($alertnotifydatetime) {
        $this->alert_notify_datetime = $alertnotifydatetime;
        return $this;
    }
    public function getAlert_notify_datetime() {
        return $this->alert_notify_datetime;
    }
    
    
    public function setMeter_id ($meter_id) {
    	$this->meter_id = $meter_id;
    	return $this;
    }
    public function getMeter_id () {
    	return $this->meter_id;
    }
    
    public function setLast_reading_datetime ($data) {
		$this->last_reading_datetime = $data;
    	return $this;
    }
    public function getLast_reading_datetime () {    	
    	return $this->last_reading_datetime;
    }
    
    public function setLast_reading_data ($lastreadingdata) {
    	$this->last_reading_data = $lastreadingdata;
    	return $this;
    }
    public function getLast_reading_data () {
    	return $this->last_reading_data;
    }
    
    public function setAlert_type ($alerttype) {
    	$this->alert_type = $alerttype;
    	return $this;
    }
    public function getAlert_type () {
    	return $this->alert_type;
    }
    

    public function setMapper(I4energy_Model_MeterPendingAlertMapper $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_MeterPendingAlertMapper());
        }
        return $this->_mapper;
    }
    
    public function listMeterPendingAlert ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listMeterPendingAlert ($filters, $orderby, $sort, $start, $limit);
    }
    
    public function init ($meterpendingalert)
    {
    	if (is_numeric($meterpendingalert))
    	{
    		$select = $this->getMapper()->getSelect();
    		$select->from("meter_pending_alert");
    		$select->where("`id`=?", $meterpendingalert);
    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    		$meterpendingalert_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	}
    	else
    	{
    		$meterpendingalert_row = $meterpendingalert;
    	}
    	
    	$this->setId($meterpendingalert_row['id']);
    	$this->setAlert_datetime($meterpendingalert_row['alert_time']);
		$this->setAlert_status($meterpendingalert_row['alert_status']);
		$this->setAlert_notify_datetime($meterpendingalert_row['alert_notify_datetime']);
		$this->setMeter_id($meterpendingalert_row['meter_id']);
		$this->setLast_reading_datetime($meterpendingalert_row['last_reading_datetime']);
		$this->setLast_reading_data($meterpendingalert_row['last_reading_data']);
		$this->setAlert_type($meterpendingalert_row['alert_type']);
    }
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);	
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    	
    	$meter_attr = new I4energy_Model_MeterAttributes();
    	$meter_attr->setId ($this->getId());
    	$meter_attr->delete();
    	
    	$meter_alert = new I4energy_Model_MeterAlerts();
    	$meter_alert->setId ($this->getId());
    	$meter_alert->delete();
    }
    
    public static function convertToUnit ($data, $type, $unit, $conversationFactor)
    {
    	$data = (float) $data;
    	if (isset ($conversationFactor) && $conversationFactor != 0 && $conversationFactor != '')
    		$data = $data*$conversationFactor;
    	
    	switch ($type)
    	{
    		case 'gas':
    			if ($unit == 'm3')
    			{
    				return number_format($data / 1000, 2, '.', '');
    			}
    			else if ($unit == 'kwh')
    			{
    				$kwh = ($data * 1.022640 * 39.3) / 3.6;
    				return number_format($kwh, 2, '.', '');
    			}
    			break;
    		case 'water':
    			if ($unit == 'm3')
    				return number_format($data / 1000, 2, '.', '');
    			else if ($unit == 'gl')
    				return number_format($data * 0.264172, 2, '.', '');
    			else if ($unit == 'pints')
					return number_format($data * 2.11338, 2, '.', '');
    			else
    				return number_format($data, 2, '.', '');
    			break;
    		case 'amps':
    			return number_format($data / 220, 2, '.', '');
    		default:
    			return number_format($data, 2, '.', '');
    			break;
    	}
    	
    	return number_format ($data, 2, '.', '');
    }
    
    public static function calculateCarbonSavings ($data, $type, $carbonSavings)
    {
    	$data = (float) $data;
    	if (isset ($carbonSavings) && $carbonSavings != 0 && $carbonSavings != '')
    	{
	    	switch ($type)
	    	{
	    		case 'gas':
	    			$kwh = ($data * 1.022640 * 39.3) / 3.6;
	    			$kgCo2 = number_format ($kwh * $carbonSavings, 2, '.', '');
	    			return $kgCo2;
	    			break;
	    		case 'power':
	    			return number_format($data * $carbonSavings, 2, '.', '');
	    			break;
	    		default:
	    			return number_format($data, 2, '.', '');
	    			break;
	    	}
    	}    	
    	return number_format($data, 2, '.', '');
    }
    
    public static function getDisplayUnit ($unit)
    {
    	switch ($unit)
    	{
    		case 'm3':
    			return 'M<sup>3</sup>';
    			break;
    		case 'c':
    			return '&deg;C';
    			break;
    		case 'l':
    			return 'L';
    			break;
    		case 'gl':
    			return 'Gl';
    			break;
    		case 'amps':
    			return 'Amps';
    			break;
    		case 'pints';
    			return 'Pints';
    			break;
    		case 'kw':
    		case 'kwh':
    			return 'kWh';
    			break;
    		case 'v':
    			return 'V';
    			break;
    		case 'ma':
    			return 'mA';
    			break;
    		case 'onoff':
    			return 'No';
    			break;
    		default:
    			return $unit;
    			break;
    	}
    	
    	return $unit;
    }
}
?>
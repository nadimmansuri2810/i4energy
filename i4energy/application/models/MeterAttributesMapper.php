<?php
class I4energy_Model_MeterAttributesMapper {
	protected $_dbtable;
	protected $__select;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_MeterAttributes');
        }
        return $this->_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    		return $this->__select;
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		return $this->__select;
    	}
    }
    
    public function save (I4energy_Model_MeterAttributes &$attr, $ignore_unset = false)
    {
    	$data = array (
    		'id' => $attr->getId()
    		,'webminscale' => $attr->getWebMinScale()
    		,'webmaxscale' => $attr->getWebMaxScale()
    		,'webupdateinterval' => $attr->getWebUpdateInterval()
    		,'isgcm' => $attr->getIsGcm()
    		,'networkname' => $attr->getNetworkName()
    		,'simcardno' => $attr->getSimCardNo()
    		,'phoneno' => $attr->getPhoneNo()
    		,'installdate' => $attr->getInstallDate()
    		,'carbon_volumecorrection' => $attr->getCarbonVolumeCorrectionFactor()
    		,'carbon_calorificvalue' => $attr->getCarbonCalorificValue()
    		,'carbon_kwhconversion' => $attr->getCarbonKwhConversionFactor()
    		,'ref_voltage' => $attr->getReferenceVoltage()
			,'hourrun_webdial_ch' => $attr->getHourRunWebDialChannel()
			,'hourrun_webdial_unit' => $attr->getHourRunWebDialUnit()
			,'hourrun_pulse1_text' => $attr->getHourRunPulseText1()
			,'hourrun_pulse2_text' => $attr->getHourRunPulseText2()
			,'hourrun_pulse3_text' => $attr->getHourRunPulseText3()
			,'control_link' => $attr->getControlLink()
    	);
    	
    	if ($ignore_unset)
    	{
    		foreach ($data as $key => $value)
    		{
    			if (!isset ($data[$key]))
    				unset ($data[$key]);
    		}
    	}
    	
    	if(isset($data['carbon_volumecorrection']) && $data['carbon_volumecorrection'] =='')
    		$data['carbon_volumecorrection'] = new Zend_Db_Expr('NULL');
    	if(isset($data['carbon_calorificvalue']) && $data['carbon_calorificvalue'] =='')
    		$data['carbon_calorificvalue'] = new Zend_Db_Expr('NULL');
    	if(isset($data['carbon_kwhconversion']) && $data['carbon_kwhconversion'] =='')
    		$data['carbon_kwhconversion'] = new Zend_Db_Expr('NULL');
    	if(isset($data['ref_voltage']) && $data['ref_voltage'] =='')
    		$data['ref_voltage'] = new Zend_Db_Expr('NULL');
    	
    	$select = $this->getSelect();
    	$select->from("meter_attr");
    	$select->where("`id`=?", $attr->getId());
    	$stmt = $this->getDbTable()->getAdapter()->query ($select);
    	$meter_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	
    	if (isset($meter_row) && $meter_row['id'] > 0)
    	{
    		$this->getDbTable()->update($data, array('id = ?' => $attr->getId()));
    		return $attr->getId();
    	}
    	else
    	{
    		$this->getDbTable()->insert($data);
    		return $attr->getId();
    	}
    }
    
    public function delete (I4energy_Model_MeterAttributes &$attr)
    {
    	$where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $attr->getId());
    	$this->getDbTable()->delete ($where);
    }
}

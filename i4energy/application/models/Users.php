<?php
class I4energy_Model_Users {
	protected $id;
	protected $role_id;
	protected $customer_id;
    protected $name;
    protected $email;
    protected $password;
    protected $mobile;
    protected $status;
    protected $token;
    protected $timezone;
    protected $dateformat;
    protected $theme;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid User property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Users property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setRole_id($roleid) {
    	$this->role_id = $roleid;
    	return $this;
    }
    public function getRole_id() {
    	return $this->role_id;
    }
    public function setCustomer_id($customerid) {
    	$this->customer_id = $customerid;
    	return $this;
    }
    public function getCustomer_id() {
    	return $this->customer_id;
    }
    
    
    public function setName ($name) {
    	$this->name = $name;
    	return $this;
    }
    public function getName () {
    	return $this->name;
    }
    
	public function setEmail($email) {
        $this->email = $email;
        return $this;
    }
    public function getEmail() {
        return $this->email;
    }
    
	public function setPassword($password) {
        $this->password = $password;
        return $this;
    }
    public function getPassword() {
        return $this->password;
    }
    
    public function setMobile ($mobile) {
    	$this->mobile = $mobile;
    	return $this;
    }
    public function getMobile () {
    	return $this->mobile;
    }
    
    public function setStatus ($status) {
    	$this->status = $status;
    	return $this;
    }
    public function getStatus () {
    	return $this->status;
    }
    
    public function setToken ($token) {
    	$this->token = $token;
    	return $this;
    }
    public function getToken () {
    	return $this->token;
    }

    public function setTimeZone ($timeZone) {
    	$this->timezone = $timeZone;
    	return $this;
    }
    public function getTimeZone () {
    	return $this->timezone;
    }
    
    public function setDateFormat ($dateFormat) {
    	return $this->dateformat = $dateFormat;
    	return $this;
    }
    public function getDateFormat () {
    	return $this->dateformat;
    }
    
    public function setTheme ($theme) {
    	return $this->theme = $theme;
    	return $this;
    }
    public function getTheme () {
    	return $this->theme;
    }
    
    public function setMapper(I4energy_Model_UsersMapper $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_UsersMapper());
        }
        return $this->_mapper;
    }
    
    public function authenticate ($username, $password) {



        // $db = Zend_Db_Table_Abstract::getDefaultAdapter();

        // $select  = $db->select()
        //               ->from("users",array('*'));
        // $select->where("`email`=?", $username);
        // $select->where("`password`=password(?)", $password);

        // $data = $db->query($select)->fetchAll();



    	$select = $this->getMapper()->getSelect();
    	$select->from("users");
    	$select->where("`email`=?", $username);
    	$select->where("`password`=password(?)", $password);
    		
    	$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    	$user_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);

    	if (sizeof ($user_row) > 0)
    	{
    		$this->init($user_row);
    		return $this;
    	}
    	
    	return null;
    }
    
    public function authenticateToken ($token) {
    	$select = $this->getMapper()->getSelect();
    	$select->from("users");
    	$select->where("`token`<>''");
    	$select->where("`token`=?", $token);
    	
    	$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    	$user_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	 
    	if (sizeof ($user_row) > 0)
    	{
    		$this->init($user_row);
    		return $this;
    	}
    	 
    	return null;
    }
    public function listUsers ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listUsers ($filters, $orderby, $sort, $start, $limit);
    }
    public function init ($user)
    {
    	if (is_numeric($user))
    	{
    		$select = $this->getMapper()->getSelect();
    		$select->from("users");
    		$select->where("`id`=?", $user);
    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    		$user_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    		
    	}
    	else
    	{
    		$user_row = $user;
    	}
    	$this->setId($user_row['id']);
    	$this->setRole_id($user_row['role_id']);
    	$this->setCustomer_id($user_row['customer_id']);
    	$this->setName($user_row['name']);
    	$this->setEmail($user_row['email']);
    	$this->setMobile($user_row['mobile']);
    	$this->setPassword($user_row['password']);
    	$this->setStatus($user_row['status']);
    	$this->setToken($user_row['token']);
    	$this->setTimeZone($user_row['timezone']);
    	$this->setDateFormat($user_row['dateformat']);
    	$this->setTheme($user_row['theme']);
    }
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
    
    public function updateToken ($token)
    {
    	$tmp_user = new I4energy_Model_Users();
    	 
    	$tmp_user->setId($this->getId());
    	$tmp_user->setToken($token);
    	
    	$this->getMapper()->save ($tmp_user, true);
    }
}
?>
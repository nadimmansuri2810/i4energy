<?php
class I4energy_Model_Sites {
	protected $id;
    protected $name;
    protected $code;
    protected $createddate;
    protected $createdby;
    protected $updateddate;
    protected $updatedby;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Site property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Site property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setName ($name) {
    	$this->name = $name;
    	return $this;
    }
    public function getName () {
    	return $this->name;
    }
    
	public function setCode($code) {
        $this->code = $code;
        return $this;
    }
    public function getCode() {
        return $this->code;
    }
    
	public function setCreatedBy($createdBy) {
        $this->createdby = $createdBy;
        return $this;
    }
    public function getCreatedBy() {
        return $this->createdby;
    }
    
    public function setCreatedDate ($createdDate) {
    	$this->createddate = $createdDate;
    	return $this;
    }
    public function getCreatedDate () {
    	return $this->createddate;
    }
    
    public function setUpdatedBy ($updatedBy) {
    	$this->updatedby = $updatedBy;
    	return $this;
    }
    public function getUpdatedBy () {
    	return $this->updatedby;
    }
    
    public function setUpdatedDate ($updatedDate) {
    	$this->updatedDate = $updatedDate;
    	return $this;
    }
    public function getUpdatedDate () {
    	return $this->updatedDate;
    }

    public function setMapper(I4energy_Model_SitesMapper $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_SitesMapper());
        }
        return $this->_mapper;
    }
    
    public function listSites ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listSites ($filters, $orderby, $sort, $start, $limit);
    }
    
    public function init ($site)
    {
    	if (is_numeric($site))
    	{
    		$select = $this->getMapper()->getSelect();
    		$select->from("sites");
    		$select->where("`id`=?", $site);
    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    		$site_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	}
    	else
    	{
    		$site_row = $site;
    	}
    	
    	$this->setId($site_row['id']);
    	$this->setName($site_row['name']);
    	$this->setCode($site_row['code']);
    	$this->setCreatedBy($site_row['createdby']);
    	$this->setCreatedDate($site_row['createddate']);
    	$this->setUpdatedBy($site_row['updatedby']);
    	$this->setUpdatedDate($site_row['updateddate']);
    }
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);	
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
}
?>
<?php
class I4energy_Model_Role {
	protected $id;
    protected $role;
    protected $_mapper;
    
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid User property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Users property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setRole ($role) {
    	$this->role = $role;
    	return $this;
    }
    public function getRole () {
    	return $this->role;
    }



    public function setMapper(I4energy_Model_RoleMapper $mapper) {
    	$this->_mapper = $mapper;
    	return $this;
    }
    public function getMapper() {
    	if (null === $this->_mapper) {
    		$this->setMapper(new I4energy_Model_RoleMapper());
    	}
    	return $this->_mapper;
    }
    
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
    
    public function init ($id)
    {
    	return $this->getMapper()->init ($id);
    }
}
?>
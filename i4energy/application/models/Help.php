<?php
class I4energy_Model_Help {
  protected $id;
  protected $title;
  protected $url;
  protected $createddate;
  protected $createdby;
  protected $updateddate;
  protected $updatedby;
  protected $_mapper;

  public function __construct(array $options = null) {
    if (is_array($options)) {
      $this->setOptions($options);
    }
  }

  public function __set($name, $value) {
    $method = 'set' . $name;
    if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid Customer property');
    }
    $this->$method($value);
  }

  public function __get($name) {
    $method = 'get' . $name;
    if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid Customer property');
    }
    return $this->$method();
  }

  public function setOptions(array $options) {
    $methods = get_class_methods($this);
    foreach ($options as $key => $value) {
      $method = 'set' . ucfirst($key);
      if (in_array($method, $methods)) {
        $this->$method($value);
      }
    }
    return $this;
  }

  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getId() {
    return $this->id;
  }

  public function setTitle($title) {
    $this->title = $title;
    return $this;
  }
  public function getTitle() {
    return $this->title;
  }

  public function setUrl($url) {
    $this->url = $url;
    return $this;
  }
  public function getUrl() {
    return $this->url;
  }

  public function setCreatedBy($createdBy) {
    $this->createdby = $createdBy;
    return $this;
  }
  public function getCreatedBy() {
    return $this->createdby;
  }

  public function setCreatedDate ($createdDate) {
    $this->createddate = $createdDate;
    return $this;
  }
  public function getCreatedDate () {
    return $this->createddate;
  }

  public function setUpdatedBy ($updatedBy) {
    $this->updatedby = $updatedBy;
    return $this;
  }
  public function getUpdatedBy () {
    return $this->updatedby;
  }

  public function setUpdatedDate ($updatedDate) {
    $this->updatedDate = $updatedDate;
    return $this;
  }
  public function getUpdatedDate () {
    return $this->updatedDate;
  }




  public function setMapper(I4energy_Model_HelpMapper $mapper) {
    $this->_mapper = $mapper;
    return $this;
  }
  public function getMapper() {
    if (null === $this->_mapper) {
      $this->setMapper(new I4energy_Model_HelpMapper());
    }
    return $this->_mapper;
  }

  public function listHelps ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
  {
    return $this->getMapper()->listHelps ($filters, $orderby, $sort, $start, $limit);
  }
  public function init ($help)
  {
    if (is_numeric($help))
    {
      $select = $this->getMapper()->getSelect();
      $select->from("helps");
      $select->where("`id`=?", $help);
      $stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
      $help_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    }
    else
    {
      $help_row = $help;
    }
    $this->setId($help_row['id']);
    $this->setTitle($help_row['title']);
    $this->setUrl($help_row['url']);
    $this->setCreatedBy($help_row['createdby']);
    $this->setCreatedDate($help_row['createddate']);
    $this->setUpdatedBy($help_row['updatedby']);
    $this->setUpdatedDate($help_row['updateddate']);
  }



  public function save ($ignore_unset = false)
  {
    return $this->getMapper()->save($this, $ignore_unset);
  }

  public function delete ()
  {
    $this->getMapper()->delete ($this);
  }


}
?>
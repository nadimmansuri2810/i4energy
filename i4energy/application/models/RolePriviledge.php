<?php
class I4energy_Model_RolePriviledge {
	protected $id;
	protected $role_id;
	protected $priviledge_id;
	protected $_mapper;
	

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid RolePriviledge property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid RolePriviledge property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
    	
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setPriviledge_id ($priviledge_id) {
    	$this->priviledge_id = $priviledge_id;
    	return $this;
    }
    public function getPriviledge_id () {
    	return $this->priviledge_id;
    }
    
    public function setRole_id ($role_id) {
    	$this->role_id = $role_id;
    	return $this;
    }
    public function getRole_id () {
    	return $this->role_id;
    }

    public function setMapper(I4energy_Model_RolePriviledgeMapper $mapper) {
    	$this->_mapper = $mapper;
    	return $this;
    }

    public function getMapper() {
    	if (null === $this->_mapper) {
    		$this->setMapper(new I4energy_Model_RolePriviledgeMapper());
    	}
    	return $this->_mapper;
    }
    
    public function listRolePriviledge ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listRolePriviledge ($filters, $orderby, $sort, $start, $limit);
    }
    
    
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
}
?>
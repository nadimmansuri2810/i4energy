<?php
class I4energy_Model_MeterUser {
	protected $id;
	protected $meter_id;
	protected $user_id;
	protected $_mapper;
	

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Meter user property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid meterUser property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
    	
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setMeter_id ($meter_id) {
    	$this->meter_id = $meter_id;
    	return $this;
    }
    public function getMeter_id () {
    	return $this->meter_id;
    }
    
    public function setUser_id ($user_id) {
    	$this->user_id = $user_id;
    	return $this;
    }
    public function getUser_id () {
    	return $this->user_id;
    }

    public function setMapper(I4energy_Model_MeterUserMapper $mapper) {
    	$this->_mapper = $mapper;
    	return $this;
    }

    public function getMapper() {
    	if (null === $this->_mapper) {
    		$this->setMapper(new I4energy_Model_MeterUserMapper());
    	}
    	return $this->_mapper;
    }
    
    public function listMeterUser ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listMeterUser ($filters, $orderby, $sort, $start, $limit);
    }
    
    
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
}
?>
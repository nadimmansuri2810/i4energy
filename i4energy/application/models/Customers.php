<?php
class I4energy_Model_Customers {
	protected $id;
	protected $name;
    protected $logo;
    protected $logotype;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Customer property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Customer property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setName($name) {
    	$this->name = $name;
    	return $this;
    }
    public function getName() {
    	return $this->name;
    }
    
    public function setLogo ($logo) {
    	$this->logo = $logo;
    	return $this;
    }
    public function getLogo () {
    	return $this->logo;
    }
    
    public function setLogotype ($logotype) {
    	$this->logotype = $logotype;
    	return $this;
    }
    public function getLogotype () {
    	return $this->logotype;
    }
    
    
    public function setMapper(I4energy_Model_CustomersMapper $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_CustomersMapper());
        }
        return $this->_mapper;
    }
    

    public function listCustomers ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listCustomers ($filters, $orderby, $sort, $start, $limit);
    }
    public function init ($customer)
    {
    	if (is_numeric($customer))
    	{
    		$select = $this->getMapper()->getSelect();
    		$select->from("customers");
    		$select->where("`id`=?", $customer);
    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    		$customer_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	}
    	else
    	{
    		$customer_row = $customer;
    	}
    	$this->setId($customer_row['id']);
    	$this->setName($customer_row['name']);
    	$this->setLogo($customer_row['logo']);
    	$this->setLogotype($customer_row['logotype']);
    }
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
    

}
?>
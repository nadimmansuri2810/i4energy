<?php
class I4energy_Model_MetersMapper {
	protected $_dbtable;
	protected $__select;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_Meters');
        }
        return $this->_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    	{
    		$this->__select->reset();
    		return $this->__select;
    	}
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		$this->__select->reset();
    		return $this->__select;
    	}
    }
    
    public function listMeters ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	$select = $this->getSelect();
    	$select->from ('meters');
    	
    	if ( is_array ($filters) )
    	{
    		for ($i=0; $i < sizeof ($filters); $i++)
    		{
	    		if (isset($filters[$i]['field']) && $filters[$i]['field'] != '' && isset($filters[$i]['operation']) && $filters[$i]['operation'] != '')
	    			$select->addCondition ($filters[$i]['field'], $filters[$i]['operation'], $filters[$i]['value']);
    		}
   		}
   		
   		if ($sort == '')
   			$sort = 'DESC';
   		
   		if ($orderby == '')
   			$select->order ('createddate ' . $sort);
   		else
   			$select->order ($orderby . ' ' . $sort);
   		
   		if ($start != null && $limit != null)
   			$select->limit ($limit, $start);
   		
   		$select->sql_cals_found_rows (true);
   		
   		$stmt = $this->getDbTable()->getAdapter()->query($select);
   		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
   		
   		$stmt = new Zend_Db_Statement_Pdo($this->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
   		$stmt->execute();
   		$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
   		
   		$response = new stdClass();
   		$response->totalrecords = $count['totalrecords'];
   		$response->returnrecords = sizeof ($rows);
   		$response->rows = $rows;
   		
   		return $response;
    }
    
    public function save (I4energy_Model_Meters &$meter, $ignore_unset = false)
    {
    	$data = array (
    		'id' => $meter->getId()
    		,'siteid' => $meter->getSiteId()
    		,'name' => $meter->getName()
    		,'description' => $meter->getDescription()
    		,'location' => $meter->getLocation()
    		,'mac' => $meter->getMac()
    		,'type' => $meter->getType()
    		,'moduleno' => $meter->getModuleNo()
    		,'slotno' => $meter->getSlotNo()
    		,'channelno' => $meter->getChannelNo()
    		,'conversationfactor' => $meter->getConversationFactor()
    		,'unit' => $meter->getUnit()
    		,'offset' => $meter->getOffset()
    		,'cost' => $meter->getCost()
    		,'carbonsavings' => $meter->getCarbonSavings()
    		,'timezone' => $meter->getTimezone()
    		,'pingrate' => $meter->getPingRate()
    		,'createddate' => $meter->getCreatedDate()
    		,'createdby' => $meter->getCreatedBy()
    		,'updateddate' => $meter->getUpdatedDate()
    		,'updatedby' => $meter->getUpdatedBy()
    		,'latestreading' => $meter->getLatestReading()
    		,'latestreadingtime' => $meter->getLatestReadingTime()
    	);
    	
    	if ($ignore_unset)
    	{
    		foreach ($data as $key => $value)
    		{
    			if (!isset ($data[$key]))
    				unset ($data[$key]);
    		}
    	}
    	if(isset($data['offset']) && $data['offset'] ==''){
    		$data['offset']=new Zend_Db_Expr('NULL');
    	}
    	if (!isset ($data['updateddate']))
    		$data['updateddate'] = gmdate('Y-m-d H:i:s');
    	
    	if ($meter->getId() == null || $meter->getId() == '')
    	{
    		if (!isset ($data['createddate']))
    			$data['createddate'] = gmdate('Y-m-d H:i:s');
    		
    		$this->getDbTable()->insert($data);
    		$lastid = $this->getDbTable()->getAdapter()->lastInsertId();
    		return $lastid;
    	}
    	else
    	{
    		$this->getDbTable()->update($data, array('id = ?' => $meter->getId()));
    		return $meter->getId();
    	}
    }
    
    public function delete (I4energy_Model_Meters &$meter)
    {
    	$where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $meter->getId());
    	$this->getDbTable()->delete ($where);
    }
}

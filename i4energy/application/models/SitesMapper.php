<?php
class I4energy_Model_SitesMapper {
	protected $sites_dbtable;
	protected $__select;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->sites_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->sites_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_Sites');
        }
        return $this->sites_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    		return $this->__select;
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		return $this->__select;
    	}
    }
    
    public function listSites ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	$select = $this->getSelect();
    	$select->from ('sites');
    	
    	if ( is_array ($filters) )
    	{
    		for ($i=0; $i < sizeof ($filters); $i++)
    		{
	    		if (isset($filters[$i]['field']) && $filters[$i]['field'] != '' && isset($filters[$i]['operation']) && $filters[$i]['operation'] != '')
	    			$select->addCondition ($filters[$i]['field'], $filters[$i]['operation'], $filters[$i]['value']);
    		}
   		}
   		
   		if ($sort == '')
   			$sort = 'DESC';
   		
   		if ($orderby == '')
   			$select->order ('createddate ' . $sort);
   		else
   			$select->order ($orderby . ' ' . $sort);
   		
   		if ($start != null && $limit != null)
   			$select->limit ($limit, $start);
   		
   		$select->sql_cals_found_rows (true);
   		
   		$stmt = $this->getDbTable()->getAdapter()->query($select);
   		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
   		
   		$stmt = new Zend_Db_Statement_Pdo($this->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
   		$stmt->execute();
   		$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
   		
   		$response = new stdClass();
   		$response->totalrecords = $count['totalrecords'];
   		$response->returnrecords = sizeof ($rows);
   		$response->rows = $rows;
   		
   		return $response;
    }
    
    public function save (I4energy_Model_Sites &$site, $ignore_unset = false)
    {
    	$data = array (
    		'id' => $site->getId()
    		,'name' => $site->getName()
    		,'code' => $site->getCode()
    		,'createddate' => $site->getCreatedDate()
    		,'createdby' => $site->getCreatedBy()
    		,'updateddate' => $site->getUpdatedDate()
    		,'updatedby' => $site->getUpdatedBy()
    	);
    	
    	if ($ignore_unset)
    	{
    		if (!isset ($data['id']))
    			unset ($data['id']);
    		if (!isset ($data['name']))
    			unset ($data['name']);
    		if (!isset ($data['code']))
    			unset ($data['code']);
    		if (!isset ($data['createddate']))
    			unset ($data['createddate']);
    		if (!isset ($data['createdby']))
    			unset ($data['createdby']);
    		if (!isset ($data['updateddate']))
    			unset ($data['updateddate']);
    		if (!isset ($data['updatedby']))
    			unset ($data['updatedby']);
    	}
    	
    	if (!isset ($data['updateddate']))
    		$data['updateddate'] = gmdate('Y-m-d H:i:s');
    	
    	if ($site->getId() == null || $site->getId() == '')
    	{
    		if (!isset ($data['createddate']))
    			$data['createddate'] = gmdate('Y-m-d H:i:s');
    		
    		$this->getDbTable()->insert($data);
    		$lastid = $this->getDbTable()->getAdapter()->lastInsertId();
    		return $lastid;
    	}
    	else
    	{
    		$this->getDbTable()->update($data, array('id = ?' => $site->getId()));
    		return $site->getId();
    	}
    }
    
    public function delete (I4energy_Model_Sites &$site)
    {
    	$where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $site->getId());
    	$this->getDbTable()->delete ($where);
    }
}

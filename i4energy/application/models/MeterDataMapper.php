<?php
class I4energy_Model_MeterDataMapper {
	protected $_dbtable;
	protected $__select;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_MeterData');
        }
        return $this->_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    	{
    		$this->__select->reset();
    		return $this->__select;
    	}
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		$this->__select->reset();
    		return $this->__select;
    	}
    }
    
    public function listMeterData ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	$select = $this->getSelect();
    	$select->from ('meter_data');
    	 
    	if ( is_array ($filters) )
    	{
    		for ($i=0; $i < sizeof ($filters); $i++)
    		{
    			if (isset($filters[$i]['field']) && $filters[$i]['field'] != '' && isset($filters[$i]['operation']) && $filters[$i]['operation'] != '')
    				$select->addCondition ($filters[$i]['field'], $filters[$i]['operation'], $filters[$i]['value']);
    		}
    	}
    		 
		if ($sort == '')
    		$sort = 'DESC';
    			 
    	if ($orderby == '')
    		$select->order ('rec_date ' . $sort);
    	else
    		$select->order ($orderby . ' ' . $sort);
    			 
    	if ($start != null && $limit != null)
    		$select->limit ($limit, $start);
    			 
    	$select->sql_cals_found_rows (true);
    		 
    	$stmt = $this->getDbTable()->getAdapter()->query($select);
    	$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
    		 
    	$stmt = new Zend_Db_Statement_Pdo($this->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
    	$stmt->execute();
    	$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    		 
    	$response = new stdClass();
    	$response->totalrecords = $count['totalrecords'];
    	$response->returnrecords = sizeof ($rows);
    	$response->rows = $rows;
    		 
    	return $response;
    }

  public function save (I4energy_Model_MeterData &$meterData, $ignore_unset = false)
  {
//    print_r("dddddddddddddd");exit;

    $data = array (
      'macid' => $meterData->getMac()
    ,'rec_date' => $meterData->getDate()
    ,'moduleid' => $meterData->getModuleNo()
    ,'slotno' => $meterData->getSlotNo()
    ,'channelno' => $meterData->getChannelNo()
    ,'data' => $meterData->getData()
    ,'data1' => $meterData->getData1()
    );

    if ($ignore_unset)
    {
      if (!isset ($data['macid']))
        unset ($data['macid']);
      if (!isset ($data['rec_date']))
        unset ($data['rec_date']);
      if (!isset ($data['moduleid']))
        unset ($data['moduleid']);
      if (!isset ($data['slotno']))
        unset ($data['slotno']);
      if (!isset ($data['channelno']))
        unset ($data['channelno']);
      if (!isset ($data['data']))
        unset ($data['data']);
      if (!isset ($data['data1']))
        unset ($data['data1']);
    }


    print_r("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    print_r($data);
    print_r("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");


//      $this->getDbTable()->insert($data);
//      $lastid = $this->getDbTable()->getAdapter()->lastInsertId();

//      return $lastid;

//    if ($meterData->getMac() == $data['macid'] && $meterData->getDate() != '')
//    {
//
//    }


//
//    if ($meterData->getMac() == null || $meterData->getMac() == '')
//    {
//      $this->getDbTable()->insert($data);
//      $lastid = $this->getDbTable()->getAdapter()->lastInsertId();
//      return $lastid;
//    }
//    else
//    {
////      print_r($data);exit;
//      $this->getDbTable()->update($data, array('macid = ?' => $meterData->getMac()));
//      return $meterData->getMac();
//    }

//
//    if (!isset ($data['date']))
//      $data['date'] = gmdate('Y-m-d H:i:s');
//
//    if ($site->getId() == null || $site->getId() == '')
//    {
//      if (!isset ($data['createddate']))
//        $data['createddate'] = gmdate('Y-m-d H:i:s');
//
//      $this->getDbTable()->insert($data);
//      $lastid = $this->getDbTable()->getAdapter()->lastInsertId();
//      return $lastid;
//    }
//    else
//    {
//      $this->getDbTable()->update($data, array('id = ?' => $site->getId()));
//      return $site->getId();
//    }
  }

}

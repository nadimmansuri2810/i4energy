<?php
class I4energy_Model_MeterAttributes {
	protected $id;
    protected $webminscale;
    protected $webmaxscale;
    protected $webupdateinterval;
    protected $isgcm;
    protected $networkname;
    protected $phoneno;
    protected $installdate;
    protected $carbonvolumecorrectionfactor;
    protected $carboncalorificvalue;
    protected $carbonkwhconversionfactor;
    protected $simcardno;
    protected $referencevoltage;
    protected $hourrun_webdial_channel;
    protected $hourrun_webdial_unit;
    protected $hourrun_pulse1_text;
    protected $hourrun_pulse2_text;
    protected $hourrun_pulse3_text;
    protected $control_link;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Meter Attribute property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Meter Attribute property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setWebMinScale ($webminscale) {
    	$this->webminscale = $webminscale;
    	return $this;
    }
    public function getWebMinScale () {
    	return $this->webminscale;
    }
    
	public function setWebMaxScale ($webmaxscale) {
    	$this->webmaxscale = $webmaxscale;
    	return $this;
    }
    public function getWebMaxScale () {
    	return $this->webmaxscale;
    }
    
	public function setWebUpdateInterval($webupdateinterval) {
        $this->webupdateinterval = $webupdateinterval;
        return $this;
    }
    public function getWebUpdateInterval() {
        return $this->webupdateinterval;
    }
    
    public function setIsGcm ($isgcm) {
    	$this->isgcm = $isgcm;
    	return $this;
    }
    public function getIsGcm () {
    	return $this->isgcm;
    }
    
    public function setNetworkName ($networkname) {
    	$this->networkname = $networkname;
    	return $this;
    }
    public function getNetworkName () {
    	return $this->networkname;
    }
    
    public function setPhoneNo ($phoneno) {
    	$this->phoneno = $phoneno;
    	return $this;
    }
    public function getPhoneNo () {
    	return $this->phoneno;
    }
    
    public function setInstallDate ($date)
    {
    	$this->installdate = $date;
    	return $this;
    }
    public function getInstallDate ()
    {
    	return $this->installdate;
    }
    
    public function setCarbonVolumeCorrectionFactor ($correctionfactor) {
    	$this->carbonvolumecorrectionfactor = $correctionfactor;
    	return $this;
    }
    public function getCarbonVolumeCorrectionFactor () {
    	return $this->carbonvolumecorrectionfactor;
    }
    
    public function setCarbonCalorificValue ($calorificvalue) {
    	$this->carboncalorificvalue = $calorificvalue;
    	return $this;
    }
    public function getCarbonCalorificValue () {
    	return $this->carboncalorificvalue;
    }
    
    public function setCarbonKwhConversionFactor ($kwhconversionfactor) {
    	$this->carbonkwhconversionfactor = $kwhconversionfactor;
    	return $this;
    }
    public function getCarbonKwhConversionFactor () {
    	return $this->carbonkwhconversionfactor;
    }
    
    public function setSimCardNo ($simcardno) {
    	$this->simcardno = $simcardno;
    	return $this;
    }
    public function getSimCardNo () {
    	return $this->simcardno;
    }
    
    public function setReferenceVoltage ($referencevoltage) {
    	$this->referencevoltage = $referencevoltage;
    	return $this;
    }
    public function getReferenceVoltage () {
    	return $this->referencevoltage;
    }

    public function setHourRunWebDialChannel ($channel) {
        $this->hourrun_webdial_channel = $channel;
        return $this;
    }
    public function getHourRunWebDialChannel () {
        return $this->hourrun_webdial_channel;
    }

    public function setHourRunWebDialUnit ($unit) {
        $this->hourrun_webdial_unit = $unit;
        return $this;
    }
    public function getHourRunWebDialUnit () {
        return $this->hourrun_webdial_unit;
    }

    public function setHourRunPulseText1 ($text) {
        $this->hourrun_pulse1_text = $text;
        return $this;
    }
    public function getHourRunPulseText1 () {
        return $this->hourrun_pulse1_text;
    }

    public function setHourRunPulseText2 ($text) {
        $this->hourrun_pulse2_text = $text;
        return $this;
    }
    public function getHourRunPulseText2 () {
        return $this->hourrun_pulse2_text;
    }

    public function setHourRunPulseText3 ($text) {
        $this->hourrun_pulse3_text = $text;
        return $this;
    }
    public function getHourRunPulseText3 () {
        return $this->hourrun_pulse3_text;
    }

    public function setControlLink ($controlLink) {
        $this->control_link = $controlLink;
        return $this;
    }
    public function getControlLink () {
        return $this->control_link;
    }

    public function setMapper(I4energy_Model_MeterAttributesMapper $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_MeterAttributesMapper());
        }
        return $this->_mapper;
    }

    public function init ($meter)
    {
    	if (is_numeric($meter))
    	{
    		$select = $this->getMapper()->getSelect();
    		$select->from("meter_attr");
    		$select->where("`id`=?", $meter);
    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    		$meter_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	}
    	else
    	{
    		$meter_row = $meter;
    	}
    	
    	$this->setId($meter_row['id']);
    	$this->setWebMinScale($meter_row['webminscale']);
    	$this->setWebMaxScale($meter_row['webmaxscale']);
    	$this->setWebUpdateInterval($meter_row['webupdateinterval']);
    	$this->setIsGcm($meter_row['isgcm']);
    	$this->setNetworkName($meter_row['networkname']);
    	$this->setPhoneNo($meter_row['phoneno']);
    	$this->setInstallDate($meter_row['installdate']);
    	$this->setCarbonVolumeCorrectionFactor($meter_row['carbon_volumecorrection']);
    	$this->setCarbonCalorificValue($meter_row['carbon_calorificvalue']);
    	$this->setCarbonKwhConversionFactor($meter_row['carbon_kwhconversion']);
    	$this->setSimCardNo($meter_row['simcardno']);
    	$this->setReferenceVoltage($meter_row['ref_voltage']);
        $this->setHourRunWebDialChannel($meter_row['hourrun_webdial_ch']);
        $this->setHourRunWebDialUnit($meter_row['hourrun_webdial_unit']);
        $this->setHourRunPulseText1($meter_row['hourrun_pulse1_text']);
        $this->setHourRunPulseText2($meter_row['hourrun_pulse2_text']);
        $this->setHourRunPulseText3($meter_row['hourrun_pulse3_text']);
        $this->setControlLink($meter_row['control_link']);
    }
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);	
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
}
?>
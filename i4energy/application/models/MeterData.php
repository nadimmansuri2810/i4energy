<?php
class I4energy_Model_MeterData {
	protected $macid;
    protected $rec_date;
    protected $moduleid;
    protected $slotno;
    protected $channelno;
    protected $data;
    protected $data1;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Meter Data property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Meter Data property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setMac($macid) {
        $this->macid = $macid;
        return $this;
    }
    public function getMac() {
        return $this->macid;
    }
    
    public function setDate ($date) {
    	$this->rec_date = $date;
    	return $this;
    }
    public function getDate () {
    	return $this->rec_date;
    }
    
	public function setModuleNo ($moduleno) {
    	$this->moduleid = $moduleno;
    	return $this;
    }
    public function getModuleNo () {
    	return $this->moduleid;
    }
    
	public function setSlotNo($slotno) {
        $this->slotno = $slotno;
        return $this;
    }
    public function getSlotNo() {
        return $this->slotno;
    }
    
    public function setChannelNo ($channel) {
    	$this->channelno = $channel;
    	return $this;
    }
    public function getChannelNo () {
    	return $this->channelno;
    }
    
    public function setData ($data) {
    	$this->data = $data;
    	return $this;
    }
    public function getData () {
    	return $this->data;
    }
    
    public function setData1 ($data1) {
    	$this->data1 = $data1;
    	return $this;
    }
    public function getData1 () {
    	return $this->data1;
    }

    public function setMapper(I4energy_Model_MeterDataMapper $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_MeterDataMapper());
        }
        return $this->_mapper;
    }
    
    public function init ($macid, $moduleid = null, $rec_date = null, $slotno = null, $channelno = null)
    {
    	if (!is_array($meter))
    	{
    		$select = $this->getMapper()->getSelect();
    		$select->from("meter_data");
    		$select->where("`macid`=?", $macid);
    		$select->where("`moduleid`=?", $moduleid);
    		$select->where("`rec_date`=?", $rec_date);
    		$select->where("`slotno`=?", $slotno);
    		$select->where("`channelno`=?", $channelno);
    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    		$meter_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	}
    	else
    	{
    		$meter_row = $meter;
    	}
    	
    	$this->setMac($meter_row['macid']);
    	$this->setDate($meter_row['rec_date']);
    	$this->setModuleNo($meter_row['moduleid']);
    	$this->setSlotNo($meter_row['slotno']);
    	$this->setChannelNo($meter_row['$channelno']);
    	$this->setData($meter_row['data']);
    	$this->setData1($meter_row['data1']);
    }
    
    public function listMeterData ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listMeterData ($filters, $orderby, $sort, $start, $limit);
    }


  public function save ($ignore_unset = false)
  {
    return $this->getMapper()->save ($this, $ignore_unset);
  }


  /*public function save ($ignore_unset = false)
  {
    return $this->getMapper()->save ($this, $ignore_unset);
  }

  public function delete ()
  {
    $this->getMapper()->delete ($this);
  }*/
}
?>
<?php
class I4energy_Model_MeterDisplayLinksMapper {
protected $_dbtable;
	protected $__select;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_MeterDisplayLinks');
        }
        return $this->_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    		return $this->__select;
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		return $this->__select;
    	}
    }
    
	    
    public function listMeterDisplayLinks ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	$select = $this->getSelect();
    	$select->from ('meter_display_links');
    	 
    	if ( is_array ($filters) )
    	{
    		for ($i=0; $i < sizeof ($filters); $i++)
    		{
    		if (isset($filters[$i]['field']) && $filters[$i]['field'] != '' && isset($filters[$i]['operation']) && $filters[$i]['operation'] != '')
    		$select->addCondition ($filters[$i]['field'], $filters[$i]['operation'], $filters[$i]['value']);
    		}
    		}
    		if ($sort == '')
    			$sort = 'DESC';
    			 
    			if ($orderby == '')
    			$select->order ('id ' . $sort);
    			else
    			$select->order ($orderby . ' ' . $sort);
    			 
    			if ($start != null && $limit != null)
    			$select->limit ($limit, $start);
    			 
    			$select->sql_cals_found_rows (true);
    		$stmt = $this->getDbTable()->getAdapter()->query($select);
    		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
    		 
    		$stmt = new Zend_Db_Statement_Pdo($this->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
    		$stmt->execute();
    		$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    		 
    		$response = new stdClass();
    		$response->totalrecords = $count['totalrecords'];
    		$response->returnrecords = sizeof ($rows);
    		$response->rows = $rows;
    		 
    		return $response;
    }
    
    public function save (I4energy_Model_MeterDisplayLinks &$meterlink, $ignore_unset = false)
    {
    	
//     	echo "usermapper";exit;
    	$data = array (
    		'id' => $meterlink->getId()
    		,'title' =>$meterlink->getTitle()
    		,'url'=>$meterlink->getUrl()		
    		,'loginrequired' => $meterlink->getLoginrequired()
       		,'customer_id' => $meterlink->getCustomerId()
    		,'theme' => $meterlink->getTheme()
    		,'meter1' => $meterlink->getMeter1()
    		,'meter2' => $meterlink->getMeter2()
    		,'meter3' => $meterlink->getMeter3()
    		,'meter4' => $meterlink->getMeter4()
    	);
    	
    	if ($ignore_unset)
    	{
    		if (!isset ($data['id']))
    			unset ($data['id']);
    		if (!isset ($data['title']))
    			unset ($data['title']);
    		if (!isset ($data['url']))
    			unset ($data['url']);
    		if (!isset ($data['loginrequired']))
    			unset ($data['loginrequired']);
    		if (!isset ($data['customer_id']))
    			unset ($data['customer_id']);
    		if (!isset ($data['theme']))
    			unset ($data['theme']);
    		if (!isset ($data['meter1']))
    			unset ($data['meter1']);
    		if (!isset ($data['meter2']))
    			unset ($data['meter2']);
    		if (!isset ($data['meter3']))
    			unset ($data['meter3']);
    		if (!isset ($data['meter4']))
    			unset ($data['meter4']);
    		
    	}
    	
    	if ($meterlink->getId() == null || $meterlink->getId() == '')
    	{
    		$this->getDbTable()->insert($data);
    		$lastid = $this->getDbTable()->getAdapter()->lastInsertId();
    		return $lastid;
    	}
    	else
    	{
    		$this->getDbTable()->update($data, array('id = ?' => $meterlink->getId()));
    		return $meterlink->getId();
    	}
    }
    
    public function delete (I4energy_Model_MeterDisplayLinks &$meterlink)
    {
    	$where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $meterlink->getId());
    	$this->getDbTable()->delete ($where);
    }
}

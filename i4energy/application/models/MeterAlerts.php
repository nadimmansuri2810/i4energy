<?php
class I4energy_Model_MeterAlerts {
	protected $id;
    protected $phoneno;
    protected $email1;
    protected $email2;
    protected $email3;
    protected $statusgoodmin;
    protected $statusgoodmax;
    protected $statuswarningmin;
    protected $statuswarningmax;
    protected $statusalertmin;
    protected $statusalertmax;
    protected $alertlimitmin;
    protected $alertlimitmax;
    protected $alerttimelimitmin;
    protected $alerttimelimitmax;
    protected $alertlimitping;
    protected $lowpingerbattery;
    protected $lowpingerbatterychannelno;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Meter Alert property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid Meter Alert property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setPhoneNo ($phoneno) {
    	$this->phoneno = $phoneno;
    	return $this;
    }
    public function getPhoneNo () {
    	return $this->phoneno;
    }
    
	public function setEmail1 ($email) {
    	$this->email1 = $email;
    	return $this;
    }
    public function getEmail1 () {
    	return $this->email1;
    }
    
    public function setEmail2 ($email) {
    	$this->email2 = $email;
    	return $this;
    }
    public function getEmail2 () {
    	return $this->email2;
    }
    
    public function setEmail3 ($email) {
    	$this->email3 = $email;
    	return $this;
    }
    public function getEmail3 () {
    	return $this->email3;
    }
    
	public function setStatusGoodMin($statusgoodmin) {
        $this->statusgoodmin = $statusgoodmin;
        return $this;
    }
    public function getStatusGoodMin() {
        return $this->statusgoodmin;
    }
    
    public function setStatusGoodMax ($statusgoodmax) {
    	$this->statusgoodmax = $statusgoodmax;
    	return $this;
    }
    public function getStatusGoodMax () {
    	return $this->statusgoodmax;
    }
    
    public function setStatusWarningMin ($statuswarningmin) {
    	$this->statuswarningmin = $statuswarningmin;
    	return $this;
    }
    public function getStatusWarningMin () {
    	return $this->statuswarningmin;
    }
    
    public function setStatusWarningMax ($statuswarningmax) {
    	$this->statuswarningmax = $statuswarningmax;
    	return $this;
    }
    public function getStatusWarningMax () {
    	return $this->statuswarningmax;
    }
    
    public function setStatusAlertMin ($statusalertmin) {
    	$this->statusalertmin = $statusalertmin;
    	return $this;
    }
    public function getStatusAlertMin () {
    	return $this->statusalertmin;
    }
    
    public function setStatusAlertMax ($statusalertmax) {
    	$this->statusalertmax = $statusalertmax;
    	return $this;
    }
    public function getStatusAlertMax () {
    	return $this->statusalertmax;
    }
    
    public function setAlertLimitMin ($alertlimitmin) {
    	$this->alertlimitmin = $alertlimitmin;
    	return $this;
    }
    public function getAlertLimitMin () {
    	return $this->alertlimitmin;
    }
    
    public function setAlertLimitMax ($alertlimitmax) {
    	$this->alertlimitmax = $alertlimitmax;
    	return $this;
    }
    
    public function getAlertLimitMax () {
    	return $this->alertlimitmax;
    }
    
    public function setAlertTimeLimitMin ($alerttimelimitmin) {
    	$this->alerttimelimitmin = $alerttimelimitmin;
       	return $this;
    }
    public function getAlertTimeLimitMin () {
    	return $this->alerttimelimitmin;
    }
    
    public function setAlertTimeLimitMax ($alerttimelimitmax) {
    	$this->alerttimelimitmax = $alerttimelimitmax;
    	return $this;
    }
    public function getAlertTimeLimitMax () {
    	return $this->alerttimelimitmax;
    }
    
    public function setAlertLimitPing ($alertlimitping) {
    	$this->alertlimitping = $alertlimitping;
    	return $this;
    }
    public function getAlertLimitPing () {
    	return $this->alertlimitping;
    }
    
    public function setLowPingerBattery ($lowpingerbattery) {
    	$this->lowpingerbattery = $lowpingerbattery;
    	return $this;
    }
    public function getLowPingerBattery () {
    	return $this->lowpingerbattery;
    }

    public function setLowPingerBatteryChannelNo($lowpingerbatterychannelno){
   		$this->lowpingerbatterychannelno = $lowpingerbatterychannelno;
       	return $this;
    }
    public function getLowPingerBatteryChannelNo(){
    	return $this->lowpingerbatterychannelno;
    }
    
    public function setMapper(I4energy_Model_MeterAlertsMapper $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_MeterAlertsMapper());
        }
        return $this->_mapper;
    }
    
    public function init ($meter)
    {
    	if (is_numeric($meter))
    	{
    		$select = $this->getMapper()->getSelect();
    		$select->from("meter_alerts");
    		$select->where("`id`=?", $meter);
    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
    		$meter_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	}
    	else
    	{
    		$meter_row = $meter;
    	}

    	$this->setId($meter_row['id']);
    	$this->setPhoneNo($meter_row['phoneno']);
    	$this->setEmail1($meter_row['email1']);
    	$this->setEmail2($meter_row['email2']);
    	$this->setEmail3($meter_row['email3']);
    	$this->setStatusGoodMin($meter_row['statusgoodmin']);
    	$this->setStatusGoodMax($meter_row['statusgoodmax']);
    	$this->setStatusWarningMin($meter_row['statuswarningmin']);
    	$this->setStatusWarningMax($meter_row['statuswarningmax']);
    	$this->setStatusAlertMin($meter_row['statusalertmin']);
    	$this->setStatusAlertMax($meter_row['statusalertmax']);
    	$this->setAlertLimitMin($meter_row['alertlimitmin']);
    	$this->setAlertLimitMax($meter_row['alertlimitmax']);
    	$this->setAlertTimeLimitMin($meter_row['alerttimelimitmin']);
    	$this->setAlertTimeLimitMax($meter_row['alerttimelimitmax']);
    	$this->setAlertLimitPing($meter_row['alertlimitping']);
    	$this->setLowPingerBattery($meter_row['lowpingerbattery']);
    	$this->setLowPingerBatteryChannelNo($meter_row['lowpingerbatterychannelno']);
    }
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);	
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
}
?>
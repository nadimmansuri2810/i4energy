<?php
class I4energy_Model_MeterDisplayLinks {
	protected $id;
    protected $title;
    protected $url;
    protected $loginrequired;
    protected $customer_id;
    protected $theme;
    protected $meter1;
    protected $meter2;
    protected $meter3;
    protected $meter4;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid MeterdisplayLink  property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid MeterdisplayLink property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    public function getId() {
        return $this->id;
    }
    
    public function setTitle ($title) {
    	$this->title = $title;
    	return $this;
    }
    public function getTitle () {
    	return $this->title;
    }
    
	public function setUrl ($url) {
    	$this->url = $url;
    	return $this;
    }
    public function getUrl () {
    	return $this->url;
    }
    
	public function setLoginrequired($loginrequired) {
        $this->loginrequired = $loginrequired;
        return $this;
    }
    public function getLoginrequired() {
        return $this->loginrequired;
    }
    
    public function setCustomerId ($customerid) {
    	$this->customer_id = $customerid;
    	return $this;
    }
    public function getCustomerId () {
    	return $this->customer_id;
    }
    
    public function setTheme ($theme) {
    	$this->theme = $theme;
    	return $this;
    }
    public function getTheme () {
    	return $this->theme;
    }
    
    public function setMeter1 ($meter1) {
    	$this->meter1 = $meter1;
    	return $this;
    }
    public function getMeter1 () {
    	return $this->meter1;
    }
    
    public function setMeter2 ($meter2) {
    	$this->meter2 = $meter2;
    	return $this;
    }
    public function getMeter2 () {
    	return $this->meter2;
    }
    
    public function setMeter3 ($meter3) {
    	$this->meter3 = $meter3;
    	return $this;
    }
    public function getMeter3 () {
    	return $this->meter3;
    }
    public function setMeter4 ($meter4) {
    	$this->meter4 = $meter4;
    	return $this;
    }
    public function getMeter4 () {
    	return $this->meter4;
    }

    public function setMapper(I4energy_Model_MeterDisplayLinksMapper  $mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new I4energy_Model_MeterDisplayLinksMapper());
        }
        return $this->_mapper;
    }
    
	public function init ($meterdisplaylink){
	    	if (is_numeric($meterdisplaylink))
	    	{
	    		$select = $this->getMapper()->getSelect();
	    		$select->from("meter_display_links");
	    		$select->where("`id`=?", $meterdisplaylink);
	    		$stmt = $this->getMapper()->getDbTable()->getAdapter()->query ($select);
	    		$link_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
	    		
	    	}
	    	else
	    	{
	    		$link_row = $meterdisplaylink;
	    	}
	    	$this->setId($link_row['id']);
	    	$this->setTitle($link_row['title']);
	    	$this->setUrl($link_row['url']);
	    	$this->setLoginrequired($link_row['loginrequired']);
	    	$this->setCustomerId($link_row['customer_id']);
	    	$this->setTheme($link_row['theme']);
	    	$this->setMeter1($link_row['meter1']);
	    	$this->setMeter2($link_row['meter2']);
	    	$this->setMeter3($link_row['meter3']);
	    	$this->setMeter4($link_row['meter4']);
	   }
    
    public function save ($ignore_unset = false)
    {
    	return $this->getMapper()->save ($this, $ignore_unset);
    }
    
    public function delete ()
    {
    	$this->getMapper()->delete ($this);
    }
    
    public function listMeterDisplayLinks ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	return $this->getMapper()->listMeterDisplayLinks ($filters, $orderby, $sort, $start, $limit);
    }
  
}
?>
<?php
class I4energy_Model_MeterAlertsMapper {
	protected $_dbtable;
	protected $__select;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_MeterAlerts');
        }
        return $this->_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    		return $this->__select;
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		return $this->__select;
    	}
    }
    
    public function save (I4energy_Model_MeterAlerts &$attr, $ignore_unset = false)
    {
    	$data = array (
    		'id' => $attr->getId(),
			'phoneno' => $attr->getPhoneNo(),
			'email1' => $attr->getEmail1(),
			'email2' => $attr->getEmail2(),
			'email3' => $attr->getEmail3(),
			'statusgoodmin' => $attr->getStatusGoodMin(),
			'statusgoodmax' => $attr->getStatusGoodMax(),
			'statuswarningmin' => $attr->getStatusWarningMin(),
			'statuswarningmax' => $attr->getStatusWarningMax(),
			'statusalertmin' => $attr->getStatusAlertMin(),
			'statusalertmax' => $attr->getStatusAlertMax(),
			'alertlimitmin' => $attr->getAlertLimitMin(),
			'alertlimitmax' => $attr->getAlertLimitMax(),
			'alerttimelimitmin' => $attr->getAlertTimeLimitMin(),
			'alerttimelimitmax' => $attr->getAlertTimeLimitMax(),
			'alertlimitping' => $attr->getAlertLimitPing(),
			'lowpingerbattery' => $attr->getLowPingerBattery(),
    		'lowpingerbatterychannelno'=>$attr->getLowPingerBatteryChannelNo(),	
    	);
    	
    	if ($ignore_unset)
    	{
    		foreach ($data as $key => $value)
    		{
    			if (!isset ($data[$key]))
    				unset ($data[$key]);
    		}
    	}
    	if(isset($data['alertlimitmin']) && $data['alertlimitmin'] =='')
    	{
    		$data['alertlimitmin']=new Zend_Db_Expr('NULL');
    	}
   		
   		if(isset($data['alertlimitmax']) && $data['alertlimitmax'] =='')
   		{
    		$data['alertlimitmax']=new Zend_Db_Expr('NULL');
    	}
    	
    	if(isset($data['alerttimelimitmin']) && $data['alerttimelimitmin'] =='')
    	{
    		$data['alerttimelimitmin']=new Zend_Db_Expr('NULL');
    	}
    	
    	if(isset($data['alerttimelimitmax']) && $data['alerttimelimitmax'] =='')
    	{
    		$data['alerttimelimitmax']=new Zend_Db_Expr('NULL');
    	}
    	
    	if(isset($data['alertlimitping']) && $data['alertlimitping'] =='')
    	{
    		$data['alertlimitping']=new Zend_Db_Expr('NULL');
    	}
    	
    	if(isset($data['lowpingerbatterychannelno']) && $data['lowpingerbatterychannelno'] =='')
    	{
    		$data['lowpingerbatterychannelno']=new Zend_Db_Expr('NULL');
    	}    	
    	
    	$select = $this->getSelect();
    	$select->from("meter_alerts");
    	$select->where("`id`=?", $attr->getId());
    	$stmt = $this->getDbTable()->getAdapter()->query ($select);
    	$meter_row = $stmt->fetch(Zend_Db::FETCH_ASSOC);
    	
    	if (isset($meter_row) && $meter_row['id'] > 0)
    	{
    		$this->getDbTable()->update($data, array('id = ?' => $attr->getId()));
    		return $attr->getId();
    	}
    	else
    	{
    		$this->getDbTable()->insert($data);
    		return $attr->getId();
    	}
    }
    
    public function delete (I4energy_Model_MeterAlerts &$attr)
    {
    	$where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $attr->getId());
    	$this->getDbTable()->delete ($where);
    }
}

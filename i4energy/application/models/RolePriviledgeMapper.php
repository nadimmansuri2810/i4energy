<?php
class I4energy_Model_RolePriviledgeMapper {
	protected $sites_dbtable;
	protected $__select;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->sites_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->sites_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_RolePriviledge');
        }
        return $this->sites_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    		return $this->__select;
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		return $this->__select;
    	}
    }
    
    public function listRolePriviledge ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	$select = $this->getSelect();
    	$select->from ('role_priviledge');
    	
//     	print_r($filters);exit;
    	
    	if ( is_array ($filters) )
    	{
    		for ($i=0; $i < sizeof ($filters); $i++)
    		{
	    		if (isset($filters[$i]['field']) && $filters[$i]['field'] != '' && isset($filters[$i]['operation']) && $filters[$i]['operation'] != '')
	    			$select->addCondition ($filters[$i]['field'], $filters[$i]['operation'], $filters[$i]['value']);
    		}
   		}
   		if ($sort == '')
   			$sort = 'DESC';
   		
   		if ($orderby == '')
   			$select->order ('id ' . $sort);
   		else
   			$select->order ($orderby . ' ' . $sort);
   		
   		if ($start != null && $limit != null)
   			$select->limit ($limit, $start);
   		
   		$select->sql_cals_found_rows (true);
   		
   		$stmt = $this->getDbTable()->getAdapter()->query($select);
   		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
   		
   		$stmt = new Zend_Db_Statement_Pdo($this->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
   		$stmt->execute();
   		$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
   		
   		$response = new stdClass();
   		$response->totalrecords = $count['totalrecords'];
   		$response->returnrecords = sizeof ($rows);
   		$response->rows = $rows;
   		
   		return $response;
    }
    
    public function save (I4energy_Model_RolePriviledge &$rolePrivilage, $ignore_unset = false)
    {
    	$data = array (
    		'id' => $rolePrivilage->getId()
    		,'priviledge_id' => $rolePrivilage->getPriviledge_id()
			,'role_id' => $rolePrivilage->getRole_id()
    		);
    	
    	if ($ignore_unset)
    	{
    		if (!isset ($data['id']))
    			unset ($data['id']);

    		if (!isset ($data['priviledge_id']))
    			unset ($data['priviledge_id']);
    		
    		if (!isset ($data['role_id']))
    			unset ($data['role_id']);
    	}
    	
    	
    	
    	if ($rolePrivilage->getId() == null || $rolePrivilage->getId() == '')
    	{
    		
    		$this->getDbTable()->insert($data);
    		$lastid = $this->getDbTable()->getAdapter()->lastInsertId();
    		return $lastid;
    	}
    	else
    	{
    		$this->getDbTable()->update($data, array('id = ?' => $rolePrivilage->getId()));
    		return $rolePrivilage->getId();
    	}
    }
    
    public function delete (I4energy_Model_RolePriviledge &$rolePriviledge)
    {
    	$where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $rolePriviledge->getId());
    	$this->getDbTable()->delete ($where);
    }
}

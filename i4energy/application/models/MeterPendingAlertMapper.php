<?php
class I4energy_Model_MeterPendingAlertMapper {
	protected $_dbtable;
	protected $__select;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbtable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbtable) {
            $this->setDbTable('I4energy_Model_DbTable_MeterPendingAlert');
        }
        return $this->_dbtable;
    }
    
    public function getSelect () {
    	if (isset ($this->__select))
    	{
    		$this->__select->reset();
    		return $this->__select;
    	}
    	else
    	{
    		$this->__select = $this->getDbTable()->select();
    		$this->__select->reset();
    		return $this->__select;
    	}
    }
    
    public function listMeterPendingAlert ($filters = array(), $orderby = '', $sort = '', $start = null, $limit = null)
    {
    	$select = $this->getSelect();
    	$select->from ('meter_pending_alert');
    	
    	if ( is_array ($filters) )
    	{
    		for ($i=0; $i < sizeof ($filters); $i++)
    		{
	    		if (isset($filters[$i]['field']) && $filters[$i]['field'] != '' && isset($filters[$i]['operation']) && $filters[$i]['operation'] != '')
	    			$select->addCondition ($filters[$i]['field'], $filters[$i]['operation'], $filters[$i]['value']);
    		}
   		}
   		
   		if ($sort == '')
   			$sort = 'DESC';
   		
   		if ($orderby == '')
   			$select->order ('alert_time ' . $sort);
   		else
   			$select->order ($orderby . ' ' . $sort);
   		
   		if ($start != null && $limit != null)
   			$select->limit ($limit, $start);
   		
   		$select->sql_cals_found_rows (true);
   		
   		$stmt = $this->getDbTable()->getAdapter()->query($select);
   		$rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
   		
   		$stmt = new Zend_Db_Statement_Pdo($this->getDbTable()->getDefaultAdapter(), 'select FOUND_ROWS() as totalrecords');
   		$stmt->execute();
   		$count = $stmt->fetch(Zend_Db::FETCH_ASSOC);
   		
   		$response = new stdClass();
   		$response->totalrecords = $count['totalrecords'];
   		$response->returnrecords = sizeof ($rows);
   		$response->rows = $rows;
   		
   		return $response;
    }
    
    public function save (I4energy_Model_MeterPendingAlert &$meterpendingalert, $ignore_unset = false)
    {
		$data = array (
    		'id' => $meterpendingalert->getId()
    		,'alert_time' => $meterpendingalert->getAlert_datetime()
    		,'alert_status' => $meterpendingalert->getAlert_status()
    		,'alert_notify_datetime' => $meterpendingalert->getAlert_notify_datetime()
    		,'meter_id' => $meterpendingalert->getMeter_id()
    		,'last_reading_datetime' => $meterpendingalert->getLast_reading_datetime()
    		,'last_reading_data' => $meterpendingalert->getLast_reading_data()
    		,'alert_type' => $meterpendingalert->getAlert_type()
    	);
    	if ($ignore_unset)
    	{
    		foreach ($data as $key => $value)
    		{
    			if (!isset ($data[$key]))
    				unset ($data[$key]);
    		}
    	}
    	
    	if (!isset ($data['alert_time']))
    		$data['alert_time'] = gmdate('Y-m-d H:i:s');
    	
    	if ($meterpendingalert->getId() == null || $meterpendingalert->getId() == '')
    	{
    		if (!isset ($data['alert_time']))
    			$data['alert_time'] = gmdate('Y-m-d H:i:s');
    		
    		$this->getDbTable()->insert($data);
    		$lastid = $this->getDbTable()->getAdapter()->lastInsertId();
    		return $lastid;
    	}
    	else
    	{
    		$this->getDbTable()->update($data, array('id = ?' => $meterpendingalert->getId()));
    		return $meterpendingalert->getId();
    	}
    }
    
    public function delete (I4energy_Model_MeterPendingAlert &$meterpendingalert)
    {
    	$where = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $meterpendingalert->getId());
    	$this->getDbTable()->delete ($where);
    }
}

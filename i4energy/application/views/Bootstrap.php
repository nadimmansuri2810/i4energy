<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initAppAutoload() 
	{
		$autoloader = new Zend_Application_Module_Autoloader 
						( array (
								'namespace' => 'I4energy_',
								'basePath'  => dirname(__FILE__),
								)
						);
		$authUser_NameSpace = new Zend_Session_Namespace('I4energySession');
		
		$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini', APPLICATION_ENV);
		$res = $config->resources->multidb->db1->toArray();
		
		$dbConfig = array (
							'host'     => $res["host"],
							'username' => $res["username"],
							'password' => $res["password"],
							'dbname'   => $res["dbname"],
							);
		if (isset ($res["unix_socket"]) && $res["unix_socket"] != '')
			$dbConfig['unix_socket'] = $res['unix_socket'];
		
		$db = Zend_Db::factory($res["adapter"], $dbConfig);
		Zend_Db_Table_Abstract::setDefaultAdapter($db);  
		Zend_Registry::set('db', $db);
		
		return $autoloader;
	}
}
